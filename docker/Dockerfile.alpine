# This Dockerfile requires the build context to be the repository root
# and not it's own folder

FROM node:20-alpine as base

FROM base as deps-env

WORKDIR /app

COPY package.json pnpm-lock.yaml /app/
RUN npm i -g pnpm
RUN pnpm i --frozen-lockfile

FROM base as build-env

WORKDIR /app

COPY package.json pnpm-lock.yaml /app/
COPY remix.config.js /app/remix.config.js
COPY --from=deps-env /app/node_modules /app/node_modules

COPY . .

RUN npm i -g pnpm
RUN pnpm build
RUN pnpm prune --prod

FROM base AS run-env

WORKDIR /app

RUN apk add --no-cache ffmpeg imagemagick

# TODO: this may not be completely right
RUN sed -i 's/name="width" value="16KP"/name="width" value="32KP"/' /etc/ImageMagick-7/policy.xml && sed -i 's/name="height" value="16KP"/name="height" value="32KP"/' /etc/ImageMagick-7/policy.xml

COPY package.json /app/package.json
COPY config.example.json /app/config.example.json
COPY graphql /app/graphql
COPY locale /app/locale
COPY remix.config.js /app/remix.config.js
COPY --from=build-env /app/public /app/public
COPY --from=build-env /app/dist /app/dist
COPY --from=build-env /app/build /app/build
COPY --from=build-env /app/node_modules /app/node_modules

ENV PV_CONFIG_FOLDER=/config
ENV NODE_ENV=production
ENV DATABASE_NAME=production
ENV PORT=3000

CMD ["npm", "start"]

