# Porn Vault

Manage your ever-growing porn collection.

[Official website/documentation](https://porn-vault.gitlab.io/porn-vault/)

![Discord](https://img.shields.io/discord/652499331265331245)
[![Subreddit subscribers](https://img.shields.io/reddit/subreddit-subscribers/porn_vault?style=social)](https://reddit.com/r/porn_vault)
[![Latest Release](https://gitlab.com/porn-vault/porn-vault/-/badges/release.svg)](https://gitlab.com/porn-vault/porn-vault/-/releases)

There's a Discord channel! Join in to the discussion: [Discord](https://discord.gg/QfeHYtKGEa)

## Contribute

- Fork & create a new branch, give it the name of your feature you're implementing (e.g. "my-new-feature") & submit a pull request

Setup

```bash
npm i -g pnpm
pnpm install
```

Run dev server

```bash
pnpm dev
```

Run lint & tests

```bash
pnpm lint
```

```bash
pnpm test
```

## Features

- Cross-platform (Win, Linux, Mac)
- Works on any somewhat-modern device including tablets and smartphones
- Self hosted, fully open source
- Serves image and video to your device
  - Built-in video player
- Browse, manage & watch scenes
- Browse & manage actors, including actor aliases
  - Automatic actor extraction from scene titles & file names
- Browse & manage movies
- Browse & manage studios (+ parent studios!)
  - Automatic studio extraction from scene titles & file names
- Browse, manage & view images
- Set and jump to time markers
- Label your collection, including sub-labels
  - Automatic label extraction from scene titles & file names
  - Search, filter and sort your collection
  - Rate items, mark as your favorites & bookmark items
- Custom data fields
  - Extend actor info with any kind of data (hair color, retired, etc)
- Automatic thumbnail generation on scene import
- Optional password protection in LAN

## Demo images

### Scene collection

![Scenes](https://gitlab.com/porn-vault/porn-vault/-/raw/dev/docs/img/scene_collection.jpg)

### Scene page

![Scene page](https://gitlab.com/porn-vault/porn-vault/-/raw/dev/docs/img/scene_details.jpg)

### Actor collection

![Actors](https://gitlab.com/porn-vault/porn-vault/-/raw/dev/docs/img/actor_collection.jpg)

### Actor page

![Actor page](https://gitlab.com/porn-vault/porn-vault/-/raw/dev/docs/img/actor_details.jpg)

### Movie collection

![Movies](https://gitlab.com/porn-vault/porn-vault/-/raw/dev/docs/img/movie_collection.jpg)

### Movie page

![Movie page](https://gitlab.com/porn-vault/porn-vault/-/raw/dev/docs/img/movie_details.jpg)

### Image collection

You can do everything you can do with scenes (e.g. rate/favorite/bookmark/label) with images as well - useful if you run an image collection only.
![Images](https://gitlab.com/porn-vault/porn-vault/-/raw/dev/docs/img/image_collection.jpg)

### Image details

![Image details](https://gitlab.com/porn-vault/porn-vault/-/raw/dev/docs/img/image_details.jpg)

### Studio collection

![Studios](https://gitlab.com/porn-vault/porn-vault/-/raw/dev/docs/img/studio_collection.jpg)

### Parent studio

![Parent studio](https://gitlab.com/porn-vault/porn-vault/-/raw/dev/docs/img/parent_studio.jpg)
