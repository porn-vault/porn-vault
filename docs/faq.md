# FAQ

## Why are actresses with single names not matched?

By default `matching.matcher.options.ignoreSingleNames` is set to `true`, so actors without last name are not matched, to prevent aggressive overmatching.

You can either toggle this option to `false` and accept the risk or create a regex alias, which gives you more control over which actors should be matched with single names:

For example for `Sybil Kailena` you could add the alias `regex:sybil`. The `regex:` prefix denotes that this alias should be handled as a regular expression. This also gives control over studio-specific aliases, like `regex:(sabrina.*?dorcel)|(dorcel.*?sabrina)`.
