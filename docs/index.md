# Hi!

::: warning
This page is tailored for PV version 0.30.0+.
:::

Porn-Vault is a self hosted organizer for porn videos and imagery. It runs on your hardware, so no data is ever transferred to external servers.

Your content can be streamed to any modern device with a web browser, like your smartphone, tablet or another computer.

Porn-Vault is licensed under the GNU General Public License v3.0, so it will always stay free and open-source.
