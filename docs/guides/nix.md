## Setting up Porn-Vault with NixOS and Nix

Using Porn-Vault with NixOS is easy. For a working, autostarting, Porn-Vault instance with the [default configuration](https://github.com/NixOS/nixpkgs/blob/nixos-unstable/nixos/modules/services/web-apps/porn-vault/default-config.nix), all you need to do is to add the following to your NixOS configuration:

```nix
services.porn-vault = {
  enable = true;
  openFirewall = true;
};
```

With the default NixOS Porn-Vault configuration, `/media/porn-vault/images` will be scanned for images and `/media/porn-vault/videos` for videos.

### Other options

You can also further configure your Porn-Vault options, such as running it on a different port or choosing to not autostart it at boot, as shown by the example below:

```nix
services.porn-vault = {
  enable = true; # Install Porn-Vault
  openFirewall = true; # Open Porn-Vault port on firewall
  autoStart = false; # Do not start automatically at boot
  port = 1234; # Port to run Porn-Vault on (default: 3000)
};
```

If you set `autoStart` to `false`, you can start Porn-Vault with

```sh
systemctl start porn-vault
```

### Further Porn-Vault configuration with NixOS

If the [default configuration on nixos](https://github.com/NixOS/nixpkgs/blob/nixos-unstable/nixos/modules/services/web-apps/porn-vault/default-config.nix) doesn't fit your needs, you can configure Porn-Vault in Nix too. The config will be recursively applied to the defaults, so you only need to specify the parts you want to change. For instance, if you want to choose a different video scan location without changing anything else you could do:

```nix
services.porn-vault = {
  enable = true;
  autoStart = true;
  openFirewall = true;
  settings = {
    import = {
      videos = [
        {
          path = "/media/porn-vault/movies";
          include = [ ];
          exclude = [ ];
          extensions = [
            ".mp4"
            ".mov"
            ".webm"
          ];
          enable = true;
        }
      ];
    };
  };
};
```

Check the [default configuration on nixos](https://github.com/NixOS/nixpkgs/blob/nixos-unstable/nixos/modules/services/web-apps/porn-vault/default-config.nix) as reference if needed.

If you prefer keeping your PV configuration non-nixed, you can import a .json from your NixOS config directory:

```nix
services.porn-vault = {
  enable = true;
  openFirewall = true;
  settings = lib.importJSON ./config.json;
};
```

You can se an example config.json [here](https://gitlab.com/porn-vault/porn-vault/-/blob/dev/config.example.json?ref_type=heads), but note that it will need some tweaking to work with the NixOS module so using the "nixed" configuration is recommended.

### Non-NixOS Nix

Since Porn-Vault is available on nixpkgs, you can use it even if you don't want to use NixOS. On this case, you'll need to set the `PV_CONFIG_FOLDER` and `CACHE_DIRECTORY` variables (which are otherwise set by the NixOS module and Systemd). For instance, you can install [nix](https://nixos.org/) on any distro and then run:

```sh
nix-shell -p porn-vault
CACHE_DIRECTORY=$(mktemp -d) PV_CONFIG_FOLDER="/home/user/pv-config" porn-vault
```

You'll need to provide your own `config.json` at the `PV_CONFIG_FOLDER` folder, as you cannot use NixOS modules outside of NixOS.

