# Scanning media

## Videos

To import scenes into your vault, add folders into the `import.videos` list in the config file:

```json
{
  "videos": [
    {
      "path": "/videos",
      "include": [],
      "exclude": [],
      "extensions": [".mp4"],
      "enable": true
    }
  ]
}
```

### path

`path` is the folder path to scan. Be sure to mount it correctly if you are using Docker.

### include

`include` takes a list of regular expressions to only import specific files. At least one expression has to match.

### exclude

`exclude` takes a list of regular expressions to omit specific files. If any expression matches, the file/folder is ignored.

### extensions

`extensions` takes a list of extensions to import. If this is empty, nothing will be imported.

### enable

`enable` is a boolean (true/false) to temporarily enable/disable scanning this folder. Disabling large folders will speed up the scanning process.

## Images

Similarly, to import images you can use `import.images`:

```json
{
  "videos": [
    {
      "path": "/images",
      "include": [],
      "exclude": [],
      "extensions": [".jpg", ".jpeg", ".png"],
      "enable": true
    }
  ]
}
```
