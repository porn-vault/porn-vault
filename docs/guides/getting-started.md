# Getting started

::: info
These pages are tailored for PV version 0.30.0+.
:::

The easiest way of running Porn-Vault is using [Nix](/guides/nix) or [Docker](/guides/docker), check those pages for instructions.

## Not using Docker or Nix?

Try [building from source](/guides/build-from-source).

> Standalone releases are planned, but not available right now

