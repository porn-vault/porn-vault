# Building from source

## Setup

- Install [Git](https://git-scm.com/)
- Install [Node.js 18 or 20](https://nodejs.org/en/)
- Open terminal and go to folder of your choice
- Clone the repository
  - `git clone https://gitlab.com/porn-vault/porn-vault.git`
- Move into folder: `cd porn-vault`
- Install dependencies
  - `npm install -g pnpm`
  - `pnpm install`

## Build and run

- Run `pnpm build` to build
- Run `pnpm start` to run

If you want, run `pnpm prune` to free some disk space.

## Development

- Run `pnpm dev` to run development server
- Run `pnpm start:dev` to run production build with development data
- Run `pnpm lint` or `pnpm lint:fix` to run linting
