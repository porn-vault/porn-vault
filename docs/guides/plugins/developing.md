# Developing plugins

## Plugins repository

Plugins are Javascript files that expose a Javascript function.
They receive a plugin context to do some task.
Per event (e.g. on creation of some item), multiple plugins can be executed serially.
Check the plugin repository (https://gitlab.com/porn-vault/plugins) for pre-made plugins and inspiration.

## Setup

1. Install NodeJS (18+)
2. Clone the plugin repository (https://gitlab.com/porn-vault/plugins)
3. Install pnpm: `npm install -g pnpm`
4. Install dependencies using pnpm: `pnpm install`
5. Run `pnpm run setup`
6. Follow the terminal instructions

In the plugins/ folder, you can find your plugin as a folder.

Run `pnpm build` to build all plugins. The resulting built file can be found in dist/.

## Example plugin

As an example, here's a plugin that will attach the label 'Female' to every item (scene or actor) that is created.

```ts
import { ActorContext, ActorOutput } from "../../types/actor";
import { applyMetadata, Plugin } from "../../types/plugin";
import { SceneContext, SceneOutput } from "../../types/scene";

import info from "./info.json";

type Context = ActorContext | SceneContext;
type Output = ActorOutput | SceneOutput;

const handler: Plugin<Context, Output> = async (_ctx) => {
  // Attach label 'Female' to every actor (actress) we create
  return {
    labels: ["female"],
  };
};

handler.requiredVersion = "0.30.0-rc.0 - 1";requirement here

applyMetadata(handler, info);

module.exports = handler;

export default handler;
```
