import { defineConfig } from "vitepress";

// https://vitepress.dev/reference/site-config
export default defineConfig({
  lang: "en-US",

  title: "Porn Vault",
  description: "Porn-Vault is a self hosted organizer for porn videos and imagery.",

  base: "/porn-vault/",

  themeConfig: {
    search: {
      provider: "local",
    },

    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: "Home", link: "/" },
      { text: "Getting started", link: "/guides/getting-started" },
      { text: "FAQ", link: "/faq" },
    ],

    sidebar: [
      {
        text: "Guides",
        items: [
          { 
            text: "Getting started",
            link: "/guides/getting-started",
            items: [
              { text: "Docker", link: "/guides/docker" },
              { text: "Nix and NixOS", link: "/guides/nix" },
            ],
          },
          { text: "Scanning media", link: "/guides/scanning-media" },
          { text: "Applying labels", link: "/guides/applying-labels" },
          { text: "Matching", link: "/guides/matching" },
          { text: "Logging", link: "/guides/logging" },
          { text: "Building from source", link: "/guides/build-from-source" },
        ],
      },
      {
        text: "Plugins",
        items: [
          { text: "Using plugins", link: "/guides/plugins/using" },
          { text: "Creating plugins", link: "/guides/plugins/developing" },
        ],
      },
    ],

    socialLinks: [{ icon: "github", link: "https://gitlab.com/porn-vault/porn-vault" }],
  },
});
