type Dimensions {
  width: Long
  height: Long
}

type SceneMeta {
  size: Long
  duration: Float
  dimensions: Dimensions!
  fps: Float
  bitrate: Int
}

type AvailableStream {
  label: String!
  mimeType: String
  streamType: String!
  transcode: Boolean!
}

type SceneSearchResults {
  numItems: Int!
  numPages: Int!
  items: [Scene!]!
}

input SceneSearchQuery {
  query: String
  favorite: Boolean
  bookmark: Boolean
  rating: Int
  include: [String!]
  exclude: [String!]
  studios: [String!]
  actors: [String!]
  sortBy: String
  sortDir: String
  skip: Int
  take: Int
  page: Int
  durationMin: Int
  durationMax: Int
  unwatchedOnly: Boolean

  rawQuery: Json
}

extend type Query {
  getSceneDiskUsage: Long!
  numScenes: Int!
  numSceneViews: Int!
  getScenes(query: SceneSearchQuery!): SceneSearchResults!
  getSceneById(id: String!): Scene
}

type Scene {
  _id: String!
  name: String!
  description: String
  addedOn: Long!
  releaseDate: Long
  favorite: Boolean!
  bookmark: Long
  rating: Int
  path: String
  streamLinks: [String!]!
  watches: [Long!]!
  customFields: Object!
  processed: Boolean

  # Resolvers
  meta: SceneMeta!
  fileExists: Boolean!
  availableFields: [CustomField!]!
  thumbnail: Image
  preview: Image
  images: [Image!]!
  actors: [Actor!]!
  labels: [Label!]!
  studio: Studio
  markers: [Marker!]!
  movies: [Movie!]!
  availableStreams: [AvailableStream!]!
  similar: [Scene!]!
  resolvedCustomFields: [CustomFieldEntry!]!
}

type RunFFProbeResult {
  ffprobe: Json!
  scene: Scene!
}

input SceneUpdateOpts {
  favorite: Boolean
  bookmark: Long
  actors: [String!]
  name: String
  description: String
  rating: Int
  labels: [String!]
  streamLinks: [String!]
  thumbnail: String
  releaseDate: Long
  studio: String
  customFields: Object
  path: String
}

extend type Mutation {
  addScene(name: String!, actors: [String!], labels: [String!]): Scene!
  screenshotScene(id: String!, sec: Float!): Image
  watchScene(id: String!): Scene!
  unwatchScene(id: String!): Scene!
  updateScenes(ids: [String!]!, opts: SceneUpdateOpts!): [Scene!]!
  removeScenes(ids: [String!]!, deleteImages: Boolean): Boolean!
  runScenePlugins(id: String!): Scene
  runFFProbe(id: String!): RunFFProbeResult
}
