import { existsSync, mkdirSync, rmSync } from "node:fs";

import {
  ensureIzzyExists,
  exitIzzy,
  izzyVersion,
  resetIzzy,
  spawnIzzy,
} from "../server/binaries/izzy";

const IZZY_PORT = 8500;
const TEST_FOLDER = ".test";

function cleanupFiles() {
  if (existsSync(TEST_FOLDER)) {
    rmSync(TEST_FOLDER, { recursive: true });
  }
  mkdirSync(TEST_FOLDER, { recursive: true });

  // Do not delete binaries, so the next run will be faster
}

export default async function () {
  console.error("Test startup");
  cleanupFiles();

  console.log(`Test izzy port = ${IZZY_PORT}`);

  await ensureIzzyExists();

  if (await izzyVersion(IZZY_PORT).catch(() => false)) {
    console.log("Izzy already running, clearing...");
    await resetIzzy(IZZY_PORT);
  } else {
    console.log("Spawning Izzy");
    await spawnIzzy(IZZY_PORT);
  }

  return async () => {
    console.log("Closing test izzy");
    await exitIzzy(IZZY_PORT);
    cleanupFiles();
  };
}
