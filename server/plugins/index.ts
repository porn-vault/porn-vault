import * as nodepath from "node:path";
import { inspect } from "node:util";

import { IConfig } from "../config/schema";
import countries, { ICountry } from "../data/countries";
import { getMatcher, getMatcherByType } from "../matching/matcher";
import CustomField from "../types/custom_field";
import { walk } from "../utils/fs/walk";
import { createPluginLogger, formatMessage, handleError, logger } from "../utils/logger";
import { libraryPath } from "../utils/path";
import { Dictionary, isString } from "../utils/types";
import { version } from "../version";
import { getPlugin } from "./register";
import { createPluginStoreAccess } from "./store";

export function resolvePlugin(
  item: string | [string, Record<string, unknown>]
): [string, Record<string, unknown> | undefined] {
  if (isString(item)) {
    return [item, undefined];
  }
  return item;
}

export async function runPluginsSerial(
  config: IConfig,
  event: string,
  inject?: Dictionary<unknown>
): Promise<Record<string, unknown>> {
  const result = {} as Dictionary<unknown>;
  if (!config.plugins.events[event]) {
    logger.debug(`No plugins defined for event ${event}.`);
    return result;
  }

  logger.info(`Running plugin event: ${event}`);

  let numErrors = 0;

  for (const pluginItem of config.plugins.events[event]) {
    const [pluginName, pluginArgs] = resolvePlugin(pluginItem);

    try {
      const pluginResult = await runPlugin(config, pluginName, {
        data: structuredClone(result),
        event,
        ...inject,
        pluginArgs,
      });
      Object.assign(result, pluginResult);
    } catch (error) {
      handleError(`Plugin error`, error);
      numErrors++;
    }
  }

  const len = config.plugins.events[event].length;
  if (!numErrors) {
    logger.info(`Ran ${len} plugins (${len} successful)`);
  } else {
    logger.error(`Ran ${len} plugins (${len - numErrors} successful, ${numErrors} errors)`);
  }
  logger.debug("Plugin series result");
  logger.debug(inspect(result, true, null, true));

  return result;
}

export async function runPlugin(
  config: IConfig,
  pluginName: string,
  inject?: Dictionary<unknown>,
  args?: Dictionary<unknown>
): Promise<unknown> {
  const pluginDefinition = config.plugins.register[pluginName];
  if (!pluginDefinition) {
    throw new Error(`${pluginName}: plugin not found.`);
  }

  const func = getPlugin(pluginName);

  const pluginArgs = structuredClone(args || pluginDefinition.args || {});
  const pluginLogger = createPluginLogger(
    pluginName,
    config.log.writeFile,
    pluginDefinition.logLevel
  );

  const pluginVersion = func.info?.version ? `v${func.info?.version}` : "unknown version";
  logger.info(`Running plugin ${pluginName} ${pluginVersion}`);
  logger.debug(formatMessage(pluginDefinition));

  const result = await func({
    // MAIN CONTEXT
    get $countries(): ICountry[] {
      return structuredClone(countries);
    },
    $config: structuredClone(config),
    $getCustomFields: () => CustomField.getAll(),
    $cwd: process.cwd(),
    $formatMessage: formatMessage,
    $getMatcher: getMatcherByType,
    $library: libraryPath(""),
    $logger: pluginLogger,
    $matcher: getMatcher(),

    // Persistent in-memory data store
    $store: createPluginStoreAccess(pluginName),
    $throw: (...msgs: unknown[]) => {
      const msg = msgs.map(formatMessage).join(" ");
      pluginLogger.error(msg);
      throw new Error(msg);
    },
    $version: version,
    $walk: walk,

    // PLUGIN
    args: pluginArgs,
    $args: pluginArgs,
    $pluginName: pluginName,
    $pluginPath: nodepath.resolve(pluginDefinition.path),

    ...inject,
  });

  if (typeof result !== "object") {
    throw new Error(`${pluginName}: malformed output.`);
  }

  logger.debug("Plugin result:");
  logger.debug(inspect(result, true, null, true));
  return result || {};
}
