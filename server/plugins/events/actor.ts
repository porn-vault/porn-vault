import { getConfig } from "../../config";
import { ApplyActorLabelsEnum } from "../../config/schema";
import { collections } from "../../database";
import { buildFieldExtractor, buildLabelExtractor } from "../../extractor";
import { runPluginsSerial } from "../../plugins";
import { indexImages } from "../../search/image";
import Actor from "../../types/actor";
import { isValidCountryCode } from "../../types/countries";
import Image from "../../types/image";
import Label from "../../types/label";
import { logger } from "../../utils/logger";
import { validRating } from "../../utils/misc";
import { normalizeAliases, normalizeDescription, normalizeName } from "../../utils/string";
import { isBoolean, isNumber, isString } from "../../utils/types";
import { createImage, createLocalImage, importImageFromPath } from "../context";

function injectServerFunctions(actor: Actor, createdImages: Image[]) {
  let labels: Label[], rating: number;

  return {
    $getLabels: async () => (labels ??= await Actor.getLabels(actor)),
    $getAverageRating: async () => (rating ??= await Actor.getAverageRating(actor)),
    $importImageFromPath: async (path: string, name: string, thumbnail?: boolean) => {
      const img = await importImageFromPath(path, name, thumbnail);
      await Image.addActors(img, [actor._id]);
      await collections.images.upsert(img._id, img);

      if (!thumbnail) {
        createdImages.push(img);
      }
      return img._id;
    },
    $createLocalImage: async (path: string, name: string, thumbnail?: boolean) => {
      logger.warn(`$createLocalImage is deprecated, use $importImageFromPath(path, name, isThumbnail) instead`);

      const img = await createLocalImage(path, name, thumbnail);
      await Image.addActors(img, [actor._id]);
      await collections.images.upsert(img._id, img);

      if (!thumbnail) {
        createdImages.push(img);
      }
      return img._id;
    },
    $createImage: async (url: string, name: string, thumbnail?: boolean) => {
      const img = await createImage(url, name, thumbnail);
      await Image.setActors(img, [actor._id]);
      await collections.images.upsert(img._id, img);
      if (!thumbnail) {
        createdImages.push(img);
      }
      return img._id;
    },
  };
}

export async function onActorCreate(
  actor: Actor,
  actorLabels: string[],
  event: "actorCreated" | "actorCustom" = "actorCreated"
): Promise<{ diff: Partial<Actor>; commit: () => Promise<void> }> {
  const config = getConfig();
  const createdImages = [] as Image[];

  const pluginResult = await runPluginsSerial(config, event, {
    actor: structuredClone(actor),
    actorName: actor.name,
    ...injectServerFunctions(actor, createdImages),
  });

  const diff: Partial<Actor> = {};

  if (
    isString(pluginResult.thumbnail) &&
    pluginResult.thumbnail.startsWith("im_") &&
    (!actor.thumbnail || config.plugins.allowActorThumbnailOverwrite)
  ) {
    diff.thumbnail = pluginResult.thumbnail;
  }

  if (
    isString(pluginResult.altThumbnail) &&
    pluginResult.altThumbnail.startsWith("im_") &&
    (!actor.altThumbnail || config.plugins.allowActorThumbnailOverwrite)
  ) {
    diff.altThumbnail = pluginResult.altThumbnail;
  }

  if (
    isString(pluginResult.avatar) &&
    pluginResult.avatar.startsWith("im_") &&
    (!actor.avatar || config.plugins.allowActorThumbnailOverwrite)
  ) {
    diff.avatar = pluginResult.avatar;
  }

  if (
    isString(pluginResult.hero) &&
    pluginResult.hero.startsWith("im_") &&
    (!actor.hero || config.plugins.allowActorThumbnailOverwrite)
  ) {
    diff.hero = pluginResult.hero;
  }

  if (isString(pluginResult.name)) {
    diff.name = normalizeName(pluginResult.name);
  }

  if (isString(pluginResult.description)) {
    diff.description = normalizeDescription(pluginResult.description);
  }

  if (isNumber(pluginResult.bornOn) && !isNaN(pluginResult.bornOn)) {
    diff.bornOn = new Date(pluginResult.bornOn).valueOf();
  }

  if (isNumber(pluginResult.addedOn) && !isNaN(pluginResult.addedOn)) {
    diff.addedOn = new Date(pluginResult.addedOn).valueOf();
  }

  if (pluginResult.aliases && Array.isArray(pluginResult.aliases)) {
    if (!diff.aliases) {
      diff.aliases = [];
    }
    diff.aliases.push(...(pluginResult.aliases as string[]));
    diff.aliases = normalizeAliases(actor.aliases);
  }

  if (pluginResult.custom && typeof pluginResult.custom === "object") {
    const localExtractFields = await buildFieldExtractor();
    for (const key in pluginResult.custom) {
      const fields = localExtractFields(key);
      if (fields.length) {
        if (!diff.customFields) {
          diff.customFields = {};
        }

        diff.customFields[fields[0]] = (pluginResult.custom as Record<string, any>)[key];
      }
    }
  }

  if (validRating(pluginResult.rating)) {
    diff.rating = pluginResult.rating;
  }

  if (isBoolean(pluginResult.favorite)) {
    diff.favorite = pluginResult.favorite;
  }

  if (isNumber(pluginResult.bookmark) || pluginResult.bookmark === null) {
    diff.bookmark = pluginResult.bookmark;
  } else if (isBoolean(pluginResult.bookmark)) {
    diff.bookmark = pluginResult.bookmark ? Date.now() : null;
  }

  if (pluginResult.nationality !== undefined) {
    if (isString(pluginResult.nationality) && isValidCountryCode(pluginResult.nationality)) {
      diff.nationality = pluginResult.nationality.toUpperCase();
    } else if (pluginResult.nationality === null) {
      diff.nationality = pluginResult.nationality;
    }
  }

  if (pluginResult.externalLinks && Array.isArray(pluginResult.externalLinks)) {
    diff.externalLinks = pluginResult.externalLinks;
  }

  if (pluginResult.labels && Array.isArray(pluginResult.labels)) {
    const labelIds = [] as string[];
    const localExtractLabels = await buildLabelExtractor();
    for (const labelName of pluginResult.labels as string[]) {
      const extractedIds = localExtractLabels(labelName);
      if (extractedIds.length) {
        labelIds.push(...extractedIds);
        logger.verbose(`Found ${extractedIds.length} labels for ${labelName}:`);
        logger.debug(extractedIds);
      } else if (config.plugins.createMissingLabels) {
        const label = new Label(labelName);
        labelIds.push(label._id);
        await collections.labels.upsert(label._id, label);
        logger.debug(`Created label ${label.name}`);
      }
    }
    actorLabels.push(...labelIds);
  }

  return {
    diff,
    commit: async () => {
      logger.debug("Committing plugin result");

      for (const image of createdImages) {
        if (
          (event === "actorCreated" &&
            config.matching.applyActorLabels.includes(
              ApplyActorLabelsEnum.enum["plugin:actor:create"]
            )) ||
          (event === "actorCustom" &&
            config.matching.applyActorLabels.includes(
              ApplyActorLabelsEnum.enum["plugin:actor:custom"]
            ))
        ) {
          await Image.setLabels(image, actorLabels);
        }
      }

      await indexImages(createdImages);
    },
  };
}
