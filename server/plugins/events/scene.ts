import tinyAsyncPool from "tiny-async-pool";

import { getConfig } from "../../config";
import { ApplyActorLabelsEnum, ApplyStudioLabelsEnum } from "../../config/schema";
import { collections } from "../../database";
import {
  buildActorExtractor,
  buildFieldExtractor,
  buildLabelExtractor,
  extractLabels,
  extractMovies,
  extractStudios,
} from "../../extractor";
import { runPluginsSerial } from "../../plugins";
import { indexActors } from "../../search/actor";
import { indexImages } from "../../search/image";
import { indexMarkers } from "../../search/marker";
import { indexMovies } from "../../search/movie";
import { indexStudios } from "../../search/studio";
import Actor from "../../types/actor";
import Image from "../../types/image";
import Label from "../../types/label";
import Marker from "../../types/marker";
import Movie from "../../types/movie";
import Scene from "../../types/scene";
import Studio from "../../types/studio";
import SceneView from "../../types/watch";
import { mapAsync } from "../../utils/async";
import { formatMessage, handleError, logger } from "../../utils/logger";
import { validRating } from "../../utils/misc";
import { normalizeDescription, normalizeName } from "../../utils/string";
import { isBoolean, isNumber, isString } from "../../utils/types";
import { createImage, createLocalImage, importImageFromPath } from "../context";
import { onActorCreate } from "./actor";
import { onMovieCreate } from "./movie";
import { onStudioCreate } from "./studio";

export async function createMarker(
  sceneId: string,
  name: string,
  seconds: number
): Promise<Marker | null> {
  const config = getConfig();
  const existingMarker = await Marker.getAtTime(
    sceneId,
    seconds,
    config.plugins.markerDeduplicationThreshold
  );
  if (existingMarker) {
    // Prevent duplicate markers
    return null;
  }
  const marker = new Marker(name, sceneId, seconds);
  await collections.markers.upsert(marker._id, marker);
  return marker;
}

function injectServerFunctions(scene: Scene, createdImages: Image[], createdMarkers: Marker[]) {
  let actors: Actor[], labels: Label[], watches: SceneView[];
  let studio: Studio | null, movies: Movie[];
  return {
    $getActors: async () => (actors ??= await Scene.getActors(scene)),
    $getLabels: async () => (labels ??= await Scene.getLabels(scene)),
    $getWatches: async () => (watches ??= await SceneView.getByScene(scene._id)),
    $getStudio: async () => (studio ??= scene.studio ? await Studio.getById(scene.studio) : null),
    $getMovies: async () => (movies ??= await Movie.getByScene(scene._id)),
    $createMarker: async (name: string, seconds: number) => {
      const marker = await createMarker(scene._id, name, seconds);
      if (marker) {
        createdMarkers.push(marker);
        return marker._id;
      }
      return null;
    },
    $createLocalImage: async (path: string, name: string, thumbnail?: boolean) => {
      const img = await createLocalImage(path, name, thumbnail);
      img.scene = scene._id;
      await collections.images.upsert(img._id, img);
      if (!thumbnail) {
        createdImages.push(img);
      }
      return img._id;
    },
    $importImageFromPath: async (path: string, name: string, thumbnail?: boolean) => {
      const img = await importImageFromPath(path, name, thumbnail);
      img.scene = scene._id;
      await collections.images.upsert(img._id, img);

      if (!thumbnail) {
        createdImages.push(img);
      }
      return img._id;
    },
    $createImage: async (url: string, name: string, thumbnail?: boolean) => {
      const img = await createImage(url, name, thumbnail);
      img.scene = scene._id;
      await collections.images.upsert(img._id, img);
      if (!thumbnail) {
        createdImages.push(img);
      }
      return img._id;
    },
  };
}

// This function has side effects
export async function onSceneCreate(
  scene: Scene,
  sceneLabels: string[],
  sceneActors: string[],
  event: "sceneCustom" | "sceneCreated" = "sceneCreated"
): Promise<{ scene: Scene; commit: () => Promise<void> }> {
  const config = getConfig();

  const createdImages = [] as Image[];
  const createdMarkers = [] as Marker[];

  const pluginResult = await runPluginsSerial(config, event, {
    scene: structuredClone(scene),
    sceneName: scene.name,
    scenePath: scene.path,
    ...injectServerFunctions(scene, createdImages, createdMarkers),
  });

  if (
    isString(pluginResult.thumbnail) &&
    pluginResult.thumbnail.startsWith("im_") &&
    (!scene.thumbnail || config.plugins.allowSceneThumbnailOverwrite)
  ) {
    scene.thumbnail = pluginResult.thumbnail;
  }

  if (isString(pluginResult.name)) {
    scene.name = normalizeName(pluginResult.name);
  }

  if (isString(pluginResult.path)) {
    const result = await Scene.changePath(scene, pluginResult.path);
    Object.assign(scene, result);
  }

  if (isString(pluginResult.description)) {
    scene.description = normalizeDescription(pluginResult.description);
  }

  if (isNumber(pluginResult.releaseDate) && !isNaN(pluginResult.releaseDate)) {
    scene.releaseDate = new Date(pluginResult.releaseDate).valueOf();
  }

  if (isNumber(pluginResult.addedOn)) {
    scene.addedOn = new Date(pluginResult.addedOn).valueOf();
  }

  const viewArray: unknown = pluginResult.views || pluginResult.watches;
  if (Array.isArray(viewArray) && viewArray.every(isNumber)) {
    for (const viewTime of viewArray) {
      await Scene.watch(scene, viewTime);
    }
  }

  if (pluginResult.custom && typeof pluginResult.custom === "object") {
    const localExtractFields = await buildFieldExtractor();
    for (const key in pluginResult.custom) {
      const fields = localExtractFields(key);
      if (fields.length) {
        // @ts-ignore
        scene.customFields[fields[0]] = pluginResult.custom[key];
      }
    }
  }

  if (validRating(pluginResult.rating)) {
    scene.rating = pluginResult.rating;
  }

  if (isBoolean(pluginResult.favorite)) {
    scene.favorite = pluginResult.favorite;
  }

  if (isNumber(pluginResult.bookmark) || pluginResult.bookmark === null) {
    scene.bookmark = pluginResult.bookmark;
  } else if (isBoolean(pluginResult.bookmark)) {
    scene.bookmark = pluginResult.bookmark ? Date.now() : null;
  }

  if (pluginResult.actors && Array.isArray(pluginResult.actors)) {
    const actorIds = [] as string[];
    const shouldApplyActorLabels =
      (event === "sceneCreated" &&
        config.matching.applyActorLabels.includes(
          ApplyActorLabelsEnum.enum["plugin:scene:create"]
        )) ||
      (event === "sceneCustom" &&
        config.matching.applyActorLabels.includes(
          ApplyActorLabelsEnum.enum["plugin:scene:custom"]
        ));

    const localExtractActors = await buildActorExtractor();

    for (const actorName of pluginResult.actors) {
      if (typeof actorName !== "string" || actorName.length === 0) {
        logger.warn(`Plugin returned "${actorName}", which is an invalid actor name`);
        continue;
      }

      const extractedIds = localExtractActors(actorName);
      if (extractedIds.length) {
        actorIds.push(...extractedIds);
      } else if (config.plugins.createMissingActors) {
        const actor = new Actor(actorName);
        actorIds.push(actor._id);
        const actorLabels = [] as string[];
        const pluginResult = await onActorCreate(actor, actorLabels);

        Object.assign(actor, pluginResult.diff);

        await Actor.setLabels(actor, actorLabels);
        await collections.actors.upsert(actor._id, actor);

        if (config.matching.matchCreatedActors) {
          await Actor.findUnmatchedScenes(actor, shouldApplyActorLabels ? actorLabels : []);
        }
        await indexActors([actor]);
        await pluginResult.commit();
        logger.debug(`Created actor ${actor.name}`);
      }

      if (shouldApplyActorLabels) {
        const actors = await Actor.getBulk(actorIds);
        const actorLabelIds = (await mapAsync(actors, Actor.getLabels)).flat().map((l) => l._id);
        logger.verbose("Applying actor labels to scene");
        sceneLabels.push(...actorLabelIds);
      }
    }
    sceneActors.push(...actorIds);
  }

  if (pluginResult.labels && Array.isArray(pluginResult.labels)) {
    const labelIds = [] as string[];
    const localExtractLabels = await buildLabelExtractor();
    for (const labelName of pluginResult.labels as string[]) {
      if (typeof labelName !== "string" || labelName.length === 0) {
        logger.warn(`Plugin returned "${labelName}", which is an invalid label name`);
        continue;
      }

      const extractedIds = localExtractLabels(labelName);
      if (extractedIds.length) {
        labelIds.push(...extractedIds);
        logger.verbose(`Found ${extractedIds.length} labels for ${labelName}:`);
        logger.debug(extractedIds);
      } else if (config.plugins.createMissingLabels) {
        const label = new Label(labelName);
        labelIds.push(label._id);
        await collections.labels.upsert(label._id, label);
        logger.debug(`Created label ${label.name}`);
      }
    }
    sceneLabels.push(...labelIds);
  }

  if (!scene.studio && pluginResult.studio) {
    const studioName = pluginResult.studio;

    if (typeof studioName !== "string" || studioName.length === 0) {
      logger.warn(`Plugin returned "${studioName}", which is an invalid studio name`);
    } else {
      let studioLabels: string[] = [];
      const studioId = (await extractStudios(studioName))[0] || null;
      const shouldApplyStudioLabels =
        (event === "sceneCreated" &&
          config.matching.applyStudioLabels.includes(
            ApplyStudioLabelsEnum.enum["plugin:scene:create"]
          )) ||
        (event === "sceneCustom" &&
          config.matching.applyStudioLabels.includes(
            ApplyStudioLabelsEnum.enum["plugin:scene:custom"]
          ));

      if (studioId) {
        scene.studio = studioId;
        if (shouldApplyStudioLabels) {
          const studio = await Studio.getById(studioId);
          if (studio) {
            studioLabels = (await Studio.getLabels(studio)).map((l) => l._id);
          }
        }
      } else if (config.plugins.createMissingStudios) {
        let studio = new Studio(studioName);
        scene.studio = studio._id;

        try {
          studio = await onStudioCreate(studio, studioLabels);
        } catch (error) {
          handleError(`Error running studio plugin for new studio, in scene plugin`, error);
        }

        await Studio.findUnmatchedScenes(studio, shouldApplyStudioLabels ? studioLabels : []);
        await collections.studios.upsert(studio._id, studio);
        await indexStudios([studio]);
        logger.debug(`Created studio ${studio.name}`);
      }
      if (shouldApplyStudioLabels) {
        logger.verbose("Applying actor labels to scene");
        sceneLabels.push(...studioLabels);
      }
    }
  }

  if (pluginResult.movie && isString(pluginResult.movie)) {
    const movieId = (await extractMovies(pluginResult.movie))[0] || null;

    if (movieId) {
      const movie = <Movie>await Movie.getById(movieId);
      const sceneIds = (await Movie.getScenes(movie)).map((sc) => sc._id);
      await Movie.setScenes(movie, sceneIds.concat(scene._id));
      await indexMovies([movie]);
    } else if (config.plugins.createMissingMovies) {
      const movie = new Movie(pluginResult.movie);

      try {
        const diff = await onMovieCreate(movie, "movieCreated");
        Object.assign(movie, diff);
      } catch (error) {
        handleError(`onMovieCreate error`, error);
      }

      await collections.movies.upsert(movie._id, movie);
      logger.debug(`Created movie ${movie.name}`);
      await Movie.setScenes(movie, [scene._id]);
      logger.debug(`Attached ${scene.name} to movie ${movie.name}`);
      await indexMovies([movie]);
    }
  }

  return {
    scene,
    commit: async () => {
      logger.debug("Committing plugin result");

      for (const image of createdImages) {
        if (config.matching.applySceneLabels) {
          await Image.setLabels(image, sceneLabels);
        }
        await Image.setActors(image, sceneActors);
      }

      await indexImages(createdImages);

      logger.verbose(`Creating ${createdMarkers.length} markers`);
      logger.silly(formatMessage(createdMarkers));

      for await (const _ of tinyAsyncPool(8, createdMarkers, async (marker) => {
        await Marker.createMarkerThumbnail(marker);
        await Marker.setActors(marker, sceneActors);

        // Extract labels
        const extractedLabels = await extractLabels(marker.name);
        logger.verbose(`Found ${extractedLabels.length} labels in marker name`);
        await Marker.setLabels(marker, extractedLabels);

        if (config.matching.applyActorLabels.includes("plugin:marker:create")) {
          for (const actorId of sceneActors) {
            const actor = await Actor.getById(actorId);

            if (actor) {
              const actorLabels = await Actor.getLabels(actor);
              await Marker.addLabels(
                marker,
                actorLabels.map((l) => l._id)
              );
            }
          }
        }
      }));

      await indexMarkers(createdMarkers);
    },
  };
}
