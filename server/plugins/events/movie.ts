import { getConfig } from "../../config";
import { ApplyStudioLabelsEnum } from "../../config/schema";
import { collections } from "../../database";
import { buildFieldExtractor, extractStudios } from "../../extractor";
import { runPluginsSerial } from "../../plugins";
import { indexImages } from "../../search/image";
import { indexStudios } from "../../search/studio";
import Actor from "../../types/actor";
import Image from "../../types/image";
import Label from "../../types/label";
import Movie from "../../types/movie";
import Scene from "../../types/scene";
import Studio from "../../types/studio";
import { logger } from "../../utils/logger";
import { validRating } from "../../utils/misc";
import { normalizeDescription, normalizeName } from "../../utils/string";
import { isBoolean, isNumber, isString } from "../../utils/types";
import { createImage, createLocalImage, importImageFromPath } from "../context";
import { onStudioCreate } from "./studio";

function injectServerFunctions(movie: Movie) {
  let actors: Actor[], labels: Label[], scenes: Scene[], rating: number;
  return {
    $getActors: async () => (actors ??= await Movie.getActors(movie)),
    $getLabels: async () => (labels ??= await Movie.getLabels(movie)),
    $getScenes: async () => (scenes ??= await Movie.getScenes(movie)),
    $getRating: async () => (rating ??= await Movie.getRating(movie)),
    $createLocalImage: async (path: string, name: string, thumbnail?: boolean) => {
      const img = await createLocalImage(path, name, thumbnail);
      await collections.images.upsert(img._id, img);

      if (!thumbnail) {
        await indexImages([img]);
      }

      return img._id;
    },
    $importImageFromPath: async (path: string, name: string, thumbnail?: boolean) => {
      const img = await importImageFromPath(path, name, thumbnail);
      await collections.images.upsert(img._id, img);

      if (!thumbnail) {
        await indexImages([img]);
      }
      return img._id;
    },
    $createImage: async (url: string, name: string, thumbnail?: boolean) => {
      const img = await createImage(url, name, thumbnail);
      await collections.images.upsert(img._id, img);
      if (!thumbnail) {
        await indexImages([img]);
      }
      return img._id;
    },
  };
}

export async function onMovieCreate(
  movie: Movie,
  event: "movieCreated" | "movieCustom" = "movieCreated"
): Promise<{ diff: Partial<Movie> }> {
  const config = getConfig();

  const pluginResult = await runPluginsSerial(config, event, {
    movie: structuredClone(movie),
    movieName: movie.name,
    ...injectServerFunctions(movie),
  });

  const diff: Partial<Movie> = {};

  if (
    isString(pluginResult.frontCover) &&
    pluginResult.frontCover.startsWith("im_") &&
    (!movie.frontCover || config.plugins.allowMovieThumbnailOverwrite)
  ) {
    const image = await Image.getById(pluginResult.frontCover);

    if (image) {
      await Image.attachDimensions(image);
    }

    diff.frontCover = pluginResult.frontCover;
  }

  if (
    isString(pluginResult.backCover) &&
    pluginResult.backCover.startsWith("im_") &&
    (!movie.backCover || config.plugins.allowMovieThumbnailOverwrite)
  ) {
    const image = await Image.getById(pluginResult.backCover);

    if (image) {
      await Image.attachDimensions(image);
    }

    diff.backCover = pluginResult.backCover;
  }

  if (
    isString(pluginResult.spineCover) &&
    pluginResult.spineCover.startsWith("im_") &&
    (!movie.spineCover || config.plugins.allowMovieThumbnailOverwrite)
  ) {
    const image = await Image.getById(pluginResult.spineCover);

    if (image) {
      await Image.attachDimensions(image);
    }

    diff.spineCover = pluginResult.spineCover;
  }

  if (isString(pluginResult.name)) {
    diff.name = normalizeName(pluginResult.name);
  }

  if (isString(pluginResult.description)) {
    diff.description = normalizeDescription(pluginResult.description);
  }

  if (isNumber(pluginResult.releaseDate) && !isNaN(pluginResult.releaseDate)) {
    diff.releaseDate = new Date(pluginResult.releaseDate).valueOf();
  }

  if (isNumber(pluginResult.addedOn)) {
    diff.addedOn = new Date(pluginResult.addedOn).valueOf();
  }

  if (validRating(pluginResult.rating)) {
    diff.rating = pluginResult.rating;
  }

  if (isBoolean(pluginResult.favorite)) {
    diff.favorite = pluginResult.favorite;
  }

  if (isNumber(pluginResult.bookmark) || pluginResult.bookmark === null) {
    diff.bookmark = pluginResult.bookmark;
  } else if (isBoolean(pluginResult.bookmark)) {
    diff.bookmark = pluginResult.bookmark ? Date.now() : null;
  }

  if (pluginResult.custom && typeof pluginResult.custom === "object") {
    const localExtractFields = await buildFieldExtractor();
    for (const key in pluginResult.custom) {
      const fields = localExtractFields(key);
      if (fields.length) {
        if (!diff.customFields) {
          diff.customFields = {};
        }

        diff.customFields[fields[0]] = (pluginResult.custom as Record<string, any>)[key];
      }
    }
  }

  if (!movie.studio && pluginResult.studio && isString(pluginResult.studio)) {
    const studioId = (await extractStudios(pluginResult.studio))[0] || null;

    if (studioId) {
      diff.studio = studioId;
    }
    else if (config.plugins.createMissingStudios) {
      const studioLabels: string[] = [];
      let studio = new Studio(pluginResult.studio);
      diff.studio = studio._id;

      studio = await onStudioCreate(studio, studioLabels, "studioCreated");
      await collections.studios.upsert(studio._id, studio);
      await Studio.findUnmatchedScenes(
        studio,
        config.matching.applyStudioLabels.includes(
          ApplyStudioLabelsEnum.enum["event:studio:create"]
        )
          ? studioLabels
          : []
      );
      await indexStudios([studio]);

      logger.debug(`Created studio ${studio.name}`);
    }
  }

  return {
    diff,
  };
}
