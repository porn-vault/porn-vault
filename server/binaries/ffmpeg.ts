import { resolve } from "node:path";

import { execa } from "execa";
import formatDuration from "format-duration";
import asyncPool from "tiny-async-pool";

import { getConfig } from "../config";
import { mkdirpAsync, rimrafAsync, statAsync } from "../utils/fs/async";
import { formatMessage, handleError, logger } from "../utils/logger";
import { generateTimestampsAtIntervals } from "../utils/misc";

export async function takeScreenshot(
  inPath: string,
  ms: number,
  outPath: string,
  width?: number
): Promise<void> {
  const config = getConfig();

  const durationStr = formatDuration(ms, { leading: true });
  logger.silly(`Taking screenshot of ${inPath} at ${durationStr} to ${outPath}`);

  const proc = await execa(config.binaries.ffmpeg, [
    "-y",
    "-ss",
    durationStr,
    "-i",
    inPath,
    "-frames:v",
    "1",
    "-q:v",
    "2",
    ...(width ? ["-vf", `scale=${width}:-1`] : []),
    outPath,
  ]);

  if (proc.exitCode) {
    throw new Error(`FFmpeg exited with non-zero status code ${proc.exitCode}`);
  }
}

type ScreenshotOptions = {
  videoFile: string;
  timestamps: number[];
  outputFolder: string;
  width: number;
};

export type ThumbnailFile = {
  name: string;
  path: string;
  size: number;
  time: number;
};

export async function multiScreenshot(opts: ScreenshotOptions): Promise<ThumbnailFile[]> {
  logger.debug("Running multi screenshot");
  logger.silly(`Screenshot options: ${formatMessage({ ...opts })}`);

  await mkdirpAsync(opts.outputFolder);
  const filePattern = `screenshot-{{index}}.jpg`;

  const screenshotFilenames: string[] = [];

  // NOTE: tiny-async-pool doesn't support index
  const indexedTimestampes = opts.timestamps.map((ts, index) => [index, ts]);

  for await (const imgPath of asyncPool(4, indexedTimestampes, async ([index, timestamp]) => {
    logger.silly(`Taking screenshot ${index}`);

    const outPath = resolve(
      opts.outputFolder,
      filePattern.replace("{{index}}", index.toString().padStart(4, "0"))
    );

    await takeScreenshot(opts.videoFile, timestamp, outPath, opts.width);

    return outPath;
  })) {
    screenshotFilenames.push(imgPath);
  }

  const screenshotFiles = await Promise.all(
    screenshotFilenames.map(async (name) => {
      const filePath = resolve(opts.outputFolder, name);
      const stats = await statAsync(filePath);

      return {
        name,
        path: filePath,
        size: stats.size,
        time: stats.mtime.getTime(),
      };
    })
  );

  screenshotFiles.sort((a, b) => a.name.localeCompare(b.name, undefined, { numeric: true }));

  logger.verbose(`Generated ${screenshotFiles.length} screenshots.`);

  return screenshotFiles;
}

const PREVIEW_IMAGE_WIDTH = 160;
const PREVIEW_COUNT = 100;

export async function generatePreview(
  sceneId: string,
  scenePath: string,
  durationSecs: number,
  outPath: string
): Promise<void> {
  logger.debug(`Creating 100 small previews for ${sceneId}.`);

  const tmpFolder = resolve("tmp", "preview", sceneId);

  const timestamps = generateTimestampsAtIntervals(PREVIEW_COUNT, durationSecs * 1000, {
    startPercentage: 2,
    endPercentage: 100,
  });
  const files = await multiScreenshot({
    videoFile: scenePath,
    width: PREVIEW_IMAGE_WIDTH,
    timestamps,
    outputFolder: tmpFolder,
  });

  logger.debug(`Creating preview strip for ${sceneId}`);
  await execa(getConfig().binaries.imagemagick.montagePath, [
    ...files.map((x) => x.path),
    "-tile",
    "100x1",
    "-geometry",
    "+0+0",
    outPath,
  ]);

  logger.debug(`Cleanup temp folder`);

  await rimrafAsync(tmpFolder).catch((error) => {
    handleError("Error during cleanup", error);
  });
}
