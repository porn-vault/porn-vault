import { execa } from "execa";

import { getConfig } from "../config";

// TODO: type
export async function ffprobeFile(path: string): Promise<any> {
  const proc = await execa(getConfig().binaries.ffprobe, [
    "-v",
    "quiet",
    "-print_format",
    "json",
    "-show_format",
    "-show_streams",
    path,
  ]);
  if (proc.exitCode) {
    throw new Error(`FFprobe exited with non-zero status code ${proc.exitCode}`);
  }
  return JSON.parse(proc.stdout.trim());
}
