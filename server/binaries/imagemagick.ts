import { platform } from "node:os";

import { execa } from "execa";

import { getConfig } from "../config";
import { logger } from "../utils/logger";

export async function getImageDimensions(input: string): Promise<{
  width: number;
  height: number;
}> {
  const proc = await execa(getConfig().binaries.imagemagick.identifyPath, [
    "-format",
    "%[w] %[h]",
    input,
  ]);
  const dims = proc.stdout
    .trim()
    .split(" ")
    .map((x) => parseInt(x));
  return { width: dims[0], height: dims[1] };
}

export function imagemagickHelp(cmd: string, path: string) {
  if (platform() !== "win32" && path.endsWith(".exe")) {
    logger.warn(
      `It looks like you're not running Windows, but your config contains an .exe path.
Make sure to install imagemagick and set the correct imagemagick path in the config.
If you have magick.exe, you may be able to use "{path}/magick.exe ${cmd}"`
    );
  }
  if (platform() === "linux") {
    logger.warn(
      `Maybe try using something like: "sudo apt-get install imagemagick" and adjusting the ${cmd}Path in the config to "${cmd}"`
    );
  } else if (platform() === "darwin") {
    logger.warn(
      `Maybe try using something like: "brew install imagemagick" and adjusting the ${cmd}Path in the config to "${cmd}"`
    );
  } else if (platform() === "win32") {
    logger.warn(
      `Maybe try downloading the .exe from : "https://imagemagick.org/script/download.php" and adjusting the ${cmd}Path in the config to "${cmd}"`
    );
  }
}
