import { collections } from "../database";
import { handleError, logger } from "../utils/logger";
import m0 from "./0000_image_folders";

const migrations = [m0];

export type Migration = {
  name: string;
  fn: () => Promise<unknown>;
};

export async function runMigrations() {
  logger.info(
    `Running ${migrations.length} ${migrations.length === 1 ? "migration" : "migrations"}`
  );

  try {
    for (const { id, name, version, fn } of migrations) {
      logger.debug(`Checking migration "${name}"`);

      const inDb = await collections.migrations.get(name);
      if (inDb?.done) {
        logger.debug(
          `Skipping migration "${name}", already applied: ${new Date(
            inDb.addedOn
          ).toLocaleString()}`
        );
        continue;
      }

      logger.warn(`Running migration "${name}"`);
      const result = await fn();
      logger.info(`Migration "${name}" successful.`);

      await collections.migrations.upsert(name, {
        _id: id,
        addedOn: Date.now(),
        done: true,
        name,
        result,
        version,
      });

      logger.debug("Saved migration record in DB");
    }
  } catch (error) {
    handleError("Migration error", error, true);
    return;
  }

  logger.info("Migrations done");
}
