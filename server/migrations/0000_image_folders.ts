import { copyFileSync, existsSync, readdirSync, renameSync, rmdirSync, unlinkSync } from "node:fs";
import { extname, resolve } from "node:path";

import { collections } from "../database";
import Image from "../types/image";
import Marker from "../types/marker";
import Scene from "../types/scene";
import { mkdirpSync } from "../utils/fs/async";
import { logger } from "../utils/logger";
import { getFolderPartition, libraryPath } from "../utils/path";

async function partitionImageFolder(
  baseFolder: string,
  process: (filename: string, newPath: string, oldPath: string) => Promise<void>
): Promise<number> {
  if (!existsSync(baseFolder)) {
    logger.debug(`${baseFolder} already migrated (doesn't exist anymore)`);
    return 0;
  }

  let numMoved = 0;

  logger.verbose(`Processing ${baseFolder}`);

  for (const file of readdirSync(baseFolder, { withFileTypes: true })) {
    if (!file.isDirectory()) {
      if ([".jpeg", ".jpg", ".png"].includes(extname(file.name))) {
        // Move file
        const oldPath = resolve(baseFolder, file.name);

        const newFolder = getFolderPartition(baseFolder);
        mkdirpSync(newFolder);
        const newFilePath = resolve(newFolder, file.name);

        logger.debug(`Copying ${oldPath} -> ${newFilePath}`);
        copyFileSync(oldPath, newFilePath);

        await process(file.name, newFilePath, oldPath);

        logger.debug(`Remove unused ${oldPath}`);
        unlinkSync(oldPath);

        numMoved++;
      } else {
        logger.warn(`Invalid file in ${baseFolder}: ${file.name}: Ignoring it`);
      }
    }
  }

  logger.verbose(`Processed ${baseFolder}: Moved ${numMoved} files`);

  return numMoved;
}

async function fn() {
  let numMoved = 0;

  numMoved += await partitionImageFolder(
    libraryPath("previews/"),
    async (filename, newPath, oldPath) => {
      const scene = await Scene.getById(filename.replace(".jpg", ""));

      if (!scene || !scene.preview) {
        logger.warn(`Dangling scene preview: ${oldPath}`);
      } else {
        const image = await Image.getById(scene.preview);

        if (!image) {
          logger.warn(`Dangling scene preview: ${oldPath}`);
        } else {
          image.path = newPath;
          await collections.images.upsert(image._id, image);
        }
      }
    }
  );

  numMoved += await partitionImageFolder(
    libraryPath("images/"),
    async (filename, newPath, oldPath) => {
      const image = await Image.getById(filename.replace(".jpg", ""));

      if (!image) {
        logger.warn(`Dangling image: ${oldPath}`);
      } else {
        image.path = newPath;
        await collections.images.upsert(image._id, image);
      }
    }
  );

  numMoved += await partitionImageFolder(
    libraryPath("thumbnails/images"),
    async (filename, newPath, oldPath) => {
      const image = await Image.getById(filename.replace(".jpg", ""));

      if (!image) {
        logger.warn(`Dangling image thumbnail: ${oldPath}`);
      } else {
        image.thumbPath = newPath;
        await collections.images.upsert(image._id, image);
      }
    }
  );

  numMoved += await partitionImageFolder(
    libraryPath("thumbnails/markers"),
    async (filename, newPath, oldPath) => {
      const image = await Marker.getById(filename.replace(".jpg", ""));

      if (!image) {
        logger.warn(`Dangling image thumbnail: ${oldPath}`);
      } else {
        image.thumbnail = newPath;
        await collections.markers.upsert(image._id, image);
      }
    }
  );

  logger.info(`Moved ${numMoved} image files`);

  const thumbFolder = libraryPath("thumbnails");

  for (const file of readdirSync(thumbFolder, { withFileTypes: true }).filter(
    (x) => !x.isDirectory()
  )) {
    const path = resolve(thumbFolder, file.name);
    logger.verbose(`Deleting old thumbnail ${path}`);

    if (file.name.startsWith("sc_")) {
      const scene = await Scene.getById(file.name.replace(".jpg", "").replace(" (thumbnail)", ""));

      if (!scene || !scene.thumbnail) {
        logger.warn(`Dangling scene thumbnail: ${path}`);
      } else {
        const image = await Image.getById(scene.thumbnail);

        if (!image) {
          logger.warn(`Dangling scene thumbnail: ${path}`);
        } else {
          await collections.images.remove(image._id);
        }

        await Scene.setInitialThumbnail(scene);
      }
    } else if (file.name.startsWith("im_")) {
      const image = await Image.getById(file.name.replace(".jpg", ""));

      if (!image) {
        logger.warn(`Dangling scene thumbnail: ${path}`);
      } else {
        await collections.images.remove(image._id);
      }
    } else {
      throw new Error(`Invalid file in thumbnail folder: ${file.name}`);
    }

    unlinkSync(path);
  }

  return {
    filesMoved: numMoved,
  };
}

export default {
  id: "0000_image_folders",
  name: "Migrate image folders",
  version: "0.30.0",
  fn,
};
