import Axios, { AxiosError, AxiosResponse } from "axios";

import { izzyHost } from "../../binaries/izzy";
import { getConfig } from "../../config";
import { formatMessage, logger } from "../../utils/logger";

// TS bindings for Izzy
export namespace Izzy {
  // Secondary index definition
  // Can only be used on string values
  export interface IIndexCreation<T extends { _id: string }> {
    name: string;
    key: keyof Omit<T, "_id">;
  }

  // Represents a collection ("table")
  export class Collection<T extends { _id: string }> {
    name: string;
    file: string;
    indexes: IIndexCreation<T>[];

    constructor(name: string, file: string, indexes = [] as IIndexCreation<T>[]) {
      this.name = name;
      this.file = file;
      this.indexes = indexes;
    }

    // Returns amount of items in collection
    async count(): Promise<number> {
      logger.silly(`Getting collection count: ${this.name}`);
      const res = await Axios.get<{ count: number }>(
        `http://${izzyHost}:${getConfig().binaries.izzyPort}/collection/${this.name}/count`
      );
      return res.data.count;
    }

    // Compacts database file
    async compact(): Promise<AxiosResponse<unknown>> {
      logger.silly(`Compacting collection: ${this.name}`);
      return Axios.post(
        `http://${izzyHost}:${getConfig().binaries.izzyPort}/collection/compact/${this.name}`
      );
    }

    // Inserts or overwrites item
    async upsert(id: string, obj: T): Promise<T> {
      logger.silly(`Upsert ${id} in collection: ${this.name}`);
      const res = await Axios.post<T>(
        `http://${izzyHost}:${getConfig().binaries.izzyPort}/collection/${this.name}/${id}`,
        obj
      );
      return res.data;
    }

    // Partially updates object by merging props (shallow)
    async partialUpdate(id: string, obj: Partial<Omit<T, "_id">>): Promise<T> {
      logger.silly(`Partial update ${id} in collection: ${this.name}`);
      const res = await Axios.patch<T>(
        `http://${izzyHost}:${getConfig().binaries.izzyPort}/collection/${this.name}/${id}`,
        obj
      );
      return res.data;
    }

    async clear(): Promise<void> {
      logger.silly(`Clear: ${this.name}`);
      await Axios.delete<T>(`http://${izzyHost}:${getConfig().binaries.izzyPort}/collection`);
      await createCollection(this.name, this.file, this.indexes);
    }

    // Removes item
    async remove(id: string): Promise<void> {
      logger.silly(`Remove ${id} in collection: ${this.name}`);
      await Axios.delete(
        `http://${izzyHost}:${getConfig().binaries.izzyPort}/collection/${this.name}/${id}`
      );
    }

    // Gets one item from collection, order is not guaranteed
    async getHead(): Promise<T | null> {
      logger.silly(`Get head from collection: ${this.name}`);
      const res = await Axios.get<T | null>(
        `http://${izzyHost}:${getConfig().binaries.izzyPort}/collection/${this.name}/head`
      );
      return res.data;
    }

    async* iterate(chunkSize = 100): AsyncIterable<T> {
      let page = 0;

      while (true) {
        const items = await this.getTail(page * chunkSize, chunkSize);

        for (const item of items) {
          yield item;
        }

        if (!items.length) {
          break;
        }

        page += 1;
      }
    }

    // Tails collection
    async getTail(skip: number, take: number): Promise<T[]> {
      logger.silly(`Get tail from collection: ${this.name} [s: ${skip}, t: ${take}]`);

      const res = await Axios.get<T[]>(
        `http://${izzyHost}:${getConfig().binaries.izzyPort}/collection/${this.name
        }/tail?skip=${skip}&take=${take}`
      );

      return res.data;
    }

    // Gets all items from collections
    async getAll(): Promise<T[]> {
      logger.silly(`Get all from collection: ${this.name}`);
      const res = await Axios.get<{ items: T[] }>(
        `http://${izzyHost}:${getConfig().binaries.izzyPort}/collection/${this.name}`
      );
      return res.data.items;
    }

    // Gets item by ID
    async get(id: string): Promise<T | null> {
      logger.silly(`Getting ${id} from collection: ${this.name}`);
      try {
        const res = await Axios.get<T | null>(
          `http://${izzyHost}:${getConfig().binaries.izzyPort}/collection/${this.name}/${id}`
        );
        return res.data;
      } catch (error) {
        const _err = error as AxiosError;
        if (!_err.response) {
          throw error;
        }
        if (_err.response.status === 404) {
          return null;
        }
        throw _err;
      }
    }

    // Gets multiple items using one request
    async getBulk(items: readonly string[]): Promise<T[]> {
      logger.silly(`Getting ${items.length} items in bulk from collection: ${this.name}`);
      const { data } = await Axios.post<{ items: T[] }>(
        `http://${izzyHost}:${getConfig().binaries.izzyPort}/collection/${this.name}/bulk`,
        { items }
      );
      const filtered = data.items.filter(Boolean);
      if (filtered.length < data.items.length) {
        logger.warn(
          `Retrieved some null value from getBulk (set logger to 'debug' for more info): `
        );
        logger.debug(`Requested: ${formatMessage(items)}`);
        logger.debug(`Result: ${formatMessage(data.items)}`);
        logger.warn(
          "This is not breaking, but it does mean your database probably contains some invalid value or the search index is out of sync. Try reindexing."
        );
      }
      return filtered;
    }

    // Queries an index by key
    async query(index: string, key: string | null): Promise<T[]> {
      logger.silly(`Querying index ${index} by ${key} from collection: ${this.name}`);
      const { data } = await Axios.get<{ items: T[] }>(
        `http://${izzyHost}:${getConfig().binaries.izzyPort}/collection/${this.name
        }/${index}/${key}`
      );

      // TODO: Monkey patch... izzy bug?
      const filtered = data.items.filter(Boolean);
      if (filtered.length < data.items.length) {
        logger.warn(`Retrieved some null value from query`);
      }

      return filtered;
    }

    // Queries an index by key
    async deleteByQuery(index: string, key: string): Promise<void> {
      logger.silly(`Deleting indexed ${index} by ${key} from collection: ${this.name}`);
      await Axios.delete(
        `http://${izzyHost}:${getConfig().binaries.izzyPort}/collection/${this.name
        }/${index}/${key}`
      );
    }

    // Queries an index by multiple keys
    async queryBulk(index: string, keys: readonly string[] | null): Promise<Record<string, T[]>> {
      logger.silly(`Querying index ${index} by ${keys?.join(",")} from collection: ${this.name}`);
      const { data } = await Axios.post<Record<string, T[]>>(
        `http://${izzyHost}:${getConfig().binaries.izzyPort}/collection/${this.name
        }/index/${index}/query-bulk`,
        { keys }
      );

      // TODO: Monkey patch... izzy bug?
      for (const key in data) {
        const filtered = data[key].filter(Boolean);

        if (data[key].length > filtered.length) {
          logger.warn(`Retrieved some null value from queryBulk`);
        }

        data[key] = filtered;
      }

      return data;
    }
  }

  // Creates new collection, will be successful when the collection already exists
  export async function createCollection<T extends { _id: string }>(
    name: string,
    file: string,
    indexes = [] as IIndexCreation<T>[]
  ): Promise<Collection<T>> {
    try {
      logger.debug(`Creating collection: ${name} (persistence: ${file})`);
      logger.silly(indexes);
      await Axios.post(`http://${izzyHost}:${getConfig().binaries.izzyPort}/collection/${name}`, {
        file,
        indexes,
      });

      return new Collection(name, file, indexes);
    } catch (error) {
      const _err = error as AxiosError;
      if (_err.response && _err.response.status === 409) {
        return new Collection(name, file, indexes);
      }
      throw _err;
    }
  }
}
