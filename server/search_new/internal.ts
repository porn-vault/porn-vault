import { URLSearchParams } from "node:url";

import Axios, { AxiosError } from "axios";

import { izzyHost } from "../binaries/izzy";
import { getConfig } from "../config";
import { logger } from "../utils/logger";

export type Schema<T> = Record<keyof Omit<T, "_id">, "Text" | "Keyword" | "Integer" | "Json" | "Boolean">

export type BaseMapping = {
    _id: string;
    addedOn: number;
    name: string;
    rawName: string;
};

export class SearchIndex<T extends BaseMapping> {
    name: string;
    path: string;
    schema: Schema<T>;

    constructor(name: string, path: string, schema: Schema<T>) {
        this.name = name;
        this.path = path;
        this.schema = schema;
    }

    async aggregate<T>(aggregation: Record<string, any>): Promise<T> {
        const res = await Axios.post(
            `http://${izzyHost}:${getConfig().binaries.izzyPort}/search/${this.name}/aggregate`,
            aggregation
        );
        return res.data.result;
    }

    async deleteIndex(): Promise<void> {
        logger.info(`Deleting search index ${this.name}`);

        await Axios.delete(
            `http://${izzyHost}:${getConfig().binaries.izzyPort}/search/${this.name}`,
        );
    }

    async bulkRemove(ids: string[]): Promise<void> {
        if (!ids.length) {
            return;
        }

        await Axios.delete(
            `http://${izzyHost}:${getConfig().binaries.izzyPort}/search/${this.name}/items`,
            {
                data: {
                    items: ids
                }
            }
        );
    }

    async removeItem(id: string): Promise<void> {
        await Axios.delete(
            `http://${izzyHost}:${getConfig().binaries.izzyPort}/search/${this.name}/items/${id}`,
        );
    }

    async indexItems(items: T[]): Promise<void> {
        await Axios.post<{ count: number }>(
            `http://${izzyHost}:${getConfig().binaries.izzyPort}/search/${this.name}/items`,
            items,
        );
    }

    async query(query: string, skip?: number, take?: number, order?: { by: string, dir: "asc" | "desc" }): Promise<{ hits_count: number, ids: string[] }> {
        const searchParams = new URLSearchParams();
        searchParams.append("q", query.trim());

        if (skip) {
            searchParams.append("skip", String(skip));
        }
        if (take) {
            searchParams.append("take", String(take));
        }
        if (order) {
            searchParams.append("order_by", order.by);
            searchParams.append("order_dir", order.dir);
        }

        const q = searchParams.toString();

        const body = await Axios.get<{ result: { hits_count: number, ids: string[] } }>(
            `http://${izzyHost}:${getConfig().binaries.izzyPort}/search/${this.name}?${q}`
        );

        return body.data.result;
    }

    async count(query?: string): Promise<number> {
        const qs = query ? `?q=${query}` : "";

        logger.debug(`Counting search index ${this.name} with query ${query ?? "<empty>"}`);

        const body = await Axios.get<{ count: number }>(
            `http://${izzyHost}:${getConfig().binaries.izzyPort}/search/${this.name}/count${qs}`
        );
        return body.data.count;
    }

    async ensure(): Promise<void> {
        await createSearchIndex(this.name, this.path, this.schema);
    }
}

export async function createSearchIndex<T extends BaseMapping>(
    name: string, path: string, schema: Schema<T>
): Promise<SearchIndex<T>> {
    try {
        logger.debug(`Creating collection: ${name} (persistence: ${path})`);
        logger.silly(schema);

        await Axios.put(`http://${izzyHost}:${getConfig().binaries.izzyPort}/search/${name}`, {
            path,
            schema,
        });

        return new SearchIndex(name, path, schema);
    } catch (error) {
        const _err = error as AxiosError;
        if (_err.response && _err.response.status === 409) {
            return new SearchIndex(name, path, schema);
        }
        throw _err;
    }
}