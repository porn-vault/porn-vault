import { formatName } from "..";
import { SearchIndex } from "../internal";

export interface StudioSearchDoc {
    _id: string;
    name: string;
    rawName: string;
    addedOn: number;

    aliases: string[];
    parents: string[];
    parentNames: string[];
    labels: string[];
    labelNames: string[];
    numLabels: number,
    bookmark: number;
    favorite: boolean;
    rating: number;
    averageRating: number;
    numScenes: number;
    custom: Record<string, boolean | string | number | string[] | null>;
    // TODO: aliases
}

export const index = new SearchIndex<StudioSearchDoc>(formatName("studios"), "SET_IN_ENSURE", {
    name: "Text",
    rawName: "Keyword",
    addedOn: "Integer",
    labels: "Keyword",
    labelNames: "Text",
    rating: "Integer",
    bookmark: "Integer",
    favorite: "Boolean",
    custom: "Json",
    aliases: "Text",
    averageRating: "Integer",
    numScenes: "Integer",
    numLabels: "Integer",
    parentNames: "Text",
    parents: "Keyword",
});

