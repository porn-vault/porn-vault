import { formatName } from "..";
import { SearchIndex } from "../internal";

export type ActorSearchDoc = {
    _id: string;
    name: string;
    rawName: string;
    addedOn: number;

    letter: string;
    aliases: string[];
    labels: string[];
    numLabels: number;
    labelNames: string[];
    rating: number;
    averageRating: number;
    score: number;
    bookmark: number;
    favorite: boolean;
    numViews: number;
    lastViewedOn: number;
    bornOn: number;
    numScenes: number;
    nationalityName: string;
    countryCode: string;
    custom: Record<string, boolean | string | number | string[] | null>;
    studios: string[];
    studioNames: string[];
}

export const index = new SearchIndex<ActorSearchDoc>(formatName("actors"), "SET_IN_ENSURE", {
    name: "Text",
    rawName: "Keyword",
    addedOn: "Integer",
    labels: "Keyword",
    labelNames: "Text",
    studios: "Keyword",
    studioNames: "Text",
    numLabels: "Integer",
    rating: "Integer",
    bookmark: "Integer",
    numViews: "Integer",
    lastViewedOn: "Integer",
    score: "Integer",
    favorite: "Boolean",
    custom: "Json",
    aliases: "Text",
    averageRating: "Integer",
    bornOn: "Integer",
    countryCode: "Keyword",
    letter: "Keyword",
    nationalityName: "Text",
    numScenes: "Integer",
});
