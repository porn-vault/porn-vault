import { formatName } from "..";
import { SearchIndex } from "../internal";

export interface MovieSearchDoc {
    _id: string;
    addedOn: number;
    name: string;
    rawName: string;

    actors: string[];
    labels: string[];
    actorNames: string[];
    labelNames: string[];
    numLabels: number;
    rating: number;
    bookmark: number;
    favorite: boolean;
    releaseDate: number;
    releaseYear: number;
    duration: number;
    studios: string[];
    studioNames: string[];
    numScenes: number;
    custom: Record<string, boolean | string | number | string[] | null>;
    numActors: number;
    size: number;
}

export const index = new SearchIndex<MovieSearchDoc>(formatName("movies"), "SET_IN_ENSURE", {
    name: "Text",
    rawName: "Keyword",
    addedOn: "Integer",
    actors: "Keyword",
    labels: "Keyword",
    actorNames: "Text",
    labelNames: "Text",
    studios: "Keyword",
    studioNames: "Text",
    numActors: "Integer",
    numLabels: "Integer",
    rating: "Integer",
    bookmark: "Integer",
    releaseDate: "Integer",
    releaseYear: "Integer",
    duration: "Integer",
    favorite: "Boolean",
    custom: "Json",
    size: "Integer",
    numScenes: "Integer"
});
