import { formatName } from "..";
import { SearchIndex } from "../internal";

export interface ImageSearchDoc {
    _id: string;
    name: string;
    rawName: string;
    addedOn: number;

    actors: string[];
    labels: string[];
    actorNames: string[];
    labelNames: string[];
    numLabels: number;
    bookmark: number;
    favorite: boolean;
    rating: number;
    album: string;
    albumName: string;
    scene: string;
    sceneName: string;
    movies: string[];
    movieNames: string[];
    studios: string[];
    studioNames: string[];
    custom: Record<string, boolean | string | number | string[] | null>;
    numActors: number;
    path: string;
}

export const index = new SearchIndex<ImageSearchDoc>(formatName("images"), "SET_IN_ENSURE", {
    name: "Text",
    rawName: "Keyword",
    addedOn: "Integer",
    actors: "Keyword",
    labels: "Keyword",
    actorNames: "Text",
    labelNames: "Text",
    studios: "Keyword",
    studioNames: "Text",
    movies: "Keyword",
    movieNames: "Text",
    numActors: "Integer",
    numLabels: "Integer",
    rating: "Integer",
    bookmark: "Integer",
    favorite: "Boolean",
    custom: "Json",
    path: "Keyword",
    album: "Keyword",
    albumName: "Text",
    scene: "Keyword",
    sceneName: "Text",
});
