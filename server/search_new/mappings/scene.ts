import { formatName } from "..";
import { SearchIndex } from "../internal";

export type SceneSearchDoc = {
    _id: string;
    name: string;
    rawName: string;
    addedOn: number;

    path: string;
    rawPath: string;

    numActors: number;
    actors: string[];
    labels: string[];
    numLabels: number;
    actorNames: string[];
    labelNames: string[];
    rating: number;
    bookmark: number;
    favorite: boolean;
    numViews: number;
    lastViewedOn: number;
    releaseDate: number;
    releaseYear: number;
    duration: number;
    studios: string[];
    studioNames: string[];
    resolution: number;
    bitrate: number;
    size: number;
    score: number;
    movies: string[];
    movieNames: string[];
    numMovies: number;
    custom: Record<string, boolean | string | number | string[] | null>;
};

export const index = new SearchIndex<SceneSearchDoc>(formatName("scenes"), "SET_IN_ENSURE", {
    name: "Text",
    rawName: "Keyword",
    addedOn: "Integer",
    actors: "Keyword",
    labels: "Keyword",
    actorNames: "Text",
    labelNames: "Text",
    studios: "Keyword",
    studioNames: "Text",
    movies: "Keyword",
    movieNames: "Text",
    path: "Text",
    rawPath: "Keyword",
    numActors: "Integer",
    numLabels: "Integer",
    rating: "Integer",
    bookmark: "Integer",
    numViews: "Integer",
    lastViewedOn: "Integer",
    releaseDate: "Integer",
    releaseYear: "Integer",
    duration: "Integer",
    resolution: "Integer",
    bitrate: "Integer",
    size: "Integer",
    score: "Integer",
    numMovies: "Integer",
    favorite: "Boolean",
    custom: "Json",
});
