import { formatName } from "..";
import { SearchIndex } from "../internal";

export interface MarkerSearchDoc {
    _id: string;
    addedOn: number;
    name: string;
    rawName: string;

    actors: string[];
    actorNames: string[];
    labels: string[];
    labelNames: string[];
    studios: string[];
    studioNames: string[];
    numLabels: number;
    rating: number;
    bookmark: number;
    favorite: boolean;
    scene: string;
    sceneName: string;
    custom: Record<string, boolean | string | number | string[] | null>;
    numActors: number;
}

export const index = new SearchIndex<MarkerSearchDoc>(formatName("markers"), "SET_IN_ENSURE", {
    name: "Text",
    rawName: "Keyword",
    addedOn: "Integer",
    actors: "Keyword",
    labels: "Keyword",
    actorNames: "Text",
    labelNames: "Text",
    studios: "Keyword",
    studioNames: "Text",
    numActors: "Integer",
    numLabels: "Integer",
    rating: "Integer",
    bookmark: "Integer",
    favorite: "Boolean",
    custom: "Json",
    scene: "Keyword",
    sceneName: "Text",
});
