import { join } from "node:path";

import type { IConfig } from "../config/schema";
import { logger } from "../utils/logger";
import { ActorSearchDoc, index as actorIndex } from "./mappings/actor";
import { ImageSearchDoc, index as imageIndex } from "./mappings/image";
import { index as markerIndex,MarkerSearchDoc } from "./mappings/marker";
import { index as movieIndex,MovieSearchDoc } from "./mappings/movie";
import { index as sceneIndex,SceneSearchDoc } from "./mappings/scene";
import { index as studioIndex,StudioSearchDoc } from "./mappings/studio";

export function formatName(name: string) {
    if (!process.env.DATABASE_NAME || process.env.DATABASE_NAME === "production") {
        return `pv-${name}`;
    }
    return `pv-${process.env.DATABASE_NAME}-${name}`;
}

export const indexes = {
    actors: actorIndex,
    movies: movieIndex,
    scenes: sceneIndex,
    studios: studioIndex,
    images: imageIndex,
    markers: markerIndex,
};

export type {
    ActorSearchDoc,
    MovieSearchDoc,
    SceneSearchDoc,
    StudioSearchDoc,
    ImageSearchDoc,
    MarkerSearchDoc,
};

export async function ensureSearchIndexesExist(config: IConfig): Promise<void> {
    for (const indexName in indexes) {
        const index = indexes[indexName as keyof typeof indexes];

        index.path = join(config.persistence.libraryPath, "indexes", index.name);

        logger.info(`Creating search index ${index.name} in ${index.path}`);
        await index.ensure();
    }
}
