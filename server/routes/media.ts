import { existsSync } from "node:fs";
import { resolve } from "node:path";

import { Router } from "express";

import { collections } from "../database";
import Image from "../types/image";
import Scene from "../types/scene";
import { logger } from "../utils/logger";
import sceneRouter from "./scene";

const router = Router();

export const IMAGE_CACHE_CONTROL = "public, max-age=31536000, immutable";

router.use("/scene", sceneRouter);

/**
 * Returns a preview image
 */
router.get("/preview/:id", async (req, res) => {
  const sc = await Scene.getById(String(req.params.id));

  if (sc?.preview) {
    res.redirect(`/api/media/image/${sc.preview}`);
  } else {
    res.sendStatus(404);
  }
});

/**
 * Returns an image by (absolute) path
 */
router.get("/image/path", async (req, res) => {
  const pathParam = req.query.path;
  if (!pathParam) {
    return res.sendStatus(400);
  }

  const img = await Image.getByPath(String(pathParam));

  if (img) {
    res.redirect(`/api/media/image/${img._id}`);
  } else {
    res.sendStatus(404);
  }
});

router.get("/image/:image", async (req, res) => {
  const image = await Image.getById(req.params.image);
  if (!image?.path) {
    res.sendStatus(404);
    return;
  }

  const resolved = resolve(image.path);
  if (!existsSync(resolved)) {
    res.sendStatus(404);
    return;
  }

  res.set("Cache-control", IMAGE_CACHE_CONTROL);
  res.sendFile(resolved);
});

router.get("/image/:image/thumbnail", async (req, res) => {
  const image = await Image.getById(req.params.image);

  if (!image?.path) {
    return res.sendStatus(404);
  }

  if (!existsSync(image.path)) {
    return res.sendStatus(404);
  }

  if (image.path.endsWith(".gif")) {
    if (image.thumbPath && !existsSync(image.thumbPath)) {
      await collections.images.partialUpdate(image._id, { thumbPath: null });
    }

    const resolved = resolve(image.path);
    return res.sendFile(resolved);
  }

  if (image && !image.thumbPath) {
    logger.debug(`Thumbnail of ${req.params.image} does not exist (yet), generating now...`);
    const thumbPath = await Image.createThumbnail(image.path, image._id);
    logger.debug(`Image processing done.`);

    await collections.images.partialUpdate(image._id, { thumbPath });

    return res.redirect(`/api/media/image/${image._id}/thumbnail`);
  }

  if (image?.thumbPath) {
    const resolved = resolve(image.thumbPath);

    if (!existsSync(resolved)) {
      res.sendStatus(404);
    } else {
      res.set("Cache-control", IMAGE_CACHE_CONTROL);
      res.sendFile(resolved);
    }
  }
});

export default router;
