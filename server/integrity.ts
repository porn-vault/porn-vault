import { collections } from "./database";
import Actor from "./types/actor";
import Marker from "./types/marker";
import Movie from "./types/movie";
import Scene from "./types/scene";
import Studio from "./types/studio";
import { logger } from "./utils/logger";

export async function repairIntegrity(): Promise<void> {
  {
    logger.info("Checking scenes");

    for await (const scene of collections.scenes.iterate()) {
      const diff: Partial<Scene> = {};

      const imageProps = ["thumbnail", "preview"] satisfies (keyof Scene)[];

      for (const prop of imageProps) {
        if (scene[prop]) {
          const image = await collections.images.get(scene[prop]!);

          if (!image) {
            logger.info(`Scene ${scene._id} ${prop} is missing`);
            diff[prop] = null;
          }
        }
      }

      if (scene.studio) {
        const image = await collections.studios.get(scene.studio);

        if (!image) {
          logger.info(`Scene ${scene._id} studio is missing`);
          diff.studio = null;
        }
      }

      if (Object.keys(diff).length > 0) {
        logger.info(`Update scene ${scene._id}: ${JSON.stringify(diff, null, 2)}`);
        await collections.scenes.partialUpdate(scene._id, diff);
      }
    }

    logger.info("Compacting scenes collection");
    await collections.scenes.compact();

    logger.info("Scenes done");
  }

  {
    logger.info("Checking actors");

    for await (const actor of collections.actors.iterate()) {
      const diff: Partial<Actor> = {};

      const imageProps = ["thumbnail", "avatar", "altThumbnail", "hero"] satisfies (keyof Actor)[];

      for (const prop of imageProps) {
        if (actor[prop]) {
          const image = await collections.images.get(actor[prop]!);

          if (!image) {
            logger.info(`Actor ${actor._id} ${prop} is missing`);
            diff[prop] = null;
          }
        }
      }

      if (Object.keys(diff).length > 0) {
        logger.info(`Update actor ${actor._id}: ${JSON.stringify(diff, null, 2)}`);
        await collections.actors.partialUpdate(actor._id, diff);
      }
    }

    logger.info("Compacting actors collection");
    await collections.actors.compact();

    logger.info("Actors done");
  }

  {
    logger.info("Checking movies");

    for await (const movie of collections.movies.iterate()) {
      const diff: Partial<Movie> = {};

      const imageProps = ["frontCover", "backCover", "spineCover"] satisfies (keyof Movie)[];

      for (const prop of imageProps) {
        if (movie[prop]) {
          const image = await collections.images.get(movie[prop]!);

          if (!image) {
            logger.info(`Movie ${movie._id} ${prop} is missing`);
            diff[prop] = null;
          }
        }
      }

      if (movie.studio) {
        const image = await collections.studios.get(movie.studio);

        if (!image) {
          logger.info(`Movie ${movie._id} studio is missing`);
          diff.studio = null;
        }
      }

      if (Object.keys(diff).length > 0) {
        logger.info(`Update movie ${movie._id}: ${JSON.stringify(diff, null, 2)}`);
        await collections.movies.partialUpdate(movie._id, diff);
      }
    }

    logger.info("Compacting movies collection");
    await collections.movies.compact();

    logger.info("Movies done");
  }

  {
    logger.info("Checking studios");

    for await (const studio of collections.studios.iterate()) {
      const diff: Partial<Studio> = {};

      const imageProps = ["thumbnail"] satisfies (keyof Studio)[];

      for (const prop of imageProps) {
        if (studio[prop]) {
          const image = await collections.images.get(studio[prop]!);

          if (!image) {
            logger.info(`Studio ${studio._id} ${prop} is missing`);
            diff[prop] = null;
          }
        }
      }

      if (studio.parent) {
        const image = await collections.studios.get(studio.parent);

        if (!image) {
          logger.info(`Studio ${studio._id} parent is missing`);
          diff.parent = null;
        }
      }

      if (Object.keys(diff).length > 0) {
        logger.info(`Update studio ${studio._id}: ${JSON.stringify(diff, null, 2)}`);
        await collections.studios.partialUpdate(studio._id, diff);
      }
    }

    logger.info("Compacting studios collection");
    await collections.studios.compact();

    logger.info("Studios done");
  }

  {
    logger.info("Checking markers");

    for await (const marker of collections.markers.iterate()) {
      const scene = await collections.scenes.get(marker.scene);

      if (!scene) {
        logger.info(`Marker ${marker._id} scene is missing, deleting marker`);
        await collections.scenes.remove(marker.scene);
      }
      else {
        const diff: Partial<Marker> = {};

        const imageProps = ["thumbnail"] satisfies (keyof Marker)[];

        for (const prop of imageProps) {
          if (marker[prop]) {
            const image = await collections.images.get(marker[prop]!);

            if (!image) {
              logger.info(`Marker ${marker._id} ${prop} is missing`);
              diff[prop] = null;
            }
          }
        }

        if (Object.keys(diff).length > 0) {
          logger.info(`Update marker ${marker._id}: ${JSON.stringify(diff, null, 2)}`);
          await collections.markers.partialUpdate(marker._id, diff);
        }
      }
    }

    logger.info("Compacting markers collection");
    await collections.markers.compact();

    logger.info("Markers done");
  }

  {
    logger.info("Checking movie scene relations");

    for await (const relation of collections.movieScenes.iterate()) {
      let shouldDelete = false;

      const scene = await collections.scenes.get(relation.scene);
      if (!scene) {
        logger.warn(`Movie scene target scene ${relation.scene} does not exist`);
        shouldDelete = true;
      }

      const movie = await collections.movies.get(relation.movie);
      if (!movie) {
        logger.warn(`Movie scene target movie ${relation.movie} does not exist`);
        shouldDelete = true;
      }

      if (shouldDelete) {
        logger.info(`Deleting unnecessary relation: ${relation._id}`);
        await collections.movieScenes.remove(relation._id);
      }
    }

    logger.info("Compacting movieScenes collection");
    await collections.movieScenes.compact();
  }

  {
    logger.info("Checking label relations");

    for await (const relation of collections.labelledItems.iterate()) {
      const collection = {
        "actor": collections.actors,
        "scene": collections.scenes,
        "marker": collections.markers,
        "studio": collections.studios,
        "image": collections.images,
      }[relation.type];

      let shouldDelete = false;

      const item = await collection.get(relation.item);
      if (!item) {
        logger.warn(`Label target ${relation.type} ${relation.item} does not exist`);
        shouldDelete = true;
      }

      const label = await collections.labels.get(relation.label);
      if (!label) {
        logger.warn(`Label target label ${relation.label} does not exist`);
        shouldDelete = true;
      }

      if (shouldDelete) {
        logger.info(`Deleting unnecessary relation: ${relation._id}`);
        await collections.labelledItems.remove(relation._id);
      }
    }

    logger.info("Compacting labelledItems collection");
    await collections.labelledItems.compact();
  }
}
