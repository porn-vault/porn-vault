import { getImageDimensions } from "./binaries/imagemagick";
import { collections } from "./database";
import { attachScenePreviewGrid } from "./routes/scene";
import Image from "./types/image";
import Marker from "./types/marker";
import Scene from "./types/scene";
import { statAsync } from "./utils/fs/async";
import { handleError, logger } from "./utils/logger";

export async function generateMissingMarkerThumbnails(): Promise<void> {
  for await (const marker of collections.markers.iterate(100)) {
    if (!marker.thumbnail) {
      logger.info(`Generating thumbnail for marker ${marker._id} (scene = ${marker.scene})`);

      try {
        const diff = await Marker.createMarkerThumbnail(marker);
        await collections.markers.partialUpdate(marker._id, diff);
      } catch (error) {
        handleError("Failed to generate marker thumbnail", error, false);
      }
    }
  }
}

export async function generateMissingScenePreviewStrips(): Promise<void> {
  let count = 0;
  let generated = 0;

  for await (const scene of collections.scenes.iterate(100)) {
    if (!scene.preview) {
      logger.info(`[${generated + 1}] Generating preview strip for "${scene.name}" (${scene._id})`);

      try {
        const preview = await Scene.generatePreview(scene);

        if (preview) {
          const previewImg = new Image(`${scene.name} (preview)`);
          const stats = await statAsync(preview);
          previewImg.path = preview;
          previewImg.scene = scene._id;
          previewImg.meta.size = stats.size;

          const dims = await getImageDimensions(previewImg.path);
          previewImg.meta.dimensions.width = dims.width;
          previewImg.meta.dimensions.height = dims.height;

          await collections.images.upsert(previewImg._id, previewImg);
          await collections.scenes.partialUpdate(scene._id, {
            preview: previewImg._id,
          });
        }

        generated += 1;
      } catch (error) {
        handleError(`Failed to generate preview strip for ${scene._id}`, error);
      }
    }

    count += 1;
  }

  logger.info(`Checked ${count} scenes for preview strip generation, generated: ${generated}`);
}

export async function generateMissingScenePreviewGrids(): Promise<void> {
  let count = 0;
  let generated = 0;

  for await (const scene of collections.scenes.iterate(100)) {
    if (!scene.previewGrid) {
      logger.info(`[${generated + 1}] Generating preview grid for "${scene.name}" (${scene._id})`);

      try {
        await attachScenePreviewGrid(scene);
        generated += 1;
      } catch (error) {
        handleError(`Failed to generate preview grid for ${scene._id}`, error);
      }
    }

    count += 1;
  }

  logger.info(`Checked ${count} scenes for preview grid generation, generated: ${generated}`);
}
