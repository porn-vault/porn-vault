import path from "node:path";

import { execaSync } from "execa";

import { IConfig } from "../config/schema";
// import { checkUnusedPlugins, prevalidatePlugins } from "../plugins/validate";
import { logger } from "../utils/logger";

export function validateFFMPEGPaths(config: IConfig): void {
  if (execaSync(config.binaries.ffmpeg, ["-version"]).exitCode > 0) {
    throw new Error(
      `FFMPEG binary not found at "${config.binaries.ffmpeg}" for "config.binaries.ffmpeg"`
    );
  }

  if (execaSync(config.binaries.ffprobe, ["-version"]).exitCode > 0) {
    throw new Error(
      `FFPROBE binary not found at "${config.binaries.ffprobe}" for "config.binaries.ffprobe"`
    );
  }
}

/**
 * Does extra validation on the config.
 * Exits if invalid.
 * Sets the ffmpeg binary paths to the ones in the config
 *
 * @param config - the config the check
 * @throws
 */
export function validateConfigExtra(config: IConfig): void {
  /* prevalidatePlugins(config);
  checkUnusedPlugins(config); */

  logger.info(
    `Registered plugins: ${JSON.stringify(
      Object.keys(config.plugins.register)
    )}`
  );
  logger.debug("Loaded config:");
  logger.debug(config);

  validateFFMPEGPaths(config);
}
