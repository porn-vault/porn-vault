import { platform } from "node:os";

import { DEFAULT_WORD_MATCHER } from "../matching/wordMatcher";
import {
  ApplyActorLabelsEnum,
  ApplyStudioLabelsEnum,
  H264Preset,
  IConfig,
  WebmDeadline,
} from "./schema";

function isWindows(): boolean {
  return platform() === "win32";
}

function exeName(str: string): string {
  return `${str}${isWindows() ? ".exe" : ""}`;
}

const defaultConfig: IConfig = {
  auth: {
    password: null,
  },
  binaries: {
    ffmpeg: exeName("ffmpeg"),
    ffprobe: exeName("ffprobe"),
    izzyPort: 8000,
    imagemagick: {
      convertPath: "convert",
      identifyPath: "identify",
      montagePath: "montage",
    },
  },
  import: {
    images: [],
    videos: [],
    scanInterval: 10800000,
  },
  log: {
    level: "info",
    maxSize: "20m",
    maxFiles: "5",
    writeFile: [
      {
        level: "error",
        prefix: "errors-",
        silent: false,
      },
      {
        level: "silly",
        prefix: "full-",
        silent: true,
      },
    ],
  },
  matching: {
    applyActorLabels: [
      ApplyActorLabelsEnum.enum["event:actor:create"],
      ApplyActorLabelsEnum.enum["event:actor:find-unmatched-scenes"],
      ApplyActorLabelsEnum.enum["plugin:actor:create"],
      ApplyActorLabelsEnum.enum["event:scene:create"],
      ApplyActorLabelsEnum.enum["plugin:scene:create"],
      ApplyActorLabelsEnum.enum["event:image:create"],
    ],
    applySceneLabels: true,
    applyStudioLabels: [
      ApplyStudioLabelsEnum.enum["event:studio:create"],
      ApplyStudioLabelsEnum.enum["event:studio:find-unmatched-scenes"],
      ApplyStudioLabelsEnum.enum["plugin:studio:create"],
      ApplyStudioLabelsEnum.enum["event:scene:create"],
      ApplyStudioLabelsEnum.enum["plugin:scene:create"],
    ],
    extractSceneActorsFromFilepath: true,
    extractSceneLabelsFromFilepath: true,
    extractSceneMoviesFromFilepath: true,
    extractSceneStudiosFromFilepath: true,
    matcher: DEFAULT_WORD_MATCHER,
    matchCreatedActors: true,
    matchCreatedStudios: true,
    matchCreatedLabels: true,
  },
  persistence: {
    backup: {
      enable: true,
      maxAmount: 10,
    },
    libraryPath: process.cwd(),
  },
  plugins: {
    allowActorThumbnailOverwrite: false,
    allowMovieThumbnailOverwrite: false,
    allowSceneThumbnailOverwrite: false,
    allowStudioThumbnailOverwrite: false,
    createMissingActors: false,
    createMissingLabels: false,
    createMissingMovies: false,
    createMissingStudios: false,
    events: {
      actorCreated: [],
      actorCustom: [],
      sceneCreated: [],
      sceneCustom: [],
      movieCustom: [],
      studioCreated: [],
      studioCustom: [],
    },
    register: {},
    markerDeduplicationThreshold: 5,
  },
  processing: {
    generatePreviews: true,
    readImagesOnImport: false,
    generateImageThumbnails: true,
  },
  server: {
    https: {
      certificate: "",
      enable: false,
      key: "",
    }
  },
  transcode: {
    hwaDriver: null,
    vaapiDevice: null,
    h264: {
      preset: H264Preset.enum.veryfast,
      crf: 23,
    },
    webm: {
      deadline: WebmDeadline.enum.realtime,
      cpuUsed: 5,
      crf: 31,
    },
  },
};

export default defaultConfig;
