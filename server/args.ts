import yargs from "yargs";
import { hideBin } from "yargs/helpers";

import { version } from "./version";

const argv = yargs(hideBin(process.argv))
  .version(version)
  .option("generate-missing-marker-thumbnails", {
    type: "boolean",
    description: "Generates missing marker thumbnails",
    default: false,
  })
  .option("generate-missing-scene-preview-strips", {
    type: "boolean",
    description: "Generates missing scene preview strips",
    default: false,
  })
  .option("generate-missing-scene-preview-grids", {
    type: "boolean",
    description: "Generates missing scene preview grids",
    default: false,
  })
  .option("repair-integrity", {
    type: "boolean",
    description: "Repair database integrity as much as possible",
    default: false,
  })
  .option("skip-compaction", {
    type: "boolean",
    description: "Skip database file compaction (decreases startup time)",
    default: false,
  })
  .option("update-izzy", {
    type: "boolean",
    description: "Remove database binary and download latest version",
    default: false,
  })
  .option("reindex", {
    type: "boolean",
    description: "Delete search indices and rebuild (non-destructive)",
    default: false,
  })
  .option("reload-izzy", {
    type: "boolean",
    description: "Reload database from files (non-destructive)",
    default: false,
  })
  .option("migrate", {
    alias: ["migration"],
    type: "boolean",
    description: `Perform migrations (create a backup of library/ before!)`,
    default: false,
  })
  .parse();

export default argv as Awaited<typeof argv>;
