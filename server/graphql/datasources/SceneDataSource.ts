import DataLoader from "dataloader";

import Actor from "../../types/actor";
import Scene from "../../types/scene";
import SceneView from "../../types/watch";
import { BatchActorLoader, BatchImageLoader, BatchLabelLoader } from "./loaders";

export const batchSceneViewsLoader = new DataLoader(async (sceneIds: readonly string[]) => {
  if (sceneIds.length === 0) {
    return [];
  }

  const views = await SceneView.getByScenesBulk(sceneIds);

  return sceneIds.map((sceneId) => {
    const view = views[sceneId];

    if (!view) {
      return [];
    }

    return view;
  });
});

export class SceneDataSource {
  async getActorsForScene(scene: Scene): Promise<Actor[]> {
    return BatchActorLoader.load(scene._id);
  }

  async getLabelsForScene(scene: Scene) {
    return BatchLabelLoader.load(scene._id);
  }

  async getThumbnailForScene(scene: Scene) {
    if (!scene.thumbnail) {
      return;
    }
    return BatchImageLoader.load(scene.thumbnail);
  }

  async getWatchesForScene(sceneId: string) {
    return batchSceneViewsLoader.load(sceneId);
  }

  clearAll() {
    batchSceneViewsLoader.clearAll();
  }
}
