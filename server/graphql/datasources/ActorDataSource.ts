import Actor from "../../types/actor";
import Image from "../../types/image";
import Label from "../../types/label";
import { BatchImageLoader, BatchLabelLoader } from "./loaders";

export class ActorDataSource {
  async getLabelsForActor(actor: Actor): Promise<Label[]> {
    return BatchLabelLoader.load(actor._id);
  }

  async getAvatarForActor(actor: Actor): Promise<Image | null> {
    if (!actor.avatar) {
      return null;
    }
    return BatchImageLoader.load(actor.avatar);
  }

  async getThumbnailForActor(actor: Actor) {
    if (!actor.thumbnail) {
      return null;
    }
    return BatchImageLoader.load(actor.thumbnail);
  }

  async getAltThumbnailForActor(actor: Actor) {
    if (!actor.altThumbnail) {
      return null;
    }
    return BatchImageLoader.load(actor.altThumbnail);
  }

  clearAll() {}
}
