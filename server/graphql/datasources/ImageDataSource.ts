import DataLoader from "dataloader";

import Actor from "../../types/actor";
import Image from "../../types/image";
import Scene from "../../types/scene";
import { BatchActorLoader, BatchLabelLoader } from "./loaders";

/* TODO: check if this is right */
const batchScenesLoader = new DataLoader(async (sceneIds: readonly string[]) => {
  const scenes = (await Scene.getBulk(sceneIds)).reduce((acc, current) => {
    return {
      ...acc,
      [current._id]: current,
    };
  }, {}) as { [sceneId: string]: Scene };

  return sceneIds.map((imageId) => {
    if (!scenes[imageId]) {
      return null;
    }

    return scenes[imageId];
  });
});

export class ImageDataSource {
  async getActorsForImage(image: Image): Promise<Actor[]> {
    return BatchActorLoader.load(image._id);
  }

  async getLabelsForImage(image: Image) {
    return BatchLabelLoader.load(image._id);
  }

  async getSceneForImage(sceneId: string) {
    return batchScenesLoader.load(sceneId);
  }

  clearAll() {
    batchScenesLoader.clearAll();
  }
}
