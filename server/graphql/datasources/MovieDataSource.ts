import DataLoader from "dataloader";

import Actor from "../../types/actor";
import ActorReference from "../../types/actor_reference";
import Image from "../../types/image";
import Label from "../../types/label";
import LabelledItem from "../../types/labelled_item";
import MovieScene from "../../types/movie_scene";
import Scene from "../../types/scene";
import { BatchImageLoader } from "./loaders";

const batchScenesLoader = new DataLoader(async (movieIds: readonly string[]) => {
  const movieScenes = await MovieScene.getByMovies(movieIds);
  const allSceneIds = Object.values(movieScenes)
    .flatMap((movieScenes) => movieScenes)
    .map((movieScene) => movieScene.scene);

  const allScenes = await Scene.getBulk([...new Set(allSceneIds)]);

  return movieIds.map((movieId) => {
    const scenesForThisMovie = movieScenes[movieId];

    if (!scenesForThisMovie) {
      return [];
    }

    const sceneIdsInThisMovie = Object.values(scenesForThisMovie)
      .flatMap((movieScenes) => movieScenes)
      .map((movieScene) => movieScene.scene);

    return allScenes.filter((scene) =>
      sceneIdsInThisMovie.some((sceneId) => sceneId === scene._id)
    );
  });
});

const batchActorsLoader = new DataLoader(async (movieIds: readonly string[]) => {
  // first get all scenes for the movies
  const movieScenes = await MovieScene.getByMovies(movieIds);
  const sceneIds = Object.values(movieScenes)
    .flatMap((scenes) => scenes)
    .map(({ scene }) => scene);

  const allRefs = await ActorReference.getByItemBulk([...new Set(sceneIds)]);

  // now get all actors in all scenes of all movies
  const allActorIds = Object.values(allRefs)
    .flatMap((refs) => refs)
    .map((actorRef) => actorRef.actor);

  const allActors = await Actor.getBulk([...new Set(allActorIds)]);

  // map each movieId to an array of Actors
  return movieIds.map((movieId) => {
    if (!movieScenes[movieId]) {
      return [];
    }

    const scenes = movieScenes[movieId];

    // find all actorReferences for this particular movie
    const actorRefsForAllScenesInMovie: Record<string, ActorReference[]> = Object.keys(allRefs)
      .filter((sceneId) => scenes.some(({ scene }) => scene === sceneId))
      .reduce((obj, key) => {
        return Object.assign(obj, { [key]: allRefs[key] });
      }, {});

    // map to a set of actorIds contained in this movie
    const allActorIdsInThisMovie = Object.values(actorRefsForAllScenesInMovie)
      .flatMap((actors) => actors)
      .map((ref) => ref.actor);

    // now filter all actors which are in this particular movie
    return allActors.filter((actor) =>
      allActorIdsInThisMovie.some((actorId) => actorId === actor._id)
    );
  });
});

const batchLabelsLoader = new DataLoader(async (movieIds: readonly string[]) => {
  // first get all scenes for the movies
  const movieScenes = await MovieScene.getByMovies(movieIds);
  const sceneIds = Object.values(movieScenes)
    .flatMap((scenes) => scenes)
    .map(({ scene }) => scene);

  const allRefs = await LabelledItem.getByItemBulk([...new Set(sceneIds)]);

  // now get all actors in all scenes of all movies
  const allLabelIds = Object.values(allRefs)
    .flatMap((refs) => refs)
    .map((actorRef) => actorRef.label);

  const allLabels = await Label.getBulk([...new Set(allLabelIds)]);

  // map each movieId to an array of Actors
  return movieIds.map((movieId) => {
    if (!movieScenes[movieId]) {
      return [];
    }

    const scenes = movieScenes[movieId];

    // find all labelReferences for this particular movie
    const labelRefsForAllScenesInMovie: Record<string, LabelledItem[]> = Object.keys(allRefs)
      .filter((sceneId) => scenes.some(({ scene }) => scene === sceneId))
      .reduce((obj, key) => {
        return Object.assign(obj, { [key]: allRefs[key] });
      }, {});

    // map to a set of labelIds contained in this movie
    const allLabelIdsInThisMovie = Object.values(labelRefsForAllScenesInMovie)
      .flatMap((labels) => labels)
      .map((ref) => ref.label);

    // now filter all labels which are in this particular movie
    return allLabels.filter((label) =>
      allLabelIdsInThisMovie.some((labelId) => labelId === label._id)
    );
  });
});

export class MovieDataSource {
  async getActorsForMovie(movieId: string): Promise<Actor[]> {
    return batchActorsLoader.load(movieId);
  }

  async getScenesForMovie(movieId: string): Promise<Scene[]> {
    return batchScenesLoader.load(movieId);
  }

  async getCoverForMovie(imageId: string): Promise<Image> {
    return BatchImageLoader.load(imageId);
  }

  async getLabelsForMovie(movieId: string): Promise<Label[]> {
    return batchLabelsLoader.load(movieId);
  }

  clearAll() {
    batchActorsLoader.clearAll();
    batchScenesLoader.clearAll();
  }
}
