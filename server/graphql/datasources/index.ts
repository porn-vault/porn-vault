import { ActorDataSource } from "./ActorDataSource";
import { ImageDataSource } from "./ImageDataSource";
import { BatchActorLoader, BatchImageLoader, BatchLabelLoader } from "./loaders";
import { MarkerDataSource } from "./MarkerDataSource";
import { MovieDataSource } from "./MovieDataSource";
import { SceneDataSource } from "./SceneDataSource";
import { StudioDataSource } from "./StudioDataSource";

const sceneDataSource = new SceneDataSource();
const movieDataSource = new MovieDataSource();
const actorDataSource = new ActorDataSource();
const imageDataSource = new ImageDataSource();
const studioDataSource = new StudioDataSource();
const markerDataSource = new MarkerDataSource();

export function getSceneDataSource(): SceneDataSource {
  return sceneDataSource;
}

export function getMovieDataSource(): MovieDataSource {
  return movieDataSource;
}

export function getActorDataSource(): ActorDataSource {
  return actorDataSource;
}

export function getImageDataSource(): ImageDataSource {
  return imageDataSource;
}

export function getMarkerDataSource(): MarkerDataSource {
  return markerDataSource;
}

export function getStudioDataSource(): StudioDataSource {
  return studioDataSource;
}

export function clearCaches(): void {
  for (const cache of [
    sceneDataSource,
    movieDataSource,
    actorDataSource,
    imageDataSource,
    studioDataSource,
    markerDataSource,
    BatchImageLoader,
    BatchLabelLoader,
    BatchActorLoader,
  ]) {
    cache.clearAll();
  }
}
