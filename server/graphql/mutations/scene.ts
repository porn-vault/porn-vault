import { getConfig } from "../../config";
import { ApplyActorLabelsEnum, ApplyStudioLabelsEnum } from "../../config/schema";
import { collections } from "../../database";
import { extractActors, extractLabels } from "../../extractor";
import { onSceneCreate } from "../../plugins/events/scene";
import { removeSceneFromQueue } from "../../queue/processing";
import { indexImages } from "../../search/image";
import { indexScenes, removeScene } from "../../search/scene";
import { indexes } from "../../search_new";
import Actor from "../../types/actor";
import ActorReference from "../../types/actor_reference";
import Image from "../../types/image";
import Label from "../../types/label";
import LabelledItem from "../../types/labelled_item";
import Marker from "../../types/marker";
import MovieScene from "../../types/movie_scene";
import Scene from "../../types/scene";
import Studio from "../../types/studio";
import { mapAsync } from "../../utils/async";
import { formatMessage, handleError, logger } from "../../utils/logger";
import { normalizeDescription, normalizeName } from "../../utils/string";
import { Dictionary, isBoolean, isNumber, isString } from "../../utils/types";
import { clearCaches } from "../datasources";
import { batchSceneViewsLoader } from "../datasources/SceneDataSource";

type ISceneUpdateOpts = Partial<{
  favorite: boolean;
  bookmark: number;
  actors: string[];
  name: string;
  description: string;
  rating: number;
  labels: string[];
  streamLinks: string[];
  thumbnail: string;
  releaseDate: number;
  studio: string | null;
  customFields: Dictionary<string[] | boolean | string | null>;
  path: string;
}>;

async function runScenePlugins(id: string): Promise<Scene | null> {
  let scene = await Scene.getById(id);

  if (!scene) {
    throw new Error("Scene not found");
  }

  const labels = (await Scene.getLabels(scene)).map((l) => l._id);
  const actors = (await Scene.getActors(scene)).map((a) => a._id);

  const result = await onSceneCreate(scene, labels, actors, "sceneCustom");
  scene = result.scene;

  await Scene.setLabels(scene, labels);
  await Scene.setActors(scene, actors);
  await collections.scenes.upsert(scene._id, scene);
  await indexScenes([scene]);

  await result.commit();

  clearCaches();

  return scene;
}

export default {
  async runScenePlugins(_: unknown, { id }: { id: string }): Promise<Scene> {
    logger.debug(`Mutation: runScenePlugins, for scene ${id}`);
    const result = await runScenePlugins(id);
    if (!result) {
      throw new Error("Scene not found");
    }
    return result;
  },

  async screenshotScene(
    _: unknown,
    { id, sec }: { id: string; sec: number }
  ): Promise<Image | null> {
    const scene = await Scene.getById(id);

    if (scene) {
      const image = await Scene.setThumbnailToSeconds(scene, sec);
      return image;
    }
    return null;
  },

  async unwatchScene(_: unknown, { id }: { id: string }): Promise<Scene | null> {
    const scene = await Scene.getById(id);

    if (scene) {
      await Scene.unwatch(scene);
      batchSceneViewsLoader.clearAll();
      return scene;
    }

    return null;
  },

  async watchScene(_: unknown, { id }: { id: string }): Promise<Scene | null> {
    const scene = await Scene.getById(id);

    if (scene) {
      await Scene.watch(scene);
      batchSceneViewsLoader.clearAll();
      return scene;
    }

    return null;
  },

  async addScene(
    _: unknown,
    args: { actors: string[]; labels: string[]; name: string }
  ): Promise<Scene> {
    for (const actor of args.actors || []) {
      const actorInDb = await Actor.getById(actor);
      if (!actorInDb) {
        throw new Error(`Actor ${actor} not found`);
      }
    }

    for (const label of args.labels || []) {
      const labelInDb = await Label.getById(label);
      if (!labelInDb) {
        throw new Error(`Label ${label} not found`);
      }
    }

    const config = getConfig();

    const sceneName = args.name;
    const scene = new Scene(sceneName);

    let actorIds = [] as string[];
    if (args.actors) {
      actorIds = args.actors;
    }

    // Extract actors
    const extractedActors = await extractActors(scene.name);
    logger.verbose(`Found ${extractedActors.length} actors in scene title.`);
    actorIds.push(...extractedActors);
    await Scene.setActors(scene, actorIds);

    let labels = [] as string[];
    if (args.labels) {
      labels = args.labels;
    }

    // Extract labels
    const extractedLabels = await extractLabels(scene.name);
    logger.verbose(`Found ${extractedLabels.length} labels in scene title.`);
    labels.push(...extractedLabels);

    if (
      config.matching.applyActorLabels.includes(ApplyActorLabelsEnum.enum["event:scene:create"])
    ) {
      logger.verbose("Applying actor labels to scene");
      const actors = await Actor.getBulk(actorIds);
      const actorLabels = (
        await mapAsync(actors, async (actor) => (await Actor.getLabels(actor)).map((l) => l._id))
      ).flat();
      labels.push(...actorLabels);
    }

    await Scene.setLabels(scene, labels);
    await collections.scenes.upsert(scene._id, scene);
    logger.verbose(`Scene '${sceneName}' done.`);

    return scene;
  },

  async updateScenes(
    _: unknown,
    { ids, opts }: { ids: string[]; opts: ISceneUpdateOpts }
  ): Promise<Scene[]> {
    const config = getConfig();
    const updatedScenes: Scene[] = [];

    for (const id of ids) {
      logger.silly(`Updating scene ${id}`);
      const scene = await Scene.getById(id);

      if (!scene) {
        continue;
      }

      const diff: Partial<Scene> = {};

      const labelsToApply: string[] = [];

      if (isString(opts.name)) {
        diff.name = normalizeName(opts.name);
      }

      if (isString(opts.description)) {
        diff.description = normalizeDescription(opts.description);
      }

      if (isString(opts.thumbnail)) {
        diff.thumbnail = opts.thumbnail;
      }
      if (opts.thumbnail === null) {
        diff.thumbnail = null;
      }

      if (isString(opts.path)) {
        const diff2 = await Scene.changePath(scene, opts.path);
        Object.assign(diff, diff2);
      }

      if (opts.studio !== undefined) {
        diff.studio = opts.studio;

        if (opts.studio) {
          const studio = await Studio.getById(opts.studio);

          if (studio) {
            if (
              config.matching.applyStudioLabels.includes(
                ApplyStudioLabelsEnum.enum["event:scene:update"]
              )
            ) {
              const studioLabels = (await Studio.getLabels(studio)).map((l) => l._id);
              logger.verbose("Applying studio labels to scene");
              labelsToApply.push(...studioLabels);
            }
          }
        }
      }

      if (Array.isArray(opts.labels)) {
        // If the update sets labels, use those and ignore the existing
        labelsToApply.push(...opts.labels);
      } else {
        const existingLabels = (await Scene.getLabels(scene)).map((l) => l._id);
        labelsToApply.push(...existingLabels);
      }

      if (Array.isArray(opts.actors)) {
        const actorIds = [...new Set(opts.actors)];
        await Scene.setActors(scene, actorIds);

        if (
          config.matching.applyActorLabels.includes(ApplyActorLabelsEnum.enum["event:scene:update"])
        ) {
          const actors = await Actor.getBulk(actorIds);
          const actorLabelIds = (await mapAsync(actors, Actor.getLabels)).flat().map((l) => l._id);

          logger.verbose("Applying actor labels to scene");
          labelsToApply.push(...actorLabelIds);
        }
      }
      const uniqueLabels = [...new Set(labelsToApply)];
      await Scene.setLabels(scene, uniqueLabels);

      if (Array.isArray(opts.streamLinks)) {
        diff.streamLinks = [...new Set(opts.streamLinks)];
      }

      if (isNumber(opts.bookmark) || opts.bookmark === null) {
        diff.bookmark = opts.bookmark;
      }

      if (isBoolean(opts.favorite)) {
        diff.favorite = opts.favorite;
      }

      if (isNumber(opts.rating)) {
        diff.rating = opts.rating;
      }

      if (opts.releaseDate !== undefined) {
        diff.releaseDate = opts.releaseDate;
      }

      if (opts.customFields) {
        for (const key in opts.customFields) {
          const value = opts.customFields[key] !== undefined ? opts.customFields[key] : null;
          logger.debug(`Set scene custom.${key} to ${JSON.stringify(value)}`);
          opts.customFields[key] = value;
        }
        diff.customFields = opts.customFields;
      }

      const updatedScene = await collections.scenes.partialUpdate(scene._id, diff);
      updatedScenes.push(updatedScene);
    }

    clearCaches();
    await indexScenes(updatedScenes);

    return updatedScenes;
  },

  async removeScenes(
    _: unknown,
    { ids, deleteImages }: { ids: string[]; deleteImages?: boolean }
  ): Promise<boolean> {
    for (const id of ids) {
      const scene = await Scene.getById(id);

      if (scene) {
        await Scene.remove(scene);
        await removeScene(scene._id);

        if (deleteImages) {
          const cache: string[] = [];

          await Image.iterateByScene(scene._id, async (image) => {
            await Image.remove(image);
            await LabelledItem.removeByItem(image._id);
            await ActorReference.removeByItem(image._id);
            cache.push(image._id);
          });

          await indexes.images.bulkRemove(cache);
          logger.verbose(`Deleted images of scene ${scene._id}`);
        } else {
          await Image.iterateByScene(scene._id, async (image) => {
            image.scene = null;
            await collections.images.upsert(image._id, image);
            await indexImages([image]);
          });
          logger.verbose(`Removed scene ${scene._id} from images`);
        }

        await Marker.removeByScene(scene._id);

        logger.info(`Deleted scene ${scene._id}`);

        await LabelledItem.removeByItem(scene._id);
        await ActorReference.removeByItem(scene._id);
        await MovieScene.removeByScene(scene._id);

        try {
          logger.debug("Deleting scene from queue (if needed)");
          await removeSceneFromQueue(scene._id);
        } catch (err) {
          handleError(
            `Could not delete scene ${scene._id} from queue (ignore if 404, just means that the deleted scene wasn't going to be processed)`,
            err
          );
        }
      }
    }

    clearCaches();

    return true;
  },

  async runFFProbe(
    _: unknown,
    { id }: { id: string }
  ): Promise<null | { scene: Scene; ffprobe: unknown }> {
    const scene = await Scene.getById(id);
    if (!scene?.path) {
      return null;
    }

    try {
      logger.verbose(`Extracting video metadata of ${scene._id}`);

      const { raw, meta } = await Scene.getVideoFileMetadata(scene.path);

      await collections.scenes.partialUpdate(scene._id, { meta });
      logger.silly(`Scene ${scene._id} metadata is now ${formatMessage(meta)}`);

      return {
        ffprobe: raw,
        scene: {
          ...scene,
          meta,
        },
      };
    } catch (err) {
      handleError(`Could not extract metadata of ${scene._id}`, err);
      return null;
    }
  },
};
