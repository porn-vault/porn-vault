import { GraphQLError } from "graphql";

import { getConfig } from "../../config";
import { ApplyActorLabelsEnum } from "../../config/schema";
import { collections } from "../../database";
import { onActorCreate } from "../../plugins/events/actor";
import { indexActors, removeActors } from "../../search/actor";
import Actor from "../../types/actor";
import ActorReference from "../../types/actor_reference";
import { isValidCountryCode } from "../../types/countries";
import LabelledItem from "../../types/labelled_item";
import { logger } from "../../utils/logger";
import { filterInvalidExternalUrls, isArrayEq } from "../../utils/misc";
import { normalizeAliases, normalizeDescription, normalizeName } from "../../utils/string";
import { Dictionary, isBoolean, isNumber, isString } from "../../utils/types";
import { clearCaches } from "../datasources";

type IActorUpdateOpts = Partial<{
  name: string;
  description: string;
  rating: number;
  labels: string[];
  aliases: string[];
  avatar: string;
  thumbnail: string;
  altThumbnail: string;
  hero: string;
  favorite: boolean;
  bookmark: number | null;
  bornOn: number;
  customFields: Dictionary<string[] | boolean | string | null>;
  nationality: string | null;
  externalLinks: { url: string; text: string }[];
}>;

async function runActorPlugins(id: string): Promise<Actor | null> {
  const actor = await Actor.getById(id);

  if (!actor) {
    throw new Error("Actor not found");
  }

  logger.info(`Running plugin action event for '${actor.name}'`);

  const labels = (await Actor.getLabels(actor)).map((l) => l._id);

  const pluginResult = await onActorCreate(actor, labels, "actorCustom");

  await Actor.setLabels(actor, labels);
  const updatedActor = await collections.actors.partialUpdate(actor._id, pluginResult.diff);
  await indexActors([actor]);

  await pluginResult.commit();
  clearCaches();

  return updatedActor;
}

export default {
  async runActorPlugins(_: unknown, { id }: { id: string }): Promise<Actor> {
    const result = await runActorPlugins(id);
    if (!result) {
      throw new Error("Actor not found");
    }
    return result;
  },

  async addActor(
    _: unknown,
    args: {
      name: string;
      aliases?: string[];
      labels?: string[];
      nationality: string | null;
    }
  ): Promise<Actor> {
    if (args.name.trim().length === 0) {
      throw new GraphQLError("An actor name must not be empty", {
        extensions: {
          code: "BAD_USER_INPUT",
        },
      });
    }

    const config = getConfig();
    const aliases = normalizeAliases(args.aliases ?? []);

    const actor = new Actor(args.name, aliases);

    if (isString(args.nationality) && isValidCountryCode(args.nationality)) {
      actor.nationality = args.nationality;
    }

    let actorLabels = [] as string[];
    if (args.labels) {
      actorLabels = args.labels;
    }

    const pluginResult = await onActorCreate(actor, actorLabels);
    Object.assign(actor, pluginResult.diff);

    await collections.actors.upsert(actor._id, actor);
    await Actor.setLabels(actor, actorLabels);

    if (config.matching.matchCreatedActors) {
      await Actor.findUnmatchedScenes(
        actor,
        config.matching.applyActorLabels.includes(ApplyActorLabelsEnum.enum["event:actor:create"])
          ? actorLabels
          : []
      );
    }

    await indexActors([actor]);
    await pluginResult.commit();
    clearCaches();

    return actor;
  },

  async updateActors(
    _: unknown,
    { ids, opts }: { ids: string[]; opts: IActorUpdateOpts }
  ): Promise<Actor[]> {
    const config = getConfig();
    const updatedActors = [] as Actor[];

    let didLabelsChange = false;

    for (const id of ids) {
      const actor = await Actor.getById(id);

      if (actor) {
        const diff: Partial<Actor> = {};

        if (isString(opts.name)) {
          diff.name = normalizeName(opts.name);
        }

        if (isString(opts.description)) {
          diff.description = normalizeDescription(opts.description);
        }

        if (Array.isArray(opts.aliases)) {
          diff.aliases = normalizeAliases(opts.aliases);
        }

        if (Array.isArray(opts.externalLinks)) {
          diff.externalLinks = [...new Set(filterInvalidExternalUrls(opts.externalLinks))];
        }

        if (Array.isArray(opts.labels)) {
          const oldLabels = await Actor.getLabels(actor);
          await Actor.setLabels(actor, opts.labels);

          if (
            !isArrayEq(
              oldLabels,
              opts.labels,
              (l) => l._id,
              (l) => l
            )
          ) {
            didLabelsChange = true;
          }
        }

        if (typeof opts.nationality !== undefined) {
          if (isString(opts.nationality) && isValidCountryCode(opts.nationality)) {
            diff.nationality = opts.nationality;
          } else if (opts.nationality === null) {
            diff.nationality = opts.nationality;
          }
        }

        if (isNumber(opts.bookmark) || opts.bookmark === null) {
          diff.bookmark = opts.bookmark;
        }

        if (isBoolean(opts.favorite)) {
          diff.favorite = opts.favorite;
        }

        if (isString(opts.avatar) || opts.avatar === null) {
          diff.avatar = opts.avatar;
        }

        if (isString(opts.thumbnail) || opts.thumbnail === null) {
          diff.thumbnail = opts.thumbnail;
        }

        if (isString(opts.altThumbnail) || opts.altThumbnail === null) {
          diff.altThumbnail = opts.altThumbnail;
        }

        if (isString(opts.hero) || opts.hero === null) {
          diff.hero = opts.hero;
        }

        if (isNumber(opts.rating)) {
          diff.rating = opts.rating;
        }

        if (opts.bornOn !== undefined) {
          diff.bornOn = opts.bornOn;
        }

        if (opts.customFields) {
          for (const key in opts.customFields) {
            const value = opts.customFields[key] !== undefined ? opts.customFields[key] : null;
            logger.debug(`Set actor custom.${key} to ${JSON.stringify(value)}`);
            opts.customFields[key] = value;
          }
          diff.customFields = opts.customFields;
        }

        const updatedActor = await collections.actors.partialUpdate(actor._id, diff);

        updatedActors.push(updatedActor);
      } else {
        throw new Error(`Actor ${id} not found`);
      }

      if (didLabelsChange) {
        const labelsToPush = config.matching.applyActorLabels.includes(
          ApplyActorLabelsEnum.enum["event:actor:update"]
        )
          ? (await Actor.getLabels(actor)).map((l) => l._id)
          : [];

        await Actor.pushLabelsToCurrentScenes(actor, labelsToPush).catch((err) => {
          logger.error(`Error while pushing actor "${actor.name}"'s labels to scenes`);
          logger.error(err);
        });
      }
    }

    clearCaches();
    await indexActors(updatedActors);

    return updatedActors;
  },

  async removeActors(_: unknown, { ids }: { ids: string[] }): Promise<boolean> {
    for (const id of ids) {
      const actor = await Actor.getById(id);

      if (actor) {
        await Actor.remove(actor);
        await removeActors([actor._id]);
        await LabelledItem.removeByItem(actor._id);
        await ActorReference.removeByActor(actor._id);
      }
    }

    clearCaches();

    return true;
  },

  async attachActorToUnmatchedScenes(_: unknown, { id }: { id: string }): Promise<Actor | null> {
    const config = getConfig();

    const actor = await Actor.getById(id);
    if (!actor) {
      logger.error(`Did not find actor for id "${id}" to attach to unmatched scenes`);
      return null;
    }

    try {
      const labelsToPush = config.matching.applyActorLabels.includes(
        ApplyActorLabelsEnum.enum["event:actor:find-unmatched-scenes"]
      )
        ? (await Actor.getLabels(actor)).map((l) => l._id)
        : [];

      await Actor.findUnmatchedScenes(actor, labelsToPush);
    } catch (err) {
      logger.error(`Error attaching "${actor.name}" to unmatched scenes`);
      logger.error(err);
      return null;
    }

    clearCaches();

    return actor;
  },
};
