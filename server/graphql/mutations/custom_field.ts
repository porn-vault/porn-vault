import { collections } from "../../database";
import CustomField, { CustomFieldTarget, CustomFieldType } from "../../types/custom_field";

export default {
  async updateCustomField(
    _: unknown,
    {
      id,
      input: { name, values, unit },
    }: {
      id: string;
      input: {
        name?: string | null;
        values?: string[] | null;
        unit?: string | null;
      };
    }
  ): Promise<CustomField> {
    const field = await CustomField.getById(id);

    if (field) {
      const diff: Partial<CustomField> = {};

      if (name) {
        diff.name = name;
      }

      if (values && field.type.includes("SELECT")) {
        diff.values = values;
      }

      if (field.type !== CustomFieldType.BOOLEAN && unit) {
        diff.unit = unit || null;
      }

      const updatedField = await collections.customFields.partialUpdate(field._id, diff);

      return updatedField;
    } else {
      throw new Error("Custom field not found");
    }
  },

  async removeCustomField(_: unknown, { id }: { id: string }): Promise<boolean> {
    await CustomField.remove(id);
    return true;
  },

  async createCustomField(
    _: unknown,
    {
      input: { name, target, type, values, unit },
    }: {
      input: {
        name: string;
        target: CustomFieldTarget[];
        type: CustomFieldType;
        values?: string[] | null;
        unit: string | null;
      };
    }
  ): Promise<CustomField> {
    if (!name.length) {
      throw new Error("Invalid name");
    }

    const field = new CustomField(name, target, type);

    if (type !== CustomFieldType.BOOLEAN && unit) {
      field.unit = unit;
    }

    if (type === CustomFieldType.SINGLE_SELECT || type === CustomFieldType.MULTI_SELECT) {
      if (values) {
        field.values = values;
      } else {
        throw new Error("Values have to be defined for select fields");
      }
    }

    await collections.customFields.upsert(field._id, field);

    return field;
  },
};
