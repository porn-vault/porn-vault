import { GraphQLError } from "graphql";

import { getConfig } from "../../config";
import { collections } from "../../database";
import { buildExtractor } from "../../extractor";
import { indexActors } from "../../search/actor";
import { indexImages } from "../../search/image";
import { indexScenes } from "../../search/scene";
import { indexStudios } from "../../search/studio";
import Actor from "../../types/actor";
import Image from "../../types/image";
import Label from "../../types/label";
import LabelledItem from "../../types/labelled_item";
import Scene from "../../types/scene";
import Studio from "../../types/studio";
import { formatMessage, logger } from "../../utils/logger";
import { isHexColor, normalizeAliases, normalizeName } from "../../utils/string";
import { BatchLabelLoader } from "../datasources/loaders";

type ILabelUpdateOpts = Partial<{
  name: string;
  aliases: string[];
  thumbnail: string;
  color: string;
}>;

export default {
  async attachLabels(
    _: unknown,
    { item, labels }: { item: string; labels: string[] }
  ): Promise<true> {
    if (item.startsWith("sc_")) {
      const scene = await Scene.getById(item);
      if (scene) {
        await Scene.addLabels(scene, labels);
        await indexScenes([scene]);
      }
    } else if (item.startsWith("im_")) {
      const image = await Image.getById(item);
      if (image) {
        await Image.addLabels(image, labels);
        await indexImages([image]);
      }
    } else if (item.startsWith("st_")) {
      const studio = await Studio.getById(item);
      if (studio) {
        await Studio.addLabels(studio, labels);
        await indexStudios([studio]);
      }
    } else if (item.startsWith("ac_")) {
      const actor = await Actor.getById(item);
      if (actor) {
        await Actor.addLabels(actor, labels);
        await indexActors([actor]);
      }
    }

    BatchLabelLoader.clearAll();

    return true;
  },

  async detachLabel(
    _: unknown,
    { items, label }: { items: string[]; label: string }
  ): Promise<true> {
    const itemsSet = [...new Set(items)];

    await Promise.all(itemsSet.map((item) => LabelledItem.remove(item, label)));

    const sceneIds = itemsSet.filter((x) => x.startsWith("sc_"));
    const actorIds = itemsSet.filter((x) => x.startsWith("ac_"));
    const studioIds = itemsSet.filter((x) => x.startsWith("st_"));
    const imageIds = itemsSet.filter((x) => x.startsWith("im_"));

    const scenes = await Scene.getBulk(sceneIds);
    const actors = await Actor.getBulk(actorIds);
    const studios = await Studio.getBulk(studioIds);
    const images = await Image.getBulk(imageIds);

    await Promise.all([
      indexScenes(scenes),
      indexActors(actors),
      indexStudios(studios),
      indexImages(images),
    ]);

    BatchLabelLoader.clearAll();

    return true;
  },

  async removeLabels(_: unknown, { ids }: { ids: string[] }): Promise<boolean> {
    for (const id of ids) {
      const label = await Label.getById(id);

      if (label) {
        await Label.remove(label._id);
        await LabelledItem.removeByLabel(id);
      }
    }

    BatchLabelLoader.clearAll();

    return true;
  },

  async addLabel(
    _: unknown,
    args: { name: string; aliases?: string[]; color?: string }
  ): Promise<Label> {
    if (args.name.length === 0) {
      throw new GraphQLError("A label name must not be empty", {
        extensions: {
          code: "BAD_USER_INPUT",
        },
      });
    }

    const aliases = normalizeAliases(args.aliases ?? []);
    const label = new Label(args.name, aliases);
    if (args.color) {
      label.color = args.color;
    }

    const config = getConfig();

    if (config.matching.matchCreatedLabels) {
      const localExtractLabels = await buildExtractor(
        () => [label],
        (label) => [label.name, ...label.aliases],
        false
      );

      const toIndex: Scene[] = [];

      for await (const scene of collections.scenes.iterate()) {
        if (localExtractLabels(scene.path || scene.name).includes(label._id)) {
          await Scene.addLabels(scene, [label._id]);
          toIndex.push(scene);
          logger.debug(`Updated labels of ${scene._id}.`);
        }
      }

      await indexScenes(toIndex);
    }

    /* for (const image of await Image.getAll()) {
      if (isBlacklisted(image.name)) continue;

      if (isMatchingItem(image.path || image.name, label, false)) {
        const labels = (await Image.getLabels(image)).map((l) => l._id);
        labels.push(label._id);
        await Image.setLabels(image, labels);
        await indexImages([image]);
        logger.debug(`Updated labels of ${image._id}.`);
      } 
    } */

    logger.debug(`Created label, ${formatMessage(label)}`);
    await collections.labels.upsert(label._id, label);

    return label;
  },

  async updateLabels(
    _: unknown,
    { ids, opts }: { ids: string[]; opts: ILabelUpdateOpts }
  ): Promise<Label[]> {
    const updatedLabels = [] as Label[];

    for (const id of ids) {
      const label = await Label.getById(id);

      if (label) {
        const diff: Partial<Label> = {};

        if (Array.isArray(opts.aliases)) {
          diff.aliases = normalizeAliases(opts.aliases);
        }

        if (opts.name) {
          diff.name = normalizeName(opts.name);
        }

        if (opts.thumbnail) {
          diff.thumbnail = opts.thumbnail;
        }

        if (opts.color && isHexColor(opts.color)) {
          diff.color = opts.color;
        } else if (opts.color === "") {
          diff.color = null;
        }

        const updatedLabel = await collections.labels.partialUpdate(label._id, diff);
        updatedLabels.push(updatedLabel);
      } else {
        throw new Error(`Label ${id} not found`);
      }
    }

    BatchLabelLoader.clearAll();

    return updatedLabels;
  },
};
