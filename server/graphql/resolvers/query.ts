import { getLength, isProcessing } from "../../queue/processing";
import { indexes } from "../../search_new/index";
import Actor from "../../types/actor";
import CustomField, { CustomFieldTarget } from "../../types/custom_field";
import Image from "../../types/image";
import Label from "../../types/label";
import Marker from "../../types/marker";
import Movie from "../../types/movie";
import Scene from "../../types/scene";
import Studio from "../../types/studio";
import SceneView from "../../types/watch";
import { version } from "../../version";
import { collections } from "./../../database";
import { getActors, getUnwatchedActors } from "./search/actor";
import { getImages } from "./search/image";
import { getMarkers } from "./search/marker";
import { getMovies } from "./search/movie";
import { getScenes } from "./search/scene";
import { getStudios } from "./search/studio";

export default {
  version: () => version,

  async getWatches(_: void, { page }: { page?: number | null }): Promise<SceneView[]> {
    const PAGE_SIZE = 24;
    return await collections.views.getTail((page ?? 0) * PAGE_SIZE, PAGE_SIZE);
  },

  async topActors(
    _: unknown,
    { skip, take }: { skip: number; take: number }
  ): Promise<(Actor | null)[]> {
    return Actor.getTopActors(skip, take);
  },

  getUnwatchedActors,

  async getQueueInfo(): Promise<{
    length: number;
    processing: boolean;
  }> {
    return {
      length: await getLength(),
      processing: isProcessing(),
    };
  },

  getStudios,
  getMovies,
  getActors,
  getScenes,
  getImages,
  getMarkers,

  async getImageById(_: unknown, { id }: { id: string }): Promise<Image | null> {
    return await Image.getById(id);
  },

  async getSceneById(_: unknown, { id }: { id: string }): Promise<Scene | null> {
    return await Scene.getById(id);
  },

  async getActorById(_: unknown, { id }: { id: string }): Promise<Actor | null> {
    const actor = await Actor.getById(id);
    return actor;
  },

  async getMovieById(_: unknown, { id }: { id: string }): Promise<Movie | null> {
    return await Movie.getById(id);
  },

  async getStudioById(_: unknown, { id }: { id: string }): Promise<Studio | null> {
    return await Studio.getById(id);
  },

  async getLabelById(_: unknown, { id }: { id: string }): Promise<Label | null> {
    return await Label.getById(id);
  },
  async getMarkerById(_: unknown, { id }: { id: string }): Promise<Marker | null> {
    return await Marker.getById(id);
  },
  async getCustomFields(
    _: unknown,
    { target }: { target: CustomFieldTarget }
  ): Promise<CustomField[]> {
    const allFields = await CustomField.getAll();
    if (target) {
      return allFields.filter((field) => field.target.includes(target));
    }
    return allFields;
  },
  async getLabels(): Promise<Label[]> {
    const labels = await Label.getAll();
    return labels.sort((a, b) => a.name.localeCompare(b.name));
  },

  async getSceneDiskUsage(): Promise<number> {
    const { disk_space } = await indexes.scenes.aggregate<{
      disk_space: { value: number }
    }>({
      disk_space: {
        sum: {
          field: "size"
        }
      }
    });
    return Math.floor(disk_space.value);
  },

  async numSceneViews(): Promise<number> {
    return collections.views.count();
  },

  async numScenes(): Promise<number> {
    return indexes.scenes.count();
  },
  async numActors(): Promise<number> {
    return indexes.actors.count();
  },
  async numMovies(): Promise<number> {
    return indexes.movies.count();
  },
  async numLabels(): Promise<number> {
    return collections.labels.count();
  },
  async numStudios(): Promise<number> {
    return indexes.studios.count();
  },
  async numImages(): Promise<number> {
    return indexes.images.count();
  },
  async numMarkers(): Promise<number> {
    return indexes.markers.count();
  },
};
