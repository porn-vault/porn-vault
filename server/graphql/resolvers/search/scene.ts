import { getPage } from "../../../search/common";
import { ISceneSearchQuery } from "../../../search/scene";
import { indexes } from "../../../search_new/index";
import Scene from "../../../types/scene";
import { logger } from "../../../utils/logger";

const textFields = [
  "name",
  "actorNames",
  "labelNames",
  "studioNames",
  "movieNames"
];

export async function getScenes(
  _: unknown,
  { query }: { query: Partial<ISceneSearchQuery> }
): Promise<{
  numItems: number;
  numPages: number;
  items: Scene[];
}> {
  const timeNow = Date.now();

  const { from, size } = getPage(query.page, query.skip, query.take);

  const builtQuery = [
    ...(query.query ? [textFields.map(f => `${f}:(${query.query})`).join(" OR ")] : []),
    ...(query.favorite ? ["favorite:true"] : []),
    ...(query.bookmark ? ["bookmark:>0"] : []),
    ...(query.include?.length ? [`labels: IN [${query.include.join(" ")}]`] : []),
    ...(query.exclude?.length ? [`-labels: IN [${query.exclude.join(" ")}]`] : []),
    ...(query.actors?.length ? [`actors: IN [${query.actors.join(" ")}]`] : []),
    ...(query.studios?.length ? [`studios: IN [${query.studios.join(" ")}]`] : []),
    ...(query.unwatchedOnly ? [`numViews:0`] : []),
    ...(query.rating ? [`rating:>=${query.rating}`] : []),
    ...(query.durationMax || query.durationMax ? [
      `duration:[${query.durationMin ?? "*"} TO ${query.durationMax ?? "*"}]`,
    ] : [])
  ].map(x => `(${x})`).join(" AND ");

  logger.silly(`Built search query: ${builtQuery}`);

  let order;

  if (query.sortBy && query.sortBy !== "relevance") {
    order = {
      by: query.sortBy,
      dir: (query.sortDir ?? "desc") as "asc" | "desc",
    };
  }
  else {
    if (!query.query) {
      order = {
        by: "addedOn",
        dir: "desc" as const,
      };
    }
  }

  const result = await indexes.scenes.query(builtQuery, from, size, order);
  logger.verbose(`Search results: ${result.hits_count} hits found in ${(Date.now() - timeNow) / 1000}s`);

  const scenes = await Scene.getBulk(result.ids);
  logger.verbose(`Search done in ${(Date.now() - timeNow) / 1000}s.`);

  return {
    numItems: result.hits_count,
    numPages: Math.ceil(result.hits_count / size),
    items: scenes,
  };
}
