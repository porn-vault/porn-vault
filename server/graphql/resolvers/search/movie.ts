import { getPage } from "../../../search/common";
import { IMovieSearchQuery, } from "../../../search/movie";
import { indexes } from "../../../search_new";
import Movie from "../../../types/movie";
import { logger } from "../../../utils/logger";

const textFields = [
  "name",
  "actorNames",
  "labelNames",
  "studioNames",
];

export async function getMovies(
  _: unknown,
  { query }: { query: Partial<IMovieSearchQuery> }
): Promise<{
  numItems: number;
  numPages: number;
  items: Movie[];
}> {
  const timeNow = Date.now();

  const { from, size } = getPage(query.page, query.skip, query.take);

  const builtQuery = [
    ...(query.query ? [textFields.map(f => `${f}:(${query.query})`).join(" OR ")] : []),
    ...(query.favorite ? ["favorite:true"] : []),
    ...(query.bookmark ? ["bookmark:>0"] : []),
    ...(query.include?.length ? [`labels: IN [${query.include.join(" ")}]`] : []),
    ...(query.exclude?.length ? [`-labels: IN [${query.exclude.join(" ")}]`] : []),
    ...(query.actors?.length ? [`actors: IN [${query.actors.join(" ")}]`] : []),
    ...(query.studios?.length ? [`studios: IN [${query.studios.join(" ")}]`] : []),
    ...(query.rating ? [`rating:>=${query.rating}`] : []),
    ...(query.durationMax || query.durationMax ? [
      `duration:[${query.durationMin ?? "*"} TO ${query.durationMax ?? "*"}]`,
    ] : [])
  ].map(x => `(${x})`).join(" AND ");

  logger.silly(`Built search query: ${builtQuery}`);

  let order;

  if (query.sortBy && query.sortBy !== "relevance") {
    order = {
      by: query.sortBy,
      dir: (query.sortDir ?? "desc") as "asc" | "desc",
    };
  }
  else {
    if (!query.query) {
      order = {
        by: "addedOn",
        dir: "desc" as const,
      };
    }
  }

  const result = await indexes.movies.query(builtQuery, from, size, order);
  logger.verbose(`Search results: ${result.hits_count} hits found in ${(Date.now() - timeNow) / 1000}s`);

  const movies = await Movie.getBulk(result.ids);
  logger.verbose(`Search done in ${(Date.now() - timeNow) / 1000}s.`);

  return {
    numItems: result.hits_count,
    numPages: Math.ceil(result.hits_count / size),
    items: movies,
  };
}
