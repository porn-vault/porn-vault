import { getPage } from "../../../search/common";
import { IStudioSearchQuery, } from "../../../search/studio";
import { indexes } from "../../../search_new";
import Studio from "../../../types/studio";
import { logger } from "../../../utils/logger";

const textFields = [
  "name",
  "labelNames",
  "parentNames",
  "aliases"
];

export async function getStudios(
  _: unknown,
  { query }: { query: Partial<IStudioSearchQuery>; }
): Promise<{
  numItems: number;
  numPages: number;
  items: Studio[];
}> {
  const timeNow = Date.now();

  const { from, size } = getPage(query.page, query.skip, query.take);

  const builtQuery = [
    ...(query.query ? [textFields.map(f => `${f}:(${query.query})`).join(" OR ")] : []),
    ...(query.favorite ? ["favorite:true"] : []),
    ...(query.bookmark ? ["bookmark:>0"] : []),
    ...(query.include?.length ? [`labels: IN [${query.include.join(" ")}]`] : []),
    ...(query.exclude?.length ? [`-labels: IN [${query.exclude.join(" ")}]`] : []),
  ].map(x => `(${x})`).join(" AND ");

  logger.silly(`Built search query: ${builtQuery}`);

  let order;

  if (query.sortBy && query.sortBy !== "relevance") {
    order = {
      by: query.sortBy,
      dir: (query.sortDir ?? "desc") as "asc" | "desc",
    };
  }
  else {
    if (!query.query) {
      order = {
        by: "addedOn",
        dir: "desc" as const,
      };
    }
  }

  const result = await indexes.studios.query(builtQuery, from, size, order);
  logger.verbose(`Search results: ${result.hits_count} hits found in ${(Date.now() - timeNow) / 1000}s`);

  const studios = await Studio.getBulk(result.ids);
  logger.verbose(`Search done in ${(Date.now() - timeNow) / 1000}s.`);


  return {
    numItems: result.hits_count,
    numPages: Math.ceil(result.hits_count / size),
    items: studios,
  };
}
