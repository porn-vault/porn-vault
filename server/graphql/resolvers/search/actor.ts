import { IActorSearchQuery, } from "../../../search/actor";
import { getPage } from "../../../search/common";
import { indexes } from "../../../search_new";
import Actor from "../../../types/actor";
import { logger } from "../../../utils/logger";

export async function getUnwatchedActors(
  _: unknown,
  { take, skip, }: { skip?: number; take?: number; }
): Promise<Actor[] | undefined> {
  const timeNow = Date.now();

  const result = await indexes.actors.query(`numViews:0`, skip, take);
  logger.verbose(`Unwatched actors results: ${result.hits_count} hits found in ${(Date.now() - timeNow) / 1000}s`);

  const actors = await Actor.getBulk(result.ids);
  logger.verbose(`Unwatched actors done in ${(Date.now() - timeNow) / 1000}s.`);

  return actors;
}

const textFields = [
  "name",
  "labelNames",
  "nationalityName",
  "aliases",
];

export async function getActors(
  _: unknown,
  { query }: { query: Partial<IActorSearchQuery> }
): Promise<{
  numItems: number;
  numPages: number;
  items: Actor[];
}> {
  const timeNow = Date.now();

  const { from, size } = getPage(query.page, query.skip, query.take);

  const builtQuery = [
    ...(query.query ? [textFields.map(f => `${f}:(${query.query})`).join(" OR ")] : []),
    ...(query.favorite ? ["favorite:true"] : []),
    ...(query.bookmark ? ["bookmark:>0"] : []),
    ...(query.include?.length ? [`labels: IN [${query.include.join(" ")}]`] : []),
    ...(query.exclude?.length ? [`-labels: IN [${query.exclude.join(" ")}]`] : []),
    ...(query.studios?.length ? [`studios: IN [${query.studios.join(" ")}]`] : []),
    ...(query.rating ? [`rating:>=${query.rating}`] : []),
    ...(query.nationality ? [`nationality:${query.nationality}`] : []),
    ...(query.letter ? [`letter:${query.letter}`] : []),
  ].map(x => `(${x})`).join(" AND ");

  logger.silly(`Built search query: ${builtQuery}`);

  let order;

  if (query.sortBy && query.sortBy !== "relevance") {
    order = {
      by: query.sortBy,
      dir: (query.sortDir ?? "desc") as "asc" | "desc",
    };
  }
  else {
    if (!query.query) {
      order = {
        by: "addedOn",
        dir: "desc" as const,
      };
    }
  }

  const result = await indexes.actors.query(builtQuery, from, size, order);
  logger.verbose(`Search results: ${result.hits_count} hits found in ${(Date.now() - timeNow) / 1000}s`);

  const actors = await Actor.getBulk(result.ids);
  logger.verbose(`Search done in ${(Date.now() - timeNow) / 1000}s.`);

  return {
    numItems: result.hits_count,
    numPages: Math.ceil(result.hits_count / size),
    items: actors,
  };
}
