import { getPage } from "../../../search/common";
import { IImageSearchQuery, } from "../../../search/image";
import { indexes } from "../../../search_new";
import Image from "../../../types/image";
import { logger } from "../../../utils/logger";

const textFields = [
  "name",
  "actorNames",
  "labelNames",
  "studioNames",
];

export async function getImages(
  _: unknown,
  { query }: { query: Partial<IImageSearchQuery> }
): Promise<{
  numItems: number;
  numPages: number;
  items: Image[];
}> {
  const timeNow = Date.now();

  const { from, size } = getPage(query.page, query.skip, query.take);

  const builtQuery = [
    ...(query.query ? [textFields.map(f => `${f}:(${query.query})`).join(" OR ")] : []),
    ...(query.favorite ? ["favorite:true"] : []),
    ...(query.bookmark ? ["bookmark:>0"] : []),
    ...(query.include?.length ? [`labels: IN [${query.include.join(" ")}]`] : []),
    ...(query.exclude?.length ? [`-labels: IN [${query.exclude.join(" ")}]`] : []),
    ...(query.actors?.length ? [`actors: IN [${query.actors.join(" ")}]`] : []),
    ...(query.studios?.length ? [`studios: IN [${query.studios.join(" ")}]`] : []),
    ...(query.scenes?.length ? [`scene: IN [${query.scenes.join(" ")}]`] : []),
    ...(query.rating ? [`rating:>=${query.rating}`] : []),
  ].map(x => `(${x})`).join(" AND ");

  logger.silly(`Built search query: ${builtQuery}`);

  let order;

  if (query.sortBy && query.sortBy !== "relevance") {
    order = {
      by: query.sortBy,
      dir: (query.sortDir ?? "desc") as "asc" | "desc",
    };
  }
  else {
    if (!query.query) {
      order = {
        by: "addedOn",
        dir: "desc" as const,
      };
    }
  }

  const result = await indexes.images.query(builtQuery, from, size, order);
  logger.verbose(`Search results: ${result.hits_count} hits found in ${(Date.now() - timeNow) / 1000}s`);

  const images = await Image.getBulk(result.ids);
  logger.verbose(`Search done in ${(Date.now() - timeNow) / 1000}s.`);


  return {
    numItems: result.hits_count,
    numPages: Math.ceil(result.hits_count / size),
    items: images,
  };
}
