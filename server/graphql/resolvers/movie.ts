import { getImageDimensions } from "../../binaries/imagemagick";
import { collections } from "../../database";
import { indexes } from "../../search_new";
import Actor from "../../types/actor";
import Image from "../../types/image";
import Label from "../../types/label";
import Movie from "../../types/movie";
import Scene from "../../types/scene";
import Studio from "../../types/studio";
import { logger } from "../../utils/logger";
import { getMovieDataSource } from "../datasources";

export default {
  async similar(movie: Movie): Promise<Movie[]> {
    const timeNow = Date.now();

    const actors = (await Movie.getActors(movie)).map(x => x._id);
    const labels = (await Movie.getLabels(movie)).map(x => x._id);
    const studios = movie.studio ? [movie.studio, ...(await Studio.getSubStudios(movie.studio)).map(x => x._id)] : [];

    let builtQuery = [
      ...(labels?.length ? [`labels: IN [${labels.join(" ")}]`] : []),
      ...(actors?.length ? [`actors: IN [${actors.join(" ")}]`] : []),
      ...(studios?.length ? [`studios: IN [${studios.join(" ")}]`] : []),
    ].map(x => `(${x})`).join(" OR ");

    // Ignore self
    builtQuery = `(${builtQuery}) AND -_id:${movie._id}`;

    logger.silly(`Built similar query: ${builtQuery}`);

    const result = await indexes.movies.query(builtQuery, 0, 8);
    logger.verbose(`Similar results: ${result.hits_count} hits found in ${(Date.now() - timeNow) / 1000}s`);

    const movies = await Movie.getBulk(result.ids);
    logger.verbose(`Similar done in ${(Date.now() - timeNow) / 1000}s.`);

    return movies;
  },

  async frontCover(movie: Movie, _: any): Promise<Image | null> {
    if (!movie.frontCover) {
      return null;
    }

    const image = await getMovieDataSource().getCoverForMovie(movie.frontCover);
    if (!image) {
      return null;
    }

    // Pre 0.27 compatibility: add image dimensions on demand and save to db
    image.meta.dimensions = await Image.attachDimensions(image);

    return image;
  },

  async backCover(movie: Movie, _: any): Promise<Image | null> {
    if (!movie.backCover) {
      return null;
    }

    const image = await getMovieDataSource().getCoverForMovie(movie.backCover);
    if (!image) {
      return null;
    }

    // Pre 0.27 compatibility: add image dimensions on demand and save to db
    image.meta.dimensions = await Image.attachDimensions(image);

    return image;
  },

  async spineCover(movie: Movie, _: any): Promise<Image | null> {
    if (!movie.spineCover) {
      return null;
    }

    const image = await getMovieDataSource().getCoverForMovie(movie.spineCover);
    if (!image) {
      return null;
    }

    // Pre 0.27 compatibility: add image dimensions on demand and save to db
    image.meta.dimensions = await Image.attachDimensions(image);

    return image;
  },

  async scenes(movie: Movie, _: any): Promise<Scene[]> {
    return getMovieDataSource().getScenesForMovie(movie._id);
  },

  async actors(movie: Movie, _: any): Promise<Actor[]> {
    const actors = await getMovieDataSource().getActorsForMovie(movie._id);
    return actors.sort((a, b) => a.name.localeCompare(b.name));
  },

  async labels(movie: Movie, _: any): Promise<Label[]> {
    const labels = await getMovieDataSource().getLabelsForMovie(movie._id);
    return labels.sort((a, b) => a.name.localeCompare(b.name));
  },

  rating(movie: Movie): Promise<number> {
    return Movie.getRating(movie);
  },

  async duration(movie: Movie, _: any): Promise<number> {
    const scenes = await getMovieDataSource().getScenesForMovie(movie._id);
    return Movie.calculateDurationFromScenes(scenes);
  },

  async size(movie: Movie): Promise<number | null> {
    const scenesWithSource = (await Movie.getScenes(movie)).filter(
      (scene) => scene.meta && scene.path
    );
    return scenesWithSource.reduce((dur, scene) => dur + (scene.meta?.size || 0), 0);
  },

  async studio(movie: Movie): Promise<Studio | null> {
    if (movie.studio) {
      return Studio.getById(movie.studio);
    }
    return null;
  },

  async numScenes(movie: Movie): Promise<number> {
    return indexes.scenes.count(`movies:${movie._id}`);
  },
};
