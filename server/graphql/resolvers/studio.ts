import { collections } from "../../database";
import { indexes } from "../../search_new";
import Actor from "../../types/actor";
import CustomField, { CustomFieldTarget } from "../../types/custom_field";
import Image from "../../types/image";
import Label from "../../types/label";
import Movie from "../../types/movie";
import Scene from "../../types/scene";
import Studio from "../../types/studio";
import { logger } from "../../utils/logger";
import { getStudioDataSource } from "../datasources";

export default {
  aliases(studio: Studio): string[] {
    return studio.aliases ?? [];
  },
  rating(studio: Studio): number {
    return studio.rating ?? 0;
  },
  async averageRating(studio: Studio): Promise<number> {
    return await Studio.getAverageRating(studio);
  },
  async thumbnail(studio: Studio): Promise<Image | null> {
    try {
      if (studio.thumbnail) {
        const thumb = await getStudioDataSource().getThumbnailForStudio(studio.thumbnail);
        return thumb ?? null;
      }
    } catch (error) {
      if (studio.thumbnail) {
        // NOTE: Dirty cleanup hack
        const thumb = await Image.getById(studio.thumbnail);
        if (!thumb) {
          logger.debug(`Setting ${studio._id} thumbnail to null because it does not exist`);
          studio.thumbnail = null;
          await collections.studios.upsert(studio._id, studio);
        } else {
          return thumb;
        }
      }
    }
    return null;
  },
  scenes(studio: Studio): Promise<Scene[]> {
    return Studio.getScenes(studio);
  },
  async actors(studio: Studio): Promise<Actor[]> {
    const actors = await Studio.getActors(studio);
    return actors.sort((a, b) => a.name.localeCompare(b.name));
  },
  async labels(studio: Studio): Promise<Label[]> {
    const labels = await getStudioDataSource().getLabelsForStudio(studio);
    return labels.sort((a, b) => a.name.localeCompare(b.name));
  },
  movies(studio: Studio): Promise<Movie[]> {
    return Studio.getMovies(studio);
  },
  async parent(studio: Studio): Promise<Studio | null> {
    if (studio.parent) {
      return await Studio.getById(studio.parent);
    }
    return null;
  },
  async substudios(studio: Studio): Promise<Studio[]> {
    return Studio.getSubStudios(studio._id);
  },
  async numScenes(studio: Studio): Promise<number> {
    return indexes.scenes.count(`studios:${studio._id}`);
  },
  async availableFields(): Promise<CustomField[]> {
    const fields = await CustomField.getAll();
    return fields.filter(({ target }) => target.includes(CustomFieldTarget.STUDIOS));
  },
};
