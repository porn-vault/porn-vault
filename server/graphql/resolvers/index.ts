import ActorMutations from "../mutations/actor";
import CustomFieldMutations from "../mutations/custom_field";
import ImageMutations from "../mutations/image";
import LabelMutations from "../mutations/label";
import MarkerMutations from "../mutations/marker";
import MovieMutations from "../mutations/movie";
import SceneMutations from "../mutations/scene";
import StudioMutations from "../mutations/studio";
import ActorResolver from "./actor";
import CustomFieldResolver from "./custom_field";
import ImageResolver from "./image";
import LabelResolver from "./label";
import MarkerResolver from "./marker";
import MovieResolver from "./movie";
import QueryResolvers from "./query";
import SceneResolver from "./scene";
import SceneViewResolver from "./scene_view";
import StudioResolver from "./studio";

const resolvers = {
  Mutation: {
    ...ImageMutations,
    ...ActorMutations,
    ...LabelMutations,
    ...SceneMutations,
    ...MovieMutations,
    ...StudioMutations,
    ...MarkerMutations,
    ...CustomFieldMutations,
  },

  Actor: ActorResolver,
  Scene: SceneResolver,
  Image: ImageResolver,
  Query: QueryResolvers,
  Label: LabelResolver,
  Movie: MovieResolver,
  Studio: StudioResolver,
  CustomField: CustomFieldResolver,
  Marker: MarkerResolver,
  SceneView: SceneViewResolver,
};

export default resolvers;
