import { existsSync } from "node:fs";

import { getImageDimensions } from "../../binaries/imagemagick";
import { collections } from "../../database";
import { indexes } from "../../search_new";
/* import { CopyMP4Transcoder } from "../../transcode/copyMp4";
import { SceneStreamTypes } from "../../transcode/transcoder"; */
import Actor from "../../types/actor";
import CustomField, { CustomFieldTarget } from "../../types/custom_field";
import Image from "../../types/image";
import Label from "../../types/label";
import Marker from "../../types/marker";
import Movie from "../../types/movie";
import Scene, { SceneMeta } from "../../types/scene";
import Studio from "../../types/studio";
import { handleError, logger } from "../../utils/logger";
import { getSceneDataSource } from "../datasources";

interface AvailableStreams {
  label: string;
  mimeType?: string;
  /* TODO: RESTORE */
  //  streamType: SceneStreamTypes;
  transcode: boolean;
}

export default {
  fileExists(scene: Scene): boolean {
    if (!scene.path) {
      return false;
    }
    return existsSync(scene.path);
  },

  async similar(scene: Scene): Promise<Scene[]> {
    const timeNow = Date.now();

    const actors = (await Scene.getActors(scene)).map(x => x._id);
    const labels = (await Scene.getLabels(scene)).map(x => x._id);
    const studios = scene.studio ? [scene.studio, ...(await Studio.getSubStudios(scene.studio)).map(x => x._id)] : [];

    let builtQuery = [
      ...(labels?.length ? [`labels: IN [${labels.join(" ")}]`] : []),
      ...(actors?.length ? [`actors: IN [${actors.join(" ")}]`] : []),
      ...(studios?.length ? [`studios: IN [${studios.join(" ")}]`] : []),
    ].map(x => `(${x})`).join(" OR ");

    // Ignore self
    builtQuery = `(${builtQuery}) AND -_id:${scene._id}`;

    logger.silly(`Built similar query: ${builtQuery}`);

    const result = await indexes.scenes.query(builtQuery, 0, 8);
    logger.verbose(`Similar results: ${result.hits_count} hits found in ${(Date.now() - timeNow) / 1000}s`);

    const scenes = await Scene.getBulk(result.ids);
    logger.verbose(`Similar done in ${(Date.now() - timeNow) / 1000}s.`);

    return scenes;
  },

  async meta(scene: Scene): Promise<SceneMeta> {
    if (!scene.meta.size && scene.path) {
      const { meta } = await Scene.getVideoFileMetadata(scene.path);
      await collections.scenes.partialUpdate(scene._id, {
        meta,
      });
    }

    return scene.meta;
  },

  async actors(scene: Scene, _: any): Promise<Actor[]> {
    return Scene.getActors(scene);
  },
  async images(scene: Scene): Promise<Image[]> {
    return await Image.getByScene(scene._id);
  },
  async labels(scene: Scene, _: any): Promise<Label[]> {
    return Scene.getLabels(scene);
  },
  async thumbnail(scene: Scene, _: any): Promise<Image | null> {
    if (!scene.thumbnail) {
      return null;
    }

    try {
      const thumb = await getSceneDataSource().getThumbnailForScene(scene);
      return thumb ?? null;
    } catch (error) {
      if (scene.thumbnail) {
        // NOTE: Dirty cleanup hack
        const thumb = await Image.getById(scene.thumbnail);

        if (!thumb) {
          logger.debug(`Setting ${scene._id} thumbnail to null because it does not exist`);
          await collections.scenes.partialUpdate(scene._id, {
            thumbnail: null,
          });
        } else {
          return thumb;
        }
      }

      return null;
    }
  },
  async preview(scene: Scene): Promise<Image | null> {
    if (!scene.preview) {
      return null;
    }
    const image = await Image.getById(scene.preview);
    if (!image) {
      return null;
    }

    // Pre 0.27 compatibility: add image dimensions on demand and save to db
    if (image.path && (!image.meta.dimensions.height || !image.meta.dimensions.width)) {
      const dims = await getImageDimensions(image.path);
      image.meta.dimensions.width = dims.width;
      image.meta.dimensions.height = dims.height;

      await collections.images.upsert(image._id, image);
    }

    return image;
  },
  async studio(scene: Scene): Promise<Studio | null> {
    // TODO: DataLoader
    if (scene.studio) {
      return Studio.getById(scene.studio);
    }
    return null;
  },
  async markers(scene: Scene): Promise<Marker[]> {
    return await Scene.getMarkers(scene);
  },
  async availableFields(): Promise<CustomField[]> {
    const fields = await CustomField.getAll();
    return fields.filter((field) => field.target.includes(CustomFieldTarget.SCENES));
  },
  async movies(scene: Scene): Promise<Movie[]> {
    return Scene.getMovies(scene);
  },
  async watches(scene: Scene, _: any): Promise<number[]> {
    const watches = await getSceneDataSource().getWatchesForScene(scene._id);
    return watches.map((x) => x.date);
  },
  async resolvedCustomFields(scene: Scene): Promise<{ field: CustomField; value: any }[]> {
    const fields = await CustomField.getAll();

    return Object.entries(scene.customFields).reduce((arr, [key, value]) => {
      arr.push({
        field: fields.find((f) => f._id === key)!,
        value,
      });
      return arr;
    }, [] as { field: CustomField; value: any }[]);
  },
  async availableStreams(scene: Scene): Promise<AvailableStreams[]> {
    if (!scene.path) {
      return [];
    }

    if (!existsSync(scene.path)) {
      return [];
    }

    if (!scene.meta.container || !scene.meta.videoCodec) {
      logger.verbose(
        `Scene ${scene._id} doesn't have codec information to determine available streams, running ffprobe`
      );
      const { meta } = await Scene.getVideoFileMetadata(scene.path);

      // Doesn't matter if this fails
      await collections.scenes.partialUpdate(scene._id, { meta }).catch((err) => {
        handleError("Failed to update scene after updating codec information", err);
      });
    }

    const streams: AvailableStreams[] = [];

    /* TODO: RESTORE */

    // Attempt direct stream, set it as first item
    /* streams.unshift({
      label: "direct play",
      mimeType:
        // Attempt to trick the browser into playing mkv by using the mp4 mime type
        // Otherwise let the browser handle the unknown mime type
        [".mp4", ".mkv"].includes(getExtension(scene.path))
          ? "video/mp4"
          : undefined,
      streamType: SceneStreamTypes.DIRECT,
      transcode: false,
    }); */

    // Mkv might contain mp4 compatible streams
    /* if (
      scene.meta.container === FFProbeContainers.MKV &&
      scene.meta.videoCodec &&
      new CopyMP4Transcoder(scene).isVideoValidForContainer()
    ) {
      streams.push({
        label: "mkv direct stream",
        mimeType: "video/mp4",
        streamType: SceneStreamTypes.MP4_DIRECT,
        transcode: true,
      });
    } */

    // Fallback transcode: mp4 (potentially hardware accelerated)
    /* streams.push({
      label: "mp4 transcode",
      mimeType: "video/mp4",
      streamType: SceneStreamTypes.MP4_TRANSCODE,
      transcode: true,
    }); */

    // Fallback transcode: webm
    /* streams.push({
      label: "webm transcode",
      mimeType: "video/webm",
      streamType: SceneStreamTypes.WEBM_TRANSCODE,
      transcode: true,
    }); */

    // Otherwise video cannot be streamed

    return streams;
  },
};
