import Actor from "../../types/actor";
import Image from "../../types/image";
import Label from "../../types/label";
import Marker from "../../types/marker";
import Scene from "../../types/scene";
import { getMarkerDataSource } from "../datasources";

export default {
  async actors(marker: Marker, _: any): Promise<Actor[]> {
    const actors = await getMarkerDataSource().getActorsForMarker(marker);
    return actors.sort((a, b) => a.name.localeCompare(b.name));
  },

  async labels(marker: Marker, _: any): Promise<Label[]> {
    const labels = await getMarkerDataSource().getLabelsForMarker(marker);
    return labels.sort((a, b) => a.name.localeCompare(b.name));
  },

  async thumbnail(marker: Marker, _: any): Promise<Image | null> {
    if (!marker.thumbnail) {
      return null;
    }

    return await getMarkerDataSource().getThumbnailForMarker(marker.thumbnail);
  },

  async scene(marker: Marker): Promise<Scene | null> {
    return Scene.getById(marker.scene);
  },
};
