import http from "node:http";

import { ApolloServer, BaseContext } from "@apollo/server";
import { expressMiddleware } from "@apollo/server/express4";
import { ApolloServerPluginDrainHttpServer } from "@apollo/server/plugin/drainHttpServer";
import { GraphQLFileLoader } from "@graphql-tools/graphql-file-loader";
import { loadSchema } from "@graphql-tools/load";
import { type Application,json } from "express";
import GraphQLJson, { GraphQLJSONObject } from "graphql-type-json";
import { GraphQLUpload,graphqlUploadExpress  } from "graphql-upload";

import { logger } from "../utils/logger";
import resolvers from "./resolvers";
import GraphQLLong from "./types/long";

async function loadSchemaFromDisk() {
  logger.debug("Loading GraphQL schema from ./graphql");

  const typeDefs = await loadSchema("./graphql/**/*.graphql", {
    loaders: [new GraphQLFileLoader()],
  });

  return {
    typeDefs,
    resolvers: {
      Upload: GraphQLUpload,
      Long: GraphQLLong,
      Json: GraphQLJson,
      Object: GraphQLJSONObject,
      ...resolvers,
    },
  };
}

export async function createGraphqlServer(
  app: Application
): Promise<ApolloServer<BaseContext>> {
  logger.debug("Setting up GraphQL server");
  const httpServer = http.createServer(app);

  const server = new ApolloServer({
    ...(await loadSchemaFromDisk()),
    plugins: [ApolloServerPluginDrainHttpServer({ httpServer })],
  });
  await server.start();

  return server;
}

export async function mountGraphqlServer(app: Application) {
  const server = await createGraphqlServer(app);
  app.use(
    ["/graphql", "/api/graphql"],
    graphqlUploadExpress(),
    json(),
    expressMiddleware(server)
  );
}
