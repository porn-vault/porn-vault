import { existsSync } from "node:fs";
import { resolve } from "node:path";

import { execa } from "execa";

import { getImageDimensions } from "../binaries/imagemagick";
import { getConfig } from "../config";
import { collections } from "../database";
import { indexes } from "../search_new";
import { unlinkAsync } from "../utils/fs/async";
import { generateHash } from "../utils/hash";
import { handleError, logger } from "../utils/logger";
import { getFolderPartition, libraryPath } from "../utils/path";
import { normalizeName } from "../utils/string";
import Actor from "./actor";
import ActorReference from "./actor_reference";
import { iterate } from "./common";
import Label from "./label";

export class ImageDimensions {
  width: number | null = null;
  height: number | null = null;
}

export class ImageMeta {
  size: number | null = null;
  dimensions = new ImageDimensions();
}

export default class Image {
  _id: string;
  name: string;
  path: string | null = null;
  thumbPath: string | null = null;
  scene: string | null = null;
  addedOn = +new Date();
  favorite = false;
  bookmark: number | null = null;
  rating = 0;
  customFields: Record<string, boolean | string | number | string[] | null> = {};

  meta = new ImageMeta();
  album?: string | null = null;
  studio: string | null = null;
  hash: string | null = null;
  color: string | null = null;

  static async createThumbnail(fromPath: string, imageId: string): Promise<string | null> {
    if (fromPath.endsWith(".gif")) {
      return null;
    }

    const outPath = resolve(getFolderPartition(libraryPath("thumbnails/images")), `${imageId}.jpg`);

    logger.debug(`Creating image thumbnail at ${outPath} for ${fromPath}`);
    await execa(getConfig().binaries.imagemagick.convertPath, [
      fromPath,
      "-resize",
      "480x480",
      outPath,
    ]);
    logger.debug(`Stored image thumbnail at ${outPath}`);

    if (!existsSync(outPath)) {
      throw new Error(`Thumbnail generation failed for image: ${fromPath}`);
    }

    return outPath;
  }

  static async iterate(
    func: (scene: Image) => void | unknown | Promise<void | unknown>,
    extraFilter: unknown[] = []
  ) {
    return iterate(async () => ({ items: [] }), Image.getBulk, func, "image", extraFilter);
  }

  static async extractColor(image: Image): Promise<void> {
    /* if (!image.path) {
    } */
    /* TODO: RESTORE */
    /* const palette = await Vibrant.from(image.path).getPalette();

    const color =
      palette.DarkVibrant?.getHex() ||
      palette.DarkMuted?.getHex() ||
      palette.Vibrant?.getHex() ||
      palette.Vibrant?.getHex();

    if (color) {
      image.color = color;
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      collections.images.upsert(image._id, image).catch(() => {});
    } */
  }

  static color(image: Image): string | null {
    if (!image.path) {
      return null;
    }
    if (image.color) {
      return image.color;
    }

    /* if (image.path && !image.path.endsWith(".webp")) {
      logger.debug(`Extracting color from image "${image._id}"`);
      Image.extractColor(image).catch((err: Error) => {
        handleError(`Image color extraction failed for image "${image._id}" (${image.path})`, err);
      });
    } */

    return null;
  }

  static async attachDimensions(image: Image): Promise<ImageDimensions> {
    if (!image.path) {
      return {
        width: null, height: null,
      };
    }
    if (image.meta.dimensions.width || image.meta.dimensions.height) {
      return image.meta.dimensions;
    }
    const dimensions = await getImageDimensions(image.path);
    await collections.images.partialUpdate(image._id, {
      meta: {
        ...image.meta,
        dimensions,
      }
    });
    return dimensions;
  }

  static async remove(image: Image): Promise<void> {
    await collections.images.remove(image._id);

    try {
      if (image.path) {
        await unlinkAsync(image.path);
      }
      if (image.thumbPath) {
        await unlinkAsync(image.thumbPath);
      }
    } catch (error) {
      handleError(`Could not delete source file for image ${image._id}`, error);
    }
  }

  /**
   * Removes the given studio from all images that
   * are associated to the studio
   *
   * @param studioId - id of the studio to remove
   */
  static async filterStudio(studioId: string): Promise<void> {
    await Image.iterateByStudio(studioId, async (image) => {
      await collections.images.partialUpdate(image._id, {
        studio: null
      });
    });
  }

  // TODO:
  static async iterateByScene(
    sceneId: string,
    func: (scene: Image) => void | unknown | Promise<void | unknown>
  ): Promise<void | Image> {
    return Image.iterate(func, [
      {
        query_string: {
          query: `scene:${sceneId}`,
        },
      },
    ]);
  }

  static async getByScene(id: string): Promise<Image[]> {
    const result = await indexes.images.query(`scene:${id}`, 0, 9_999);
    return Image.getBulk(result.ids);
  }

  // TODO:
  static async iterateByStudio(
    studioId: string,
    func: (scene: Image) => void | unknown | Promise<void | unknown>
  ): Promise<void | Image> {
    return Image.iterate(func, [
      {
        query_string: {
          query: `studio:${studioId}`,
        },
      },
    ]);
  }

  static async getById(_id: string): Promise<Image | null> {
    return collections.images.get(_id);
  }

  static getBulk(_ids: string[]): Promise<Image[]> {
    return collections.images.getBulk(_ids);
  }

  static async getAll(): Promise<Image[]> {
    return collections.images.getAll();
  }

  static async getActors(image: Image): Promise<Actor[]> {
    const references = await ActorReference.getByItem(image._id);
    return (await collections.actors.getBulk(references.map((r) => r.actor))).sort((a, b) =>
      a.name.localeCompare(b.name)
    );
  }

  static async setActors(image: Image, actorIds: string[]): Promise<void> {
    return Actor.setForItem(image._id, actorIds, "image");
  }

  static async addActors(image: Image, actorIds: string[]): Promise<void> {
    return Actor.addForItem(image._id, actorIds, "image");
  }

  static async setLabels(image: Image, labelIds: string[]): Promise<void> {
    return Label.setForItem(image._id, labelIds, "image");
  }

  static async addLabels(image: Image, labelIds: string[]): Promise<void> {
    return Label.addForItem(image._id, labelIds, "image");
  }

  static async getLabels(image: Image): Promise<Label[]> {
    return Label.getForItem(image._id);
  }

  static async getByPath(path: string): Promise<Image | undefined> {
    const resolved = resolve(path);
    const images = await collections.images.query("path-index", encodeURIComponent(resolved));
    return images[0];
  }

  constructor(name: string) {
    this._id = `im_${generateHash()}`;
    this.name = normalizeName(name);
  }
}
