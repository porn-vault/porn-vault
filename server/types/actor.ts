import dayjs from "dayjs";

import { getConfig } from "../config";
import { collections } from "../database";
import { buildExtractor } from "../extractor";
import { clearCaches } from "../graphql/datasources";
import { getActors as searchActors } from "../graphql/resolvers/search/actor";
import { ignoreSingleNames } from "../matching/matcher";
import { indexScenes } from "../search/scene";
import { mapAsync } from "../utils/async";
import { generateHash } from "../utils/hash";
import { handleError, logger } from "../utils/logger";
import { arrayDiff, createObjectSet } from "../utils/misc";
import { normalizeAliases, normalizeName } from "../utils/string";
import ActorReference from "./actor_reference";
import Label from "./label";
import Movie from "./movie";
import Scene, { getAverageRating } from "./scene";
import Studio from "./studio";
import SceneView from "./watch";

export default class Actor {
  _id: string;
  name: string;
  aliases: string[] = [];
  addedOn = +new Date();
  bornOn: number | null = null;
  thumbnail: string | null = null;
  altThumbnail: string | null = null;
  hero?: string | null = null;
  avatar?: string | null = null;
  favorite = false;
  bookmark: number | null = null;
  rating = 0;
  externalLinks?: { url: string; text: string }[] = [];
  customFields: Record<string, boolean | string | number | string[] | null> = {};

  description?: string | null = null;
  nationality?: string | null = null;

  /*  static async iterate(
     func: (scene: Actor) => void | unknown | Promise<void | unknown>,
     extraFilter: unknown[] = []
   ) {
     return iterate(async () => ({ items: [] }), Actor.getBulk, func, "actor", extraFilter);
   } */

  static async getStudioFeatures(actor: Actor): Promise<Studio[]> {
    const scenes = await Scene.getByActor(actor._id);
    const rawStudios = await Studio.getBulk(scenes.map((scene) => scene.studio!).filter(Boolean));
    return createObjectSet(rawStudios, "_id");
  }

  static async getAverageRating(actor: Actor): Promise<number> {
    logger.silly(`Calculating average rating for "${actor.name}"`);
    const scenes = await Scene.getByActor(actor._id);
    return getAverageRating(scenes);
  }

  static getAge(actor: Actor): number | null {
    if (actor.bornOn) {
      return dayjs().diff(actor.bornOn, "years");
    }
    return null;
  }

  static async remove(actor: Actor): Promise<void> {
    return collections.actors.remove(actor._id);
  }

  static async setLabels(actor: Actor, labelIds: string[]): Promise<void> {
    return Label.setForItem(actor._id, labelIds, "actor");
  }

  static async addLabels(actor: Actor, labelIds: string[]): Promise<void> {
    return Label.addForItem(actor._id, labelIds, "actor");
  }

  static async getLabels(actor: Actor): Promise<Label[]> {
    return Label.getForItem(actor._id);
  }

  static async setForItem(
    itemId: string,
    actorIds: string[],
    type: "scene" | "actor" | "marker" | "image"
  ): Promise<void> {
    const oldRefs = await ActorReference.getByItem(itemId);

    const { removed, added } = arrayDiff(oldRefs, [...new Set(actorIds)], "actor", (l) => l);

    for (const oldRef of removed) {
      await collections.actorReferences.remove(oldRef._id);
    }

    for (const id of added) {
      const actorRef = new ActorReference(itemId, id, type);
      logger.debug(`Adding actor to ${type}: ${JSON.stringify(actorRef)}`);
      await collections.actorReferences.upsert(actorRef._id, actorRef);
    }
  }

  static async addForItem(
    itemId: string,
    actorIds: string[],
    type: "scene" | "actor" | "marker" | "image"
  ): Promise<void> {
    const oldRefs = await ActorReference.getByItem(itemId);

    const { added } = arrayDiff(oldRefs, [...new Set(actorIds)], "actor", (l) => l);

    for (const id of added) {
      const actorRef = new ActorReference(itemId, id, type);
      logger.debug(`Adding actor to ${type}: ${JSON.stringify(actorRef)}`);
      await collections.actorReferences.upsert(actorRef._id, actorRef);
    }
  }

  static async getById(_id: string): Promise<Actor | null> {
    return collections.actors.get(_id);
  }

  static getBulk(_ids: string[]): Promise<Actor[]> {
    return collections.actors.getBulk(_ids);
  }

  static async getAll(): Promise<Actor[]> {
    return collections.actors.getAll();
  }

  static async getWatches(actor: Actor): Promise<SceneView[]> {
    const scenes = await Scene.getByActor(actor._id);

    return (
      await mapAsync(scenes, (scene) => {
        return SceneView.getByScene(scene._id);
      })
    )
      .flat()
      .sort((a, b) => a.date - b.date);
  }

  static async countUniqueViews(actor: Actor): Promise<number> {
    const scenes = await Scene.getByActor(actor._id);
    const sceneViews = await mapAsync(scenes, (scene) => {
      return SceneView.getCount(scene._id);
    });
    return sceneViews.filter((x) => x > 0).length;
  }

  static calculateScore(actor: Actor, uniqueViews: number, avgRating: number): number {
    const ratingFactor = avgRating / 10;
    const viewScore = uniqueViews * 1.5 * ratingFactor;
    const favScore = +actor.favorite * 10;
    const ratingScore = actor.rating * 5;
    return Math.floor(viewScore + favScore + ratingScore);
  }

  static async getTopActors(skip = 0, take = 0): Promise<(Actor | null)[]> {
    const { items } = await searchActors(null, {
      query: {
        sortBy: "score",
        sortDir: "desc",
        skip,
        take,
      },
    });
    return items;
  }

  constructor(name: string, aliases: string[] = []) {
    this._id = `ac_${generateHash()}`;
    this.name = normalizeName(name);
    this.aliases = normalizeAliases(aliases);
  }

  static async getMovies(actor: Actor): Promise<Movie[]> {
    const scenes = await Scene.getByActor(actor._id);
    const movies = await mapAsync(scenes, Scene.getMovies);
    return createObjectSet(movies.flat(), "_id");
  }

  static async getCollabs(actor: Actor): Promise<
    {
      scene: Scene;
      actors: Actor[];
    }[]
  > {
    const scenes = await Scene.getByActor(actor._id);
    const actorRefs = await ActorReference.getByItemBulk(scenes.map((scene) => scene._id));

    const doGetCollabs = async (sceneId: string) => {
      const actors = (
        await Actor.getBulk(actorRefs[sceneId].map((actorRef) => actorRef.actor))
      ).filter((sceneActor) => sceneActor._id !== actor._id);

      return {
        scene: scenes.find((scene) => scene._id === sceneId) as Scene,
        actors,
      };
    };

    return await mapAsync(Object.keys(actorRefs), doGetCollabs);
  }

  /**
   * Adds the actor's labels to its attached scenes
   *
   * @param actor - the actor
   * @param actorLabels - the actor's labels. Will be applied to scenes if given.
   */
  static async pushLabelsToCurrentScenes(actor: Actor, actorLabels?: string[]): Promise<void> {
    if (!actorLabels?.length) {
      // Prevent looping if there are no labels to add
      return;
    }

    const actorScenes = await Scene.getByActor(actor._id);
    if (!actorScenes.length) {
      logger.debug(`No scenes to update actor "${actor.name}" labels for`);
      return;
    }

    logger.verbose(`Attaching actor "${actor.name}" labels to existing scenes`);

    for (const scene of actorScenes) {
      await Scene.addLabels(scene, actorLabels);
    }

    try {
      clearCaches();
      await indexScenes(actorScenes);
    } catch (error) {
      logger.error(error);
    }

    logger.verbose(`Updated labels of all actor "${actor.name}"'s scenes`);
  }

  /**
   * Attaches the actor and its labels to all matching scenes that it
   * isn't already attached to
   *
   * @param actor - the actor
   * @param actorLabels - the actor's labels. Will be applied to scenes if given.
   */
  static async findUnmatchedScenes(actor: Actor, actorLabels?: string[]): Promise<void> {
    const config = getConfig();

    // Prevent looping on scenes if we know it'll never be matched
    if (
      config.matching.matcher.options.ignoreSingleNames &&
      !ignoreSingleNames([actor.name]).length
    ) {
      return;
    }

    const localExtractActors = await buildExtractor(
      () => [actor],
      (actor) => [actor.name, ...actor.aliases],
      false
    );
    const matchedScenes: Scene[] = [];

    logger.verbose(`Attaching actor "${actor.name}" labels to scenes`);
    logger.verbose(`Attaching actor "${actor.name}" to unmatched scenes`);

    for await (const scene of collections.scenes.iterate()) {
      if (localExtractActors(scene.path || scene.name).includes(actor._id)) {
        logger.debug(`Found scene "${scene.name}"`);
        matchedScenes.push(scene);

        if (actorLabels?.length) {
          logger.debug(`Adding ${actorLabels.length} actor labels to scene`);
          await Scene.addLabels(scene, actorLabels);
        }

        await Scene.addActors(scene, [actor._id]);
      }
    }

    logger.verbose(`Attached actor "${actor.name}" to ${matchedScenes.length} scenes`);

    try {
      clearCaches();
      await indexScenes(matchedScenes);
    } catch (error) {
      handleError("Error indexing scenes", error);
    }

    logger.debug(
      `Added actor "${actor.name}" ${actorLabels?.length ? "with" : "without"
      } labels to scenes: ${JSON.stringify(
        matchedScenes.map((s) => s._id),
        null,
        2
      )}`
    );
  }
}
