import { existsSync } from "node:fs";
import { rename, stat } from "node:fs/promises";
import { basename, dirname, resolve } from "node:path";

import { generatePreview, takeScreenshot } from "../binaries/ffmpeg";
import { getConfig } from "../config";
import { collections } from "../database";
import { extractActors, extractLabels, extractMovies, extractStudios } from "../extractor";
import { ffprobeVideoFile, IDimensions } from "../ffmpeg/ffprobe";
import { getSceneDataSource } from "../graphql/datasources";
import { onSceneCreate } from "../plugins/events/scene";
import { enqueueScene } from "../queue/processing";
import { indexActors } from "../search/actor";
import { indexScenes } from "../search/scene";
import { mapAsync } from "../utils/async";
import { mkdirpAsync, statAsync, unlinkAsync } from "../utils/fs/async";
import { generateHash } from "../utils/hash";
import { handleError, logger } from "../utils/logger";
import { getFolderPartition, libraryPath } from "../utils/path";
import { normalizeName, removeExtension } from "../utils/string";
import { ApplyActorLabelsEnum, ApplyStudioLabelsEnum } from "./../config/schema";
import { FFProbeAudioCodecs, FFProbeContainers, FFProbeVideoCodecs } from "./../ffmpeg/ffprobe";
import Actor from "./actor";
import ActorReference from "./actor_reference";
import Image from "./image";
import Label from "./label";
import Marker from "./marker";
import Movie from "./movie";
import Studio from "./studio";
import SceneView from "./watch";

export function getAverageRating(items: { rating: number }[]): number {
  const filtered = items.filter(({ rating }) => rating);
  if (!filtered.length) {
    return 0;
  }
  const sum = filtered.reduce((sum, { rating }) => sum + rating, 0);
  logger.silly(`Rating sum: ${sum}`);
  const average = sum / filtered.length;
  logger.silly(`${average} average rating`);
  return average;
}

export type ThumbnailFile = {
  name: string;
  path: string;
  size: number;
  time: number;
};

export type ScreenShotOptions = {
  file: string;
  pattern: string;
  count: number;
  thumbnailPath: string;
};

export class SceneMeta {
  size: number | null = null;
  duration: number | null = null;
  dimensions: IDimensions | null = null;
  fps: number | null = null;
  videoCodec: FFProbeVideoCodecs | null = null;
  audioCodec: FFProbeAudioCodecs | null = null;
  container: FFProbeContainers | null = null;
  bitrate: number | null = null;
}

export default class Scene {
  _id: string;
  hash?: string | null; // deprecated
  name: string;
  description: string | null = null;
  addedOn = +new Date();
  releaseDate: number | null = null;
  thumbnail: string | null = null;
  preview: string | null = null;
  previewGrid: string | null | undefined = null;
  favorite = false;
  bookmark: number | null = null;
  rating = 0;
  customFields: Record<string, boolean | string | number | string[] | null> = {};
  path: string | null = null;
  streamLinks: string[] = [];
  meta = new SceneMeta();
  album?: string | null = null;
  studio: string | null = null;
  processed?: boolean = false;

  /**
   * Sets the scene's path if it is not the same as the scene's current path
   * 
   * May move the scene file if the path is not occupied by a file
   */
  static async changePath(scene: Scene, path: string): Promise<Partial<Scene>> {
    const cleanPath = path.trim();

    if (!cleanPath.length) {
      // Clear scene path
      logger.debug(
        `Empty path, setting to null & clearing scene metadata for scene "${scene._id}"`
      );

      return {
        path: null,
        meta: new SceneMeta(),
        processed: false,
        previewGrid: null,
      };
    } else {
      const newPath = resolve(cleanPath);

      if (scene.path === newPath) {
        logger.debug(`Scene "${scene._id}" already has this path: ${newPath}`);
        return {
          path: scene.path,
          meta: scene.meta,
          processed: scene.processed,
          previewGrid: scene.previewGrid,
        }
      }

      if (existsSync(newPath)) {
        if (!scene.path || !existsSync(scene.path)) {
          logger.debug(`Setting path of scene "${scene._id}" to: "${newPath}"`);

          return {
            path: newPath,
            previewGrid: null,
            meta: (await Scene.getVideoFileMetadata(newPath)).meta,
          };
        }

        throw new Error(`Path "${newPath}" already in use, won't overwrite`);
      }

      const otherScene = await Scene.getByPath(newPath);
      if (otherScene) {
        if (otherScene._id !== scene._id) {
          throw new Error("Scene path already in use");
        }
        else {
          logger.debug(`Scene "${scene._id}" already has this path: ${newPath}`);
          return {
            path: newPath,
            meta: scene.meta,
            processed: scene.processed,
            previewGrid: scene.previewGrid,
          };
        }
      }

      if (scene.path) {
        await mkdirpAsync(dirname(scene.path));
        await rename(scene.path, newPath);
      }

      if ((await stat(newPath)).isDirectory()) {
        throw new Error(`"${newPath}" is a directory`);
      }

      logger.debug(`Setting path of scene "${scene._id}" to: "${newPath}"`);

      return {
        path: newPath,
        previewGrid: null,
        meta: (await Scene.getVideoFileMetadata(newPath)).meta,
      };

    }

    return {};
  }

  /*  static async iterate(
     func: (scene: Scene) => void | unknown | Promise<void | unknown>,
     extraFilter: unknown[] = []
   ) {
     return iterate(async () => ({ items: [] }), Scene.getBulk, func, "scene", extraFilter);
   } */

  static calculateScore(scene: Scene, numViews: number): number {
    return numViews + +scene.favorite * 5 + scene.rating;
  }

  /**
   * Extracts metadata from a video file
   */
  static async getVideoFileMetadata(path: string): Promise<{ meta: SceneMeta; raw: unknown }> {
    const { parsed: metadata, raw } = await ffprobeVideoFile(path);

    const meta = new SceneMeta();
    meta.dimensions = metadata.dimensions;
    meta.container = metadata.container;
    meta.videoCodec = metadata.videoCodec;
    meta.audioCodec = metadata.audioCodec;
    meta.fps = metadata.fps;
    meta.bitrate = metadata.bitrate;
    meta.duration = metadata.duration;

    meta.size = (await statAsync(path)).size;

    return { raw, meta };
  }

  static async onImport(videoPath: string, extractInfo = true): Promise<Scene> {
    logger.debug(`Importing "${videoPath}"`);
    const config = getConfig();

    const sceneName = removeExtension(basename(videoPath));
    let scene = new Scene(sceneName);
    scene.path = videoPath;

    if (scene.path) {
      scene.meta = (await Scene.getVideoFileMetadata(scene.path)).meta;
    }

    const sceneActors = [] as string[];
    const sceneLabels = [] as string[];

    let actors = [] as Actor[];

    if (extractInfo && config.matching.extractSceneActorsFromFilepath) {
      // Extract actors
      let extractedActors = [] as string[];
      extractedActors = await extractActors(scene.path);
      sceneActors.push(...extractedActors);

      logger.debug(`Found ${extractedActors.length} actors in scene path.`);

      actors = await Actor.getBulk(extractedActors);

      if (
        config.matching.applyActorLabels.includes(ApplyActorLabelsEnum.enum["event:scene:create"])
      ) {
        logger.debug("Applying actor labels to scene");
        const actors = await Actor.getBulk(extractedActors);
        const actorLabels = (
          await mapAsync(actors, async (actor) => (await Actor.getLabels(actor)).map((l) => l._id))
        ).flat();
        sceneLabels.push(...actorLabels);
      }
    }

    if (extractInfo && config.matching.extractSceneLabelsFromFilepath) {
      // Extract labels
      const extractedLabels = await extractLabels(scene.path);
      sceneLabels.push(...extractedLabels);
      logger.debug(`Found ${extractedLabels.length} labels in scene path.`);
    }

    if (extractInfo && config.matching.extractSceneStudiosFromFilepath) {
      // Extract studio
      const extractedStudio = (await extractStudios(scene.path))[0] || null;
      scene.studio = extractedStudio;

      if (scene.studio) {
        logger.debug("Found studio in scene path");

        if (
          config.matching.applyStudioLabels.includes(
            ApplyStudioLabelsEnum.enum["event:scene:create"]
          )
        ) {
          const studio = await Studio.getById(scene.studio);

          if (studio) {
            logger.debug("Applying studio labels to scene");
            sceneLabels.push(...(await Studio.getLabels(studio)).map((l) => l._id));
          }
        }
      }
    }

    if (extractInfo && config.matching.extractSceneMoviesFromFilepath) {
      // Extract movie
      const extractedMovie = (await extractMovies(scene.path))[0] || null;

      if (extractedMovie) {
        logger.debug("Found movie in scene path");

        const movie = <Movie>await Movie.getById(extractedMovie);
        const scenes = (await Movie.getScenes(movie)).map((sc) => sc._id);
        scenes.push(scene._id);
        await Movie.setScenes(movie, scenes);
        logger.debug("Added scene to movie");
      }
    }

    const pluginResult = await onSceneCreate(scene, sceneLabels, sceneActors);
    scene = pluginResult.scene;

    if (!scene.thumbnail && scene.path) {
      const thumbnail = await Scene.setInitialThumbnail(scene);

      if (thumbnail?.path) {
        const image = new Image(`${sceneName} (thumbnail)`);
        image.path = thumbnail.path;
        image.scene = scene._id;
        image.meta.size = (await stat(thumbnail.path)).size;
        await Image.setLabels(image, sceneLabels);
        await Image.setActors(image, sceneActors);
        logger.debug(`Creating image with id ${image._id}...`);
        await collections.images.upsert(image._id, image);
        scene.thumbnail = image._id;
      }
    }

    logger.debug(`Creating scene with id ${scene._id}...`);
    await Scene.setLabels(scene, sceneLabels);
    await Scene.setActors(scene, sceneActors);
    await collections.scenes.upsert(scene._id, scene);
    await indexScenes([scene]);
    logger.info(`Scene '${scene.name}' created.`);

    await pluginResult.commit();

    if (actors.length) {
      await indexActors(actors);
    }

    logger.verbose(`Queueing ${scene._id} for further processing...`);
    await enqueueScene(scene._id);

    return scene;
  }

  static async watch(scene: Scene, time = Date.now()): Promise<void> {
    logger.debug(`Watch scene ${scene._id}`);
    const watchItem = new SceneView(scene._id, time);
    await collections.views.upsert(watchItem._id, watchItem);
    await indexScenes([scene]);
    await indexActors(await Scene.getActors(scene));
  }

  static async unwatch(scene: Scene): Promise<void> {
    const watches = await SceneView.getByScene(scene._id);
    const last = watches[watches.length - 1];
    if (last) {
      logger.debug(`Remove most recent view of scene ${scene._id}`);
      await collections.views.remove(last._id);
    }
    await indexScenes([scene]);
    await indexActors(await Scene.getActors(scene));
  }

  static async remove(scene: Scene): Promise<void> {
    await collections.scenes.remove(scene._id);

    if (scene.path && existsSync(scene.path)) {
      try {
        await unlinkAsync(scene.path);
      } catch (error) {
        handleError(`Could not delete source file for scene ${scene._id}`, error);
      }
    }

    if (scene.previewGrid && existsSync(scene.previewGrid)) {
      try {
        await unlinkAsync(scene.previewGrid);
      } catch (error) {
        handleError(`Could not delete preview grid for scene ${scene._id}`, error);
      }
    }

  }

  /**
   * Removes the given studio from all images that
   * are associated to the studio
   *
   * @param studioId - id of the studio to remove
   */
  static async filterStudio(studioId: string): Promise<void> {
    const scenes = await Scene.getByStudio(studioId);
    for (const scene of scenes) {
      await collections.scenes.partialUpdate(scene._id, {
        studio: null
      });
    }
    await indexScenes(scenes);
  }

  static async getByActor(id: string): Promise<Scene[]> {
    const references = await ActorReference.getByActor(id);
    return await collections.scenes.getBulk(
      references.filter((r) => r.type === "scene").map((r) => r.item)
    );
  }

  static async getByStudio(id: string): Promise<Scene[]> {
    return collections.scenes.query("studio-index", id);
  }

  static async getByStudios(ids: readonly string[]): Promise<Record<string, Scene[]>> {
    return collections.scenes.queryBulk("studio-index", ids);
  }

  static async getMarkers(scene: Scene): Promise<Marker[]> {
    return Marker.getByScene(scene._id);
  }

  static async getMovies(scene: Scene): Promise<Movie[]> {
    return Movie.getByScene(scene._id);
  }

  static async getActors(scene: Scene): Promise<Actor[]> {
    const actors = await getSceneDataSource().getActorsForScene(scene);
    return actors.sort((a, b) => a.name.localeCompare(b.name));
  }

  static async setActors(scene: Scene, actorIds: string[]): Promise<void> {
    return Actor.setForItem(scene._id, actorIds, "scene");
  }

  static async addActors(scene: Scene, actorIds: string[]): Promise<void> {
    return Actor.addForItem(scene._id, actorIds, "scene");
  }

  static async setLabels(scene: Scene, labelIds: string[]): Promise<void> {
    return Label.setForItem(scene._id, labelIds, "scene");
  }

  static async addLabels(scene: Scene, labelIds: string[]): Promise<void> {
    return Label.addForItem(scene._id, labelIds, "scene");
  }

  static async getLabels(scene: Scene): Promise<Label[]> {
    const labels = await getSceneDataSource().getLabelsForScene(scene);
    return labels.sort((a, b) => a.name.localeCompare(b.name));
  }

  static async getByPath(path: string): Promise<Scene | undefined> {
    const resolved = resolve(path);
    const scenes = await collections.scenes.query("path-index", encodeURIComponent(resolved));
    return scenes[0];
  }

  static async getById(_id: string): Promise<Scene | null> {
    return collections.scenes.get(_id);
  }

  static getBulk(_ids: readonly string[]): Promise<Scene[]> {
    return collections.scenes.getBulk(_ids);
  }

  static async getAll(): Promise<Scene[]> {
    return collections.scenes.getAll();
  }

  constructor(name: string) {
    this._id = `sc_${generateHash()}`;
    this.name = normalizeName(name);
  }

  static async generatePreview(scene: Scene): Promise<string | null> {
    if (!scene.path) {
      logger.warn("No scene path, aborting preview generation.");
      return null;
    }
    if (!scene.meta.duration) {
      logger.warn("No scene duration, aborting preview generation.");
      return null;
    }

    const outFile = resolve(getFolderPartition(libraryPath("previews")), `${scene._id}.jpg`);

    await generatePreview(scene._id, scene.path, scene.meta.duration, outFile);

    return outFile;
  }

  static async setInitialThumbnail(scene: Scene): Promise<Image | null> {
    if (!scene.meta.duration) {
      logger.warn("No scene duration, aborting preview generation.");
      return null;
    }

    const secs = scene.meta.duration / 2;
    return Scene.setThumbnailToSeconds(scene, secs);
  }

  static async setThumbnailToSeconds(scene: Scene, timestampSecs: number): Promise<Image | null> {
    if (!scene.path) {
      logger.debug("No scene path.");
      return null;
    }

    const image = new Image(`${scene.name} (thumbnail)`);
    const imagePath = resolve(
      getFolderPartition(libraryPath("thumbnails/scenes")),
      `${image._id}.jpg`
    );
    image.path = imagePath;
    image.scene = scene._id;

    logger.debug("Generating screenshot for scene");
    await takeScreenshot(scene.path, timestampSecs * 1000, imagePath);
    logger.debug("Screenshot done");

    await collections.images.upsert(image._id, image);

    /* const actors = (await Scene.getActors(scene)).map((l) => l._id);
    await Image.setActors(image, actors);

    const labels = (await Scene.getLabels(scene)).map((l) => l._id);
    await Image.setLabels(image, labels); */

    scene.thumbnail = image._id;
    await collections.scenes.upsert(scene._id, scene);

    return image;
  }
}
