import { collections } from "../database/index";
import { generateHash } from "../utils/hash";

export default class ActorReference {
  _id: string;
  item: string;
  actor: string;
  type: "scene" | "actor" | "marker" | "image";

  constructor(item: string, actor: string, type: "scene" | "actor" | "marker" | "image") {
    this._id = `ar_${generateHash()}`;
    this.item = item;
    this.actor = actor;
    this.type = type;
  }

  static async getAll(): Promise<ActorReference[]> {
    return collections.actorReferences.getAll();
  }

  static async getByActor(actorId: string): Promise<ActorReference[]> {
    return collections.actorReferences.query("actor-index", actorId);
  }

  static async getByItem(itemId: string): Promise<ActorReference[]> {
    return collections.actorReferences.query("item-index", itemId);
  }

  static async getByItemBulk(items: readonly string[]): Promise<Record<string, ActorReference[]>> {
    return collections.actorReferences.queryBulk("item-index", items);
  }

  static async get(from: string, to: string): Promise<ActorReference | undefined> {
    const fromReferences = await collections.actorReferences.query("item-index", from);
    return fromReferences.find((r) => r.actor === to);
  }

  static async removeByActor(actorId: string): Promise<void> {
    await collections.actorReferences.deleteByQuery("actor-index", actorId);
  }

  static async removeByItem(itemId: string): Promise<void> {
    await collections.actorReferences.deleteByQuery("item-index", itemId);
  }

  static async removeById(_id: string): Promise<void> {
    await collections.actorReferences.remove(_id);
  }
}
