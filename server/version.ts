import { readFileSync } from "node:fs";

export const version = (JSON.parse(readFileSync("package.json", "utf-8")) as { version: string })
  .version;
