import { createWriteStream, existsSync } from "node:fs";
import { Stream } from "node:stream";

import axios from "axios";

import { logger } from "../utils/logger";

export async function downloadFile(url: string, file: string): Promise<void> {
  if (existsSync(file)) {
    return;
  }

  logger.info(`Downloading ${url} to ${file}`);

  const response = await axios({
    url: url,
    method: "GET",
    responseType: "stream",
  });
  const stream = response.data as Stream;

  const writer = createWriteStream(file);
  stream.pipe(writer);

  await new Promise((resolve, reject) => {
    writer.on("finish", resolve);
    writer.on("error", (err) => {
      logger.error(`Error while downloading ${url}`);
      reject(err);
    });
  });
}
