import { IConfig } from "../config/schema";

export function protocol(config: IConfig): "https" | "http" {
  return config.server.https.enable ? "https" : "http";
}
