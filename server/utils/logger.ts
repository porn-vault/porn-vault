import "winston-daily-rotate-file";

import express from "express";
import winston from "winston";

import { getConfig } from "../config";
import { configPath } from "./path";
import { isString } from "./types";

export function formatMessage(message: unknown) {
  if (message instanceof Error) {
    return message.message;
  }
  return isString(message) ? message : JSON.stringify(message, null, 2);
}

let logger = createVaultLogger(process.env.PV_LOG_LEVEL || "info", []);

export function handleError(message: string, error: unknown, bail = false) {
  logger.error(`${bail ? "FATAL: " : ""}${message}: ${formatMessage(error)}`);

  if (error instanceof Error) {
    logger.debug(error.stack);
  }

  if (bail) {
    process.exit(1);
  }
}

function fileTransports(items: { level: string; prefix: string; silent: boolean }[]) {
  return items.map(({ level, prefix, silent }) => createFileTransport(level, prefix, silent));
}

function createFileTransport(level: string, prefix = "", silent: boolean) {
  const config = getConfig();

  return new winston.transports.DailyRotateFile({
    filename: `${prefix}pv-%DATE%`,
    datePattern: "YYYY-MM-DD-HH",
    maxSize: config.log.maxSize,
    maxFiles: config.log.maxFiles,
    level,
    extension: ".log",
    silent,
    dirname: configPath("logs"),
    auditFile: configPath("logs", `${prefix}pv_audit.json`),
    format: winston.format.combine(
      winston.format.timestamp(),
      winston.format.printf(({ level, message, timestamp }) => {
        const msg = formatMessage(message);
        return `${timestamp as string} [vault] ${level}: ${msg}`;
      })
    ),
  });
}

export function createVaultLogger(
  consoleLevel: string,
  files: { level: string; prefix: string; silent: boolean }[]
) {
  return winston.createLogger({
    transports: [
      new winston.transports.Console({
        level: consoleLevel,
        format: winston.format.combine(
          winston.format.colorize(),
          winston.format.timestamp(),
          winston.format.printf(({ level, message, timestamp }) => {
            const msg = formatMessage(message);
            return `${timestamp as string} [vault] ${level}: ${msg}`;
          })
        ),
      }),
      ...fileTransports(files),
    ],
  });
}

export function setLogger(_logger: winston.Logger) {
  logger.debug("Setting logger");
  logger = _logger;
}

export { logger };

export function createPluginLogger(
  name: string,
  files: { level: string; prefix: string; silent: boolean }[],
  logLevel?: string
) {
  const config = getConfig();
  const level = logLevel || config.log.level;

  logger.debug(`Creating plugin logger: ${name} with level ${logLevel}`);

  return winston.createLogger({
    format: winston.format.combine(
      winston.format.colorize(),
      winston.format.timestamp(),
      winston.format.printf(({ level, message, timestamp }) => {
        const msg = isString(message) ? message : JSON.stringify(message, null, 2);
        return `${<string>timestamp} [plugin:${name}] ${level}: ${msg}`;
      })
    ),
    transports: [
      new winston.transports.Console({
        level,
      }),
      ...fileTransports(files),
    ],
  });
}

export const httpLog = (
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
): void => {
  logger.http(`${req.method} ${req.path}`);
  next();
};
