import { mkdirSync } from "node:fs";
import { copyFile, mkdir,readdir, readFile, rm, stat, unlink, writeFile } from "node:fs/promises";

export const statAsync = stat;
export const unlinkAsync = unlink;
export const readdirAsync = readdir;
export const readFileAsync = readFile;
export const writeFileAsync = writeFile;
export const copyFileAsync = copyFile;
export const rmAsync = rm;

export async function rimrafAsync(path: string): Promise<void> {
  await rm(path, { recursive: true });
}

export async function mkdirpAsync(path: string) {
  await mkdir(path, { recursive: true });
}

export function mkdirpSync(path: string) {
  mkdirSync(path, { recursive: true })!;
}
