export type Dictionary<T> = Record<string, T>;

export function isString(x: unknown): x is string {
  return typeof x === "string";
}

export function isNumber(x: unknown): x is number {
  return typeof x === "number";
}

export function isBoolean(x: unknown): x is boolean {
  return typeof x === "boolean";
}

export type DeepPartial<T> = {
  [P in keyof T]?: DeepPartial<T[P]>;
};
