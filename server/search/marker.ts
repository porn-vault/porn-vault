import { indexes,MarkerSearchDoc } from "../search_new";
import Marker from "../types/marker";
import Scene from "../types/scene";
import Studio from "../types/studio";
import { mapAsync } from "../utils/async";
import {
  getActorNames,
} from "./common";
import { buildIndex, indexItems, ProgressCallback } from "./internal/buildIndex";

export async function createMarkerSearchDoc(marker: Marker): Promise<MarkerSearchDoc> {
  const labels = await Marker.getLabels(marker);
  const scene = await Scene.getById(marker.scene);
  const actors = await Marker.getActors(marker);

  const studio = scene?.studio ? await Studio.getById(scene.studio) : null;
  const parentStudios = studio ? await Studio.getParents(studio) : [];

  return {
    _id: marker._id,
    addedOn: marker.addedOn,
    name: marker.name,
    rawName: marker.name,
    actors: actors.map((a) => a._id),
    actorNames: [...new Set(actors.map(getActorNames).flat())],
    labels: labels.map((l) => l._id),
    labelNames: labels.map((l) => l.name),
    numLabels: labels.length,
    scene: scene ? scene._id : "",
    sceneName: scene ? scene.name : "",
    rating: marker.rating,
    bookmark: marker.bookmark ?? 0,
    favorite: marker.favorite,
    custom: marker.customFields,
    numActors: actors.length,
    studios: studio ? [studio, ...parentStudios].map((s) => s._id) : [],
    studioNames: studio
      ? [...new Set([studio, ...parentStudios].flatMap((s) => [s.name, ...(s.aliases ?? [])]))]
      : [],
  };
}

async function addMarkerSearchDocs(docs: MarkerSearchDoc[]): Promise<void> {
  await indexes.markers.indexItems(
    docs
  );
}

export async function removeMarker(markerId: string): Promise<void> {
  await indexes.markers.removeItem(markerId);
}

export async function removeMarkers(markerIds: string[]): Promise<void> {
  // TODO: bulk delete
  await mapAsync(markerIds, removeMarker);
}

export async function indexMarkers(
  markers: Marker[],
  progressCb?: ProgressCallback
): Promise<number> {
  return indexItems(markers, createMarkerSearchDoc, addMarkerSearchDocs, progressCb);
}

export async function buildMarkerIndex(): Promise<void> {
  await buildIndex(indexes.markers.name, Marker.getAll, indexMarkers);
}

export interface IMarkerSearchQuery {
  query: string;
  favorite?: boolean;
  bookmark?: boolean;
  rating: number;
  include?: string[];
  exclude?: string[];
  sortBy?: string;
  sortDir?: string;
  skip?: number;
  take?: number;
  page?: number;
  // TODO: actors, studios

  rawQuery?: unknown;
}
