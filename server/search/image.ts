import asyncPool from "tiny-async-pool";

import { ImageSearchDoc, indexes } from "../search_new";
import Image from "../types/image";
import Movie from "../types/movie";
import Scene from "../types/scene";
import Studio from "../types/studio";
import {
  getActorNames,
} from "./common";
import { buildIndex, ProgressCallback } from "./internal/buildIndex";

const blacklist = [
  "(altThumbnail)",
  "(alt. thumbnail)",
  "(logo)",
  "(thumbnail)",
  "(preview)",
  "(front cover)",
  "(back cover)",
  "(spine cover)",
  "(hero)",
  "(hero image)",
  "(avatar)",
];

export function isBlacklisted(name: string): boolean {
  return blacklist.some((ending) => name.endsWith(ending));
}

export const sliceArray =
  (size: number) =>
    <T>(arr: T[], cb: (value: T[], index: number, arr: T[]) => unknown): void => {
      let index = 0;
      let slice = arr.slice(index, index + size);
      while (slice.length) {
        const result = cb(slice, index, arr);
        if (result) break;
        index += size;
        slice = arr.slice(index, index + size);
      }
    };

export const getSlices =
  (size: number) =>
    <T>(arr: T[]): T[][] => {
      const slices = [] as T[][];
      sliceArray(size)(arr, (slice) => {
        slices.push(slice);
      });
      return slices;
    };

export async function indexImages(images: Image[], progressCb?: ProgressCallback): Promise<number> {
  if (!images.length) {
    return 0;
  }
  let indexedImageCount = 0;
  const slices = getSlices(2500)(images);

  for await (const _ of asyncPool(4, slices, async (slice) => {
    const docs = [] as ImageSearchDoc[];

    for await (const _ of asyncPool(16, slice, async (image) => {
      if (!isBlacklisted(image.name)) {
        docs.push(await createImageSearchDoc(image));
      }
    })) {
    }
    await addImageSearchDocs(docs);
    indexedImageCount += slice.length;
    if (progressCb) {
      progressCb({
        indexedCount: indexedImageCount,
        totalToIndexCount: images.length,
      });
    }
  })) {
  }

  return indexedImageCount;
}

async function addImageSearchDocs(docs: ImageSearchDoc[]): Promise<void> {
  await indexes.images.indexItems(
    docs
  );
}

export async function buildImageIndex(): Promise<void> {
  await buildIndex(indexes.images.name, Image.getAll, indexImages);
}

export async function createImageSearchDoc(image: Image): Promise<ImageSearchDoc> {
  const labels = await Image.getLabels(image);
  const actors = await Image.getActors(image);
  const scene = image.scene ? await Scene.getById(image.scene) : null;

  const studio = image.studio ? await Studio.getById(image.studio) : null;
  const parentStudios = studio ? await Studio.getParents(studio) : [];

  const movies = scene ? await Movie.getByScene(scene._id) : [];

  return {
    _id: image._id,
    addedOn: image.addedOn,
    name: image.name,
    rawName: image.name,
    labels: labels.map((l) => l._id),
    actors: actors.map((a) => a._id),
    actorNames: [...new Set(actors.map(getActorNames).flat())],
    labelNames: labels.map((l) => l.name),
    numLabels: labels.length,
    rating: image.rating || 0,
    bookmark: image.bookmark ?? 0,
    favorite: image.favorite,
    scene: image.scene ?? "",
    sceneName: scene ? scene.name : "",
    movies: movies.map((x) => x._id),
    movieNames: movies.map((x) => x.name),
    studios: studio ? [studio, ...parentStudios].map((s) => s._id) : [],
    studioNames: studio ? [...new Set([studio, ...parentStudios].map((s) => s.name))] : [],
    custom: image.customFields,
    numActors: actors.length,
    path: image.path ?? "",
    album: "",
    albumName: "",
  };
}

export interface IImageSearchQuery {
  query: string;
  favorite?: boolean;
  bookmark?: boolean;
  rating: number;
  include?: string[];
  exclude?: string[];
  studios?: string[];
  actors?: string[];
  scenes?: string[];
  movies?: string[];
  sortBy?: string;
  sortDir?: string;
  skip?: number;
  take?: number;
  page?: number;
  emptyField?: string;

  rawQuery?: unknown;
}
