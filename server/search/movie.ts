import { indexes,MovieSearchDoc } from "../search_new";
import Movie from "../types/movie";
import Studio from "../types/studio";
import { mapAsync } from "../utils/async";
import {
  getActorNames,
} from "./common";
import { buildIndex, indexItems, ProgressCallback } from "./internal/buildIndex";

export async function createMovieSearchDoc(movie: Movie): Promise<MovieSearchDoc> {
  const labels = await Movie.getLabels(movie);
  const actors = await Movie.getActors(movie);
  const scenes = await Movie.getScenes(movie);

  const studio = movie.studio ? await Studio.getById(movie.studio) : null;
  const parentStudios = studio ? await Studio.getParents(studio) : [];

  return {
    _id: movie._id,
    addedOn: movie.addedOn,
    name: movie.name,
    rawName: movie.name,
    labels: labels.map((l) => l._id),
    actors: actors.map((a) => a._id),
    actorNames: actors.map(getActorNames).flat(),
    labelNames: labels.map((l) => [l.name]).flat(),
    numLabels: labels.length,
    studios: studio ? [studio, ...parentStudios].map((s) => s._id) : [],
    studioNames: studio
      ? [...new Set([studio, ...parentStudios].flatMap((s) => [s.name, ...(s.aliases ?? [])]))]
      : [],
    rating: await Movie.getRating(movie),
    bookmark: movie.bookmark ?? 0,
    favorite: movie.favorite,
    duration: Math.floor(await Movie.calculateDuration(movie)),
    releaseDate: movie.releaseDate ?? 0,
    releaseYear: movie.releaseDate ? new Date(movie.releaseDate).getFullYear() : 0,
    numScenes: scenes.length,
    custom: movie.customFields,
    numActors: actors.length,
    size: Math.floor(scenes.map(x => x.meta?.size ?? 0).reduce((sum, x) => sum + x, 0)),
  };
}

async function addMovieSearchDocs(docs: MovieSearchDoc[]): Promise<void> {
  await indexes.movies.indexItems(
    docs
  );
}

export async function removeMovie(movieId: string): Promise<void> {
  await indexes.movies.removeItem(movieId);
}

export async function removeMovies(movieIds: string[]): Promise<void> {
  // TODO: bulk delete
  await mapAsync(movieIds, removeMovie);
}

export async function indexMovies(movies: Movie[], progressCb?: ProgressCallback): Promise<number> {
  return indexItems(movies, createMovieSearchDoc, addMovieSearchDocs, progressCb);
}

export async function buildMovieIndex(): Promise<void> {
  await buildIndex(indexes.movies.name, Movie.getAll, indexMovies);
}

export interface IMovieSearchQuery {
  query: string;
  favorite?: boolean;
  bookmark?: boolean;
  rating: number;
  include?: string[];
  exclude?: string[];
  studios?: string[];
  actors?: string[];
  sortBy?: string;
  sortDir?: string;
  skip?: number;
  take?: number;
  page?: number;
  durationMin?: number;
  durationMax?: number;

  rawQuery?: unknown;
}
