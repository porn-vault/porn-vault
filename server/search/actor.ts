import { indexes } from "../search_new";
import { ActorSearchDoc } from "../search_new/mappings/actor";
import Actor from "../types/actor";
import { getNationality } from "../types/countries";
import Scene from "../types/scene";
import Studio from "../types/studio";
import { mapAsync } from "../utils/async";
import { logger } from "../utils/logger";
import {
  CustomFieldFilter,
  normalizeAliasesForSearch,
} from "./common";
import { buildIndex, indexItems, ProgressCallback } from "./internal/buildIndex";

const PLACEHOLDER_LETTER = "$";

function getFirstLetter(actor: Actor): string {
  const firstChar = actor.name.at(0)?.toUpperCase() || PLACEHOLDER_LETTER;

  if (/^[A-Z]$/i.test(firstChar)) {
    return firstChar;
  }
  return PLACEHOLDER_LETTER;
}

export async function createActorSearchDoc(actor: Actor): Promise<ActorSearchDoc> {
  const labels = await Actor.getLabels(actor);

  const watches = await Actor.getWatches(actor);
  const numScenes = (await Scene.getByActor(actor._id)).length;

  const nationality = actor.nationality ? getNationality(actor.nationality) : null;

  const baseStudios = await Actor.getStudioFeatures(actor);
  const studios = (await mapAsync(baseStudios, Studio.getParents)).flat();

  const avgRating = await Actor.getAverageRating(actor);
  const uniqueViews = await Actor.countUniqueViews(actor);

  return {
    _id: actor._id,
    addedOn: actor.addedOn,
    name: actor.name,
    letter: getFirstLetter(actor),
    rawName: actor.name,
    aliases: normalizeAliasesForSearch(actor.aliases),
    labels: labels.map((l) => l._id),
    numLabels: labels.length,
    labelNames: labels.map((l) => l.name),
    score: Math.floor(Actor.calculateScore(actor, uniqueViews, avgRating)),
    rating: actor.rating,
    averageRating: Math.floor(avgRating),
    bookmark: actor.bookmark ?? 0,
    favorite: actor.favorite,
    numViews: watches.length,
    lastViewedOn: watches.sort((a, b) => b.date - a.date)[0]?.date || 0,
    bornOn: actor.bornOn ?? 0,
    numScenes,
    nationalityName: nationality ? nationality.nationality : "",
    countryCode: nationality ? nationality.alpha2 : "",
    custom: actor.customFields,
    studios: [...new Set(studios.map((st) => st._id))],
    studioNames: [...new Set(studios.map((st) => st.name))],
  };
}

export async function removeActor(actorId: string): Promise<void> {
  await indexes.actors.removeItem(actorId);
}

export async function removeActors(actorIds: string[]): Promise<void> {
  // TODO: bulk delete
  await mapAsync(actorIds, removeActor);
}

export async function indexActors(actors: Actor[], progressCb?: ProgressCallback): Promise<number> {
  logger.verbose(`Indexing ${actors.length} actors`);
  return indexItems(actors, createActorSearchDoc, addActorSearchDocs, progressCb);
}

async function addActorSearchDocs(docs: ActorSearchDoc[]): Promise<void> {
  await indexes.actors.indexItems(
    docs
  );
}

export async function buildActorIndex(): Promise<void> {
  await buildIndex(indexes.actors.name, Actor.getAll, indexActors);
}

export interface IActorSearchQuery {
  query: string;
  letter?: string;
  favorite?: boolean;
  bookmark?: boolean;
  rating: number;
  include?: string[];
  exclude?: string[];
  nationality?: string;
  sortBy?: string;
  sortDir?: string;
  skip?: number;
  take?: number;
  page?: number;
  studios?: string[];
  custom?: CustomFieldFilter[];

  rawQuery?: unknown; // TODO:
}
