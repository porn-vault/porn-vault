import { logger } from "../../utils/logger";

const DEFAULT_INDEX_SLICE_SIZE = 5000;

export type ProgressCallback = (progressCb: {
  indexedCount: number;
  totalToIndexCount: number;
}) => void;

export async function indexItems<CollectionType, IndexItemType>(
  items: CollectionType[],
  createSearchDoc: (item: CollectionType) => Promise<IndexItemType>,
  addSearchDocs: (indexItems: IndexItemType[]) => Promise<void>,
  progressCb?: ProgressCallback
): Promise<number> {
  let docsToIndex: IndexItemType[] = [];
  let indexedItemCount = 0;

  async function doAddSearchDocs() {
    await addSearchDocs(docsToIndex);
    indexedItemCount += docsToIndex.length;
    docsToIndex = [];
    if (progressCb) {
      progressCb({
        indexedCount: indexedItemCount,
        totalToIndexCount: items.length,
      });
    }
  }

  for (const item of items) {
    docsToIndex.push(await createSearchDoc(item));

    if (docsToIndex.length === DEFAULT_INDEX_SLICE_SIZE) {
      await doAddSearchDocs();
    }
  }
  if (docsToIndex.length) {
    await doAddSearchDocs();
  }
  return indexedItemCount;
}

export async function buildIndex<CollectionType>(
  indexName: string,
  getAllCollectionItems: () => Promise<CollectionType[]>,
  indexer: (
    collectionObjs: CollectionType[],
    progressCb?: ProgressCallback
  ) => Promise<number>
): Promise<void> {
  const timeNow = Date.now();

  logger.info(`Building ${indexName}`);

  const allCollectionItems = await getAllCollectionItems();

  const indexedCount = await indexer(
    allCollectionItems,
  );

  logger.info(
    `Index build of ${indexName} done in ${(Date.now() - timeNow) / 1000}s.`
  );
  logger.debug(`Index ${indexName} size: ${indexedCount} items`);
}
