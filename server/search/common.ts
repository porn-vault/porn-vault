import Actor from "../types/actor";

export type BaseMapping = {
  id: string;
  addedOn: number;
  name: string;
  rawName: string;
};

export interface ISearchResults {
  items: string[];
  total: number;
  numPages: number;
}

export type CustomFieldFilter = {
  id: string;
  op: "gt" | "lt" | "term" | "match" | "wildcard";
  value: unknown;
};

/* export function buildCustomFilter(filters?: CustomFieldFilter[]): unknown[] {
  if (!filters) {
    return [];
  }

  return filters.map(({ op, id, value }) => {
    if (op === "lt" || op === "gt") {
      return {
        range: {
          [`custom.${id}`]: {
            [op]: value,
          },
        },
      };
    }

    if (op === "wildcard") {
      return {
        wildcard: {
          [`custom.${id}`]: `*${<string>value}*`,
        },
      };
    }

    return {
      [op]: {
        [`custom.${id}`]: value,
      },
    };
  });
} */

export const DEFAULT_PAGE_SIZE = 24;

export function getActorNames(actor: Actor): string[] {
  return [...new Set([actor.name, ...normalizeAliasesForSearch(actor.aliases)])];
}

export function normalizeAliasesForSearch(aliases: string[]): string[] {
  return aliases.filter((alias) => !alias.startsWith("regex:"));
}

// TODO: reuse
/* export function sort(sortBy?: string, sortDir?: string, query?: string): Record<string, unknown> {
  if (sortBy === "relevance" && !query) {
    return {
      sort: { addedOn: "desc" },
    };
  }
  if (sortBy && sortBy !== "relevance") {
    return {
      sort: {
        [sortBy]: sortDir || "desc",
      },
    };
  }
  return {};
} */

export function getPageSize(take?: number): number {
  return take || DEFAULT_PAGE_SIZE;
}

export function getPage(
  page?: number,
  skip?: number,
  take?: number
): { from: number; size: number } {
  const pageSize = getPageSize(take);
  return {
    from: skip || Math.max(0, +(page || 0) * pageSize),
    size: pageSize,
  };
}
