import * as fs from "node:fs";

import { createRequestHandler } from "@remix-run/express";
import { broadcastDevReady, installGlobals } from "@remix-run/node";
import chokidar from "chokidar";
import compression from "compression";
import express, { json, type NextFunction, type Request, type Response } from "express";

import { locales } from "../locale";
import { mountGraphqlServer } from "./graphql/server";
import apiRouter from "./routes/api";
import { startupVault } from "./startup";
import { httpLog, logger } from "./utils/logger";

installGlobals();

const BUILD_PATH = "../build/index.js";

/**
 * @type { import('@remix-run/node').ServerBuild | Promise<import('@remix-run/node').ServerBuild> }
 */
let build = await import(BUILD_PATH);

const app = express();

app.use(compression());
app.use(json());

// Remix fingerprints its assets so we can cache forever.
app.use("/build", express.static("public/build", { immutable: true, maxAge: "1y" }));

// Everything else (like favicon.ico) is cached for an hour. You may want to be
// more aggressive with this caching.
app.use(express.static("public", { maxAge: "1h" }));

await startupVault(app);

app.use(httpLog);

app.use("/api", apiRouter);
await mountGraphqlServer(app);

logger.debug(`Supported languages:`);
logger.debug(locales.join(", "));

app.all(
  "*",
  (req, res, next) => {
    if (!locales.some((x) => req.originalUrl.startsWith(`/${x}`))) {
      res.redirect(`/en${req.originalUrl}`);
    } else {
      next();
    }
  },
  process.env.NODE_ENV === "development"
    ? createDevRequestHandler()
    : createRequestHandler({
        build,
        mode: process.env.NODE_ENV,
      })
);

const port = process.env.PORT || 3000;
app.listen(port, async () => {
  console.log(`Express server listening on port ${port}`);

  if (process.env.NODE_ENV === "development") {
    broadcastDevReady(build);
  }
});

function createDevRequestHandler() {
  const watcher = chokidar.watch("./build/index.js", {
    ignoreInitial: true,
    awaitWriteFinish: {
      stabilityThreshold: 500,
    },
  });

  watcher.on("all", async () => {
    logger.verbose("Reloading Remix build");

    // 1. purge require cache && load updated server build
    const stat = fs.statSync("./build/index.js");
    build = import(BUILD_PATH + "?t=" + stat.mtimeMs);
    // 2. tell dev server that this app server is now ready
    broadcastDevReady(await build);
  });

  return async (req: Request, res: Response, next: NextFunction) => {
    try {
      //
      return createRequestHandler({
        build: await build,
        mode: "development",
      })(req, res, next);
    } catch (error) {
      next(error);
    }
  };
}
