import { SelectableLabel } from "~/components/LabelDropdownChoice";

import { IActor } from "./actor";
import ILabel from "./label";
import { IScene } from "./scene";

export interface IImage {
  _id: string;
  name: string;
  path?: string;
  favorite: boolean;
  bookmark: boolean;
  rating: number;
  actors: IActor[];
  scene?: IScene;
  labels: SelectableLabel[];
  meta: {
    dimensions?: {
      width: number;
      height: number;
    };
    size?: number;
  };
}
