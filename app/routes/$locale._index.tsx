import { Masonry } from "masonic";
import { useState } from "react";
import { useTranslations } from "use-intl";

import { useMount } from "~/composables/use_mounted";

import PageWrapper from "../components/PageWrapper";
import FavoritesCard from "../components/widgets/FavoritesCard";
import LibraryTimeCard from "../components/widgets/LibraryTimeCard";
import ProcessingCard from "../components/widgets/ProcessingCard";
import ScanCard from "../components/widgets/ScanCard";
import StatsCard from "../components/widgets/StatsCard";

const widgets = [
  <StatsCard />,
  <ProcessingCard />,
  <ScanCard />,
  <LibraryTimeCard />,
  <FavoritesCard />,
];

export default function IndexPage() {
  const t = useTranslations();

  const [showComponent, setShowComponent] = useState(false);

  useMount(() => {
    setShowComponent(true);
  });

  return (
    <PageWrapper title={t("heading.overview")}>
      {/* NOTE: only render on client */}
      {showComponent && (
        <Masonry
          items={widgets}
          rowGutter={5}
          columnGutter={5}
          render={({ data }) => data}
          columnWidth={300}
        />
      )}
    </PageWrapper>
  );
}
