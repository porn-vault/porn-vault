import { LoaderArgs } from "@remix-run/node";
import { useLoaderData, useLocation, useNavigate, useRevalidator } from "@remix-run/react";
import { useMemo } from "react";

import ActorList from "~/components/ActorList";
import AutoLayout from "~/components/AutoLayout";
import BookmarkIconButton from "~/components/BookmarkIconButton";
import Card from "~/components/Card";
import CardTitle from "~/components/CardTitle";
import FavoriteIconButton from "~/components/FavoriteIconButton";
import Flex from "~/components/Flex";
import LocaleLink from "~/components/Link";
import PageWrapper from "~/components/PageWrapper";
import Pagination from "~/components/Pagination";
import Rating from "~/components/Rating";
import Spacer from "~/components/Spacer";
import { useSafeMode } from "~/composables/use_safe_mode";
import { useSettings } from "~/composables/use_settings";
import { sceneCardFragment } from "~/fragments/scene";
import { IScene } from "~/types/scene";
import { graphqlQuery } from "~/util/gql";
import { bookmarkScene, favoriteScene, rateScene } from "~/util/mutations/scene";
import { thumbnailUrl } from "~/util/thumbnail";

export const loader = async ({ request }: LoaderArgs) => {
  const url = new URL(request.url);

  const q = `
  query ($page: Int) {
    numSceneViews
    getWatches(page: $page) {
      date
      scene {
        ...SceneCard
      }
    }
  }
  ${sceneCardFragment}
  `;

  const page = Number(url.searchParams.get("page") ?? 1) - 1;

  const { numSceneViews, getWatches } = await graphqlQuery<{
    numSceneViews: number;
    getWatches: {
      date: number;
      scene: IScene;
    }[];
  }>(q, {
    page,
  });

  return {
    page,
    numSceneViews,
    views: getWatches.filter((x) => !!x.scene),
  };
};

function ViewHistoryItem(scene: IScene): JSX.Element {
  const { revalidate } = useRevalidator();
  const { blur: safeModeBlur } = useSafeMode();
  const { sceneImageAspect } = useSettings();

  return <Card padless layout="h" innerClassName="!gap-3 !items-stretch">
    <LocaleLink className="shrink-0 w-36" to={`/scene/${scene._id}`}>
      <img
        style={{
          filter: safeModeBlur,
          aspectRatio: sceneImageAspect.cssValue
        }}
        className="object-cover hover:scale-[1.1] transition-all rounded-lg w-full" src={scene.thumbnail?._id && thumbnailUrl(scene.thumbnail._id)}
      />
    </LocaleLink>
    <div className="truncate pr-2 pt-2 flex flex-col grow gap-1">
      <Flex className="truncate gap-1 font-semibold" layout="h">
        {scene.studio && (
          <LocaleLink to={`/studio/${scene.studio._id}`}>
            <div className="text-xs font-semibold uppercase text-gray-600 transition-colors hover:text-gray-400 dark:text-gray-500 dark:hover:text-gray-600">
              {scene.studio.name}
            </div>
          </LocaleLink>
        )}
        <Spacer />
        {scene.releaseDate && (
          <div className="text-xs text-gray-600 dark:text-gray-400">
            {new Date(scene.releaseDate).toLocaleDateString()}
          </div>
        )}
      </Flex>
      <div className="truncate ellipsis overflow-hidden whitespace-nowrap font-semibold">{scene.name}</div>
      {!!scene.actors.length && <ActorList actors={scene.actors} />}
      <div className="flex gap-2 items-center justify-between">
        <Rating
          onChange={async (rating) => {
            await rateScene(scene._id, rating);
            revalidate();
          }}
          value={scene.rating || 0}
        />
        <div className="flex items-center gap-1">
          <FavoriteIconButton
            value={scene.favorite}
            onClick={async () => {
              const newValue = !scene.favorite;
              await favoriteScene(scene._id, newValue);
              revalidate();
            }}
          />
          <BookmarkIconButton
            value={scene.bookmark}
            onClick={async () => {
              const newValue = scene.bookmark ? null : new Date();
              await bookmarkScene(scene._id, newValue);
              revalidate();
            }}
          />
        </div>
      </div>
    </div>
  </Card>
}

export default function ViewHistoryPage() {
  const location = useLocation();
  const navigate = useNavigate();
  const { page, views, numSceneViews } = useLoaderData<typeof loader>();

  const numPages = Math.ceil(numSceneViews / 24);

  const grouped = useMemo(() => {
    const obj: Record<string, { date: Date, scenes: typeof views[0]["scene"][] }> = {}

    for (const view of views) {
      const date = new Date(view.date);
      const formatted = `${date.getFullYear()}-${date.getMonth()}-${date.getDate()}`

      if (obj[formatted]) {
        obj[formatted].scenes.push(view.scene);
      }
      else {
        obj[formatted] = {
          date,
          scenes: [view.scene]
        };
      }
    }
    return Object.values(obj);
  }, [views]);

  return (
    <PageWrapper title="View history">
      <AutoLayout className="max-w-sm w-full mx-auto" gap={10}>
        <div className="flex items-center">
          <CardTitle>View history</CardTitle>
        </div>
        <div className="flex justify-center">
          <Pagination
            numPages={numPages}
            current={page}
            onChange={(x) => {
              navigate(`${location.pathname}?page=${x + 1}`);
            }}
          />
        </div>
        <div className="flex flex-col gap-4">
          {
            grouped.map(({ date, scenes }) => <div className="flex flex-col gap-1">
              <div className="text-sm dark:text-gray-50 font-medium opacity-60">{date.toLocaleDateString()}</div>
              {scenes.map((scene) => (
                <ViewHistoryItem key={scene._id} {...scene} />
              ))}
            </div>)
          }
        </div>
        <div className="flex justify-center">
          <Pagination
            numPages={numPages}
            current={page}
            onChange={(x) => {
              navigate(`${location.pathname}?page=${x + 1}`);
            }}
          />
        </div>
      </AutoLayout>
    </PageWrapper>
  );
}
