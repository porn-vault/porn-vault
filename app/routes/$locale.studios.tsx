import PaginatedStudioList, { DEFAULT_QUERY } from "../components/PaginatedStudioList";
import { createLocalStorageQueryManager } from "../util/persistent_query";

const storageKey = "studios_search_query";
const persistentQuery = createLocalStorageQueryManager<typeof DEFAULT_QUERY>(storageKey);

export default function StudioListPage() {
  return (
    <PaginatedStudioList withCreator mergeQuery={{}} persistentQuery={persistentQuery} writeHead />
  );
}
