import { LoaderArgs } from "@remix-run/node";
import { useLoaderData, useRevalidator } from "@remix-run/react";
import { useTranslations } from "use-intl";

import AutoLayout from "~/components/AutoLayout";
import CardTitle from "~/components/CardTitle";
import { IMarker } from "~/types/marker";
import { IPaginationResult } from "~/types/pagination";

import ActorCard from "../components/ActorCard";
import ListContainer from "../components/ListContainer";
import MarkerCard from "../components/MarkerCard";
import MovieCard from "../components/MovieCard";
import PageWrapper from "../components/PageWrapper";
import SceneCard from "../components/SceneCard";
import { actorCardFragment } from "../fragments/actor";
import { markerCardFragment } from "../fragments/marker";
import { movieCardFragment } from "../fragments/movie";
import { sceneCardFragment } from "../fragments/scene";
import { IActor } from "../types/actor";
import { IMovie } from "../types/movie";
import { IScene } from "../types/scene";
import { graphqlQuery } from "../util/gql";

async function searchAll(query: string) {
  const q = `
  query($sc: SceneSearchQuery!, $ac: ActorSearchQuery!, $mo: MovieSearchQuery!, $ma: MarkerSearchQuery!) {
    getScenes(query: $sc) {
      items {
        ...SceneCard
      }
      numItems
    }
    getActors(query: $ac) {
      items {
        ...ActorCard
      }
      numItems
    }
    getMovies(query: $mo) {
      items {
        ...MovieCard
      }
      numItems
    }
    getMarkers(query: $ma) {
      items {
        ...MarkerCard
      }
      numItems
    }
  }

  ${actorCardFragment}
  ${sceneCardFragment}
  ${movieCardFragment}
  ${markerCardFragment}
`;

  const data = await graphqlQuery<{
    getScenes: IPaginationResult<IScene>;
    getActors: IPaginationResult<IActor>;
    getMovies: IPaginationResult<IMovie>;
    getMarkers: IPaginationResult<IMarker>;
  }>(q, {
    sc: {
      query,
      take: 10,
    },
    ac: {
      query,
      take: 10,
    },
    mo: {
      query,
      take: 10,
    },
    ma: {
      query,
      take: 10,
    },
  });

  return {
    sceneResult: data.getScenes,
    actorResult: data.getActors,
    movieResult: data.getMovies,
    markerResult: data.getMarkers,
  };
}

export const loader = async ({ request }: LoaderArgs) => {
  const { searchParams } = new URL(request.url);

  const q = searchParams.get("q") ?? "";
  const result = await searchAll(q);

  return {
    ...result,
    q,
  };
};

export default function SearchPage() {
  const t = useTranslations();
  const { actorResult, movieResult, sceneResult, markerResult, q } = useLoaderData<typeof loader>();
  const { revalidate } = useRevalidator();

  const highlight = q.trim().split(" ");

  return (
    <PageWrapper title="Search results">
      <AutoLayout>
        {/* Actors */}
        <AutoLayout gap={5}>
          <CardTitle>{t("foundActors", { numItems: actorResult.numItems })}</CardTitle>
          <ListContainer>
            {actorResult.items.map((actor) => (
              <ActorCard
                highlight={highlight}
                onBookmark={() => { }}
                onFav={() => { }}
                onRate={() => { }}
                actor={actor}
                key={actor._id}
              />
            ))}
          </ListContainer>
        </AutoLayout>
        {/* Movies */}
        <AutoLayout gap={5}>
          <CardTitle>{t("foundMovies", { numItems: movieResult.numItems })}</CardTitle>
          <ListContainer>
            {movieResult.items.map((movie) => (
              <MovieCard
                highlight={highlight}
                onBookmark={revalidate}
                onFav={revalidate}
                key={movie._id}
                movie={movie}
              />
            ))}
          </ListContainer>
        </AutoLayout>
        {/* Scenes */}
        <AutoLayout gap={5}>
          <CardTitle>{t("foundScenes", { numItems: sceneResult.numItems })}</CardTitle>
          <ListContainer>
            {sceneResult.items.map((scene) => (
              <SceneCard
                highlight={highlight}
                onBookmark={revalidate}
                onFav={revalidate}
                onRate={revalidate}
                key={scene._id}
                scene={scene}
              />
            ))}
          </ListContainer>
        </AutoLayout>
        {/* Markers */}
        <AutoLayout gap={5}>
          <CardTitle>{t("foundMarkers", { numItems: markerResult.numItems })}</CardTitle>
          <ListContainer>
            {markerResult.items.map((marker) => (
              <MarkerCard
                onBookmark={revalidate}
                onFav={revalidate}
                onRate={revalidate}
                key={marker._id}
                marker={marker}
                onDelete={revalidate}
              />
            ))}
          </ListContainer>
        </AutoLayout>
      </AutoLayout>
    </PageWrapper>
  );
}
