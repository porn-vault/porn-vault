import clsx from "clsx";
import { ReactNode } from "react";
import useSWR from "swr";
import { useTranslations } from "use-intl";

import AutoLayout from "../components/AutoLayout";
import Button from "../components/Button";
import Card from "../components/Card";
import CardSection from "../components/CardSection";
import CardTitle from "../components/CardTitle";
import Credits from "../components/Credits";
import PageWrapper from "../components/PageWrapper";
import DisplaySettings from "../components/settings/DisplaySettings";
import IzzyTable from "../components/settings/IzzyTable";
import Text from "../components/Text";
import { useScanStatus } from "../composables/use_scan_status";
import { getFullStatus, StatusData } from "../util/status";

function SettingsSection({ title, children }: { title: string; children: ReactNode }) {
  return (
    <AutoLayout gap={10}>
      <CardTitle>{title}</CardTitle>
      <Card>{children}</Card>
    </AutoLayout>
  );
}

function StatusSection({ status }: { status: StatusData }) {
  const t = useTranslations();

  return (
    <>
      <SettingsSection title="Izzy (database)">
        <CardSection title={t("status")}>
          <Text>{status.izzy.status}</Text>
        </CardSection>
        <CardSection title={t("version")}>
          <Text>{status.izzy.version}</Text>
        </CardSection>
        <CardSection scrollable title="Collections">
          <IzzyTable collections={status.izzy.collections} />
        </CardSection>
      </SettingsSection>
    </>
  );
}

export default function SettingsPage() {
  const t = useTranslations();

  const { data: status } = useSWR("settings:status", getFullStatus, {
    revalidateOnFocus: true,
    revalidateOnReconnect: true,
    revalidateOnMount: true,
    refreshInterval: 15_000,
  });

  const { scanStatus } = useScanStatus();

  return (
    <PageWrapper title={t("heading.settings")}>
      <div className="flex justify-center">
        <AutoLayout className="max-w-3xl w-full">
          <AutoLayout>
            <SettingsSection title={t("heading.displaySettings")}>
              <DisplaySettings />
            </SettingsSection>
            {!!scanStatus && (
              <SettingsSection title={t("heading.scanFolders")}>
                <CardSection scrollable title={t("heading.videos")}>
                  <table className="w-full overflow-hidden border border-gray-200 dark:border-gray-800">
                    <thead className="text-sm uppercase text-gray-800 dark:text-gray-200">
                      <tr>
                        <th className="py-2 px-3">Path</th>
                        <th className="py-2 px-3">Include</th>
                        <th className="py-2 px-3">Exclude</th>
                        <th className="py-2 px-3">Extensions</th>
                        <th className="py-2 px-3">Enabled</th>
                      </tr>
                    </thead>
                    <tbody className="text-gray-700 dark:text-gray-400">
                      {scanStatus.folders?.videos.map((folder, index) => (
                        <tr
                          className={clsx("text-sm", {
                            "bg-gray-200 dark:bg-gray-800": index % 2 === 0,
                          })}
                          key={folder.path}
                        >
                          <td className="py-2 px-3">{folder.path}</td>
                          <td className="py-2 px-3">{folder.include.join(", ") || "-"}</td>
                          <td className="py-2 px-3">{folder.exclude.join(", ") || "-"}</td>
                          <td className="py-2 px-3">{folder.extensions.join(", ") || "-"}</td>
                          <td className="py-2 px-3">{folder.enable ? "Yes" : "No"}</td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </CardSection>
                <CardSection scrollable title={t("heading.images")}>
                  <table className="w-full overflow-hidden border border-gray-200 dark:border-gray-800">
                    <thead className="text-sm uppercase text-gray-800 dark:text-gray-200">
                      <tr>
                        <th className="py-2 px-3 text-left">Path</th>
                        <th className="py-2 px-3 text-left">Include</th>
                        <th className="py-2 px-3 text-left">Exclude</th>
                        <th className="py-2 px-3 text-left">Extensions</th>
                        <th className="py-2 px-3 text-left">Enabled</th>
                      </tr>
                    </thead>
                    <tbody className="text-gray-700 dark:text-gray-400">
                      {scanStatus.folders?.images.map((folder, index) => (
                        <tr
                          className={clsx({
                            "text-sm bg-gray-200 dark:bg-gray-800": index % 2 === 0,
                          })}
                          key={folder.path}
                        >
                          <td className="py-2 px-3 text-left">{folder.path}</td>
                          <td className="py-2 px-3 text-left">
                            {folder.include.join(", ") || "-"}
                          </td>
                          <td className="py-2 px-3 text-left">
                            {folder.exclude.join(", ") || "-"}
                          </td>
                          <td className="py-2 px-3 text-left">
                            {folder.extensions.join(", ") || "-"}
                          </td>
                          <td className="py-2 px-3 text-left">{folder.enable ? "Yes" : "No"}</td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </CardSection>
              </SettingsSection>
            )}
            {!!status && <StatusSection status={status} />}
            <SettingsSection title="Search index">
              <Button
                onClick={() => {
                  if (window.confirm("Are you sure? (this action is non-destructive)")) {
                    fetch("/api/system/reindex", {
                      method: "POST",
                    })
                      .then(() => window.location.reload())
                      .catch((error) => {
                        console.error(error);
                      });
                  }
                }}
              >
                Reindex
              </Button>
            </SettingsSection>
            <SettingsSection title="Caches">
              <Button
                onClick={() => {
                  if (window.confirm("Are you sure? (this action is non-destructive)")) {
                    fetch("/api/system/cache", {
                      method: "DELETE",
                    })
                      .then(() => {
                        alert("Caches deleted");
                      })
                      .catch((error) => {
                        console.error(error);
                      });
                  }
                }}
              >
                Clear caches
              </Button>
            </SettingsSection>

            <SettingsSection title="Credits">
              <Credits />
            </SettingsSection>
          </AutoLayout>
        </AutoLayout>
      </div>
    </PageWrapper>
  );
}
