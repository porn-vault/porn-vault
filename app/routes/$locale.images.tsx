import PaginatedImageList, { DEFAULT_QUERY } from "../components/PaginatedImageList";
import { createLocalStorageQueryManager } from "../util/persistent_query";

const storageKey = "images_search_query";
const persistentQuery = createLocalStorageQueryManager<typeof DEFAULT_QUERY>(storageKey);

export default function ImageListPage() {
  return (
    <PaginatedImageList withUploader writeHead persistentQuery={persistentQuery} mergeQuery={{}} />
  );
}
