import PaginatedMovieList, { DEFAULT_QUERY } from "../components/PaginatedMovieList";
import { createLocalStorageQueryManager } from "../util/persistent_query";

const storageKey = "movies_search_query";
const persistentQuery = createLocalStorageQueryManager<typeof DEFAULT_QUERY>(storageKey);

export default function MoviesPage() {
  return (
    <PaginatedMovieList withCreator writeHead persistentQuery={persistentQuery} mergeQuery={{}} />
  );
}
