import { LoaderArgs, redirect } from "@remix-run/node";
import { useLoaderData, useRevalidator } from "@remix-run/react";
import DeleteIcon from "mdi-react/DeleteIcon.js";
import prettyBytes from "pretty-bytes";
import { useState } from "react";
import { useTranslations } from "use-intl";

import ActorCard from "~/components/ActorCard";
import BookmarkIconButton from "~/components/BookmarkIconButton";
import Button from "~/components/Button";
import FavoriteIconButton from "~/components/FavoriteIconButton";
import IconButton from "~/components/IconButton";
import ListWrapper from "~/components/ListWrapper";
import MovieDetailsImageList from "~/components/movie_details/ImageList";
import MovieEditor from "~/components/movie_details/MovieEditor";
import SimilarMoviesList from "~/components/movie_details/SimilarMoviesList";
import SceneCard from "~/components/SceneCard";
import Tabs from "~/components/Tabs";
import Text from "~/components/Text";
import { useLocaleNavigate } from "~/composables/use_navigate";

import AutoLayout from "../components/AutoLayout";
import Card from "../components/Card";
import CardSection from "../components/CardSection";
import CardTitle from "../components/CardTitle";
import Description from "../components/Description";
import FileInput from "../components/FileInput";
import LabelGroup from "../components/LabelGroup";
import LocaleLink from "../components/Link";
import PageWrapper from "../components/PageWrapper";
import Rating from "../components/Rating";
import ResponsiveImage from "../components/ResponsiveImage";
import Spacer from "../components/Spacer";
import { actorCardFragment } from "../fragments/actor";
import { sceneCardFragment } from "../fragments/scene";
import { IMovie } from "../types/movie";
import { graphqlQuery } from "../util/gql";
import { uploadImage } from "../util/mutations/image";
import {
  bookmarkMovie,
  favoriteMovie,
  removeMovie,
  runMoviePlugins,
  setMovieBackCover,
  setMovieFrontCover,
  setMovieSpine,
} from "../util/mutations/movie";
import { formatDuration } from "../util/string";
import { imageUrl } from "../util/thumbnail";

export const loader = async ({ params }: LoaderArgs) => {
  const q = `
  query ($id: String!) {
    getMovieById(id: $id) {
      _id
      addedOn
      name
      description
      releaseDate
      duration
      size
      favorite
      bookmark
      rating
      frontCover {
        _id
        color
      }
      backCover {
        _id
        color
      }
      spineCover {
        _id
      }
      actors {
        ...ActorCard
      }
      labels {
        _id
        name
        color
      }
      studio {
        _id
        name
        thumbnail {
          _id
        }
      }
      scenes {
        ...SceneCard
      }
    }
  }
  ${sceneCardFragment}
  ${actorCardFragment}
  `;

  const { getMovieById } = await graphqlQuery<{
    getMovieById: IMovie;
  }>(q, {
    id: params.id,
  });

  if (!getMovieById) {
    throw redirect("/movies");
  }

  return {
    movie: getMovieById,
  };
};

type TabState = "scenes" | "actors" | "images" | "similar";

const MAX_HEIGHT = 400;

export default function MoviePage() {
  const t = useTranslations();
  const { movie } = useLoaderData<typeof loader>();

  const { revalidate } = useRevalidator();
  const navigate = useLocaleNavigate();
  const [activeTab, setActiveTab] = useState<TabState>("scenes");

  const [pluginLoader, setPluginLoader] = useState(false);

  async function toggleFav(): Promise<void> {
    const newValue = !movie.favorite;
    await favoriteMovie(movie._id, newValue);
    revalidate();
  }

  async function toggleBookmark(): Promise<void> {
    const newValue = movie.bookmark ? null : new Date();
    await bookmarkMovie(movie._id, newValue);
    revalidate();
  }

  async function handleRunMoviePlugins() {
    try {
      setPluginLoader(true);
      await runMoviePlugins(movie._id);
      revalidate();
    } catch (error) {
      console.error(error);
    } finally {
      setPluginLoader(false);
    }
  }

  return (
    <PageWrapper key={movie._id} title={movie.name}>
      <AutoLayout>
        <div
          style={{
            flexWrap: "wrap",
            display: "flex",
            justifyContent: "center",
            gap: 5,
          }}
        >
          {movie.spineCover && (
            <img
              style={{ borderRadius: 5, maxHeight: MAX_HEIGHT }}
              src={imageUrl(movie.spineCover._id)}
              alt={`${movie.name} DVD spine`}
            />
          )}
          {!movie.spineCover && (
            <div
              style={{
                background: `repeating-linear-gradient(
                  45deg,
                  #a0a0a005,
                  #a0a0a005 10px,
                  #a0a0a010 10px,
                  #a0a0a010 20px
                )`,
                border: "2px solid #a0a0a020",
                position: "relative",
                width: 128,
                maxHeight: MAX_HEIGHT,
                height: MAX_HEIGHT,
              }}
            >
              <div
                style={{
                  left: "50%",
                  top: "50%",
                  transform: "translate(-50%, -50%)",
                  position: "absolute",
                }}
              >
                <FileInput
                  onChange={async ([file]) => {
                    /* TODO: don't show as image */
                    const img = await uploadImage({
                      blob: file,
                      name: `${movie.name} (spine cover)`,
                    });
                    await setMovieSpine(movie._id, img._id);
                    revalidate();
                  }}
                  accept={[".png", ".jpg", ".jpeg", ".webp"]}
                >
                  Add spine
                </FileInput>
              </div>
            </div>
          )}
          {!movie.frontCover && (
            <div
              style={{
                background: `repeating-linear-gradient(
                45deg,
                #a0a0a005,
                #a0a0a005 10px,
                #a0a0a010 10px,
                #a0a0a010 20px
              )`,
                border: "2px solid #a0a0a020",
                position: "relative",
                aspectRatio: "0.71",
                maxHeight: MAX_HEIGHT,
                height: MAX_HEIGHT,
              }}
            >
              <div
                style={{
                  left: "50%",
                  top: "50%",
                  transform: "translate(-50%, -50%)",
                  position: "absolute",
                }}
              >
                <FileInput
                  onChange={async ([file]) => {
                    /* TODO: don't show as image */
                    const img = await uploadImage({
                      blob: file,
                      name: `${movie.name} (front cover)`,
                    });
                    await setMovieFrontCover(movie._id, img._id);
                    revalidate();
                  }}
                  accept={[".png", ".jpg", ".jpeg", ".webp"]}
                >
                  Add front cover
                </FileInput>
              </div>
            </div>
          )}
          {movie.frontCover && (
            <ResponsiveImage
              src={imageUrl(movie.frontCover._id)}
              aspectRatio="0.71"
              imgStyle={{ height: MAX_HEIGHT }}
            />
          )}
          {!movie.backCover && (
            <div
              style={{
                background: `repeating-linear-gradient(
                45deg,
                #a0a0a005,
                #a0a0a005 10px,
                #a0a0a010 10px,
                #a0a0a010 20px
              )`,
                border: "2px solid #a0a0a020",
                position: "relative",
                aspectRatio: "0.71",
                maxHeight: MAX_HEIGHT,
                height: MAX_HEIGHT,
              }}
            >
              <div
                style={{
                  left: "50%",
                  top: "50%",
                  transform: "translate(-50%, -50%)",
                  position: "absolute",
                }}
              >
                <FileInput
                  onChange={async ([file]) => {
                    /* TODO: don't show as image */
                    const img = await uploadImage({
                      blob: file,
                      name: `${movie.name} (back cover)`,
                    });
                    await setMovieBackCover(movie._id, img._id);
                    revalidate();
                  }}
                  accept={[".png", ".jpg", ".jpeg", ".webp"]}
                >
                  Add back cover
                </FileInput>
              </div>
            </div>
          )}
          {movie.backCover && (
            <ResponsiveImage
              src={imageUrl(movie.backCover._id)}
              aspectRatio="0.71"
              imgStyle={{ height: MAX_HEIGHT }}
            />
          )}
        </div>
        <div className="flex justify-center">
          <AutoLayout className="w-full max-w-[1000px] p-3">
            <Card>
              <AutoLayout gap={10} layout="h">
                <FavoriteIconButton value={movie.favorite} onClick={toggleFav} />
                <BookmarkIconButton value={movie.bookmark} onClick={toggleBookmark} />
                <Spacer />
                <MovieEditor movie={movie} onEdit={revalidate} />
                <IconButton
                  onClick={async () => {
                    if (confirm("Really delete?")) {
                      await removeMovie(movie._id);
                      navigate("/movies");
                    }
                  }}
                  Icon={DeleteIcon}
                />
              </AutoLayout>
            </Card>
            <Card>
              <CardTitle>{t("general")}</CardTitle>
              <div
                className="grid gap-3"
                style={{
                  gridTemplateColumns: "repeat(auto-fit, minmax(250px, 1fr))",
                }}
              >
                <AutoLayout>
                  <CardSection title={t("title")}>
                    <Text className="text-sm">{movie.name}</Text>
                  </CardSection>
                  {!!movie.studio && (
                    <CardSection title="Studio">
                      {movie.studio.thumbnail ? (
                        <div className="flex">
                          <LocaleLink className="hover" to={`/studio/${movie.studio._id}`}>
                            <img
                              className="max-h-[120px] max-w-[240px] object-cover"
                              // NOTE: cannot use /thumbnail because studio logos are often transparent => png
                              src={
                                movie.studio.thumbnail?._id &&
                                `/api/media/image/${movie.studio.thumbnail?._id}?password=xxx`
                              }
                              alt={`${movie.studio.name}`}
                            />
                          </LocaleLink>
                        </div>
                      ) : (
                        <Text className="text-sm">
                          <LocaleLink className="underline" to={`/studio/${movie.studio._id}`}>
                            {movie.studio.name}
                          </LocaleLink>
                        </Text>
                      )}
                    </CardSection>
                  )}
                  {movie.releaseDate && (
                    <CardSection title={t("releaseDate")}>
                      <Text>{new Date(movie.releaseDate).toLocaleDateString()}</Text>
                    </CardSection>
                  )}
                  {movie.description && (
                    <CardSection title={t("description")}>
                      <Description>{movie.description}</Description>
                    </CardSection>
                  )}
                  <CardSection title={t("rating")}>
                    <Text className="text-xs mb-2">Calculated using scene ratings.</Text>
                    <Rating readonly value={movie.rating}></Rating>
                  </CardSection>
                  {!!movie.labels.length && (
                    <CardSection title={t("heading.labels", { numItems: 2 })}>
                      <LabelGroup limit={999} value={movie.labels} />
                    </CardSection>
                  )}
                </AutoLayout>
                <AutoLayout>
                  <CardSection title={t("addedToLibrary")}>
                    <Text className="text-sm">{new Date(movie.addedOn).toLocaleString()}</Text>
                  </CardSection>
                  <CardSection title={t("videoDuration")}>
                    <Text className="text-sm">{formatDuration(movie.duration)}</Text>
                  </CardSection>
                  <CardSection title={t("fileSize")}>
                    <Text className="text-sm">{prettyBytes(movie.size)}</Text>
                  </CardSection>
                  <Button
                    loading={pluginLoader}
                    onClick={handleRunMoviePlugins}
                  >
                    Run plugins
                  </Button>
                </AutoLayout>
              </div>
            </Card>
            <Tabs
              states={["scenes", "actors", "images", "similar"] as TabState[]}
              value={activeTab}
              onChange={setActiveTab}
              formatTitle={(value) => {
                return {
                  scenes: t("heading.scenes"),
                  actors: t("heading.actors"),
                  images: t("heading.images"),
                  similar: t("heading.similarMovies"),
                }[value];
              }}
            />
            {activeTab === "scenes" && (
              <>
                <ListWrapper loading={false} noResults={!movie.scenes.length}>
                  {movie.scenes.map((scene) => (
                    <SceneCard
                      onFav={revalidate}
                      onBookmark={revalidate}
                      onRate={revalidate}
                      key={scene._id}
                      scene={scene}
                    />
                  ))}
                </ListWrapper>
              </>
            )}
            {activeTab === "actors" && (
              <>
                <ListWrapper loading={false} noResults={!movie.actors.length}>
                  {movie.actors.map((actor) => (
                    <ActorCard
                      onFav={revalidate}
                      onBookmark={revalidate}
                      onRate={revalidate}
                      key={actor._id}
                      actor={actor}
                    />
                  ))}
                </ListWrapper>
              </>
            )}
            {activeTab === "similar" && <SimilarMoviesList movieId={movie._id} />}
            {activeTab === "images" && <MovieDetailsImageList movieId={movie._id} />}
          </AutoLayout>
        </div>
      </AutoLayout>
    </PageWrapper>
  );
}
