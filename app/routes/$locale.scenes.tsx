import PaginatedSceneList, { DEFAULT_QUERY } from "~/components/PaginatedSceneList";
import { createLocalStorageQueryManager } from "~/util/persistent_query";

const storageKey = "scenes_search_query";
const persistentQuery = createLocalStorageQueryManager<typeof DEFAULT_QUERY>(storageKey);

export default function SceneListPage() {
  return <PaginatedSceneList writeHead persistentQuery={persistentQuery} mergeQuery={{}} />;
}
