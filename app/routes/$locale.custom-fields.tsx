import { LoaderArgs } from "@remix-run/node";
import { useLoaderData, useRevalidator } from "@remix-run/react";
import { useTranslations } from "use-intl";

import AutoLayout from "~/components/AutoLayout";
import Button from "~/components/Button";
import CardTitle from "~/components/CardTitle";
import ContentWrapper from "~/components/ContentWrapper";
import CustomFieldCreator from "~/components/CustomFieldCreator";
import CustomFieldListItem from "~/components/CustomFieldListItem";
import Description from "~/components/Description";
import Loader from "~/components/Loader";
import Spacer from "~/components/Spacer";
import { graphqlQuery } from "~/util/gql";

import type CustomField from "../../server/types/custom_field";
import PageWrapper from "../components/PageWrapper";

export const loader = async ({ params }: LoaderArgs) => {
  const data = await graphqlQuery<{
    getCustomFields: CustomField[];
  }>(q, {});

  return data;
};

const q = `{
  getCustomFields {
    _id
    name
    type
    values
    unit
    target
  }
}`;

export default function CustomFieldsPage() {
  const t = useTranslations();

  const data = useLoaderData<typeof loader>();
  const { revalidate, state } = useRevalidator();

  const loading = state === "loading";

  return (
    <PageWrapper title={t("heading.customFields")}>
      <div className="flex justify-center">
        <AutoLayout className="max-w-3xl w-full mx-auto">
          <CardTitle>{t("heading.customFields")}</CardTitle>
          <Description>
            Custom fields allow you to attach custom metadata to entities. They work orthogonally to
            labels, allowing more refined search queries and more detailed content libraries. They
            may also be used by external tools to tag content for processing.
          </Description>
          <div className="flex ">
            <CustomFieldCreator onCreate={revalidate} />
            <Spacer />
            <Button loading={loading} onClick={revalidate}>
              {t("action.refresh")}
            </Button>
          </div>
          <ContentWrapper
            loader={
              <div className="flex justify-center">
                <Loader />
              </div>
            }
            loading={loading}
            noResults={data.getCustomFields.length === 0}
          >
            <div className="shadow-lg rounded-xl overflow-hidden">
              {data.getCustomFields.map((field, index) => (
                <CustomFieldListItem
                  key={field._id}
                  id={field._id}
                  index={index}
                  onEdit={revalidate}
                  onDelete={revalidate}
                  {...field}
                />
              ))}
            </div>
          </ContentWrapper>
        </AutoLayout>
      </div>
    </PageWrapper>
  );
}
