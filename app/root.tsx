import { createCookie, LinksFunction, LoaderArgs } from "@remix-run/node";
import {
  Links,
  LiveReload,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration,
  useLoaderData,
  useNavigation,
} from "@remix-run/react";
import dayjs from "dayjs";
import Cookies from "js-cookie";
import nprogress from "nprogress";
import nprogressCss from "nprogress/nprogress.css";
import { useEffect, useState } from "react";
import racCss from "react-advanced-cropper/dist/style.css";
import { IntlProvider } from "use-intl";

import Layout from "./components/Layout/Layout";
import SettingsContextProvider from "./components/SettingsContextProvider";
import VideoContextProvider from "./components/VideoContextProvider";
import { Theme, ThemeContext } from "./composables/use_dark_theme";
import { useMount } from "./composables/use_mounted";
import { SafeModeContext } from "./composables/use_safe_mode";
import stylesheet from "./global.css";
import { getMessages } from "./intl";

export const links: LinksFunction = () => [
  { rel: "shortcut icon", href: "/favicon.png" },
  { rel: "stylesheet", href: stylesheet },
  { rel: "stylesheet", href: racCss },
  { rel: "stylesheet", href: nprogressCss },
];

export async function loader({ params, request }: LoaderArgs) {
  const themeMatch = (request.headers.get("Cookie") ?? "theme=light").match(/theme=(light|dark)/);

  return {
    theme: (themeMatch?.[1] || "light") as Theme,
    locale: params.locale!,
    messages: getMessages(params.locale!),
  };
}

export default function App() {
  const { locale, messages, theme: initialTheme } = useLoaderData<typeof loader>();
  const navigation = useNavigation();

  const [theme, setTheme] = useState<Theme>(initialTheme);
  const [safeMode, setSafeMode] = useState(false);

  function toggleTheme(): void {
    const nextTheme = theme === "light" ? "dark" : "light";
    setTheme(nextTheme);
    Cookies.set("theme", nextTheme, {
      expires: dayjs().add(100, "years").toDate(),
      secure: false,
    });
  }

  function toggleSafeMode(): void {
    const nextMode = !safeMode;
    setSafeMode(nextMode);
    localStorage.setItem("safeMode", String(nextMode));
  }

  useEffect(() => {
    if (navigation.state === "loading") {
      nprogress.start();
    } else {
      nprogress.done();
    }
  }, [navigation.state]);

  useMount(() => {
    nprogress.configure({
      trickleSpeed: 50,
    });

    const safeModeLocalStorage = localStorage.getItem("safeMode");
    if (["true", "false"].includes(safeModeLocalStorage!)) {
      setSafeMode(safeModeLocalStorage === "true");
    }
  });

  return (
    <html lang="en" className={theme}>
      <head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1" />
        <Meta />
        <Links />
      </head>
      <body className="bg-gray-50 dark:bg-gray-950">
        <IntlProvider locale={locale} messages={messages}>
          <SettingsContextProvider>
            <ThemeContext.Provider value={{ theme, toggle: toggleTheme }}>
              <SafeModeContext.Provider value={{ enabled: safeMode, toggle: toggleSafeMode }}>
                <VideoContextProvider>
                  <Layout>
                    <Outlet />
                  </Layout>
                </VideoContextProvider>
              </SafeModeContext.Provider>
            </ThemeContext.Provider>
          </SettingsContextProvider>
        </IntlProvider>
        <ScrollRestoration />
        <Scripts />
        <LiveReload />
      </body>
    </html>
  );
}
