import { readFileSync } from "node:fs";
import { resolve } from "node:path";

import { locales } from "../locale";

const messages = locales
  .map((locale) => [locale, JSON.parse(readFileSync(resolve(`./locale/${locale}.json`), "utf-8"))])
  .reduce((obj, [key, msgs]) => {
    obj[key] = msgs;
    return obj;
  }, {} as Record<string, any>);

export function getMessages(locale: string): any {
  return messages[locale];
}
