import { useState } from "react";
import { useTranslations } from "use-intl";

import { useWindow } from "../composables/use_window";
import { graphqlQuery } from "../util/gql";
import Button from "./Button";
import InputError from "./InputError";
import Subheading from "./Subheading";
import TextInput from "./TextInput";
import Window from "./Window";

export async function createMovie(name: string, sceneIds: string[]) {
  const query = `
  mutation ($name: String!, $scenes: [String!]!) {
    addMovie(name: $name, scenes: $scenes) {
      _id
    }
  }
        `;

  await graphqlQuery(query, {
    name,
    scenes: sceneIds,
  });
}

type Props = {
  onCreate: () => void;
};

export default function MovieCreator({ onCreate }: Props) {
  const t = useTranslations();
  const { isOpen, close, open } = useWindow();

  const [error, setError] = useState<string | undefined>();
  const [name, setName] = useState("");

  const [loading, setLoader] = useState(false);

  function reset() {
    setName("");
    setError(undefined);
  }

  function doClose() {
    reset();
    close();
  }

  return (
    <>
      <Button onClick={open}>+ {t("action.addMovie")}</Button>
      <Window
        onClose={doClose}
        isOpen={isOpen}
        title={t("action.addMovie")}
        actions={
          <>
            <Button
              disabled={!name.length}
              loading={loading}
              onClick={async () => {
                try {
                  setLoader(true);
                  await createMovie(name, []);
                  onCreate();
                  doClose();
                } catch (error) {
                  if (error instanceof Error) {
                    setError(error.message);
                  } else {
                    setError("An error occurred");
                  }
                }
                setLoader(false);
              }}
            >
              Create
            </Button>
            <Button secondary onClick={doClose}>
              Close
            </Button>
          </>
        }
      >
        <div>
          <Subheading>Movie name *</Subheading>
          <TextInput
            className="w-full"
            value={name}
            onChange={setName}
            placeholder="Enter a movie name"
          />
          {error && <InputError message={error} />}
        </div>
      </Window>
    </>
  );
}
