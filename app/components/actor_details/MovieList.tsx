import { createSessionStorageQueryManager } from "../../util/persistent_query";
import PaginatedMovieList, { DEFAULT_QUERY } from "../PaginatedMovieList";

type Props = {
  actorId: string;
};

export default function MovieList(props: Props) {
  const storageKey = `actor_details_${props.actorId}_movies_query`;
  const persistentQuery = createSessionStorageQueryManager<typeof DEFAULT_QUERY>(storageKey);

  return (
    <PaginatedMovieList
      padless
      mergeQuery={{
        actors: [props.actorId],
      }}
      persistentQuery={persistentQuery}
    />
  );
}
