import { useRevalidator } from "@remix-run/react";
import { useEffect, useState } from "react";
import { useTranslations } from "use-intl";

import Button from "../../components/Button";
import {
  bookmarkActor,
  favoriteActor,
  rateActor,
  runActorPlugins,
} from "../../util/mutations/actor";
import ActorImagesEditor from "../ActorImagesEditor";
import AutoLayout from "../AutoLayout";
import Avatar from "../Avatar";
import BookmarkIconButton from "../BookmarkIconButton";
import FavoriteIconButton from "../FavoriteIconButton";
import Flag from "../Flag";
import Rating from "../Rating";
import ExternalLink from "./ExternalLink";

type Props = {
  actorId: string;
  avatarId?: string;
  nationality?: {
    name: string;
    alias?: string;
    alpha2: string;
    nationality: string;
  };
  actorName: string;
  age?: number;
  bornOn?: number;
  favorite: boolean;
  bookmark: boolean;
  rating: number;
  links?: { text: string; url: string }[];
};

export default function ActorProfile(props: Props) {
  const t = useTranslations();
  const { revalidate } = useRevalidator();

  const [favorite, setFavorite] = useState(props.favorite);
  const [bookmark, setBookmark] = useState(props.bookmark);
  const [rating, setRating] = useState(props.rating);
  const [pluginLoader, setPluginLoader] = useState(false);

  useEffect(() => {
    setFavorite(props.favorite);
  }, [props.favorite]);

  useEffect(() => {
    setBookmark(props.bookmark);
  }, [props.bookmark]);

  useEffect(() => {
    setRating(props.rating);
  }, [props.rating]);

  async function changeRating(rating: number): Promise<void> {
    await rateActor(props.actorId, rating);
    setRating(rating);
  }

  function triggerPlugins() {
    setPluginLoader(true);

    runActorPlugins(props.actorId)
      .then(revalidate)
      .catch((error) => {
        console.error(error);
      })
      .finally(() => setPluginLoader(false));
  }

  async function toggleFav(): Promise<void> {
    const newValue = !favorite;
    await favoriteActor(props.actorId, newValue);
    setFavorite(newValue);
  }

  async function toggleBookmark(): Promise<void> {
    const newValue = bookmark ? null : new Date();
    await bookmarkActor(props.actorId, newValue);
    setBookmark(!!newValue);
  }

  return (
    <AutoLayout gap={10} className="items-center">
      <div className="relative">
        <div className="w-[140px]">
          <Avatar avatarId={props.avatarId} />
        </div>
        {props.nationality && (
          <div className="absolute bottom-0 right-0">
            <Flag name={props.nationality.nationality} code={props.nationality.alpha2} />
          </div>
        )}
      </div>
      <div className="text-center">
        <div className="font-semibold">{props.actorName}</div>
        {props.age && (
          <div
            className="text-sm text-gray-500 dark:text-gray-500"
            title={`Born on ${new Date(props.bornOn!).toLocaleDateString()}`}
          >
            {t("yearsOld", { age: props.age })}
          </div>
        )}
      </div>
      <div className="flex justify-center gap-2">
        <FavoriteIconButton value={favorite} onClick={toggleFav} />
        <BookmarkIconButton value={bookmark} onClick={toggleBookmark} />
      </div>
      {props.links && (
        <AutoLayout gap={5}>
          {props.links.map(({ url, text }) => (
            <ExternalLink url={url} key={url}>
              {text}
            </ExternalLink>
          ))}
        </AutoLayout>
      )}
      <Rating value={rating} onChange={changeRating} />
      <Button loading={pluginLoader} onClick={triggerPlugins}>
        {t("action.runPlugins")}
      </Button>
      <ActorImagesEditor actorId={props.actorId} onClose={revalidate} />
    </AutoLayout>
  );
}
