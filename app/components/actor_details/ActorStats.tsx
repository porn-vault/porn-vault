import clsx from "clsx";
import prettyBytes from "pretty-bytes";
import { useTranslations } from "use-intl";

import StatCard from "./StatCard";

type Props = {
  numScenes: number;
  numWatches: number;
  averageRating: number;
  score: number;
  percentWatched: number;
  diskSpace: number;
};

export default function ActorStats(props: Props) {
  const t = useTranslations();

  const percentWatched = Math.round(props.percentWatched * 100).toString();

  return (
    <div
      className="grid gap-2"
      style={{
        gridTemplateColumns: "repeat(auto-fit, minmax(125px, 1fr))",
      }}
    >
      <StatCard title={t("scene", { numItems: 2 })}>{props.numScenes}</StatCard>
      <StatCard title={t("views", { numItems: 2 })}>{props.numWatches}</StatCard>
      <StatCard title={t("avgRating", { numItems: 2 })}>
        <span
          className={clsx({
            "text-red-500 dark:text-red-500": props.averageRating >= 9,
          })}
        >
          {(props.averageRating / 2).toFixed(1)}
        </span>
      </StatCard>
      <StatCard title={t("scenesWatched")}>
        <span
          className={clsx({
            "text-green-700 dark:text-green-300": percentWatched === "100",
          })}
        >
          {percentWatched}%
        </span>
      </StatCard>
      <StatCard title={t("pvScore", { numItems: 2 })}>{props.score}</StatCard>
      <StatCard title={t("widgets.diskUsageScenes")}>{prettyBytes(props.diskSpace)}</StatCard>
    </div>
  );
}
