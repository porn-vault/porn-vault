import { createSessionStorageQueryManager } from "../../util/persistent_query";
import PaginatedImageList, { DEFAULT_QUERY } from "../PaginatedImageList";

type Props = {
  actorId: string;
};

export default function ActorDetailsImageList(props: Props) {
  const storageKey = `actor_details_${props.actorId}_images_query`;
  const persistentQuery = createSessionStorageQueryManager<typeof DEFAULT_QUERY>(storageKey);

  return (
    <PaginatedImageList
      padless
      withUploader
      mergeQuery={{
        actors: [props.actorId],
      }}
      actorId={props.actorId}
      persistentQuery={persistentQuery}
    />
  );
}
