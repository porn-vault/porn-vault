import { createSessionStorageQueryManager } from "../../util/persistent_query";
import PaginatedSceneList, { DEFAULT_QUERY } from "../PaginatedSceneList";

type Props = {
  actorId: string;
};

export default function ActorDetailsSceneList(props: Props) {
  const storageKey = `actor_details_${props.actorId}_scenes_query`;
  const persistentQuery = createSessionStorageQueryManager<typeof DEFAULT_QUERY>(storageKey);

  return (
    <PaginatedSceneList
      padless
      mergeQuery={{
        actors: [props.actorId],
      }}
      actorId={props.actorId}
      persistentQuery={persistentQuery}
    />
  );
}
