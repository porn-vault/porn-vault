import { editActorList } from "../../composables/use_actor_list";
import { useSimilarActors } from "../../composables/use_similar_actors";
import { IActor } from "../../types/actor";
import ActorCard from "../ActorCard";
import ListWrapper from "../ListWrapper";

type Props = {
  actorId: string;
};

export default function SimilarActorsList({ actorId }: Props): JSX.Element {
  const similarActors = useSimilarActors(actorId);

  function editActor(actorId: string, fn: (actor: IActor) => IActor) {
    similarActors.setActors((prev) => editActorList(prev, actorId, fn));
  }

  return (
    <ListWrapper
      loading={similarActors.loading}
      noResults={!similarActors.actors.length}
    >
      {similarActors.actors.map((actor) => (
        <ActorCard
          onFav={(value) => {
            editActor(actor._id, (actor) => {
              actor.favorite = value;
              return actor;
            });
          }}
          onBookmark={(value) => {
            editActor(actor._id, (actor) => {
              actor.bookmark = !!value;
              return actor;
            });
          }}
          onRate={(rating) => {
            editActor(actor._id, (actor) => {
              actor.rating = rating;
              return actor;
            });
          }}
          key={actor._id}
          actor={actor}
        />
      ))}
    </ListWrapper>
  );
}
