import { ReactNode } from "react";

import Card from "../Card";

type Props = {
  title: string;
  children: ReactNode;
};

export default function StatCard({ title, children }: Props) {
  return (
    <Card className="flex flex-col gap-1">
      <div title={title} className="capitalize text-sm text-gray-500 dark:text-gray-500 truncate">
        {title}
      </div>
      <div className="text-2xl text-black dark:text-white">{children}</div>
    </Card>
  );
}
