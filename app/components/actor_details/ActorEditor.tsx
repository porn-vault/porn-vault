import EditIcon from "mdi-react/PencilIcon.js";
import { useState } from "react";
import { useTranslations } from "use-intl";

import { useWindow } from "../../composables/use_window";
import { IActor } from "../../types/actor";
import { graphqlQuery } from "../../util/gql";
import AutoLayout from "../AutoLayout";
import Button from "../Button";
import CountryDropdownChoice, { SimpleCountry } from "../CountryDropdownChoice";
import DateInput from "../DateInput";
/* TODO: */
/* import ExternalLinksEditor from "../ExternalLinksEditor"; */
import IconButton from "../IconButton";
import Subheading from "../Subheading";
import Textarea from "../Textarea";
import TextInput from "../TextInput";
import Window from "../Window";

async function editActor({
  id,
  ...opts
}: {
  id: string;
  name: string;
  aliases: string[];
  externalLinks: { url: string; text: string }[];
  nationality: string | null;
  bornOn?: number;
}) {
  const query = `
  mutation ($ids: [String!]!, $opts: ActorUpdateOpts!) {
    updateActors(ids: $ids, opts: $opts) {
      _id
    }
  }
 `;

  await graphqlQuery(query, {
    ids: [id],
    opts,
  });
}

type Props = {
  onEdit: () => void;
  actor: IActor;
};

export default function ActorEditor({ onEdit, actor }: Props) {
  const t = useTranslations();

  const { isOpen, close, open } = useWindow();
  const [name, setName] = useState(actor.name);
  const [bornOn, setBornOn] = useState(actor.bornOn ? new Date(actor.bornOn) : null);
  const [nationality, setNationality] = useState<SimpleCountry | null | undefined>(
    actor.nationality
  );
  const [aliasInput, setAliasInput] = useState(actor.aliases.join("\n"));

  const [externalLinks, setExternalLinks] = useState(actor.externalLinks);
  const [loading, setLoader] = useState(false);

  return (
    <>
      <IconButton
        Icon={EditIcon}
        onClick={open}
        className="text-black hover:text-gray-500 dark:text-white hover:dark:text-gray-400"
      />
      <Window
        onClose={close}
        isOpen={isOpen}
        title={t("action.editActor")}
        actions={
          <>
            <Button
              loading={loading}
              onClick={async () => {
                try {
                  setLoader(true);
                  await editActor({
                    id: actor._id,
                    name,
                    aliases: aliasInput.split("\n").filter(Boolean),
                    externalLinks,
                    nationality: nationality?.alpha2 || null,
                    bornOn: bornOn?.valueOf(),
                  });
                  onEdit();
                  close();
                } catch (error) {}
                setLoader(false);
              }}
            >
              {t("action.save")}
            </Button>
            <Button secondary onClick={close}>
              Close
            </Button>
          </>
        }
      >
        <div>
          <Subheading>Actor name</Subheading>
          <TextInput value={name} onChange={setName} placeholder="Enter an actor name" />
        </div>
        <div>
          <AutoLayout layout="h">
            <div className="flex-grow">
              <Subheading>Born on</Subheading>
              <DateInput value={bornOn} onChange={setBornOn} />
            </div>
            <div className="flex-grow">
              <Subheading>Nationality</Subheading>
              <CountryDropdownChoice value={nationality} onChange={setNationality} />
            </div>
          </AutoLayout>
        </div>
        <div>
          <Subheading>Aliases</Subheading>
          <Textarea
            rows={5}
            className="w-full"
            value={aliasInput}
            onChange={setAliasInput}
            placeholder="1 per line"
          />
        </div>
        {/*  <ExternalLinksEditor
          value={externalLinks}
          onChange={setExternalLinks}
        /> */}
      </Window>
    </>
  );
}
