import { useSettings } from "~/composables/use_settings";

import { useSafeMode } from "../../composables/use_safe_mode";

type Props = {
  imageId?: string;
};

export default function HeroImage({ imageId }: Props) {
  const { blur: safeModeBlur } = useSafeMode();
  const { actorHeroAspect } = useSettings();

  return (
    <>
      {imageId && (
        <div className="overflow-hidden">
          <div className="relative" style={{ filter: safeModeBlur }}>
            <img
              className="object-cover"
              width="100%"
              style={{ aspectRatio: actorHeroAspect.cssValue }}
              src={`/api/media/image/${imageId}?password=null`}
            />
          </div>
        </div>
      )}
    </>
  );
}
