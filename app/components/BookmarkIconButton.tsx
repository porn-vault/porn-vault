import clsx from "clsx";
import BookmarkIcon from "mdi-react/BookmarkIcon.js";
import BookmarkBorderIcon from "mdi-react/BookmarkOutlineIcon.js";

import IconButton from "./IconButton";

type Props = {
  value: boolean;
  onClick?: (event: React.MouseEvent<SVGSVGElement, MouseEvent>) => void;
  alwaysLight?: boolean;
  size?: number;
  className?: string;
};

export default function BookmarkIconButton({
  size,
  value,
  onClick,
  alwaysLight,
  className,
}: Props): JSX.Element {
  const Icon = value ? BookmarkIcon : BookmarkBorderIcon;

  return (
    <IconButton
      size={size}
      alwaysLight={alwaysLight}
      Icon={Icon}
      onClick={onClick}
      className={clsx(className)}
    />
  );
}
