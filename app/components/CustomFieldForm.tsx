import { Combobox } from "@headlessui/react";
import clsx from "clsx";
import { Dispatch, Fragment, SetStateAction, useId } from "react";
import { type CustomFieldTarget } from "server/types/custom_field";

import { toTitleCase } from "~/util/string";

import SimpleSelect from "./SimpleSelect";
import TextInput from "./TextInput";

type Value = Record<string, string | string[] | boolean | number | null>;

type Props = {
  value: Value;
  fields: {
    _id: string;
    name: string;
    type: string;
    unit?: string;
    values?: string[];
  }[];
  onChange: Dispatch<SetStateAction<Value>>;
};
export default function CustomFieldForm({ fields, value, onChange }: Props): JSX.Element {
  const id = useId();

  return (
    <div>
      <div className="grid grid-cols-2 gap-4">
        {fields.map((field) => (
          <div key={field._id}>
            <div className="mb-2">{field.name}</div>
            {(() => {
              if (field.type === "NUMBER") {
                return (
                  <TextInput
                    className="w-full"
                    placeholder="Enter value"
                    value={String(value[field._id] ?? "")}
                    onChange={(s) => {
                      onChange((prev) => {
                        const value = s ? Number(s) : null;
                        return {
                          ...prev,
                          [field._id]: isNaN(value as any) ? null : value,
                        };
                      });
                    }}
                  />
                );
              }
              if (field.type === "STRING") {
                return (
                  <TextInput
                    className="w-full"
                    placeholder="Enter value"
                    value={String(value[field._id] ?? "")}
                    onChange={(s) => {
                      onChange((prev) => ({
                        ...prev,
                        [field._id]: s ?? undefined,
                      }));
                    }}
                  />
                );
              }
              if (field.type === "BOOLEAN") {
                const checkBoxId = `check-${id}-${field._id}`;

                return (
                  <>
                    <input
                      className="mr-2"
                      id={checkBoxId}
                      type="checkbox"
                      checked={!!value[field._id]}
                      onChange={(ev) => {
                        onChange((prev) => ({
                          ...prev,
                          [field._id]: ev.target.checked,
                        }));
                      }}
                    />
                    <label htmlFor={checkBoxId}>{value[field._id] ? "Yes" : "No"}</label>
                  </>
                );
              }
              if (field.type === "SINGLE_SELECT") {
                return (
                  <SimpleSelect
                    className="w-full"
                    items={(field.values ?? []).map((value) => ({
                      text: value,
                      value,
                    }))}
                    onChange={(value) => {
                      onChange((prev) => ({
                        ...prev,
                        [field._id]: value,
                      }));
                    }}
                    value={(value[field._id] as string) || ""}
                  />
                );
              }
              if (field.type === "MULTI_SELECT") {
                return (
                  <div className="relative">
                    <Combobox
                      value={(value[field._id] as string[]) ?? []}
                      multiple
                      onChange={(values) => {
                        onChange((prev) => ({
                          ...prev,
                          [field._id]: values,
                        }));
                      }}
                    >
                      <Combobox.Input
                        placeholder="Select value(s)"
                        className={clsx(
                          "w-full rounded border-2 px-2 py-1 !outline-none transition-colors",
                          "border-gray-300 bg-gray-50 text-black hover:bg-gray-200 focus:border-blue-500 focus:bg-gray-100",
                          "dark:border-gray-700 dark:bg-gray-800 dark:text-white hover:dark:bg-gray-900 focus:dark:border-blue-800 focus:dark:bg-gray-900"
                        )}
                        displayValue={(target: CustomFieldTarget[]) =>
                          target.map(toTitleCase).join(", ")
                        }
                      />
                      <Combobox.Options className="absolute left-0 top-[40px] z-[900] flex max-h-[240px] w-full flex-col gap-1 overflow-y-scroll rounded bg-white px-2 py-1 shadow dark:bg-gray-800">
                        {(field.values ?? []).map((target) => (
                          <Combobox.Option key={target} value={target} as={Fragment}>
                            {({ selected }) => (
                              <span
                                className={clsx(
                                  "cursor-pointer rounded p-2 transition-colors hover:bg-gray-300 hover:dark:bg-gray-700",
                                  { "bg-blue-200 dark:bg-blue-900": selected }
                                )}
                              >
                                {toTitleCase(target)}
                              </span>
                            )}
                          </Combobox.Option>
                        ))}
                      </Combobox.Options>
                    </Combobox>
                  </div>
                );
              }
              return null;
            })()}
          </div>
        ))}
      </div>
    </div>
  );
}
