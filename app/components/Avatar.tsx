import clsx from "clsx";

import { useSafeMode } from "../composables/use_safe_mode";
import { thumbnailUrl } from "../util/thumbnail";

type Props = {
  avatarId: string | null | undefined;
};

export default function Avatar(props: Props): JSX.Element {
  const { blur: safeModeBlur } = useSafeMode();
  const genericClasses =
    "overflow-hidden rounded-[30%] border-2 border-gray-200 dark:border-gray-800 w-full";

  return (
    <>
      {props.avatarId ? (
        <div className={genericClasses}>
          <img
            className="block object-cover"
            width="100%"
            src={thumbnailUrl(props.avatarId)}
            style={{ filter: safeModeBlur, aspectRatio: 1 }}
          />
        </div>
      ) : (
        <div className={clsx("relative", genericClasses)} style={{ aspectRatio: "1" }}>
          <span className="text-4xl absolute translate-x-[-50%] translate-y-[-50%] left-[50%] top-[50%] text-gray-300 dark:text-gray-700">
            ?
          </span>
        </div>
      )}
    </>
  );
}
