import { useState } from "react";
import { useTranslations } from "use-intl";

import { useWindow } from "../composables/use_window";
import { graphqlQuery } from "../util/gql";
import Button from "./Button";
import InputError from "./InputError";
import Subheading from "./Subheading";
import TextInput from "./TextInput";
import Window from "./Window";

export async function createStudio(name: string) {
  const query = `
  mutation ($name: String!) {
    addStudio(name: $name) {
      _id
    }
  }
        `;

  await graphqlQuery(query, {
    name,
  });
}

type Props = {
  onCreate: () => void;
};

export default function StudioCreator({ onCreate }: Props) {
  const t = useTranslations();
  const { isOpen, close, open } = useWindow();

  const [error, setError] = useState<string | undefined>();
  const [name, setName] = useState("");

  const [loading, setLoader] = useState(false);

  function reset() {
    setName("");
    setError(undefined);
  }

  function doClose() {
    reset();
    close();
  }

  return (
    <>
      <Button onClick={open}>+ {t("action.addStudio")}</Button>
      <Window
        onClose={doClose}
        isOpen={isOpen}
        title={t("action.addStudio")}
        actions={
          <>
            <Button
              loading={loading}
              onClick={async () => {
                try {
                  setLoader(true);
                  await createStudio(name);
                  onCreate();
                  doClose();
                } catch (error) {
                  if (error instanceof Error) {
                    setError(error.message);
                  } else {
                    setError("An error occurred");
                  }
                }
                setLoader(false);
              }}
              style={{ color: "white", background: "#3142da" }}
            >
              Create
            </Button>
            <Button secondary onClick={doClose}>
              Close
            </Button>
          </>
        }
      >
        <div>
          <Subheading>Studio name *</Subheading>
          <TextInput
            className="w-full"
            value={name}
            onChange={setName}
            placeholder="Enter a studio name"
          />
        </div>
        {error && <InputError message={error} />}
      </Window>
    </>
  );
}
