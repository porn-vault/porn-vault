import {
  ACTOR_HERO_RATIO,
  ACTOR_RATIO,
  SCENE_RATIO,
  useSettings,
} from "../../composables/use_settings";
import SimpleSelect from "../SimpleSelect";
import Subheading from "../Subheading";

type SceneOption = { value: SCENE_RATIO; text: string };

function SceneAspectRatios() {
  const { sceneAspectRatio, setSceneAspectRatio } = useSettings();

  return (
    <div>
      <Subheading>Scene thumbnail aspect ratio</Subheading>
      <SimpleSelect
        className="min-w-[100px]"
        onChange={(value) => setSceneAspectRatio(value as SCENE_RATIO)}
        value={sceneAspectRatio}
        items={
          [
            { value: "16:9", text: "16:9" },
            { value: "4:3", text: "4:3" },
          ] satisfies SceneOption[]
        }
      />
    </div>
  );
}

type ActorOption = { value: ACTOR_RATIO; text: string };

function ActorAspectRatios() {
  const { actorAspectRatio, setActorAspectRatio } = useSettings();

  return (
    <div>
      <Subheading>Actor thumbnail aspect ratio</Subheading>
      <SimpleSelect
        className="min-w-[100px]"
        onChange={(value) => setActorAspectRatio(value as ACTOR_RATIO)}
        value={actorAspectRatio}
        items={
          [
            { value: "9:16", text: "9:16" },
            { value: "3:4", text: "3:4" },
          ] satisfies ActorOption[]
        }
      />
    </div>
  );
}

type ActorHeroOption = { value: ACTOR_HERO_RATIO; text: string };

function ActorHeroAspectRatios() {
  const { actorHeroAspectRatio, setActorHeroAspectRatio } = useSettings();

  return (
    <div>
      <Subheading>Actor hero aspect ratio</Subheading>
      <SimpleSelect
        className="min-w-[100px]"
        onChange={(value) => setActorHeroAspectRatio(value as ACTOR_HERO_RATIO)}
        value={actorHeroAspectRatio}
        items={
          [
            /* TODO: allow more values, when layout allows it */
            /*  { value: "16:9", text: "16:9" },
            { value: "2", text: "2" },
            { value: "2.25", text: "2.25" }, */
            { value: "2.5", text: "2.5" },
            { value: "2.75", text: "2.75" },
            { value: "3", text: "3" },
          ] satisfies ActorHeroOption[]
        }
      />
    </div>
  );
}

function ShowCardLabels() {
  const { showCardLabels, setShowCardLabels } = useSettings();

  return (
    <div>
      <Subheading>Show card labels</Subheading>
      <input
        className="mr-2"
        id="show_card_labels_check"
        type="checkbox"
        checked={showCardLabels}
        onInput={ev => {
          setShowCardLabels(ev.currentTarget.checked)
        }}
      />
      <label htmlFor="show_card_labels_check">{
        showCardLabels ? "Enabled" : "Disabled"}
      </label>
    </div>
  );
}

export default function DisplaySettings() {
  return (
    <>
      <SceneAspectRatios />
      <ActorAspectRatios />
      <ActorHeroAspectRatios />
      <ShowCardLabels />
    </>
  );
}
