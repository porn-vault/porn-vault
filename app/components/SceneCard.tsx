import WatchedIcon from "mdi-react/EyeIcon.js";
import Highlighter from "react-highlight-words";

import { buildSearchWords, HIGHLIGHT_CLASSES } from "~/util/highlight";

import { useSafeMode } from "../composables/use_safe_mode";
import { useSettings } from "../composables/use_settings";
import { useTitleColor } from "../composables/use_title_color";
import { IScene } from "../types/scene";
import { bookmarkScene, favoriteScene, rateScene } from "../util/mutations/scene";
import { formatDuration } from "../util/string";
import { thumbnailUrl } from "../util/thumbnail";
import ActorList from "./ActorList";
import BookmarkIconButton from "./BookmarkIconButton";
import Card from "./Card";
import FavoriteIconButton from "./FavoriteIconButton";
import Flex from "./Flex";
import LabelGroup from "./LabelGroup";
import LocaleLink from "./Link";
import Rating from "./Rating";
import ResponsiveImage from "./ResponsiveImage";
import Spacer from "./Spacer";

type Props = {
  scene: Pick<
    IScene,
    | "_id"
    | "name"
    | "favorite"
    | "bookmark"
    | "actors"
    | "studio"
    | "rating"
    | "labels"
    | "thumbnail"
    | "meta"
    | "releaseDate"
    | "watches"
  >;
  onFav: (value: boolean) => void;
  onBookmark: (value: Date | null) => void;
  onRate: (rating: number) => void;

  highlight?: string[];
};

export default function SceneCard({ scene, onFav, onBookmark, onRate, highlight }: Props) {
  const { blur: safeModeBlur } = useSafeMode();
  const { sceneImageAspect, showCardLabels } = useSettings();
  const titleColor = useTitleColor(scene.thumbnail?.color);

  async function toggleFav(): Promise<void> {
    const newValue = !scene.favorite;
    await favoriteScene(scene._id, newValue);
    onFav(newValue);
  }

  async function toggleBookmark(): Promise<void> {
    const newValue = scene.bookmark ? null : new Date();
    await bookmarkScene(scene._id, newValue);
    onBookmark(newValue);
  }

  async function changeRating(rating: number): Promise<void> {
    await rateScene(scene._id, rating);
    onRate(rating);
  }

  return (
    <Card padless className="relative">
      <div className="block relative cursor-pointer rounded-lg transition-[filter] hover:brightness-[1.05]">
        <LocaleLink to={`/scene/${scene._id}`}>
          <ResponsiveImage
            className="rounded-lg hover:scale-[1.05]"
            aspectRatio={sceneImageAspect.cssValue}
            src={scene.thumbnail?._id && thumbnailUrl(scene.thumbnail._id)}
            imgStyle={{
              transition: "all 0.1s ease-in-out",
              filter: safeModeBlur,
            }}
          />
        </LocaleLink>
        <Flex
          className="ml-[1px] absolute left-0 bottom-0 gap-1 translate-y-[50%]"
          layout="h"
        >
          <FavoriteIconButton size={30} className="p-1 shadow-sm rounded-full bg-white bg-gray-50 dark:bg-gray-800" value={scene.favorite} onClick={toggleFav} />
          <BookmarkIconButton size={30} className="p-1 shadow-sm rounded-full bg-white bg-gray-50 dark:bg-gray-800" value={scene.bookmark} onClick={toggleBookmark} />
        </Flex>
        <Flex
          className="mr-[1px] absolute right-0 bottom-0 gap-1 translate-y-[50%]"
          layout="h"
        >
          <Flex
            layout="h"
            className="gap-1 text-xs font-semibold rounded px-1 py-[2px] shadow-sm bg-gray-50 dark:bg-gray-800"
          >
            <WatchedIcon size={16} />
            <span>
              <b>{scene.watches.length}</b>
            </span>
          </Flex>
          <div className="text-xs font-semibold rounded px-1 py-[2px] shadow-sm bg-gray-50 dark:bg-gray-800">
            <b>{formatDuration(scene.meta.duration)}</b>
          </div>
        </Flex>
      </div>
      <Flex className="pt-4 gap-[1px] px-2 pb-1">
        <Flex className="gap-1 font-semibold" layout="h">
          {scene.studio && (
            <LocaleLink to={`/studio/${scene.studio._id}`}>
              <div className="text-xs font-semibold uppercase text-gray-600 transition-colors hover:text-gray-400 dark:text-gray-500 dark:hover:text-gray-600">
                <Highlighter
                  highlightClassName={HIGHLIGHT_CLASSES}
                  searchWords={buildSearchWords(highlight)}
                  autoEscape={true}
                  textToHighlight={scene.studio.name}
                />
              </div>
            </LocaleLink>
          )}
          <Spacer />
          {scene.releaseDate && (
            <div className="text-xs text-gray-600 dark:text-gray-400">
              {new Date(scene.releaseDate).toLocaleDateString()}
            </div>
          )}
        </Flex>

        <div
          className="ellipsis overflow-hidden whitespace-nowrap font-semibold"
          style={{ color: titleColor }}
          title={scene.name}
        >
          <Highlighter
            highlightClassName={HIGHLIGHT_CLASSES}
            searchWords={buildSearchWords(highlight)}
            autoEscape={true}
            textToHighlight={scene.name}
          />
        </div>

        {!!scene.actors.length && <ActorList highlight={highlight} actors={scene.actors} />}

        <Rating onChange={changeRating} value={scene.rating || 0} />

        {showCardLabels && !!scene.labels.length && (
          <div className="mt-1">
            <LabelGroup highlight={highlight} value={scene.labels} />
          </div>
        )}
      </Flex>
    </Card>
  );
}
