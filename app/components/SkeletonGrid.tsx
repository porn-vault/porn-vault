import ListContainer from "./ListContainer";
import SkeletonCard from "./SkeletonCard";

type Props = {
  size?: number;
};

export default function SkeletonGrid({ size }: Props) {
  return (
    <ListContainer size={size}>
      {[...new Array(16)].map((_, i) => (
        <SkeletonCard key={i} />
      ))}
    </ListContainer>
  );
}
