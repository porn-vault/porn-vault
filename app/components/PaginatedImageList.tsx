import clsx from "clsx";
import { defu } from "defu";
import { Masonry } from "masonic";
import SelectAllIcon from "mdi-react/CheckboxMultipleMarkedCircleIcon.js";
import CheckedIcon from "mdi-react/CheckCircleIcon.js";
import NotCheckedIcon from "mdi-react/CircleOutlineIcon.js";
import DeleteIcon from "mdi-react/DeleteIcon.js";
import LabelIcon from "mdi-react/LabelIcon.js";
import SubtractLabelIcon from "mdi-react/LabelOffIcon.js";
import LabelOutlineIcon from "mdi-react/LabelOutlineIcon.js";
import ClearSelectionIcon from "mdi-react/SelectionRemoveIcon.js";
import StarOutline from "mdi-react/StarBorderIcon.js";
import StarHalf from "mdi-react/StarHalfFullIcon.js";
import Star from "mdi-react/StarIcon.js";
import { useState } from "react";
import { useHotkeys } from "react-hotkeys-hook";
import { useTranslations } from "use-intl";

import AutoLayout from "../components/AutoLayout";
import BookmarkIconButton from "../components/BookmarkIconButton";
import FavoriteIconButton from "../components/FavoriteIconButton";
import Flex from "../components/Flex";
import IconButtonMenu from "../components/IconButtonMenu";
import ImageCard from "../components/ImageCard";
import ImageUploader from "../components/ImageUploader";
import LabelSelector from "../components/LabelSelector";
import PageWrapper from "../components/PageWrapper";
import PaginatedQuery from "../components/PaginatedQuery";
import Rating from "../components/Rating";
import SortDirectionButton, { SortDirection } from "../components/SortDirectionButton";
import TextInput from "../components/TextInput";
import { editImageList, fetchImages } from "../composables/use_image_list";
import { useImageSelection } from "../composables/use_image_selection";
import useLabelList from "../composables/use_label_list";
import { useLightbox } from "../composables/use_lightbox";
import { PersistentQueryManager, usePaginatedQuery } from "../composables/use_paginated_query";
import { useWindowSize } from "../composables/use_window_size";
import { IImage } from "../types/image";
import {
  bookmarkImage,
  deleteImages,
  favoriteImage,
  labelImages,
  rateImage,
} from "../util/mutations/image";
import { thumbnailUrl } from "../util/thumbnail";
import Card from "./Card";
import CardTitle from "./CardTitle";
import ContentWrapper from "./ContentWrapper";
import IconButton from "./IconButton";
import Lightbox from "./Lightbox";
import SimpleSelect from "./SimpleSelect";
import SkeletonGrid from "./SkeletonGrid";
import Spacer from "./Spacer";
import GridSizeSelector, { getGridSize } from "./GridSizeSelector";

export const DEFAULT_QUERY = {
  query: "",
  favorite: false,
  bookmark: false,
  rating: 0,
  sortBy: "relevance",
  sortDir: "desc" as SortDirection,
  include: [] as string[],
  actors: [] as string[],
  movies: [] as string[],
  studios: [] as string[],
  scenes: [] as string[],
};

type Props = {
  persistentQuery?: PersistentQueryManager<typeof DEFAULT_QUERY>;
  mergeQuery: Partial<typeof DEFAULT_QUERY>;
  actorId?: string;
  sceneId?: string;
  writeHead?: boolean;
  withUploader?: boolean;
  padless?: boolean;
};

export default function PaginatedImageList(props: Props): JSX.Element {
  const t = useTranslations();

  const gridSize = getGridSize();

  const { labels: labelList, loading: labelLoader } = useLabelList();
  const hasNoLabels = !labelLoader && !labelList.length;

  /* TODO: collabs like scene */

  const {
    query,
    setQuery,
    getPage,
    gotoPage,
    gotoStart,
    items: images,
    error,
    loading,
    numItems,
    numPages,
    page,
    resetAll,
    setItems, // TODO: set
  } = usePaginatedQuery({
    defaultQuery: DEFAULT_QUERY,
    fetchFn: (page, query) => fetchImages(page, defu(query, props.mergeQuery)),
    persistentQuery: props.persistentQuery,
  });

  function editImage(imageId: string, fn: (image: IImage) => IImage) {
    setItems((prev) => editImageList(prev, imageId, fn));
  }

  const {
    selected: selectedImages,
    isSelected: isImageSelected,
    toggleImage,
    clear: clearImageSelection,
    selectAll: selectAllImages,
    allSelected: allImagesSelected,
  } = useImageSelection(images);

  async function deleteSelection(): Promise<void> {
    if (confirm(`Really delete ${selectedImages.length} images?`)) {
      const ids = selectedImages.map(({ _id }) => _id);
      await deleteImages(ids);
      clearImageSelection();
      await gotoPage(page);
    }
  }

  const { width: windowWidth } = useWindowSize();
  const [loadingPage, setLoadingPage] = useState(false);

  const { setIndex, activeImage, goBack, goNext, canGoBack, canGoNext } = useLightbox(images, {
    requestPreviousPage: () => {
      if (page > 0) {
        setLoadingPage(true);
        gotoPage(page - 1)
          .then((data) => {
            if (data) {
              setIndex(data.items.length - 1);
            }
          })
          .finally(() => {
            // NOTE: Timeout to let image load a bit
            setTimeout(() => setLoadingPage(false), 300);
          });
      }
    },
    requestNextPage: () => {
      if (page < numPages - 1) {
        setLoadingPage(true);
        gotoPage(page + 1)
          .then(() => {
            setIndex(0);
          })
          .finally(() => {
            // NOTE: Timeout to let image load a bit
            setTimeout(() => setLoadingPage(false), 300);
          });
      }
    },
  });

  useHotkeys(
    "ctrl+a",
    (ev) => {
      ev.preventDefault();
      selectAllImages();
    },
    [selectAllImages]
  );

  useHotkeys(
    "ctrl+d",
    (ev) => {
      ev.preventDefault();
      clearImageSelection();
    },
    []
  );

  return (
    <PageWrapper
      padless={props.padless}
      title={props.writeHead ? t("foundImages", { numItems }) : undefined}
    >
      <AutoLayout>
        <div className="flex gap-2 justify-between items-center">
          <CardTitle>{loading ? "Loading..." : t("foundImages", { numItems })}</CardTitle>
          <div className="flex gap-2 items-center">
            {props.withUploader && (
              <Flex layout="h" className="gap-2">
                <ImageUploader
                  onDone={gotoStart}
                  onUpload={() => { }}
                  actors={props.actorId ? [props.actorId] : []}
                  scene={props.sceneId}
                />
              </Flex>
            )}
            <GridSizeSelector />
          </div>
        </div>
        <Card className="z-[500] sticky top-14">
          <CardTitle>{selectedImages.length} images selected</CardTitle>
          <Flex layout="h" className="gap-3">
            <IconButton
              disabled={!images.length}
              Icon={allImagesSelected ? ClearSelectionIcon : SelectAllIcon}
              onClick={() => {
                if (allImagesSelected) {
                  clearImageSelection();
                } else {
                  selectAllImages();
                }
              }}
            />
            <Spacer />
            {/* TODO: */}
            <IconButton disabled={!selectedImages.length} Icon={LabelIcon} />
            {/* TODO: */}
            <IconButton disabled={!selectedImages.length} Icon={SubtractLabelIcon} />
            <IconButton
              disabled={!selectedImages.length}
              onClick={deleteSelection}
              className={clsx("", {
                "text-red-500 dark:!text-red-500": selectedImages.length,
                "text-red-300 dark:text-red-950": !selectedImages.length,
              })}
              Icon={DeleteIcon}
            />
          </Flex>
        </Card>
        <PaginatedQuery
          loading={loading}
          errorMessage={error}
          resetAll={resetAll}
          refreshPage={() => getPage(page, query)}
          filters={
            <>
              <TextInput
                className="max-w-[120px]"
                onEnter={gotoStart}
                value={query.query}
                onChange={(text) =>
                  setQuery((prev) => ({
                    ...prev,
                    query: text,
                  }))
                }
                placeholder={t("action.findContent")}
              />
              <FavoriteIconButton
                value={query.favorite}
                onClick={() => {
                  setQuery((prev) => ({
                    ...prev,
                    favorite: !prev.favorite,
                  }));
                }}
              />
              <BookmarkIconButton
                value={query.bookmark}
                onClick={() => {
                  setQuery((prev) => ({
                    ...prev,
                    bookmark: !prev.bookmark,
                  }));
                }}
              />
              <IconButtonMenu
                value={!!query.rating}
                activeIcon={query.rating === 10 ? Star : StarHalf}
                inactiveIcon={StarOutline}
              >
                <Rating
                  value={query.rating}
                  onChange={(rating) => {
                    setQuery((prev) => ({
                      ...prev,
                      rating,
                    }));
                  }}
                />
              </IconButtonMenu>
              <IconButtonMenu
                counter={query.include.length}
                value={!!query.include.length}
                activeIcon={LabelIcon}
                inactiveIcon={LabelOutlineIcon}
                isLoading={labelLoader}
                disabled={hasNoLabels}
              >
                <LabelSelector
                  multiple
                  value={query.include}
                  searchFn={(query, label) =>
                    label.name.toLowerCase().includes(query.toLowerCase()) ||
                    label.aliases.some((alias) => alias.toLowerCase().includes(query.toLowerCase()))
                  }
                  itemChoices={labelList}
                  onChange={(labels) => {
                    setQuery((prev) => ({
                      ...prev,
                      include: labels,
                    }));
                  }}
                />
              </IconButtonMenu>
              <>
                <SimpleSelect
                  items={[
                    {
                      text: t("relevance"),
                      value: "relevance",
                    },
                    {
                      text: t("addedToCollection"),
                      value: "addedOn",
                    },
                    {
                      text: t("rating"),
                      value: "rating",
                    },
                    {
                      text: t("bookmark"),
                      value: "bookmark",
                    },
                  ]}
                  value={query.sortBy}
                  onChange={(sortBy) => {
                    setQuery((prev) => ({
                      ...prev,
                      sortBy: sortBy ?? "relevance",
                    }));
                  }}
                />
                <SortDirectionButton
                  value={query.sortDir}
                  onChange={(sortDir) => {
                    setQuery((prev) => ({
                      ...prev,
                      sortDir,
                    }));
                  }}
                />
              </>
            </>
          }
          currentPage={page}
          numPages={numPages}
          setPage={gotoPage}
        >
          <ContentWrapper
            loader={<SkeletonGrid />}
            loading={loading}
            noResults={!numItems}
          >
            <Masonry
              key={images as any}
              items={images}
              rowGutter={4}
              columnGutter={4}
              columnCount={(windowWidth || 1080) < 480 ? 2 : undefined}
              columnWidth={gridSize * 1.5}
              render={({ data: image, index }) => (
                <ImageCard
                  className={
                    !isImageSelected(image._id) && selectedImages.length ? "brightness-[40%]" : ""
                  }
                  onClick={(ev) => {
                    if (ev.ctrlKey) {
                      toggleImage(image);
                      return;
                    }
                    if (ev.shiftKey) {
                      /* TODO: this could be better */

                      if (selectedImages.length > 0) {
                        const lastSelectedImage = images
                          .filter((x) => isImageSelected(x._id))
                          .pop();
                        const firstIndex = images.findIndex(
                          (x) => x._id === lastSelectedImage!._id
                        );

                        const lowerBound = Math.min(firstIndex, index);
                        const higherBound = Math.max(firstIndex, index);

                        for (let i = lowerBound + 1; i <= higherBound; i++) {
                          toggleImage(images[i]);
                        }
                      }
                      return;
                    }

                    setIndex(index);
                  }}
                  key={image._id}
                  src={thumbnailUrl(image._id)}
                  alt={image.name}
                >
                  <div
                    onClick={() => toggleImage(image)}
                    className="bg-black p-1 bg-opacity-40 rounded-lg pointer-events-auto absolute right-1 top-1"
                  >
                    <IconButton
                      size={28}
                      Icon={isImageSelected(image._id) ? CheckedIcon : NotCheckedIcon}
                    />
                  </div>
                </ImageCard>
              )}
            />
          </ContentWrapper>
          <Lightbox
            hasPrevious={canGoBack || page > 0}
            hasNext={canGoNext || page < numPages - 1}
            loading={loadingPage}
            onClose={() => setIndex(-1)}
            onPrevious={canGoBack || page > 0 ? goBack : undefined}
            onNext={canGoNext || page < numPages - 1 ? goNext : undefined}
            image={activeImage}
            onLabel={(imageId, labels) => {
              labelImages(
                [imageId],
                labels.map(({ _id }) => _id)
              ).then(() => {
                editImage(imageId, (img) => {
                  img.labels = labels;
                  return img;
                });
              });
            }}
            onFavorite={() => {
              const img = activeImage;
              if (!img) {
                return;
              }

              const newValue = !img.favorite;

              favoriteImage(img._id, newValue)
                .then(() => {
                  editImage(img._id, (img) => {
                    img.favorite = newValue;
                    return img;
                  });
                })
                .catch(() => { });
            }}
            onBookmark={() => {
              const img = activeImage;
              if (!img) {
                return;
              }

              const newValue = img.bookmark ? null : new Date();

              bookmarkImage(img._id, newValue)
                .then(() => {
                  editImage(img._id, (img) => {
                    img.bookmark = !!newValue;
                    return img;
                  });
                })
                .catch(() => { });
            }}
            onRate={(rating) => {
              const img = activeImage;
              if (!img) {
                return;
              }

              rateImage(img._id, rating)
                .then(() => {
                  editImage(img._id, (img) => {
                    img.rating = rating;
                    return img;
                  });
                })
                .catch(() => { });
            }}
          />
        </PaginatedQuery>
      </AutoLayout>
    </PageWrapper>
  );
}
