import EditIcon from "mdi-react/PencilIcon.js";
import { useState } from "react";
import { useTranslations } from "use-intl";

import { useWindow } from "../../composables/use_window";
import { IMovie } from "../../types/movie";
import { graphqlQuery } from "../../util/gql";
import Button from "../Button";
import DateInput from "../DateInput";
import IconButton from "../IconButton";
import SceneInput from "../SceneInput";
import StudioInput from "../StudioInput";
import Subheading from "../Subheading";
import Textarea from "../Textarea";
import TextInput from "../TextInput";
import Window from "../Window";

async function editMovie({
  id,
  ...opts
}: {
  id: string;
  name: string;
  scenes: string[];
  description: string | null;
  studio?: string | null;
  releaseDate?: number | null;
}) {
  const query = `
  mutation ($ids: [String!]!, $opts: MovieUpdateOpts!) {
    updateMovies(ids: $ids, opts: $opts) {
      _id
    }
  }
 `;

  await graphqlQuery(query, {
    ids: [id],
    opts,
  });
}

type Props = {
  onEdit: () => void;
  movie: IMovie;
};

export default function MovieEditor({ onEdit, movie }: Props) {
  const t = useTranslations();

  const { isOpen, close, open } = useWindow();
  const [name, setName] = useState(movie.name);
  const [description, setDescription] = useState(movie.description ?? null);
  const [scenes, setScenes] = useState(movie.scenes.map((x) => x._id));
  const [releaseDate, setReleaseDate] = useState(
    movie.releaseDate ? new Date(movie.releaseDate) : null
  );
  const [studio, setStudio] = useState(movie.studio?._id ?? null);

  const [loading, setLoader] = useState(false);

  return (
    <>
      <IconButton
        Icon={EditIcon}
        onClick={open}
        className="text-black hover:text-gray-500 dark:text-white hover:dark:text-gray-400"
      />
      <Window
        onClose={close}
        isOpen={isOpen}
        title={t("action.editMovie")}
        actions={
          <>
            <Button
              loading={loading}
              onClick={async () => {
                try {
                  setLoader(true);
                  await editMovie({
                    id: movie._id,
                    name,
                    description,
                    scenes,
                    studio,
                    releaseDate: releaseDate?.valueOf(),
                  });
                  onEdit();
                  close();
                } catch (error) { }
                setLoader(false);
              }}
            >
              {t("action.save")}
            </Button>
            <Button secondary onClick={close}>
              Close
            </Button>
          </>
        }
      >
        <div>
          <Subheading>Movie name</Subheading>
          <TextInput value={name} onChange={setName} placeholder="Enter a movie name" />
        </div>
        <div>
          <Subheading>Release date</Subheading>
          <DateInput value={releaseDate} onChange={setReleaseDate} />
        </div>
        <div>
          <Subheading>Movie description</Subheading>
          <Textarea
            className="w-full min-h-[50px] max-h-[25vh]"
            value={description ?? ""}
            onChange={setDescription}
            placeholder="Enter a description"
          />
        </div>
        <div>
          <Subheading>Scenes</Subheading>
          <SceneInput value={scenes} onChange={setScenes} />
        </div>
        <div>
          <Subheading>Studio</Subheading>
          <StudioInput value={studio} onChange={setStudio} />
        </div>
      </Window>
    </>
  );
}
