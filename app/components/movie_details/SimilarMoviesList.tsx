import { editMovieList } from "../../composables/use_movie_list";
import { useSimilarMovies } from "../../composables/use_similar_movies";
import { IMovie } from "../../types/movie";
import ListWrapper from "../ListWrapper";
import MovieCard from "../MovieCard";

type Props = {
  movieId: string;
};

export default function SimilarMoviesList({ movieId }: Props): JSX.Element {
  const similarMovies = useSimilarMovies(movieId);

  function editMovie(movieId: string, fn: (movie: IMovie) => IMovie) {
    similarMovies.setMovies((prev) => editMovieList(prev, movieId, fn));
  }

  return (
    <ListWrapper
      loading={similarMovies.loading}
      noResults={!similarMovies.movies.length}
    >
      {similarMovies.movies.map((movie) => (
        <MovieCard
          onFav={(value) => {
            editMovie(movie._id, (movie) => {
              movie.favorite = value;
              return movie;
            });
          }}
          onBookmark={(value) => {
            editMovie(movie._id, (movie) => {
              movie.bookmark = !!value;
              return movie;
            });
          }}
          key={movie._id}
          movie={movie}
        />
      ))}
    </ListWrapper>
  );
}
