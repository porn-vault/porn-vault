import { createSessionStorageQueryManager } from "../../util/persistent_query";
import PaginatedImageList, { DEFAULT_QUERY } from "../PaginatedImageList";

type Props = {
  movieId: string;
};

export default function MovieDetailsImageList(props: Props) {
  const storageKey = `movie_details_${props.movieId}_images_query`;
  const persistentQuery = createSessionStorageQueryManager<typeof DEFAULT_QUERY>(storageKey);

  return (
    <PaginatedImageList
      padless
      mergeQuery={{
        movies: [props.movieId],
      }}
      persistentQuery={persistentQuery}
    />
  );
}
