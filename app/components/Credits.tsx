import Text from "./Text";

export default function Credits() {
  return (
    <>
      <ul className="leading-[2]">
        <li>
          <Text>leadwolf</Text>
        </li>
        <li>
          <Text className="inline">DebaucheryLibrarian</Text> (
          <a className="text-blue-500 hover:text-blue-700" href="https://traxxx.me" target="_blank">
            https://traxxx.me
          </a>
          )
        </li>
        <li>
          <Text>globochem</Text>
        </li>
        <li>
          <Text>Herpes3000</Text>
        </li>
        <li>
          <Text>DGs.Ch00</Text>
        </li>
        <li>
          <Text>GernBlanston</Text>
        </li>
        <li>
          <Text>john4valor</Text>
        </li>
        <li>
          <Text>Slamanna212</Text>
        </li>
        <li>
          <Text>AntiAwkward / Thews</Text>
        </li>
      </ul>

      <div>
        Missing?{" "}
        <a
          className="text-blue-500 hover:text-blue-700"
          rel="noopener noreferrer"
          target="_blank"
          href="https://gitlab.com/porn-vault/porn-vault/-/blob/dev/components/Credits.tsx"
        >
          Edit on GitLab
        </a>
      </div>
    </>
  );
}
