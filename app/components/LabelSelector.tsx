import clsx from "clsx";

import useLabelList from "../composables/use_label_list";
import { useSelector } from "../composables/use_selector";
import ILabel from "../types/label";
import Button from "./Button";
import Loader from "./Loader";
import Paper from "./Paper";
import TextInput from "./TextInput";

type Props = {
  value: string[];
  onChange: (s: string[]) => void;
  multiple?: boolean;
  itemChoices?: ILabel[];
  searchFn?: (query: string, item: ILabel) => boolean;
};

export default function LabelSelector({ value, multiple, onChange, itemChoices, searchFn }: Props) {
  const { labels } = useLabelList();

  const { query, setQuery, loading, debouncedSearch, handleClick, isSelected, items } = useSelector(
    {
      value,
      multiple,
      onChange,
      fetchFn: async (page, query) => {
        return [];
      },
      getItemById: async (id) => {
        return null;
      },
      itemChoices: labels,
      searchFn,
    }
  );

  return (
    <div className="flex flex-col gap-2">
      <TextInput
        placeholder="Search labels"
        value={query}
        onChange={(text) => {
          setQuery(text);
          debouncedSearch();
        }}
      />
      <div className="flex flex-col gap-1">
        {loading ? (
          <div className="mt-3 flex justify-center">
            <Loader />
          </div>
        ) : (
          <>
            <Button disabled={!items.length} onClick={() => onChange(items.map((x) => x._id))}>
              Select all
            </Button>
            <Button secondary disabled={!value.length} onClick={() => onChange([])}>
              Clear
            </Button>
            <div className="flex max-h-[30vh] flex-col gap-1 overflow-y-auto">
              {items.map((label) => (
                <Paper
                  flat
                  onClick={() => handleClick(label._id)}
                  className={clsx(
                    "flex cursor-pointer items-center gap-2 rounded-lg px-3 py-2 text-black transition-all dark:text-white",
                    {
                      "bg-blue-200 dark:bg-blue-950": isSelected(label._id),
                    }
                  )}
                  key={label._id}
                >
                  <div className="text-black dark:text-white flex gap-2 items-center">
                    {label.color && (
                      <div
                        className="w-3 h-3 rounded-[30%]"
                        style={{
                          backgroundColor: label.color,
                        }}
                      />
                    )}
                    <span>{label.name}</span>
                  </div>
                </Paper>
              ))}
            </div>
          </>
        )}
      </div>
    </div>
  );
}
