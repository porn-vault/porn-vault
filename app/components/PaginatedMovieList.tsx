import { defu } from "defu";
import CameraIcon from "mdi-react/CameraIcon.js";
import CameraOutlineIcon from "mdi-react/CameraOutlineIcon.js";
import LabelIcon from "mdi-react/LabelIcon.js";
import LabelOutlineIcon from "mdi-react/LabelOutlineIcon.js";
import StarOutline from "mdi-react/StarBorderIcon.js";
import StarHalf from "mdi-react/StarHalfFullIcon.js";
import Star from "mdi-react/StarIcon.js";
import { useTranslations } from "use-intl";

import useLabelList from "../composables/use_label_list";
import { editMovieList, fetchMovies } from "../composables/use_movie_list";
import { PersistentQueryManager, usePaginatedQuery } from "../composables/use_paginated_query";
import { IMovie } from "../types/movie";
import AutoLayout from "./AutoLayout";
import BookmarkIconButton from "./BookmarkIconButton";
import FavoriteIconButton from "./FavoriteIconButton";
import Flex from "./Flex";
import IconButtonMenu from "./IconButtonMenu";
import LabelSelector from "./LabelSelector";
import ListWrapper from "./ListWrapper";
import MovieCard from "./MovieCard";
import MovieCreator from "./MovieCreator";
import PageWrapper from "./PageWrapper";
import PaginatedQuery from "./PaginatedQuery";
import Rating from "./Rating";
import SimpleSelect from "./SimpleSelect";
import SortDirectionButton, { SortDirection } from "./SortDirectionButton";
import StudioSelector from "./StudioSelector";
import TextInput from "./TextInput";
import GridSizeSelector, { getGridSize } from "./GridSizeSelector";
import CardTitle from "./CardTitle";

export const DEFAULT_QUERY = {
  query: "",
  favorite: false,
  bookmark: false,
  rating: 0,
  sortBy: "relevance",
  sortDir: "desc" as SortDirection,
  include: [] as string[],
  actors: [] as string[],
  studios: [] as string[],
};

type Props = {
  persistentQuery?: PersistentQueryManager<typeof DEFAULT_QUERY>;
  mergeQuery: Partial<typeof DEFAULT_QUERY>;
  writeHead?: boolean;
  withCreator?: boolean;
  padless?: boolean;
};

export default function PaginatedMovieList(props: Props): JSX.Element {
  const t = useTranslations();

  const gridSize = getGridSize();

  const { labels: labelList, loading: labelLoader } = useLabelList();
  const hasNoLabels = !labelLoader && !labelList.length;

  const {
    query,
    setQuery,
    getPage,
    gotoPage,
    gotoStart,
    items: movies,
    error,
    loading,
    numItems,
    numPages,
    page,
    resetAll,
    setItems,
  } = usePaginatedQuery({
    defaultQuery: DEFAULT_QUERY,
    fetchFn: (page, query) => fetchMovies(page, defu(query, props.mergeQuery)),
    persistentQuery: props.persistentQuery,
  });

  const highlight = query.query.trim().split(" ");

  function editMovie(movieId: string, fn: (movie: IMovie) => IMovie) {
    setItems((prev) => editMovieList(prev, movieId, fn));
  }

  return (
    <PageWrapper
      padless={props.padless}
      title={props.writeHead ? t("foundMovies", { numItems }) : undefined}
    >
      <AutoLayout>
        <div className="flex gap-2 justify-between items-center">
          <CardTitle>{loading ? "Loading..." : t("foundMovies", { numItems })}</CardTitle>
          <div className="flex gap-2 items-center">
            <MovieCreator onCreate={() => getPage(page, query)} />
            <GridSizeSelector />
          </div>
        </div>
        <PaginatedQuery
          loading={loading}
          errorMessage={error}
          resetAll={resetAll}
          refreshPage={() => getPage(page, query)}
          filters={
            <>
              <TextInput
                className="max-w-[120px]"
                onEnter={gotoStart}
                value={query.query}
                onChange={(text) =>
                  setQuery((prev) => ({
                    ...prev,
                    query: text,
                  }))
                }
                placeholder={t("action.findContent")}
              />
              <FavoriteIconButton
                value={query.favorite}
                onClick={() => {
                  setQuery((prev) => ({
                    ...prev,
                    favorite: !prev.favorite,
                  }));
                }}
              />
              <BookmarkIconButton
                value={query.bookmark}
                onClick={() => {
                  setQuery((prev) => ({
                    ...prev,
                    bookmark: !prev.bookmark,
                  }));
                }}
              />
              <IconButtonMenu
                value={!!query.rating}
                activeIcon={query.rating === 10 ? Star : StarHalf}
                inactiveIcon={StarOutline}
              >
                <Rating
                  value={query.rating}
                  onChange={(rating) => {
                    setQuery((prev) => ({
                      ...prev,
                      rating,
                    }));
                  }}
                />
              </IconButtonMenu>
              <IconButtonMenu
                counter={query.include.length}
                value={!!query.include.length}
                activeIcon={LabelIcon}
                inactiveIcon={LabelOutlineIcon}
                isLoading={labelLoader}
                disabled={hasNoLabels}
              >
                <LabelSelector
                  multiple
                  value={query.include}
                  searchFn={(query, label) =>
                    label.name.toLowerCase().includes(query.toLowerCase()) ||
                    label.aliases.some((alias) => alias.toLowerCase().includes(query.toLowerCase()))
                  }
                  itemChoices={labelList}
                  onChange={(labels) => {
                    setQuery((prev) => ({
                      ...prev,
                      include: labels,
                    }));
                  }}
                />
              </IconButtonMenu>
              <IconButtonMenu
                counter={query.studios.length}
                value={!!query.studios.length}
                activeIcon={CameraIcon}
                inactiveIcon={CameraOutlineIcon}
                isLoading={false}
                disabled={false}
              >
                <StudioSelector
                  multiple
                  value={query.studios}
                  onChange={(studios) => {
                    setQuery((prev) => ({
                      ...prev,
                      studios,
                    }));
                  }}
                />
              </IconButtonMenu>
              <>
                <SimpleSelect
                  items={[
                    {
                      text: t("relevance"),
                      value: "relevance",
                    },
                    {
                      text: t("addedToCollection"),
                      value: "addedOn",
                    },
                    {
                      text: t("rating"),
                      value: "rating",
                    },
                    {
                      text: t("duration"),
                      value: "duration",
                    },
                    {
                      text: t("numScenes"),
                      value: "numScenes",
                    },
                    {
                      text: t("numActors"),
                      value: "numActors",
                    },
                    {
                      text: t("releaseDate"),
                      value: "releaseDate",
                    },
                    {
                      text: t("bookmark"),
                      value: "bookmark",
                    },
                  ]}
                  value={query.sortBy}
                  onChange={(sortBy) => {
                    setQuery((prev) => ({
                      ...prev,
                      sortBy: sortBy ?? "relevance",
                    }));
                  }}
                />
                <SortDirectionButton
                  value={query.sortDir}
                  onChange={(sortDir) => {
                    setQuery((prev) => ({
                      ...prev,
                      sortDir,
                    }));
                  }}
                />
              </>
            </>
          }
          currentPage={page}
          numPages={numPages}
          setPage={gotoPage}
        >
          <ListWrapper size={gridSize} loading={loading} noResults={!numItems}>
            {movies.map((movie) => (
              <MovieCard
                highlight={highlight}
                onFav={(value) => {
                  editMovie(movie._id, (movie) => {
                    movie.favorite = value;
                    return movie;
                  });
                }}
                onBookmark={(value) => {
                  editMovie(movie._id, (movie) => {
                    movie.bookmark = !!value;
                    return movie;
                  });
                }}
                key={movie._id}
                movie={movie}
              />
            ))}
          </ListWrapper>
        </PaginatedQuery>
      </AutoLayout>
    </PageWrapper>
  );
}
