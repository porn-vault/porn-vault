import clsx from "clsx";

type Props<T extends string> = {
  states: readonly T[];
  value: T;
  onChange: (value: T) => void;
  formatTitle: (value: T) => string;
};

export default function Tabs<T extends string>(props: Props<T>): JSX.Element {
  const { states, value, onChange, formatTitle } = props;

  return (
    <div className="flex gap-3 pl-2 font-semibold">
      {states.map((state) => (
        <div
          key={state}
          className={clsx("cursor-pointer transition-colors", {
            "text-black dark:text-white": value === state,
            "text-gray-500 dark:text-gray-600 hover:text-gray-700 hover:dark:text-gray-400":
              value !== state,
          })}
          onClick={() => onChange(state)}
        >
          {formatTitle(state)}
        </div>
      ))}
    </div>
  );
}
