import clsx from "clsx";
import dayjs from "dayjs";
import utc from "dayjs/plugin/utc.js";
import { useEffect, useState } from "react";
import InputMask from "react-input-mask";

dayjs.extend(utc);

type Props = {
  value: Date | null;
  onChange: (date: Date | null) => void;
};

export default function DateInput({ value, onChange }: Props): JSX.Element {
  const [text, setText] = useState("");

  useEffect(() => {
    if (value) {
      setText(dayjs(value).format("YYYY/MM/DD"));
    }
  }, [value?.valueOf()]);

  return (
    <InputMask
      suppressHydrationWarning
      className={clsx(
        "rounded border-2 px-2 py-1 !outline-none transition-colors",
        "border-gray-300 bg-gray-50 text-black hover:bg-gray-200 focus:border-blue-500 focus:bg-gray-100",
        "dark:border-gray-700 dark:bg-gray-800 dark:text-white hover:dark:bg-gray-900 focus:dark:border-blue-800 focus:dark:bg-gray-900"
      )}
      placeholder="YYYY-MM-DD"
      value={text}
      mask="9999/99/99"
      maskChar={null}
      onChange={(ev) => {
        const text = ev.target.value;
        setText(text);

        if (/^\d{4}\/\d{2}\/\d{2}$/.test(text)) {
          const parsed = dayjs.utc(text, "YYYY/MM/DD");

          if (parsed.isValid()) {
            onChange(parsed.toDate());
          } else {
            onChange(null);
          }
        } else {
          onChange(null);
        }
      }}
    />
  );
}
