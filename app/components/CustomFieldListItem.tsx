import clsx from "clsx";
import DeleteIcon from "mdi-react/DeleteIcon.js";

import { removeCustomField } from "~/util/mutations/custom_field";

import type CustomField from "../../server/types/custom_field";
import CustomFieldEditor from "./CustomFieldEditor";
import IconButton from "./IconButton";
import Spacer from "./Spacer";

type Props = Omit<CustomField, "_id"> & {
  id: string;
  index: number;
  onEdit?: () => void;
  onDelete?: () => void;
};

export default function CustomFieldListItem({
  index,
  onDelete,
  onEdit,
  ...field
}: Props): JSX.Element {
  return (
    <div
      key={field.id}
      className={clsx(
        "flex gap-3 items-center p-3 text-gray-800 dark:text-gray-300 transition-all",
        {
          "bg-gray-50 dark:bg-gray-800": index % 2 === 0,
          "bg-gray-200 dark:bg-gray-900": index % 2 !== 0,
        }
      )}
    >
      <div>
        <div className="flex gap-2 items-center">
          <div>
            {field.name} {field.unit && <i>[{field.unit}]</i>} (for {field.target.join(", ")})
          </div>
        </div>
        <div className="text-xs text-gray-600 dark:text-gray-500">
          {field.type} {!!field.values?.length && <>({field.values.join(", ")})</>}
        </div>
      </div>
      <Spacer />
      <CustomFieldEditor {...field} onEdit={onEdit} />
      <IconButton
        onClick={async () => {
          if (confirm("Really delete?")) {
            await removeCustomField(field.id);
            onDelete?.();
          }
        }}
        size={20}
        className="text-red-500 shrink-0"
        Icon={DeleteIcon}
      />
    </div>
  );
}
