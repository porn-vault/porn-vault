import CloseIcon from "mdi-react/CloseIcon.js";
import { ReactNode, useRef, useState } from "react";
import { createPortal } from "react-dom";

import { useMount } from "~/composables/use_mounted";

import AutoLayout from "./AutoLayout";
import Card from "./Card";
import CardTitle from "./CardTitle";
import IconButton from "./IconButton";
import Spacer from "./Spacer";

type Props = {
  children: ReactNode;
  isOpen: boolean;
  title: string;
  onClose?: () => void;
  actions?: ReactNode;
};

export default function Window({ children, isOpen, title, onClose, actions }: Props) {
  const ref = useRef<Element | null>(null);
  const [mounted, setMounted] = useState(false);

  useMount(() => {
    ref.current = document.querySelector<HTMLElement>("#modal-portal");
    setMounted(true);
  });

  return mounted && ref.current
    ? createPortal(
        <>
          {isOpen && (
            <div
              className="z-[3000] fixed w-full h-full left-0 top-0"
              onClick={(event) => event.stopPropagation()}
            >
              <div
                className="absolute w-full h-full left-0 top-0"
                onClick={onClose}
                style={{
                  // lags on firefox
                  // backdropFilter: "blur(2px)",
                  background: "#000000cc",
                }}
              ></div>
              <div
                className="w-full max-w-lg absolute left-[50%] top-[50%]"
                style={{
                  transform: "translate(-50%,-50%)",
                }}
              >
                <Card className="!p-4">
                  <AutoLayout>
                    <AutoLayout layout="h">
                      <CardTitle>{title}</CardTitle>
                      <Spacer />
                      <IconButton Icon={CloseIcon} onClick={onClose} />
                    </AutoLayout>
                    <AutoLayout className="max-h-[66vh] overflow-y-auto" gap={10}>
                      {children}
                    </AutoLayout>
                    {actions && (
                      <AutoLayout layout="h" gap={5}>
                        <Spacer />
                        {actions}
                      </AutoLayout>
                    )}
                  </AutoLayout>
                </Card>
              </div>
            </div>
          )}
        </>,
        ref.current
      )
    : null;
}
