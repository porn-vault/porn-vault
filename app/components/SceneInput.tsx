import { useWindow } from "~/composables/use_window";

import Button from "./Button";
import Flex from "./Flex";
import SceneSelector from "./SceneSelector";
import Window from "./Window";

type Props = {
  value: string[];
  onChange: (scenes: string[]) => void;
};

export default function SceneInput({ value, onChange }: Props) {
  const { isOpen, open, close } = useWindow();

  return (
    <Flex layout="h">
      <Button onClick={open}>
        {value.length ? `Selected ${value.length} scenes` : "Select scenes"}
      </Button>
      <Window
        actions={
          <>
            <Button secondary onClick={close}>
              Close
            </Button>
          </>
        }
        onClose={close}
        title="Select scenes"
        isOpen={isOpen}
      >
        <SceneSelector
          multiple
          value={value}
          onChange={(ids) => {
            onChange(ids);
          }}
        />
      </Window>
    </Flex>
  );
}
