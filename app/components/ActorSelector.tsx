import clsx from "clsx";
import { useTranslations } from "use-intl";

import { fetchActors } from "../composables/use_actor_list";
import { useSelector } from "../composables/use_selector";
import { IActor } from "../types/actor";
import { graphqlQuery } from "../util/gql";
import Avatar from "./Avatar";
import Button from "./Button";
import Loader from "./Loader";
import Paper from "./Paper";
import TextInput from "./TextInput";

type Props = {
  value: string[];
  onChange: (s: string[]) => void;
  multiple?: boolean;
  itemChoices?: IActor[];
  searchFn?: (query: string, item: IActor) => boolean;
};

async function getActorById(id: string): Promise<IActor | null> {
  const query = `
query ($id: String!) {
  getActorById(id: $id) {
    _id
    name
    avatar {
      _id
    }
  }
}`;

  const { getActorById } = await graphqlQuery<{
    getActorById: IActor;
  }>(query, { id });

  return getActorById;
}

export default function ActorSelector({ value, multiple, onChange, itemChoices, searchFn }: Props) {
  const t = useTranslations();

  const {
    query,
    setQuery,
    items: actors,
    loading,
    debouncedSearch,
    handleClick,
    isSelected,
  } = useSelector({
    value,
    multiple,
    onChange,
    fetchFn: (page, query) => fetchActors(page, query).then(({ items }) => items),
    getItemById: getActorById,
    itemChoices,
    searchFn,
  });

  return (
    <div className="flex flex-col gap-2">
      <TextInput
        placeholder={t("action.findActors")}
        value={query}
        onChange={(text) => {
          setQuery(text);
          debouncedSearch();
        }}
      />
      <div className="flex flex-col gap-1">
        {loading ? (
          <div className="mt-3 flex justify-center">
            <Loader />
          </div>
        ) : (
          <>
            <Button disabled={!actors.length} onClick={() => onChange(actors.map((x) => x._id))}>
              Select all
            </Button>
            <Button secondary disabled={!value.length} onClick={() => onChange([])}>
              Clear
            </Button>
            {actors.map((actor) => (
              <Paper
                onClick={() => handleClick(actor._id)}
                className={clsx(
                  "flex cursor-pointer items-center gap-3 rounded-lg px-3 py-1 text-black transition-all dark:text-white",
                  {
                    "!bg-blue-200 dark:!bg-blue-950": isSelected(actor._id),
                    "bg-white dark:bg-gray-800": !isSelected(actor._id),
                  }
                )}
                key={actor._id}
              >
                <div className="w-[48px]">
                  <Avatar avatarId={actor.avatar?._id} />
                </div>
                <div className="max-w-[120px] truncate whitespace-pre text-black dark:text-white">
                  {actor.name}
                </div>
              </Paper>
            ))}
          </>
        )}
      </div>
    </div>
  );
}
