import MenuIcon from "mdi-react/HamburgerMenuIcon.js";
import { useState } from "react";
import { useTranslations } from "use-intl";

import { useLocaleNavigate } from "~/composables/use_navigate";

import LocaleLink from "../Link";
import Paper from "../Paper";
import Spacer from "../Spacer";
import TextInput from "../TextInput";

type Props = {
  setSidebar: (x: boolean) => void;
};

export default function Topbar({ setSidebar }: Props) {
  const t = useTranslations();
  const [searchText, setSearchText] = useState("");
  const navigate = useLocaleNavigate();

  return (
    <Paper className="topbar fixed flex w-full items-center gap-2 p-2">
      <MenuIcon
        onClick={() => setSidebar(true)}
        className="mobile-sidebar-toggle cursor-pointer dark:text-white"
      />
      <LocaleLink to="/">
        <img
          className="cursor-pointer transition-all hover:brightness-[0.8]"
          width={36}
          height={36}
          src="/favicon.png"
        />
      </LocaleLink>

      <a
        className="hover:text-gray-500 transition-colors"
        target="_blank"
        rel="noopener noreferrer"
        href="https://discord.gg/QfeHYtKGEa"
      >
        <img
          className="invert dark:invert-0 cursor-pointer transition-all hover:brightness-[0.8]"
          width={24}
          height={24}
          src="/icons/discord.svg"
        />
      </a>
      <Spacer />
      <TextInput
        onEnter={() => {
          navigate(`/search?q=${searchText.trim()}`);
        }}
        value={searchText}
        onChange={setSearchText}
        placeholder={t("action.findContent")}
      />
    </Paper>
  );
}
