import { ReactNode } from "react";

import LocaleLink from "../Link";

type Props = {
  children: ReactNode;
  url: string;
  icon: ReactNode;
  onClick?: () => void;
};

export default function SidebarLink({ children, url, onClick, icon }: Props) {
  return (
    <div onClick={onClick}>
      <LocaleLink
        className="flex items-center gap-3 rounded-lg p-2 text-sm text-gray-800 transition-colors hover:bg-gray-300 dark:text-gray-300 hover:dark:bg-gray-800"
        activeClass="bg-blue-200 dark:bg-blue-900"
        to={url}
      >
        {icon}
        <div>{children}</div>
      </LocaleLink>
    </div>
  );
}
