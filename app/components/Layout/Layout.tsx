import { ReactNode, useState } from "react";

import Sidebar from "./Sidebar";
import Topbar from "./Topbar";

type Props = {
  children: ReactNode;
};

export default function Layout({ children }: Props): JSX.Element {
  const [mobileSidebar, setMobileSidebar] = useState(false);

  function setSidebar(x: boolean) {
    setMobileSidebar(x);
  }

  return (
    <div className="layout">
      <Topbar setSidebar={setSidebar} />
      <Sidebar active={mobileSidebar} setSidebar={setSidebar} />
      {/* NOTE: Need to use clip instead of hidden because of position: sticky */}
      <div className="content pb-10 overflow-x-clip">{children}</div>
    </div>
  );
}
