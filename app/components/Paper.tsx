import clsx from "clsx";
import { CSSProperties, MouseEvent, ReactNode } from "react";

type Props = {
  children?: ReactNode;
  style?: CSSProperties;
  className?: string;
  onClick?: (event: MouseEvent<HTMLElement>) => void;
  flat?: boolean;
  bordered?: boolean;
  rounded?: boolean;
};

export default function Paper({
  bordered,
  onClick,
  children,
  style,
  className,
  flat,
  rounded,
}: Props): JSX.Element {
  return (
    <div
      onClick={onClick}
      className={clsx(className, {
        "bg-gray-100 dark:bg-gray-900 dark:text-gray-300": !flat,
        "border-2 border-gray-200 dark:border-gray-800": bordered,
        "rounded-lg": rounded,
      })}
      style={style}
    >
      {children}
    </div>
  );
}
