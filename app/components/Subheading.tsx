import { ReactNode } from "react";

type Props = {
  children: ReactNode;
};

export default function Subheading({ children }: Props) {
  return <div className="mb-1 text-sm text-gray-800 dark:text-gray-200">{children}</div>;
}
