import clsx from "clsx";

type Props = {
  value: string;
  onChange: (value: string) => void;
  onEnter?: () => void;
  placeholder: string;
  className?: string;
  rows?: number;
};

export default function Textarea({
  value,
  onChange,
  placeholder,
  onEnter,
  className,
  rows,
}: Props): JSX.Element {
  return (
    <textarea
      rows={rows}
      className={clsx(
        className,
        "rounded border-2 px-2 py-1 !outline-none transition-colors",
        "border-gray-300 bg-gray-50 text-black hover:bg-gray-200 focus:border-blue-500 focus:bg-gray-100",
        "dark:border-gray-700 dark:bg-gray-800 dark:text-white hover:dark:bg-gray-900 focus:dark:border-blue-800 focus:dark:bg-gray-900"
      )}
      placeholder={placeholder}
      value={value}
      onChange={(ev) => {
        onChange(ev.target.value);
      }}
      onKeyDown={(ev) => {
        if (ev.key === "Enter" || ev.key === "NumpadEnter") {
          onEnter?.();
        }
      }}
    />
  );
}
