import HeartIcon from "mdi-react/HeartIcon.js";

import { useSafeMode } from "../composables/use_safe_mode";
import { thumbnailUrl } from "../util/thumbnail";
import Card from "./Card";
import LocaleLink from "./Link";
import ResponsiveImage from "./ResponsiveImage";

type Props = {
  name: string;
  thumbnailId?: string;
  favorite?: boolean;
  id: string;
};

export default function ActorGridItem({ id, name, thumbnailId, favorite }: Props) {
  const { blur: safeModeBlur } = useSafeMode();

  return (
    <Card padless>
      <LocaleLink
        to={`/actor/${id}`}
        className="block cursor-pointer overflow-hidden rounded-lg transition-[filter] hover:brightness-[0.8]"
      >
        <div className="relative">
          <ResponsiveImage
            className="rounded-lg"
            aspectRatio="3 / 4"
            src={thumbnailId && thumbnailUrl(thumbnailId)}
            imgStyle={{
              filter: safeModeBlur,
            }}
          />
          {favorite && (
            <div className="absolute right-1 top-1">
              <HeartIcon className="text-red-500" />
            </div>
          )}
          <div
            className="text-white text-center absolute truncate p-2 rounded"
            style={{
              fontSize: 13,
              fontWeight: 600,
              background: "#000000bb",
              bottom: 5,
              left: 0,
              right: 0,
              margin: "0 6px",
            }}
          >
            {name}
          </div>
        </div>
      </LocaleLink>
    </Card>
  );
}
