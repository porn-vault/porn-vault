import { CSSProperties, ReactNode } from "react";

import Text from "./Text";

type Props = {
  children: ReactNode;
  style?: CSSProperties;
};

export default function Description({ children, style }: Props) {
  return (
    <Text
      className="leading-[1.5] break-words text-justify text-sm whitespace-pre-wrap overflow-hidden"
      style={style}
    >
      {children}
    </Text>
  );
}
