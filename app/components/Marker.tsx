import { useState } from "react";

import { useSafeMode } from "../composables/use_safe_mode";

type Props = {
  time: number;
  name: string;
  thumbnail: string;
  onClick?: () => void;
};

export default function Marker({ time, name, thumbnail, onClick }: Props) {
  const [hover, setHover] = useState(false);
  const { blur: safeModeBlur } = useSafeMode();

  return (
    <div
      suppressHydrationWarning
      onClick={(ev) => {
        ev.stopPropagation();
        onClick?.();
      }}
      onMouseEnter={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
      className="w-[4px] h-[14px] top-[-3px] transition-all cursor-pointer rounded-sm bg-emerald-500 hover:bg-emerald-600 absolute"
      style={{
        left: `calc(100% * ${time} - 2px)`,
      }}
    >
      {hover && (
        <div className="border-2 border-gray-400 left-[-80px] w-[160px] bottom-[25px] overflow-hidden rounded bg-black bg-opacity-80 text-center absolute">
          <img
            className="w-[160px] overflow-hidden"
            style={{ filter: safeModeBlur }}
            src={thumbnail}
          />
          <div className="my-1 text-center text-xs font-semibold text-white">{name}</div>
        </div>
      )}
    </div>
  );
}
