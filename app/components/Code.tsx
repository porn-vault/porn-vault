import { CSSProperties, useState } from "react";
import YAML from "yaml";

import { useTheme } from "~/composables/use_dark_theme";

import AutoLayout from "./AutoLayout";
import Button from "./Button";

type Props<T> = {
  value: T;
  style?: CSSProperties;
};

export default function Code<T>({ value, style }: Props<T>) {
  const { theme } = useTheme();
  const [formatter, setFormatter] = useState<"json" | "yaml">("json");

  function formatText(value: T): string {
    if (formatter === "json") {
      return JSON.stringify(value, null, 2);
    }
    return YAML.stringify(value);
  }

  return (
    <div className="p-1" style={style}>
      <AutoLayout layout="h" gap={5} className="mb-4">
        <Button secondary={formatter !== "json"} onClick={() => setFormatter("json")}>
          JSON
        </Button>
        <Button secondary={formatter !== "yaml"} onClick={() => setFormatter("yaml")}>
          YAML
        </Button>
      </AutoLayout>
      <pre className="text-sm rounded overflow-y-hidden p-1 bg-gray-200 dark:bg-black text-black dark:text-white max-h-[300px] m-0 font-mono opacity-80 leading-[1.5] overflow-y-scroll">
        {formatText(value)}
      </pre>
    </div>
  );
}
