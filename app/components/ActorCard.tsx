import dayjs from "dayjs";
import { useMemo, useState } from "react";
import Highlighter from "react-highlight-words";
import { useTranslations } from "use-intl";

import { buildSearchWords, HIGHLIGHT_CLASSES } from "~/util/highlight";

import { useSafeMode } from "../composables/use_safe_mode";
import { useSettings } from "../composables/use_settings";
import { useTitleColor } from "../composables/use_title_color";
import { IActor } from "../types/actor";
import { IScene } from "../types/scene";
import { bookmarkActor, favoriteActor, rateActor } from "../util/mutations/actor";
import { thumbnailUrl } from "../util/thumbnail";
import BookmarkIconButton from "./BookmarkIconButton";
import Card from "./Card";
import FavoriteIconButton from "./FavoriteIconButton";
import Flag from "./Flag";
import Flex from "./Flex";
import LabelGroup from "./LabelGroup";
import LocaleLink from "./Link";
import Rating from "./Rating";
import ResponsiveImage from "./ResponsiveImage";
import Spacer from "./Spacer";

type Props = {
  actor: Pick<
    IActor,
    | "_id"
    | "altThumbnail"
    | "thumbnail"
    | "favorite"
    | "bookmark"
    | "age"
    | "nationality"
    | "name"
    | "rating"
    | "labels"
    | "bornOn"
    | "numScenes"
  >;
  scene?: IScene;
  onFav: (value: boolean) => void;
  onBookmark: (value: Date | null) => void;
  onRate: (rating: number) => void;

  highlight?: string[];
};

function calculateAge(bornOn?: number, sceneDate?: number) {
  if (bornOn) {
    return dayjs(sceneDate).diff(bornOn, "years");
  }
}

export default function ActorCard({ actor, onFav, onBookmark, onRate, scene, highlight }: Props) {
  const t = useTranslations();
  const { blur: safeModeBlur } = useSafeMode();
  const { showCardLabels, actorImageAspect } = useSettings();
  const titleColor = useTitleColor(actor.thumbnail?.color);

  const [hover, setHover] = useState(false);

  const thumbSrc = useMemo(() => {
    if (hover && actor.altThumbnail) {
      return actor.altThumbnail && thumbnailUrl(actor.altThumbnail._id);
    }
    return actor.thumbnail && thumbnailUrl(actor.thumbnail._id);
  }, [hover]);

  async function toggleFav(): Promise<void> {
    const newValue = !actor.favorite;
    await favoriteActor(actor._id, newValue);
    onFav(newValue);
  }

  async function toggleBookmark(): Promise<void> {
    const newValue = actor.bookmark ? null : new Date();
    await bookmarkActor(actor._id, newValue);
    onBookmark(newValue);
  }

  async function changeRating(rating: number): Promise<void> {
    await rateActor(actor._id, rating);
    onRate(rating);
  }

  return (
    <Card padless className="relative">
      <div
        className="block cursor-pointer overflow-hidden rounded-lg transition-[filter] hover:brightness-[0.8]"
        onMouseEnter={() => setHover(true)}
        onMouseLeave={() => setHover(false)}
      >
        <LocaleLink to={`/actor/${actor._id}`}>
          <ResponsiveImage
            className="rounded-lg"
            aspectRatio={actorImageAspect.cssValue}
            src={thumbSrc}
            imgStyle={{ transition: "filter 0.1s ease-in-out", filter: safeModeBlur }}
          />
        </LocaleLink>
      </div>
      <Flex
        className="absolute left-0 top-0 gap-1 rounded p-1 text-white"
        layout="h"
        style={{
          background: "#000000bb",
        }}
      >
        <FavoriteIconButton alwaysLight value={actor.favorite} onClick={toggleFav} />
        <BookmarkIconButton alwaysLight value={actor.bookmark} onClick={toggleBookmark} />
      </Flex>
      <Flex className="gap-1 px-2 pb-1">
        <Flex className="gap-1 font-semibold" layout="h">
          {actor.nationality && (
            <Flag name={actor.nationality.nationality} size={18} code={actor.nationality.alpha2} />
          )}
          <div
            className="overflow-hidden text-ellipsis whitespace-nowrap"
            style={{
              color: titleColor,
            }}
          >
            <Highlighter
              highlightClassName={HIGHLIGHT_CLASSES}
              searchWords={buildSearchWords(highlight)}
              autoEscape={true}
              textToHighlight={actor.name}
            />
          </div>
          <Spacer />
          {actor.age && <div>{actor.age}</div>}
        </Flex>

        <div className="text-sm text-gray-400 dark:text-gray-500">
          {actor.numScenes} {t("scene", { numItems: actor.numScenes })}
        </div>

        <Rating onChange={changeRating} value={actor.rating || 0} />

        {showCardLabels && !!actor.labels.length && (
          <div className="mt-1">
            <LabelGroup highlight={highlight} value={actor.labels} />
          </div>
        )}

        {actor.bornOn && scene?.releaseDate && (
          <div className="text-xs text-gray-400 dark:text-gray-500">
            {calculateAge(actor.bornOn, scene.releaseDate)} y/o in this scene
          </div>
        )}
      </Flex>
    </Card>
  );
}
