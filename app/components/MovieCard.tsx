import { useMemo, useState } from "react";
import Highlighter from "react-highlight-words";
import { useTranslations } from "use-intl";

import { buildSearchWords, HIGHLIGHT_CLASSES } from "~/util/highlight";

import { useSafeMode } from "../composables/use_safe_mode";
import { useSettings } from "../composables/use_settings";
import { useTitleColor } from "../composables/use_title_color";
import { IMovie } from "../types/movie";
import { bookmarkMovie, favoriteMovie } from "../util/mutations/movie";
import { formatDuration } from "../util/string";
import { thumbnailUrl } from "../util/thumbnail";
import ActorList from "./ActorList";
import BookmarkIconButton from "./BookmarkIconButton";
import Card from "./Card";
import FavoriteIconButton from "./FavoriteIconButton";
import Flex from "./Flex";
import LabelGroup from "./LabelGroup";
import LocaleLink from "./Link";
import Rating from "./Rating";
import ResponsiveImage from "./ResponsiveImage";
import Spacer from "./Spacer";

type Props = {
  movie: IMovie;
  onFav: (value: boolean) => void;
  onBookmark: (value: Date | null) => void;

  highlight?: string[];
};

export default function MovieCard({ movie, onFav, onBookmark, highlight }: Props) {
  const { blur: safeModeBlur } = useSafeMode();
  const { showCardLabels } = useSettings();
  const t = useTranslations();
  const [hover, setHover] = useState(false);
  const titleColor = useTitleColor(hover ? movie.backCover?.color : movie.frontCover?.color);

  const thumbSrc = useMemo(() => {
    if (hover && movie.backCover) {
      return movie.backCover && thumbnailUrl(movie.backCover._id);
    }
    return movie.frontCover && thumbnailUrl(movie.frontCover._id);
  }, [hover]);

  async function toggleFav(): Promise<void> {
    const newValue = !movie.favorite;
    await favoriteMovie(movie._id, newValue);
    onFav(newValue);
  }

  async function toggleBookmark(): Promise<void> {
    const newValue = movie.bookmark ? null : new Date();
    await bookmarkMovie(movie._id, newValue);
    onBookmark(newValue);
  }

  return (
    <Card padless className="relative">
      <div
        className="block cursor-pointer overflow-hidden rounded-lg transition-[filter] hover:brightness-[0.8]"
        onMouseEnter={() => setHover(true)}
        onMouseLeave={() => setHover(false)}
      >
        <LocaleLink to={`/movie/${movie._id}`}>
          <ResponsiveImage
            aspectRatio="0.71"
            src={thumbSrc}
            imgStyle={{
              transition: "filter 0.1s ease-in-out",
              filter: safeModeBlur,
              display: "block",
            }}
          >
            <Flex
              layout="h"
              className="absolute bottom-1 left-1 gap-1 rounded text-xs text-white"
            >
              <div className="rounded p-1" style={{ background: "#000000bb" }}>
                <b>{movie.numScenes}</b> {t("scene", { numItems: movie.numScenes })}
              </div>
              {!!movie.duration && (
                <div className="font-semibold text-white">
                  <div className="rounded p-1" style={{ background: "#000000bb" }}>
                    <b>{formatDuration(movie.duration)}</b>
                  </div>
                </div>
              )}
            </Flex>
          </ResponsiveImage>
        </LocaleLink>
      </div>
      <Flex
        className="absolute left-0 top-0 gap-1 rounded p-1 text-white"
        layout="h"
        style={{
          background: "#000000bb",
        }}
      >
        <FavoriteIconButton alwaysLight value={movie.favorite} onClick={toggleFav} />
        <BookmarkIconButton alwaysLight value={movie.bookmark} onClick={toggleBookmark} />
      </Flex>

      <Flex className="gap-1 px-2 pb-1">
        <Flex className="gap-1 font-semibold" layout="h">
          {movie.studio && (
            <LocaleLink to={`/studio/${movie.studio._id}`}>
              <div className="text-xs font-semibold uppercase text-gray-600 transition-colors hover:text-gray-400 dark:text-gray-500 dark:hover:text-gray-600">
                {movie.studio.name}
              </div>
            </LocaleLink>
          )}
          <Spacer />
          {movie.releaseDate && (
            <div className="text-xs text-gray-600 dark:text-gray-400">
              {new Date(movie.releaseDate).toLocaleDateString()}
            </div>
          )}
        </Flex>

        <div
          className="ellipsis overflow-hidden whitespace-nowrap font-semibold"
          style={{ color: titleColor }}
          title={movie.name}
        >
          <Highlighter
            highlightClassName={HIGHLIGHT_CLASSES}
            searchWords={buildSearchWords(highlight)}
            autoEscape={true}
            textToHighlight={movie.name}
          />
        </div>

        {!!movie.actors.length && <ActorList highlight={highlight} actors={movie.actors} />}

        <Rating value={movie.rating || 0} readonly />

        {showCardLabels && !!movie.labels.length && (
          <div className="mt-1">
            <LabelGroup highlight={highlight} value={movie.labels} />
          </div>
        )}
      </Flex>
    </Card>
  );
}
