export default function Divider(): JSX.Element {
  return <div className="h-[1px] w-full bg-gray-300 dark:bg-gray-800" />;
}
