import clsx from "clsx";
import { useTranslations } from "use-intl";

import { useSelector } from "../composables/use_selector";
import { fetchStudios } from "../composables/use_studio_list";
import { IStudio } from "../types/studio";
import { graphqlQuery } from "../util/gql";
import Button from "./Button";
import Loader from "./Loader";
import Paper from "./Paper";
import TextInput from "./TextInput";

type Props = {
  value: string[];
  onChange: (s: string[]) => void;
  multiple?: boolean;
  itemChoices?: IStudio[];
  searchFn?: (query: string, item: IStudio) => boolean;
};

async function getStudioById(id: string): Promise<IStudio | null> {
  const q = `
  query ($id: String!) {
    getStudioById(id: $id) {
      _id
      name
    }
  }
  `;

  const { getStudioById } = await graphqlQuery<{
    getStudioById: IStudio;
  }>(q, { id });

  return getStudioById;
}

export default function StudioSelector({
  value,
  multiple,
  onChange,
  itemChoices,
  searchFn,
}: Props) {
  const t = useTranslations();

  const {
    query,
    setQuery,
    items: studios,
    loading,
    debouncedSearch,
    handleClick,
    isSelected,
  } = useSelector({
    value,
    multiple,
    onChange,
    fetchFn: (page, query) => fetchStudios(page, query).then(({ items }) => items),
    getItemById: getStudioById,
    itemChoices,
    searchFn,
  });

  return (
    <div className="flex flex-col gap-2">
      <TextInput
        placeholder={t("action.findStudios")}
        value={query}
        onChange={(text) => {
          setQuery(text);
          debouncedSearch();
        }}
      />
      <div className="flex flex-col gap-1">
        {loading ? (
          <div className="mt-3 flex justify-center">
            <Loader />
          </div>
        ) : (
          <>
            <Button disabled={!studios.length} onClick={() => onChange(studios.map((x) => x._id))}>
              Select all
            </Button>
            <Button secondary disabled={!value.length} onClick={() => onChange([])}>
              Clear
            </Button>
            {studios.map((studio) => (
              <Paper
                onClick={() => handleClick(studio._id)}
                className={clsx(
                  "flex cursor-pointer items-center gap-2 rounded-lg px-3 py-2 text-black transition-all dark:text-white",
                  {
                    "!bg-blue-200 dark:!bg-blue-950": isSelected(studio._id),
                    "bg-white dark:bg-gray-800": !isSelected(studio._id),
                  }
                )}
                key={studio._id}
              >
                <div className="text-black dark:text-white">{studio.name}</div>
              </Paper>
            ))}
          </>
        )}
      </div>
    </div>
  );
}
