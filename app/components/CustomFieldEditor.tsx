import EditIcon from "mdi-react/EditIcon.js";
import { useState } from "react";
import { useTranslations } from "use-intl";

import { useWindow } from "~/composables/use_window";
import { updateCustomField } from "~/util/mutations/custom_field";

import type { CustomFieldType } from "../../server/types/custom_field";
import Button from "./Button";
import IconButton from "./IconButton";
import Subheading from "./Subheading";
import Textarea from "./Textarea";
import TextInput from "./TextInput";
import Window from "./Window";

type Props = {
  id: string;
  name: string;
  unit: string | null;
  type: CustomFieldType;
  values: string[] | null;
  onEdit?: () => void;
};

export default function CustomFieldEditor(props: Props): JSX.Element {
  const t = useTranslations();
  const { isOpen, close, open } = useWindow();

  const [name, setName] = useState(props.name);
  const [unit, setUnit] = useState(props.unit || "");
  const [values, setValues] = useState<string[]>(props.values ?? []);

  const [error, setError] = useState<string | undefined>();
  const [loading, setLoader] = useState(false);

  return (
    <>
      <IconButton onClick={open} className="shrink-0" size={20} Icon={EditIcon} />
      <Window
        onClose={close}
        isOpen={isOpen}
        title={t("action.editCustomField")}
        actions={
          <>
            <Button
              loading={loading}
              onClick={async () => {
                try {
                  setLoader(true);

                  await updateCustomField(props.id, {
                    name,
                    unit,
                    values: values.filter(Boolean),
                  });

                  props.onEdit?.();
                  close();
                } catch (error) {
                  console.error(error);
                  if (error instanceof Error) {
                    setError(error.message);
                  } else {
                    setError("An error occurred");
                  }
                }

                setLoader(false);
              }}
            >
              {t("action.save")}
            </Button>
            <Button secondary onClick={close}>
              Close
            </Button>
          </>
        }
      >
        <div>
          <Subheading>Field name *</Subheading>
          <TextInput
            className="w-full"
            value={name}
            onChange={setName}
            placeholder="Enter a field name"
          />
        </div>
        {props.type !== "BOOLEAN" && (
          <div>
            <Subheading>Unit (optional)</Subheading>
            <TextInput
              className="w-full"
              value={unit}
              onChange={setUnit}
              placeholder="Enter a unit (e.g. cm)"
            />
            {props.type === "NUMBER" && (
              <div className="text-xs mt-2 opacity-80">
                Use <kbd>singular | plural</kbd> (pipe symbol) for pluralization of numeric values
              </div>
            )}
          </div>
        )}
        {props.type.includes("SELECT") && (
          <div>
            <Subheading>Predefined values</Subheading>
            <Textarea
              rows={4}
              className="w-full"
              value={values.join("\n")}
              onChange={(text) => setValues(text.split("\n").map((s) => s.trim()))}
              placeholder="One per line"
            />
          </div>
        )}
      </Window>
    </>
  );
}
