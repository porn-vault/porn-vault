import clsx from "clsx";

import { useWindowSize } from "../composables/use_window_size";
import { createPaginationArray } from "../util/pagination";
import AutoLayout from "./AutoLayout";
import Paper from "./Paper";

type Props = {
  numPages: number;
  current: number;
  onChange?: (page: number) => void;
};

export default function Pagination({ current, numPages, onChange }: Props) {
  const { width } = useWindowSize();
  const isMobile = (width ?? 1_920) < 768;

  const arr = createPaginationArray(current, numPages, isMobile ? 1 : 2);

  if (numPages < 2) {
    return null;
  }

  return (
    <AutoLayout layout="h" gap={5}>
      {arr.map((x, i) => {
        if (x === "...") {
          return (
            <div key={i} className="text-black dark:text-white">
              ...
            </div>
          );
        }
        return (
          <div key={i} onClick={() => onChange?.(x - 1)}>
            <Paper
              className={clsx(
                "w-[32px] cursor-pointer rounded border-2 border-transparent bg-gray-300 py-1 text-center font-semibold transition-all hover:translate-y-[-1px] dark:bg-gray-800",
                {
                  "!border-blue-500 dark:!border-blue-700": current === x - 1,
                }
              )}
            >
              {x}
            </Paper>
          </div>
        );
      })}
    </AutoLayout>
  );
}
