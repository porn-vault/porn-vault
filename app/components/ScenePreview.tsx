import { useMemo } from "react";

import { useSafeMode } from "../composables/use_safe_mode";
import { formatDuration } from "../util/string";

const SINGLE_PREVIEW_SPRITE_WIDTH = 160;

type Props = {
  show: boolean;

  // x coordinates on the seekbar
  absolutePosition?: number;

  // percentage value on the seekbar (0 - 1)
  percentagePosition?: number;
  thumbnail: string;
  duration: number;
};

export default function ScenePreview(props: Props) {
  const { absolutePosition, thumbnail, show, percentagePosition, duration } = props;

  const { blur: safeModeBlur } = useSafeMode();

  // total number of pixels of the scenePreview image: 16000px
  let imageOffset = useMemo(
    () => Math.floor((percentagePosition || 0) * 100) * SINGLE_PREVIEW_SPRITE_WIDTH,
    [duration, percentagePosition]
  );
  imageOffset += SINGLE_PREVIEW_SPRITE_WIDTH;
  const positionProperty = `calc(100% - ${imageOffset}px)`;

  const currentPosition = useMemo(
    () => Math.floor((percentagePosition || 0) * duration),
    [duration, percentagePosition]
  );

  if (!absolutePosition || !show) {
    return null;
  }

  return (
    <div
      style={{
        left: absolutePosition - SINGLE_PREVIEW_SPRITE_WIDTH / 2,
      }}
      className="shadow-lg w-[160px] bottom-[58px] overflow-hidden rounded bg-black bg-opacity-80 text-center absolute"
    >
      <div
        className="w-[160px] overflow-hidden h-[85px]"
        style={{
          filter: safeModeBlur,
          backgroundImage: `url(${thumbnail})`,
          backgroundPositionX: positionProperty,
          backgroundRepeat: "repeat-x",
        }}
      />
      <div className="py-1 text-center text-sm font-semibold text-white">
        {formatDuration(currentPosition)}
      </div>
    </div>
  );
}
