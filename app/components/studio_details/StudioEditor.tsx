import EditIcon from "mdi-react/PencilIcon.js";
import { useState } from "react";
import { useTranslations } from "use-intl";

import { useWindow } from "../../composables/use_window";
import { IStudio } from "../../types/studio";
import { graphqlQuery } from "../../util/gql";
import Button from "../Button";
import IconButton from "../IconButton";
import { SelectableLabel } from "../LabelDropdownChoice";
import LabelGroup from "../LabelGroup";
import StudioInput from "../StudioInput";
import Subheading from "../Subheading";
import Textarea from "../Textarea";
import TextInput from "../TextInput";
import Window from "../Window";

async function editStudio({
  id,
  ...opts
}: {
  id: string;
  name: string;
  description: string | null;
  aliases: string[];
  labels: string[];
  parent?: string | null;
}) {
  const query = `
  mutation ($ids: [String!]!, $opts: StudioUpdateOpts!) {
    updateStudios(ids: $ids, opts: $opts) {
      _id
    }
  }
 `;

  await graphqlQuery(query, {
    ids: [id],
    opts,
  });
}

type Props = {
  onEdit: () => void;
  studio: IStudio;
};

export default function StudioEditor({ onEdit, studio }: Props) {
  const t = useTranslations();

  const { isOpen, close, open } = useWindow();
  const [name, setName] = useState(studio.name);
  const [description, setDescription] = useState(studio.description ?? null);
  const [aliasInput, setAliasInput] = useState(studio.aliases?.join("\n") ?? "");
  const [parentStudio, setParent] = useState(studio.parent?._id ?? null);

  const [selectedLabels, setSelectedLabels] = useState<SelectableLabel[]>(studio.labels ?? []);
  const [loading, setLoader] = useState(false);

  return (
    <>
      <IconButton
        Icon={EditIcon}
        onClick={open}
        className="text-black hover:text-gray-500 dark:text-white hover:dark:text-gray-400"
      />
      <Window
        onClose={close}
        isOpen={isOpen}
        title={t("action.editStudio")}
        actions={
          <>
            <Button
              loading={loading}
              onClick={async () => {
                try {
                  setLoader(true);
                  await editStudio({
                    id: studio._id,
                    name,
                    description,
                    aliases: aliasInput.split("\n").filter(Boolean),
                    labels: selectedLabels.map((label) => label._id),
                    parent: parentStudio,
                  });
                  onEdit();
                  close();
                } catch (error) { }
                setLoader(false);
              }}
            >
              {t("action.save")}
            </Button>
            <Button secondary onClick={close}>
              Close
            </Button>
          </>
        }
      >
        <div>
          <Subheading>Studio name</Subheading>
          <TextInput value={name} onChange={setName} placeholder="Enter a studio name" />
        </div>
        <div>
          <Subheading>Aliases</Subheading>
          <Textarea
            className="w-full"
            value={aliasInput}
            onChange={setAliasInput}
            placeholder="1 per line"
          />
        </div>
        <div>
          <Subheading>Studio description</Subheading>
          <Textarea
            className="w-full min-h-[50px] max-h-[25vh]"
            value={description ?? ""}
            onChange={setDescription}
            placeholder="Enter a description"
          />
        </div>
        <div>
          <Subheading>Labels</Subheading>
          <LabelGroup
            limit={999}
            value={selectedLabels}
            onChange={setSelectedLabels}
            onDelete={(id) => {
              setSelectedLabels((prev) => prev.filter((x) => x._id !== id));
            }}
          />
        </div>
        <div>
          <Subheading>Parent studio</Subheading>
          <StudioInput value={parentStudio} onChange={setParent} />
        </div>
      </Window>
    </>
  );
}
