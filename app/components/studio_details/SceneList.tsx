import { IStudio } from "~/types/studio";

import { createSessionStorageQueryManager } from "../../util/persistent_query";
import PaginatedSceneList, { DEFAULT_QUERY } from "../PaginatedSceneList";

type Props = {
  studioId: string;
  substudios?: IStudio[];
};

export default function StudioDetailsSceneList(props: Props) {
  const storageKey = `studio_details_${props.studioId}_scenes_query`;
  const persistentQuery = createSessionStorageQueryManager<typeof DEFAULT_QUERY>(storageKey);

  return (
    <PaginatedSceneList
      padless
      mergeQuery={{
        studios: [props.studioId],
      }}
      studioId={props.studioId}
      persistentQuery={persistentQuery}
      substudios={props.substudios}
    />
  );
}
