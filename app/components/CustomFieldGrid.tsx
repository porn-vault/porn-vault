import ListContainer from "./ListContainer";

interface Props {
  items: {
    field: { _id: string; name: string; type: string; unit?: string };
    value: string | string[] | boolean | number | null;
  }[];
}

function displayBooleanValue(bool: boolean | null | undefined): string {
  if (bool === true) {
    return "Yes";
  }
  if (bool === false) {
    return "No";
  }
  return "-";
}

function resolveSuffix(value: number, suffix: string | undefined): string {
  if (!suffix) {
    return "";
  }
  if (!suffix.includes("|")) {
    return suffix;
  }
  const [singular, plural] = suffix.split("|");
  return value === 1 ? singular.trim() : plural.trim();
}

export default function CustomFieldGrid({ items }: Props): JSX.Element {
  return (
    <ListContainer gap={15} size={160}>
      {items
        .filter(({ value }) => value !== undefined && value !== null)
        .sort((a, b) => a.field.name.localeCompare(b.field.name))
        .map(({ field, value }) => (
          <div className="flex flex-col overflow-hidden" key={field._id}>
            <div className="font-medium">{field.name}</div>
            <div className="text-sm italic">
              {field.type === "MULTI_SELECT" && (
                <div>
                  {(value as string[]).join(", ")} {field.unit}
                </div>
              )}
              {field.type === "SINGLE_SELECT" && (
                <div>
                  {value} {field.unit}
                </div>
              )}
              {field.type === "BOOLEAN" && <div>{displayBooleanValue(value as boolean)}</div>}
              {field.type === "STRING" && (
                <div>
                  {value} {field.unit}
                </div>
              )}
              {field.type === "NUMBER" && (
                <div>
                  {value} {resolveSuffix(value as number, field.unit)}
                </div>
              )}
            </div>
          </div>
        ))}
    </ListContainer>
  );
}
