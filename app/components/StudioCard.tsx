import Highlighter from "react-highlight-words";
import { useTranslations } from "use-intl";

import { buildSearchWords, HIGHLIGHT_CLASSES } from "~/util/highlight";

import { useSettings } from "../composables/use_settings";
import { IStudio } from "../types/studio";
import { bookmarkStudio, favoriteStudio } from "../util/mutations/studio";
import { imageUrl } from "../util/thumbnail";
import BookmarkIconButton from "./BookmarkIconButton";
import Card from "./Card";
import FavoriteIconButton from "./FavoriteIconButton";
import Flex from "./Flex";
import LabelGroup from "./LabelGroup";
import LocaleLink from "./Link";
import Rating from "./Rating";
import ResponsiveImage from "./ResponsiveImage";

type Props = {
  studio: Omit<IStudio, "substudios">;
  onFav: (value: boolean) => void;
  onBookmark: (value: Date | null) => void;

  highlight?: string[];
};

export default function StudioCard({ studio, onFav, onBookmark, highlight }: Props) {
  const t = useTranslations();
  const { showCardLabels } = useSettings();

  async function toggleFav(): Promise<void> {
    const newValue = !studio.favorite;
    await favoriteStudio(studio._id, newValue);
    onFav(newValue);
  }

  async function toggleBookmark(): Promise<void> {
    const newValue = studio.bookmark ? null : new Date();
    await bookmarkStudio(studio._id, newValue);
    onBookmark(newValue);
  }

  return (
    <Card padless className="relative">
      <div className="block cursor-pointer overflow-hidden rounded-lg transition-[filter] hover:brightness-[0.8]">
        <LocaleLink to={`/studio/${studio._id}`}>
          <ResponsiveImage
            objectFit="contain"
            className="rounded-lg"
            aspectRatio="4 / 3"
            imgStyle={{ padding: 10 }}
            // NOTE: cannot use thumbnail because studio logos are often transparent => png
            src={studio.thumbnail?._id && imageUrl(studio.thumbnail._id)}
          />
        </LocaleLink>
      </div>

      <Flex
        className="absolute left-0 top-0 gap-1 rounded p-1 text-white"
        layout="h"
        style={{
          background: "#000000bb",
        }}
      >
        <FavoriteIconButton alwaysLight value={studio.favorite} onClick={toggleFav} />
        <BookmarkIconButton alwaysLight value={studio.bookmark} onClick={toggleBookmark} />
      </Flex>

      <Flex className="gap-1 px-2 pb-1">
        <Flex className="gap-1 font-semibold" layout="h">
          {studio.parent && (
            <LocaleLink to={`/studio/${studio.parent._id}`}>
              <div
                className="text-uppercase"
                style={{
                  fontSize: 12,
                  opacity: 0.8,
                }}
              >
                <Highlighter
                  highlightClassName={HIGHLIGHT_CLASSES}
                  searchWords={buildSearchWords(highlight)}
                  autoEscape={true}
                  textToHighlight={studio.parent.name}
                />
              </div>
            </LocaleLink>
          )}
        </Flex>

        <div className="ellipsis overflow-hidden whitespace-nowrap font-semibold">
          <Highlighter
            highlightClassName={HIGHLIGHT_CLASSES}
            searchWords={buildSearchWords(highlight)}
            autoEscape={true}
            textToHighlight={studio.name}
          />
        </div>

        <div className="text-sm text-gray-600 dark:text-gray-500">
          {studio.numScenes} {t("scene", { numItems: studio.numScenes })}
        </div>

        <Rating value={studio.averageRating || 0} readonly />

        {showCardLabels && !!studio.labels.length && (
          <div>
            <LabelGroup value={studio.labels} />
          </div>
        )}
      </Flex>
    </Card>
  );
}
