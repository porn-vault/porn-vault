import ChevronDownIcon from "mdi-react/ChevronDownIcon.js";
import ChevronUpIcon from "mdi-react/ChevronUpIcon.js";
import { useState } from "react";
import { useTranslations } from "use-intl";

import useLabelList from "../composables/use_label_list";
import { useWindow } from "../composables/use_window";
import Button from "./Button";
import Label from "./Label";
import type { SelectableLabel } from "./LabelDropdownChoice";
import LabelSelector from "./LabelSelector";
import Window from "./Window";

type AddLabelMenuProps = {
  value: string[];
  onChange: (labels: SelectableLabel[]) => void;
  onEdit: () => Promise<void>;
};

function AddLabelMenu({ value, onChange, onEdit }: AddLabelMenuProps) {
  const { isOpen, open, close } = useWindow();
  const { labels: labelList } = useLabelList();
  const t = useTranslations();

  return (
    <div className="inline-block">
      <Label text="Edit +" onClick={open} className="bg-black" />
      <Window
        actions={
          <>
            <Button
              onClick={async () => {
                await onEdit();
                close();
              }}
            >
              {t("action.save")}
            </Button>
            <Button secondary onClick={close}>
              Close
            </Button>
          </>
        }
        onClose={close}
        title="Select labels"
        isOpen={isOpen}
      >
        <LabelSelector
          multiple
          value={value}
          onChange={(labelIds) => {
            onChange(labelIds.map((id) => labelList.find((x) => x._id === id)!));
          }}
        />
      </Window>
    </div>
  );
}

type Props = {
  value: Pick<SelectableLabel, "_id" | "name" | "color">[];
  limit?: number;
  onChange?: (labels: SelectableLabel[]) => void;
  onDelete?: (labelId: string) => void;
  onEdit?: () => Promise<void>;

  highlight?: string[];
};

export default function LabelGroup({
  value,
  limit,
  onChange,
  onDelete,
  onEdit,
  highlight,
}: Props): JSX.Element {
  const max = limit || 5;

  const [expanded, setExpanded] = useState(false);
  const slice = expanded ? value : value.slice(0, max);

  return (
    <div className="flex flex-col gap-1">
      <div className="flex flex-wrap gap-1">
        {slice.map((label) => (
          <Label
            highlight={highlight}
            key={label._id}
            color={label.color}
            onDelete={onDelete && (() => onDelete(label._id))}
            text={label.name}
          />
        ))}
        {onChange && (
          <AddLabelMenu
            onEdit={async () => {
              await onEdit?.();
            }}
            onChange={(val) => onChange?.(val)}
            value={value.map((x) => x._id)}
          />
        )}
      </div>
      <div>
        {max < value.length && (
          <div
            className="flex cursor-pointer justify-center text-xs font-semibold dark:text-gray-500"
            onClick={() => {
              setExpanded(!expanded);
            }}
          >
            {expanded ? <ChevronUpIcon /> : <ChevronDownIcon />}
          </div>
        )}
      </div>
    </div>
  );
}
