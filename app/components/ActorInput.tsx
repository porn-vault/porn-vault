import { useWindow } from "~/composables/use_window";

import ActorSelector from "./ActorSelector";
import Button from "./Button";
import Flex from "./Flex";
import Window from "./Window";

type Props = {
  value: string[];
  onChange: (actor: string[]) => void;
};

export default function ActorInput({ value, onChange }: Props) {
  const { isOpen, open, close } = useWindow();

  return (
    <Flex layout="h">
      <Button onClick={open}>Select actors</Button>
      <Window
        actions={
          <>
            <Button secondary onClick={close}>
              Close
            </Button>
          </>
        }
        onClose={close}
        title="Select actors"
        isOpen={isOpen}
      >
        <ActorSelector multiple value={value} onChange={onChange} />
      </Window>
    </Flex>
  );
}
