import { useRef } from "react";
import {
  CircleStencil,
  Cropper,
  CropperRef,
  ImageRestriction,
  RectangleStencil,
} from "react-advanced-cropper";

import Button from "./Button";
import Flex from "./Flex";

type ImageCropperProps = {
  onCancel: () => void;
  onUpload: (blob: Blob) => void;
  src?: string;
  aspectRatio: number;
  loading: boolean;
};

// should only do cropping and onUpload should emit new crop dimensions to the parent component
export function ImageCropper({ onCancel, onUpload, src, aspectRatio, loading }: ImageCropperProps) {
  const cropperRef = useRef<CropperRef>(null);

  function doCrop() {
    if (cropperRef.current) {
      const canvas = cropperRef.current?.getCanvas();
      canvas?.toBlob((blob) => {
        if (blob !== null) {
          onUpload(blob);
        }
      });
    }
  }

  const stencil = aspectRatio === 1 ? CircleStencil : RectangleStencil;

  return (
    <Flex className="gap-2">
      <div className="w-full overflow-hidden rounded-lg">
        <Cropper
          // defaultSize={defaultSize as DefaultSize}
          stencilComponent={stencil}
          src={src}
          ref={cropperRef}
          stencilProps={{
            aspectRatio,
          }}
          imageRestriction={ImageRestriction.fitArea}
          style={{
            maxHeight: "50vh"
          }}
        />
      </div>
      <div className="flex gap-2 justify-center">
        <Button onClick={doCrop} loading={loading}>
          Upload
        </Button>
        <Button onClick={onCancel} secondary>
          Cancel
        </Button>
      </div>
    </Flex>
  );
}
