import clsx from "clsx";
import BookmarkIcon from "mdi-react/BookmarkIcon.js";
import BookmarkBorderIcon from "mdi-react/BookmarkOutlineIcon.js";
import HeartIcon from "mdi-react/HeartIcon.js";
import HeartBorderIcon from "mdi-react/HeartOutlineIcon.js";
import AddMarkerIcon from "mdi-react/PlaylistAddIcon.js";
import { useEffect, useState } from "react";
import InputMask from "react-input-mask";
import { useTranslations } from "use-intl";

import { useVideoControls } from "../composables/use_video_control";
import { useWindow } from "../composables/use_window";
import { IActor } from "../types/actor";
import { graphqlQuery } from "../util/gql";
import { formatDuration } from "../util/string";
import AutoLayout from "./AutoLayout";
import BookmarkIconButton from "./BookmarkIconButton";
import Button from "./Button";
import FavoriteIconButton from "./FavoriteIconButton";
import IconButton from "./IconButton";
import InputError from "./InputError";
import LabelDropdownChoice, { SelectableLabel } from "./LabelDropdownChoice";
import Rating from "./Rating";
import Subheading from "./Subheading";
import TextInput from "./TextInput";
import Window from "./Window";

async function createMarker(
  name: string,
  scene: string,
  time: number,
  rating: number,
  favorite: boolean,
  bookmark: boolean,
  labels: string[],
  actors?: string[]
) {
  const query = `
  mutation ($name: String!, $scene: String!, $time: Int!, $rating: Int, $favorite: Boolean, $bookmark: Long, $labels: [String!]!, $actors: [String!]!) {
    createMarker(name: $name, scene: $scene, time: $time, rating: $rating, favorite: $favorite, bookmark: $bookmark, labels: $labels, actors: $actors) {
      _id
    }
  }
        `;

  await graphqlQuery(query, {
    name,
    labels,
    scene,
    time,
    rating,
    favorite,
    bookmark,
    actors,
  });
}

type Props = {
  onCreate: () => void;
  sceneId: string;
  actors: IActor[];
  onOpen: () => void;
};

export default function MarkerCreator({ onCreate, onOpen, sceneId, actors }: Props) {
  const t = useTranslations();
  const { isOpen, close, open } = useWindow();

  const [time, setTime] = useState(0);
  const [error, setError] = useState<string | undefined>();
  const [name, setName] = useState("");
  const [rating, setRating] = useState(0);
  const [fav, setFav] = useState(false);
  const [bookmark, setBookmark] = useState(false);

  const [loading, setLoader] = useState(false);

  /* TODO: */
  const [selectedLabels, setSelectedLabels] = useState<readonly SelectableLabel[]>([]);
  /*   const [selectedActors, setSelectedActors] = useState<readonly SelectableActor[]>(actors); */

  const { currentTime } = useVideoControls();

  function doOpen() {
    open();
    onOpen();
  }

  function reset() {
    setName("");
    setError(undefined);
    setRating(0);
    setFav(false);
    setBookmark(false);
    setSelectedLabels([]);
  }

  function doClose() {
    reset();
    close();
  }

  useEffect(() => {
    setTime(currentTime);
  }, [currentTime]);

  return (
    <>
      <IconButton Icon={AddMarkerIcon} onClick={doOpen} />
      <Window
        onClose={doClose}
        isOpen={isOpen}
        title={`Add marker`}
        actions={
          <>
            <Button
              disabled={!name.length}
              loading={loading}
              onClick={async () => {
                try {
                  setLoader(true);
                  await createMarker(
                    name,
                    sceneId,
                    Math.floor(time),
                    rating,
                    fav,
                    bookmark,
                    selectedLabels.map(({ _id }) => _id),
                    /* TODO: selectedActors.map(({ _id }) => _id) */ []
                  );
                  onCreate();
                  reset();
                  close();
                } catch (error) {
                  if (error instanceof Error) {
                    setError(error.message);
                  } else {
                    setError("An error occurred");
                  }
                }
                setLoader(false);
              }}
            >
              Create
            </Button>
            <Button secondary onClick={doClose}>
              Close
            </Button>
          </>
        }
      >
        <div>
          <Subheading>Time *</Subheading>
          <InputMask
            suppressHydrationWarning
            className={clsx(
              "rounded border-2 px-2 py-1 !outline-none transition-colors",
              "border-gray-300 bg-gray-50 text-black hover:bg-gray-200 focus:border-blue-500 focus:bg-gray-100",
              "dark:border-gray-700 dark:bg-gray-800 dark:text-white hover:dark:bg-gray-900 focus:dark:border-blue-800 focus:dark:bg-gray-900"
            )}
            mask="99:99:99"
            placeholder="Enter time (HH:MM:SS)"
            value={formatDuration(time, false)}
            onChange={(ev) => {
              const text = ev.target.value.replaceAll("_", "0");

              const [_, hh, mm, ss] = text.match(/^(\d{2}):(\d{2}):(\d{2})$/) ?? [];

              if (hh && mm && ss) {
                setTime(parseInt(hh) * 3600 + parseInt(mm) * 60 + parseInt(ss));
              } else {
                setTime(0);
              }
            }}
          />
        </div>
        <div>
          <Subheading>Marker title *</Subheading>
          <TextInput
            placeholder="Enter marker title"
            className="w-full"
            value={name}
            onChange={setName}
          />
          {error && <InputError message={error} />}
        </div>
        <div>
          <Subheading>{t("rating")}</Subheading>
          <Rating value={rating} onChange={setRating} />
        </div>
        <div className="flex justify-start gap-2">
          <FavoriteIconButton value={fav} onClick={() => setFav((x) => !x)} />
          <BookmarkIconButton value={bookmark} onClick={() => setBookmark((x) => !x)} />
        </div>
        {/*      <div>
          <Subheading>Actors</Subheading>
          <ActorDropdownChoice selectedActors={selectedActors} onChange={setSelectedActors} />
        </div>
        <div>
          <Subheading>Labels</Subheading> */}
        {/* TODO: */}
        {/* <LabelDropdownChoice selectedLabels={selectedLabels} onChange={setSelectedLabels} /> */}
        {/*       </div> */}
      </Window>
    </>
  );
}
