import { useWindow } from "~/composables/use_window";

import Button from "./Button";
import Flex from "./Flex";
import StudioSelector from "./StudioSelector";
import Window from "./Window";

type Props = {
  value: string | null;
  onChange: (studio: string | null) => void;
};

export default function StudioInput({ value, onChange }: Props) {
  const { isOpen, open, close } = useWindow();

  return (
    <Flex layout="h">
      <Button onClick={open}>Select studio</Button>
      <Window
        actions={
          <>
            <Button secondary onClick={close}>
              Close
            </Button>
          </>
        }
        onClose={close}
        title="Select studio"
        isOpen={isOpen}
      >
        <StudioSelector
          multiple
          value={value ? [value] : []}
          onChange={([studioId]) => {
            onChange(studioId ?? null);
          }}
        />
      </Window>
    </Flex>
  );
}
