import { Popover } from "@headlessui/react";
import { MdiReactIconComponentType } from "mdi-react";
import { ReactNode } from "react";

import Card from "./Card";
import IconButton from "./IconButton";

type Props = {
  value: boolean;
  activeIcon: MdiReactIconComponentType;
  inactiveIcon: MdiReactIconComponentType;
  children: ReactNode;
  counter?: number;
  isLoading?: boolean;
  disabled?: boolean;
};

export default function IconButtonMenu({
  counter,
  children,
  value,
  activeIcon,
  inactiveIcon,
  disabled,
  isLoading,
}: Props) {
  const Icon = value ? activeIcon : inactiveIcon;

  return (
    <Popover className="relative">
      <Popover.Button disabled={isLoading || disabled} className="flex items-center">
        <IconButton disabled={isLoading || disabled} Icon={Icon} />
        {!!counter && (
          <div
            style={{
              fontWeight: "bold",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              fontSize: 11,
              color: "black",
              backgroundColor: "#aaaaff",
              width: 14,
              height: 14,
              position: "absolute",
              right: -4,
              bottom: 0,
              borderRadius: "50%",
            }}
          >
            {counter}
          </div>
        )}
      </Popover.Button>
      <Popover.Panel className="absolute z-10">
        <Card padless className="w-full p-2 !shadow-xl">
          {children}
        </Card>
      </Popover.Panel>
    </Popover>
  );
}
