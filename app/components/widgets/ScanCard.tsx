import StatsIcon from "mdi-react/ChartBarStackedIcon.js";
import { ReactNode } from "react";
import { useTranslations } from "use-intl";

import { useScanStatus } from "../../composables/use_scan_status";
import Button from "../Button";
import Text from "../Text";
import WidgetCard from "./WidgetCard";

export default function ScanCard() {
  const t = useTranslations();
  const { scanStatus, startScan } = useScanStatus();

  const Inner = ({ children }: { children: ReactNode }) => (
    <WidgetCard icon={<StatsIcon />} title={t("widgets.scanStats")}>
      {children}
    </WidgetCard>
  );

  if (!scanStatus) {
    return <Inner>No data</Inner>;
  }

  return (
    <Inner>
      <Text>{scanStatus?.folders.videos.length} video folders</Text>
      <Text>{scanStatus?.folders.images.length} image folders</Text>
      <Button onClick={startScan} loading={scanStatus?.isScanning}>
        {t("action.startScan")}
      </Button>
    </Inner>
  );
}
