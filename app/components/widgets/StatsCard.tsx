import StatsIcon from "mdi-react/ChartBarStackedIcon.js";
import prettyBytes from "pretty-bytes";
import useSWR from "swr";
import { useTranslations } from "use-intl";

import { graphqlQuery } from "../../util/gql";
import Divider from "../Divider";
import WidgetCard from "./WidgetCard";

async function getInfo() {
  const query = `
  {
    numScenes
    numActors
    numMovies
    numImages
    numStudios
    getSceneDiskUsage
  }`;

  const data = await graphqlQuery<{
    numScenes: number;
    numActors: number;
    numMovies: number;
    numImages: number;
    numStudios: number;
    getSceneDiskUsage: number;
  }>(query, {});

  return {
    numScenes: data.numScenes,
    numActors: data.numActors,
    numMovies: data.numMovies,
    numImages: data.numImages,
    numStudios: data.numStudios,
    diskUsage: data.getSceneDiskUsage,
  };
}

export default function StatsCard() {
  const t = useTranslations();

  const { data: stats } = useSWR("stats", getInfo, {
    revalidateOnFocus: true,
    revalidateOnReconnect: true,
    revalidateOnMount: true,
    refreshInterval: 60_000,
  });

  return (
    <WidgetCard icon={<StatsIcon />} title={t("heading.stats")}>
      <div>
        <span className="text-4xl">{stats?.numScenes || 0}</span>{" "}
        <span className="opacity-60">{t("scene", { numItems: stats?.numScenes || 0 })}</span>
      </div>
      <div>
        <span className="text-4xl">{stats?.numActors || 0}</span>{" "}
        <span className="opacity-60"> {t("actor", { numItems: stats?.numActors || 0 })}</span>
      </div>
      <div>
        <span className="text-4xl">{stats?.numMovies || 0}</span>{" "}
        <span className="opacity-60"> {t("movie", { numItems: stats?.numMovies || 0 })}</span>
      </div>
      <div>
        <span className="text-4xl">{stats?.numStudios || 0}</span>{" "}
        <span className="opacity-60"> {t("studio", { numItems: stats?.numStudios || 0 })}</span>
      </div>
      <div>
        <span className="text-4xl">{stats?.numImages || 0}</span>{" "}
        <span className="opacity-60"> {t("image", { numItems: stats?.numImages || 0 })}</span>
      </div>
      <Divider />
      <div>
        <span style={{ fontSize: 16, opacity: 0.8 }}>{t("widgets.diskUsageScenes")}:</span>{" "}
        <span style={{ fontWeight: 500 }}>{prettyBytes(stats?.diskUsage || 0)}</span>
      </div>
    </WidgetCard>
  );
}
