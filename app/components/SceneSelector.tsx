import clsx from "clsx";
import { useTranslations } from "use-intl";

import { fetchScenes } from "../composables/use_scene_list";
import { useSelector } from "../composables/use_selector";
import { IScene } from "../types/scene";
import { graphqlQuery } from "../util/gql";
import Button from "./Button";
import Loader from "./Loader";
import Paper from "./Paper";
import TextInput from "./TextInput";

type Props = {
  value: string[];
  onChange: (s: string[]) => void;
  multiple?: boolean;
  itemChoices?: IScene[];
  searchFn?: (query: string, item: IScene) => boolean;
};

async function getSceneById(id: string): Promise<IScene | null> {
  const q = `
  query ($id: String!) {
    getSceneById(id: $id) {
      _id
      name
    }
  }
  `;

  const { getSceneById } = await graphqlQuery<{
    getSceneById: IScene;
  }>(q, { id });

  return getSceneById;
}

export default function SceneSelector({ value, multiple, onChange, itemChoices, searchFn }: Props) {
  const t = useTranslations();

  const {
    query,
    setQuery,
    items: Scenes,
    loading,
    debouncedSearch,
    handleClick,
    isSelected,
  } = useSelector({
    value,
    multiple,
    onChange,
    fetchFn: (page, query) => fetchScenes(page, query).then(({ items }) => items),
    getItemById: getSceneById,
    itemChoices,
    searchFn,
  });

  return (
    <div className="flex flex-col gap-2">
      <TextInput
        placeholder={t("action.findScenes")}
        value={query}
        onChange={(text) => {
          setQuery(text);
          debouncedSearch();
        }}
      />
      <div className="flex flex-col gap-1">
        {loading ? (
          <div className="mt-3 flex justify-center">
            <Loader />
          </div>
        ) : (
          <>
            <Button disabled={!Scenes.length} onClick={() => onChange(Scenes.map((x) => x._id))}>
              Select all
            </Button>
            <Button secondary disabled={!value.length} onClick={() => onChange([])}>
              Clear
            </Button>
            {Scenes.map((Scene) => (
              <Paper
                onClick={() => handleClick(Scene._id)}
                className={clsx(
                  "flex cursor-pointer items-center gap-2 rounded-lg px-3 py-2 text-black transition-all dark:text-white",
                  {
                    "!bg-blue-200 dark:!bg-blue-950": isSelected(Scene._id),
                    "bg-white dark:bg-gray-800": !isSelected(Scene._id),
                  }
                )}
                key={Scene._id}
              >
                <div className="text-black dark:text-white">{Scene.name}</div>
              </Paper>
            ))}
          </>
        )}
      </div>
    </div>
  );
}
