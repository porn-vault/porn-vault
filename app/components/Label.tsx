import clsx from "clsx";
import Color from "color";
import Highlighter from "react-highlight-words";

import { buildSearchWords,HIGHLIGHT_CLASSES } from "~/util/highlight";

type Props = {
  className?: string;
  text: string;
  color?: string;
  onClick?: () => void;
  onDelete?: () => void;

  highlight?: string[];
};

export default function Label({ className, text, color, onClick, onDelete, highlight }: Props) {
  return (
    <div
      className={clsx(
        className,
        "border-1 flex items-center gap-1 rounded px-2 py-[2px] text-[11px] transition-all",
        {
          "hover:!bg-blue-800 cursor-pointer": !!onClick,
        }
      )}
      style={{
        background: color || "#000000dd",
        color: new Color(color).isLight() ? "black" : "white",
      }}
      onClick={onClick}
    >
      <div>
        <Highlighter
          highlightClassName={HIGHLIGHT_CLASSES}
          searchWords={buildSearchWords(highlight)}
          autoEscape={true}
          textToHighlight={text}
        />
      </div>
      {onDelete && (
        <div className="cursor-pointer opacity-60 hover:opacity-100" onClick={onDelete}>
          X
        </div>
      )}
    </div>
  );
}
