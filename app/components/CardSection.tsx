import { ReactNode } from "react";

import Subheading from "./Subheading";

type Props = {
  title: ReactNode;
  children: ReactNode;
  scrollable?: boolean;
  actions?: ReactNode;
};

export default function CardSection({ actions, title, children, scrollable }: Props) {
  return (
    <div className={scrollable ? "overflow-x-scroll" : ""}>
      <div className="mb-1 flex justify-between items-center gap-2">
        <Subheading>{title}</Subheading>
        {actions}
      </div>
      <div>{children}</div>
    </div>
  );
}
