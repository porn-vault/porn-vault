import clsx from "clsx";
import DeleteIcon from "mdi-react/DeleteIcon.js";
import EditIcon from "mdi-react/PencilIcon.js";
import { useEffect, useState } from "react";

import { useWindow } from "~/composables/use_window";
import { graphqlQuery } from "~/util/gql";
import { deleteLabels } from "~/util/mutations/label";

import Button from "./Button";
import IconButton from "./IconButton";
import InputError from "./InputError";
import Spacer from "./Spacer";
import Subheading from "./Subheading";
import Textarea from "./Textarea";
import TextInput from "./TextInput";
import Window from "./Window";

type Props = {
  id: string;
  name: string;
  index: number;
  color?: string;
  aliases: string[];
  onUpdate?: () => void;
  onDelete?: () => void;
};

export default function LabelListItem({ onUpdate, onDelete, ...label }: Props): JSX.Element {
  const { isOpen, close, open } = useWindow();

  const [name, setName] = useState(label.name);
  const [aliasInput, setAliasInput] = useState(label.aliases.join("\n"));
  const [color, setColor] = useState(label.color ?? "#000000");
  const [error, setError] = useState<string | undefined>();

  async function updateLabel(): Promise<void> {
    setError(undefined);

    try {
      const mutation = `
mutation($ids: [String!]!, $opts: LabelUpdateOpts!) {
  updateLabels(ids: $ids, opts: $opts) {
    _id
    name
    aliases
    color
  }
}`;

      await graphqlQuery(mutation, {
        ids: [label.id],
        opts: {
          name,
          aliases: aliasInput.split("\n").map((s) => s.trim()),
          color,
        },
      });

      close();
      onUpdate?.();
    } catch (error) {
      if (error instanceof Error) {
        setError(error.message);
      } else {
        setError("An error occurred");
      }
    }
  }

  useEffect(() => {
    setName(label.name);
    setAliasInput(label.aliases.join("\n"));
    setColor(label.color ?? "#000000");
  }, [label.name, label.aliases, label.color]);

  async function removeLabel(labelId: string): Promise<void> {
    await deleteLabels([labelId]);
    onDelete?.();
  }

  return (
    <>
      <div
        key={label.id}
        className={clsx(
          "flex gap-3 items-center p-3 text-gray-800 dark:text-gray-300 transition-all",
          {
            "bg-gray-50 dark:bg-gray-800": label.index % 2 === 0,
            "bg-gray-200 dark:bg-gray-900": label.index % 2 !== 0,
          }
        )}
      >
        <div>
          <div className="flex gap-2 items-center">
            {label.color && (
              <div
                className="w-3 h-3 rounded-[30%]"
                style={{
                  backgroundColor: label.color,
                }}
              />
            )}
            <div>{name}</div>
          </div>
          <div className="text-xs text-gray-600 dark:text-gray-500">{label.aliases.join(", ")}</div>
        </div>
        <Spacer />
        <IconButton className="shrink-0" size={20} Icon={EditIcon} onClick={open} />
        <IconButton
          onClick={() => {
            if (confirm("Really delete?")) {
              removeLabel(label.id);
            }
          }}
          size={20}
          className="text-red-500 shrink-0"
          Icon={DeleteIcon}
        />
      </div>
      <Window
        onClose={close}
        isOpen={isOpen}
        title={`Edit label`}
        actions={
          <>
            <Button disabled={!name} onClick={updateLabel}>
              Edit
            </Button>
            <Button secondary onClick={close}>
              Close
            </Button>
          </>
        }
      >
        <div>
          <Subheading>Label name</Subheading>
          <TextInput
            className="w-full"
            value={name}
            onChange={setName}
            placeholder="Enter a label name"
          />
          {error && <InputError message={error} />}
        </div>
        <div>
          <Subheading>Aliases</Subheading>
          <Textarea
            className="w-full"
            value={aliasInput}
            onChange={setAliasInput}
            placeholder="1 per line"
          />
        </div>
        <div>
          <Subheading>Color</Subheading>
          <input
            type="color"
            value={color}
            onChange={(event) => setColor(event.currentTarget.value)}
          />
        </div>
      </Window>
    </>
  );
}
