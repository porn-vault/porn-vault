import { useState } from "react";

import { graphqlQuery } from "../../util/gql";
import Button from "../Button";
import Code from "../Code";
import Window from "../Window";

type Props = {
  sceneId: string;
  disabled: boolean;
};

async function runFFprobe(sceneId: string) {
  const q = `
  mutation($id: String!) {
    runFFProbe(id: $id) {
      ffprobe
    }
  }`;

  const { runFFProbe } = await graphqlQuery<{
    runFFProbe: {
      ffprobe: unknown;
    };
  }>(q, {
    id: sceneId,
  });

  return runFFProbe.ffprobe;
}

export default function FFProbeButton({ sceneId, disabled }: Props) {
  const [ffprobeData, setFFprobeData] = useState<unknown | null>(null);
  const [loader, setLoader] = useState(false);

  return (
    <>
      <Button
        disabled={disabled}
        loading={loader}
        onClick={() => {
          setLoader(true);
          runFFprobe(sceneId)
            .then(setFFprobeData)
            .catch(() => { })
            .finally(() => {
              setLoader(false);
            });
        }}
      >
        Run FFprobe
      </Button>
      <Window
        isOpen={!!ffprobeData}
        title="FFprobe result"
        onClose={() => setFFprobeData(null)}
        actions={
          <>
            <Button
              onClick={() => {
                navigator.clipboard
                  .writeText(JSON.stringify(ffprobeData, null, 2).trim())
                  .catch(() => { });
              }}
            >
              Copy
            </Button>
            <Button secondary onClick={() => setFFprobeData(null)}>
              Close
            </Button>
          </>
        }
      >
        <Code
          style={{
            maxHeight: "50vh",
            overflowX: "scroll",
            overflowY: "scroll",
            maxWidth: 700,
          }}
          value={ffprobeData}
        />
      </Window>
    </>
  );
}
