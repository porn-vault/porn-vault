import EditIcon from "mdi-react/PencilIcon.js";
import { useState } from "react";
import { useTranslations } from "use-intl";

import { useWindow } from "../../composables/use_window";
import { IScene } from "../../types/scene";
import { graphqlQuery } from "../../util/gql";
import ActorInput from "../ActorInput";
import Button from "../Button";
import DateInput from "../DateInput";
import IconButton from "../IconButton";
import StudioInput from "../StudioInput";
import Subheading from "../Subheading";
import Textarea from "../Textarea";
import TextInput from "../TextInput";
import Window from "../Window";

async function editScene({
  id,
  ...opts
}: {
  id: string;
  name: string;
  path: string;
  description?: string | null;
  actors?: string[];
  labels?: string[];
  studio?: string | null;
  releaseDate?: number | null;
}) {
  const mutation = `
mutation ($ids: [String!]!, $opts: SceneUpdateOpts!) {
  updateScenes(ids: $ids, opts: $opts) {
    _id
  }
}`;

  await graphqlQuery(mutation, {
    ids: [id],
    opts,
  });
}

type Props = {
  onEdit: () => void;
  scene: IScene;
};

export default function SceneEditor({ onEdit, scene }: Props) {
  const t = useTranslations();

  const { isOpen, close, open } = useWindow();
  const [name, setName] = useState(scene.name);
  const [path, setPath] = useState(scene.path);
  const [description, setDescription] = useState(scene.description);
  const [releaseDate, setReleaseDate] = useState(
    scene.releaseDate ? new Date(scene.releaseDate) : null
  );

  const [actors, setActors] = useState(scene.actors.map((x) => x._id));
  const [studio, setStudio] = useState(scene.studio?._id ?? null);

  const [loading, setLoader] = useState(false);

  return (
    <>
      <IconButton
        Icon={EditIcon}
        onClick={open}
        className="text-black hover:text-gray-500 dark:text-white hover:dark:text-gray-400"
      />
      <Window
        onClose={close}
        isOpen={isOpen}
        title={t("action.editScene")}
        actions={
          <>
            <Button
              loading={loading}
              onClick={async () => {
                try {
                  setLoader(true);
                  await editScene({
                    id: scene._id,
                    name,
                    path,
                    actors,
                    studio,
                    description,
                    releaseDate: releaseDate?.valueOf(),
                  });
                  onEdit();
                  close();
                } catch (error) {}
                setLoader(false);
              }}
            >
              {t("action.save")}
            </Button>
            <Button secondary onClick={close}>
              Close
            </Button>
          </>
        }
      >
        <div>
          <Subheading>Scene name</Subheading>
          <TextInput
            className="w-full"
            value={name}
            onChange={setName}
            placeholder="Enter a scene name"
          />
        </div>
        <div>
          <Subheading>Scene path</Subheading>
          <TextInput
            className="w-full"
            value={path}
            onChange={setPath}
            placeholder="Enter a file path"
          />
        </div>
        <div>
          <Subheading>Release date</Subheading>
          <DateInput value={releaseDate} onChange={setReleaseDate} />
        </div>
        <div>
          <Subheading>Description</Subheading>
          <Textarea
            rows={4}
            className="w-full"
            value={description ?? ""}
            onChange={setDescription}
            placeholder="Enter a description"
          />
        </div>
        <div>
          <Subheading>Actors</Subheading>
          <ActorInput value={actors} onChange={setActors} />
        </div>
        <div>
          <Subheading>Studio</Subheading>
          <StudioInput value={studio} onChange={setStudio} />
        </div>
      </Window>
    </>
  );
}
