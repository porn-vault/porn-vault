import { createSessionStorageQueryManager } from "../../util/persistent_query";
import PaginatedImageList, { DEFAULT_QUERY } from "../PaginatedImageList";

type Props = {
  sceneId: string;
};

export default function SceneDetailsImageList(props: Props) {
  const storageKey = `scene_details_${props.sceneId}_images_query`;
  const persistentQuery = createSessionStorageQueryManager<typeof DEFAULT_QUERY>(storageKey);

  return (
    <PaginatedImageList
      padless
      withUploader
      mergeQuery={{
        scenes: [props.sceneId],
      }}
      sceneId={props.sceneId}
      persistentQuery={persistentQuery}
      /* NOTE: no need to apply actors because the backend will apply them automatically */
    />
  );
}
