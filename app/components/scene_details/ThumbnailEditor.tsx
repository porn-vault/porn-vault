import { useState } from "react";

import { useSettings } from "../../composables/use_settings";
import { uploadImage } from "../../util/mutations/image";
import { setSceneThumbnail } from "../../util/mutations/scene";
import { ImageCropper } from "../Cropper";
import FileInput from "../FileInput";
import Window from "../Window";

type Props = {
  sceneId: string;
  onDone?: () => void;
};

export default function SceneThumbnailEditor({ sceneId, onDone }: Props): JSX.Element {
  const { sceneImageAspect } = useSettings();

  const [file, setFile] = useState<{
    buffer: string;
    name?: string;
  } | null>(null);

  function changeImage(buffer: string, name?: string): void {
    setFile({ buffer, name });
  }

  function handleOnChange(event: ProgressEvent<FileReader>, filename?: string) {
    const fileReader = event.target as FileReader;

    const name = filename;

    if (!fileReader.result) {
      return;
    }

    if (fileReader.result instanceof ArrayBuffer) {
      const base64 = btoa(String.fromCharCode(...new Uint8Array(fileReader.result)));
      changeImage(base64, name);
    } else {
      changeImage(fileReader.result, name);
    }
  }

  async function doUpload(blob: Blob): Promise<void> {
    if (!file) {
      return;
    }

    const uploadedImage = await uploadImage(
      { blob, name: `${sceneId} (thumbnail)` },
      undefined,
      sceneId
    );
    await setSceneThumbnail(sceneId, uploadedImage._id);
    setFile(null);
    onDone?.();
  }

  return (
    <>
      <FileInput
        onChange={([file]) => {
          if (file) {
            const fileReader = new FileReader();
            fileReader.onload = handleOnChange;
            fileReader.readAsDataURL(file);
          } else {
            setFile(null);
          }
        }}
      >
        Upload thumbnail
      </FileInput>
      {file && (
        <Window isOpen title="Set thumbnail" onClose={() => setFile(null)}>
          <ImageCropper
            aspectRatio={sceneImageAspect.numericValue}
            src={file.buffer}
            loading={false}
            onCancel={() => setFile(null)}
            onUpload={doUpload}
          />
        </Window>
      )}
    </>
  );
}
