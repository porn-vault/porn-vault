import { editSceneList } from "../../composables/use_scene_list";
import { useSimilarScenes } from "../../composables/use_similar_scenes";
import { IScene } from "../../types/scene";
import ListWrapper from "../ListWrapper";
import SceneCard from "../SceneCard";

type Props = {
  sceneId: string;
};

export default function SimilarScenesList({ sceneId }: Props): JSX.Element {
  const similarScenes = useSimilarScenes(sceneId);

  function editScene(sceneId: string, fn: (scene: IScene) => IScene) {
    similarScenes.setScenes((prev) => editSceneList(prev, sceneId, fn));
  }

  return (
    <ListWrapper
      loading={similarScenes.loading}
      noResults={!similarScenes.scenes.length}
    >
      {similarScenes.scenes.map((scene) => (
        <SceneCard
          onFav={(value) => {
            editScene(scene._id, (scene) => {
              scene.favorite = value;
              return scene;
            });
          }}
          onBookmark={(value) => {
            editScene(scene._id, (scene) => {
              scene.bookmark = !!value;
              return scene;
            });
          }}
          onRate={(rating) => {
            editScene(scene._id, (scene) => {
              scene.rating = rating;
              return scene;
            });
          }}
          key={scene._id}
          scene={scene}
        />
      ))}
    </ListWrapper>
  );
}
