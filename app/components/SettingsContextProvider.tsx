import { ReactNode, useEffect, useState } from "react";

import {
  ACTOR_HERO_RATIO,
  ACTOR_RATIO,
  SCENE_RATIO,
  SettingsContext,
} from "../composables/use_settings";

type Props = {
  children: ReactNode;
};

export default function SettingsContextProvider(props: Props) {
  const [showCardLabels, setShowCardLabels] = useState(true);

  const [sceneAspectRatio, setSceneAspectRatio] = useState<SCENE_RATIO>("4:3");
  const [actorAspectRatio, setActorAspectRatio] = useState<ACTOR_RATIO>("3:4");
  const [actorHeroAspectRatio, setActorHeroAspectRatio] = useState<ACTOR_HERO_RATIO>("2.75");

  useEffect(() => {
    const storageValue = localStorage.getItem("pm_showCardLabels");
    if (storageValue !== null) {
      setShowCardLabels(storageValue === "true");
    }

    const sceneRatioValue = localStorage.getItem("pm_sceneRatio");
    if (sceneRatioValue !== null) {
      setSceneAspectRatio(sceneRatioValue as SCENE_RATIO);
    }

    const actorRatioValue = localStorage.getItem("pm_actorRatio");
    if (actorRatioValue !== null) {
      setActorAspectRatio(actorRatioValue as ACTOR_RATIO);
    }

    const actorHeroRatioValue = localStorage.getItem("pm_actorHeroRatio");
    if (actorHeroRatioValue !== null) {
      setActorHeroAspectRatio(actorHeroRatioValue as ACTOR_HERO_RATIO);
    }
  }, []);

  function toggleShowCardLabels(isSet: boolean) {
    localStorage.setItem("pm_showCardLabels", isSet.toString());
    setShowCardLabels(!showCardLabels);
  }

  function toggleSceneAspectRatio(ratio: SCENE_RATIO) {
    localStorage.setItem("pm_sceneRatio", ratio);
    setSceneAspectRatio(ratio);
  }

  function toggleActorAspectRatio(ratio: ACTOR_RATIO) {
    localStorage.setItem("pm_actorRatio", ratio);
    setActorAspectRatio(ratio);
  }

  function toggleActorHeroAspectRatio(ratio: ACTOR_HERO_RATIO) {
    localStorage.setItem("pm_actorHeroRatio", ratio);
    setActorHeroAspectRatio(ratio);
  }

  return (
    <SettingsContext.Provider
      value={{
        showCardLabels,
        sceneAspectRatio,
        setSceneAspectRatio: toggleSceneAspectRatio,
        actorAspectRatio,
        setActorAspectRatio: toggleActorAspectRatio,
        setShowCardLabels: toggleShowCardLabels,

        actorHeroAspectRatio,
        setActorHeroAspectRatio: toggleActorHeroAspectRatio,
      }}
    >
      {props.children}
    </SettingsContext.Provider>
  );
}
