import clsx from "clsx";
import { CSSProperties, ReactNode } from "react";

type Props = {
  children: ReactNode;
  style?: CSSProperties;
  className?: string;
};

export default function CardTitle({ className, children, style }: Props) {
  return (
    <div
      className={clsx(className, "text-lg font-semibold text-black dark:text-gray-300")}
      style={style}
    >
      {children}
    </div>
  );
}
