import LargeIcon from "mdi-react/GridLargeIcon.js";
import SmallIcon from "mdi-react/GridIcon.js";
import { useAtom } from 'jotai'
import { atomWithStorage } from 'jotai/utils'

import IconButton from "./IconButton";

const gridSizeAtom = atomWithStorage<"sm" | "lg">("gridSize", "lg");

export function getGridSize(): number {
    const [size, _] = useAtom(gridSizeAtom);
    return size === "lg" ? 240 : 180;
}

export default function GridSizeSelector(): JSX.Element {
    const [size, setSize] = useAtom(gridSizeAtom);

    return <div className="flex gap-2">
        <IconButton
            onClick={() => {
                setSize(size === "lg" ? "sm" : "lg")
            }}
            Icon={size === "lg" ? LargeIcon : SmallIcon}>

        </IconButton>
    </div>
}