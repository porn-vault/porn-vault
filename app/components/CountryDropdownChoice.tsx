import { Combobox } from "@headlessui/react";
import clsx from "clsx";
import { Fragment, useMemo, useState } from "react";

import allCountries, { ICountry } from "../../server/data/countries";

export type SimpleCountry = Pick<ICountry, "name" | "alpha2" | "nationality" | "alias">;

type Props = {
  value: SimpleCountry | null | undefined;
  onChange?: (country?: SimpleCountry | null) => void;
  relevancy?: number;
};

export default function CountryDropdownChoice({ value, onChange, relevancy: minRelevancy }: Props) {
  const [query, setQuery] = useState("");

  const countries = useMemo(() => {
    return allCountries.filter(({ relevancy }) => relevancy > (minRelevancy ?? 1));
  }, [minRelevancy]);

  const filteredCountries =
    query === ""
      ? countries
      : countries.filter((country) =>
          [country.name, country.alias, country.alpha3]
            .map((x) => x?.toLowerCase())
            .some((x) => x?.includes(query.toLowerCase()))
        );

  return (
    <div className="relative">
      <Combobox value={value} by="alpha2" onChange={onChange} nullable>
        <Combobox.Input
          placeholder="Select country"
          className={clsx(
            "w-full rounded border-2 px-2 py-1 !outline-none transition-colors",
            "border-gray-300 bg-gray-50 text-black hover:bg-gray-200 focus:border-blue-500 focus:bg-gray-100",
            "dark:border-gray-700 dark:bg-gray-800 dark:text-white hover:dark:bg-gray-900 focus:dark:border-blue-800 focus:dark:bg-gray-900"
          )}
          onChange={(event) => setQuery(event.target.value)}
          displayValue={(country: SimpleCountry) => country?.name}
        />
        <Combobox.Options className="absolute left-0 top-[40px] z-[900] flex max-h-[240px] w-full flex-col gap-1 overflow-y-scroll rounded bg-white px-2 py-1 shadow dark:bg-gray-800">
          {filteredCountries.map((country) => (
            <Combobox.Option key={country.alpha2} value={country} as={Fragment}>
              {({ selected }) => (
                <span
                  className={clsx(
                    "cursor-pointer rounded p-2 transition-colors hover:bg-gray-300 hover:dark:bg-gray-700",
                    { "bg-blue-200 dark:bg-blue-900": selected }
                  )}
                >
                  {country.name}
                </span>
              )}
            </Combobox.Option>
          ))}
        </Combobox.Options>
      </Combobox>
    </div>
  );
}
