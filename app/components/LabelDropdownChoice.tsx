import { Combobox } from "@headlessui/react";
import clsx from "clsx";
import { Fragment, useState } from "react";

import useLabelList from "../composables/use_label_list";
import ILabel from "../types/label";

export type SelectableLabel = Pick<ILabel, "_id" | "name" | "color">;

type Props = {
  selectedLabels: SelectableLabel[];
  onChange: (value: SelectableLabel[]) => void;
};

export default function LabelDropdownChoice({ selectedLabels, onChange }: Props) {
  const { labels } = useLabelList();

  const [query, setQuery] = useState("");

  const filteredLabels =
    query === ""
      ? labels
      : labels.filter((label) => label.name.toLowerCase().includes(query.toLowerCase()));

  return (
    <div className="relative">
      <Combobox value={selectedLabels} by="_id" onChange={onChange} multiple>
        <Combobox.Input
          placeholder="Select labels"
          className={clsx(
            "w-full rounded border-2 px-2 py-1 !outline-none transition-colors",
            "border-gray-300 bg-gray-50 text-black hover:bg-gray-200 focus:border-blue-500 focus:bg-gray-100",
            "dark:border-gray-700 dark:bg-gray-800 dark:text-white hover:dark:bg-gray-900 focus:dark:border-blue-800 focus:dark:bg-gray-900"
          )}
          onChange={(event) => setQuery(event.target.value)}
          displayValue={(labels: ILabel[]) => labels.map((x) => x.name).join(", ")}
        />
        <Combobox.Options className="absolute left-0 top-[40px] z-[900] flex max-h-[240px] w-full flex-col gap-1 overflow-y-scroll rounded bg-white px-2 py-1 shadow dark:bg-gray-800">
          {filteredLabels.map((label) => (
            <Combobox.Option key={label._id} value={label} as={Fragment}>
              {({ selected }) => (
                <span
                  className={clsx(
                    "cursor-pointer rounded p-2 transition-colors hover:bg-gray-300 hover:dark:bg-gray-700",
                    { "bg-blue-200 dark:bg-blue-900": selected }
                  )}
                >
                  {label.name}
                </span>
              )}
            </Combobox.Option>
          ))}
        </Combobox.Options>
      </Combobox>
    </div>
  );
}
