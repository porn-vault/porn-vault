import clsx from "clsx";
import { CSSProperties, ReactNode } from "react";

type Props = {
  children?: ReactNode;
  style?: CSSProperties;
  className?: string;
  layout?: "h" | "v";
  wrap?: boolean;
};

export default function Flex({ children, style, className, layout, wrap }: Props) {
  const _layout = layout ?? "v";
  const dir = _layout === "v" ? "flex-col" : "flex-row";
  const center = _layout === "h" ? "items-center" : "justify-center";

  return (
    <div
      className={clsx("flex", dir, center, className, {
        "flex-wrap": wrap,
      })}
      style={style}
    >
      {children}
    </div>
  );
}
