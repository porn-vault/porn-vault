import { ReactNode } from "react";

type Props = {
  leftChildren: ReactNode;
  rightChildren: ReactNode;
};

export default function ComplexGrid({ leftChildren, rightChildren }: Props) {
  return (
    <div className="flex justify-center">
      <div className="complex-grid w-full">
        <div>
          <div className="complex-grid-left-sticky">{leftChildren}</div>
        </div>
        <div className="w-full">{rightChildren}</div>
      </div>
    </div>
  );
}
