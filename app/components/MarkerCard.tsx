import { useSafeMode } from "../composables/use_safe_mode";
import { useSettings } from "../composables/use_settings";
import { IMarker } from "../types/marker";
import { graphqlQuery } from "../util/gql";
import { bookmarkMarker, favoriteMarker, rateMarker } from "../util/mutations/marker";
import { formatDuration } from "../util/string";
import { thumbnailUrl } from "../util/thumbnail";
import ActorList from "./ActorList";
import BookmarkIconButton from "./BookmarkIconButton";
import Card from "./Card";
import FavoriteIconButton from "./FavoriteIconButton";
import Flex from "./Flex";
import LabelGroup from "./LabelGroup";
import LocaleLink from "./Link";
import Rating from "./Rating";
import ResponsiveImage from "./ResponsiveImage";
import Spacer from "./Spacer";

type Props = {
  marker: IMarker;
  onFav: (value: boolean) => void;
  onBookmark: (value: Date | null) => void;
  onRate: (rating: number) => void;
  // onEdit: () => void;
  onClick?: () => void;
  onDelete: () => void;
};

export default function MarkerCard({
  marker,
  onFav,
  onBookmark,
  onRate,
  onClick,
  // onEdit,
  onDelete,
}: Props) {
  const { blur: safeModeBlur } = useSafeMode();
  const { showCardLabels } = useSettings();

  async function toggleFav(): Promise<void> {
    const newValue = !marker.favorite;
    await favoriteMarker(marker._id, newValue);
    onFav(newValue);
  }

  async function toggleBookmark(): Promise<void> {
    const newValue = marker.bookmark ? null : new Date();
    await bookmarkMarker(marker._id, newValue);
    onBookmark(newValue);
  }

  async function changeRating(rating: number): Promise<void> {
    await rateMarker(marker._id, rating);
    onRate(rating);
  }

  async function deleteMarker(): Promise<void> {
    const query = `
mutation ($ids: [String!]!) {
  removeMarkers(ids: $ids)
}
 `;

    await graphqlQuery(query, {
      ids: [marker._id],
    });
    onDelete && onDelete();
  }

  const thumbnail = (
    <ResponsiveImage
      className="rounded-lg"
      aspectRatio="4 / 3"
      src={marker.thumbnail?._id && thumbnailUrl(marker.thumbnail._id)}
      imgStyle={{ transition: "filter 0.1s ease-in-out", filter: safeModeBlur }}
    />
  );

  return (
    <Card padless className="relative">
      <div className="block cursor-pointer overflow-hidden rounded-lg transition-[filter] hover:brightness-[0.8]">
        {typeof onClick !== "undefined" || !marker.scene ? (
          <div onClick={onClick}>{thumbnail}</div>
        ) : (
          <LocaleLink to={`/scene/${marker.scene._id}?t=${marker.time}`}>{thumbnail}</LocaleLink>
        )}
      </div>
      <Flex
        className="absolute left-0 top-0 gap-1 rounded p-1 text-white"
        layout="h"
        style={{
          background: "#000000bb",
        }}
      >
        <FavoriteIconButton alwaysLight value={marker.favorite} onClick={toggleFav} />
        <BookmarkIconButton alwaysLight value={marker.bookmark} onClick={toggleBookmark} />
      </Flex>
      <Flex className="gap-1 px-2 pb-1">
        <Flex className="gap-1 font-semibold" layout="h">
          <div className="overflow-hidden text-ellipsis whitespace-nowrap">{marker.name}</div>
          <Spacer />
          <div>
            <span className="text-sm font-semibold">{formatDuration(marker.time)}</span>
          </div>
        </Flex>

        <div className="text-xs text-gray-800 dark:text-gray-400">
          <span className="text-gray-600 dark:text-gray-500">Scene:</span>{" "}
          <span className="italic">
            <LocaleLink
              className="hover:brightness-[0.8] transition-all"
              to={`/scene/${marker.scene._id}`}
            >
              {marker.scene.name}
            </LocaleLink>
          </span>{" "}
        </div>

        {!!marker.actors.length && <ActorList actors={marker.actors} />}

        <Rating onChange={changeRating} value={marker.rating || 0} />

        {showCardLabels && !!marker.labels.length && (
          <div className="mt-1">
            <LabelGroup value={marker.labels} />
          </div>
        )}
      </Flex>
    </Card>

    /* 
     <AutoLayout
        gap={5}
        layout="h"
        style={{
          color: "white",
          background: "#000000bb",
          borderRadius: 5,
          padding: 3,
          position: "absolute",
          right: 1,
          top: 1,
        }}
      >
        <MarkerEditor
          markerId={marker._id}
          onEdit={() => {
            onEdit && onEdit();
          }}
        />
        <DeleteIcon
          className="hover"
          onClick={async (event) => {
            event.stopPropagation();
            if (window.confirm("Really delete this marker?")) {
              await deleteMarker();
            }
          }}
        />
      </AutoLayout>
    */
  );
}
