import clsx from "clsx";
import { ReactNode } from "react";

import Head from "./Head";

type Props = {
  children: ReactNode;
  padless?: boolean;
  title?: string;
};

export default function PageWrapper({ children, padless, title }: Props) {
  const _padless = padless ?? false;

  return (
    <div
      className={clsx({
        "pb-4 px-4 pt-4": !_padless,
      })}
    >
      {/* container for modal windows to avoid z-index issues */}
      <div id="modal-portal" />
      {title && <Head title={`${title} - Porn Vault`} />}
      <div>{children}</div>
    </div>
  );
}
