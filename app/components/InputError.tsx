type Props = {
  message?: string;
};

export default function InputError({ message }: Props) {
  return (
    <span className="mt-1 text-xs text-red-700 dark:text-red-400">
      {message || "An error occurred"}
    </span>
  );
}
