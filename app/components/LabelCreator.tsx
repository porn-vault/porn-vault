import { useState } from "react";
import { useTranslations } from "use-intl";

import { useWindow } from "../composables/use_window";
import ILabel from "../types/label";
import { graphqlQuery } from "../util/gql";
import Button from "./Button";
import InputError from "./InputError";
import Subheading from "./Subheading";
import Textarea from "./Textarea";
import TextInput from "./TextInput";
import Window from "./Window";

async function createLabel(name: string, color: string, aliases: string[]): Promise<ILabel> {
  const query = `
  mutation ($name: String!, $aliases: [String!]!, $color: String!) {
    addLabel(name: $name, aliases: $aliases, color: $color) {
      _id
    }
  }
 `;

  const result = await graphqlQuery<{ addLabel: ILabel }>(query, {
    name,
    aliases,
    color,
  });

  return result?.addLabel;
}

type Props = {
  onCreate: (label: ILabel) => void;
};

export default function LabelCreator({ onCreate }: Props) {
  const t = useTranslations();
  const [loading, setLoader] = useState(false);
  const [error, setError] = useState<string | undefined>();
  const [name, setName] = useState("");
  const [aliasInput, setAliasInput] = useState("");
  const [color, setColor] = useState("#000000");
  const { isOpen, close, open } = useWindow();

  const reset = () => {
    setName("");
    setError(undefined);
    setAliasInput("");
    setColor("#000000");
  };

  const doClose = () => {
    reset();
    close();
  };

  return (
    <>
      <Button onClick={open} style={{ marginRight: 10 }}>
        + {t("action.add")}
      </Button>
      <Window
        onClose={doClose}
        isOpen={isOpen}
        title={`Create label`}
        actions={
          <>
            <Button
              disabled={!name}
              loading={loading}
              onClick={async () => {
                try {
                  setLoader(true);
                  const newLabel = await createLabel(name, color, aliasInput.split("\n"));
                  onCreate(newLabel);
                  doClose();
                } catch (error) {
                  if (error instanceof Error) {
                    setError(error.message);
                  } else {
                    setError("An error occurred");
                  }
                }
                setLoader(false);
              }}
            >
              Create
            </Button>
            <Button secondary onClick={doClose}>
              Close
            </Button>
          </>
        }
      >
        <div>
          <Subheading>Label name</Subheading>
          <TextInput
            className="w-full"
            value={name}
            onChange={setName}
            placeholder="Enter a label name"
          />
          {error && <InputError message={error} />}
        </div>
        <div>
          <Subheading>Aliases</Subheading>
          <Textarea
            className="w-full"
            value={aliasInput}
            onChange={setAliasInput}
            placeholder="1 per line"
          />
        </div>
        <div>
          <Subheading>Color</Subheading>
          <input
            type="color"
            value={color}
            onChange={(event) => setColor(event.currentTarget.value)}
          />
        </div>
      </Window>
    </>
  );
}
