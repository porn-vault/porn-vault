import clsx from "clsx";
import { CSSProperties, ReactNode } from "react";

import Loader from "./Loader";

type Props = {
  children?: ReactNode;
  onClick?: () => void;
  style?: CSSProperties;
  className?: string;
  loading?: boolean;
  disabled?: boolean;
  secondary?: boolean;
};

export default function Button({
  children,
  onClick,
  style,
  className,
  loading,
  disabled,
  secondary,
}: Props) {
  return (
    <button
      onClick={() => {
        if (!loading && !disabled) {
          onClick?.();
        }
      }}
      style={style || {}}
      className={clsx(
        "relative rounded px-4 py-2 text-sm font-semibold transition-all",
        {
          "bg-blue-600 dark:bg-blue-900 text-white dark:text-white": !disabled && !secondary,
          "cursor-not-allowed !bg-gray-200 dark:!bg-gray-800 !text-gray-500 dark:!text-gray-600":
            disabled,
          "hover:bg-blue-700 hover:dark:bg-blue-800": !loading && !disabled && !secondary,
          "hover:bg-blue-200 hover:dark:bg-blue-900 text-black dark:text-white":
            !loading && !disabled && secondary,
        },
        className
      )}
    >
      <div
        className="flex items-center justify-center"
        style={{
          opacity: +!loading,
        }}
      >
        {children}
      </div>
      {loading && (
        <div className="absolute left-1/2 top-1/2 translate-x-[-50%] translate-y-[-50%]">
          <Loader />
        </div>
      )}
    </button>
  );
}
