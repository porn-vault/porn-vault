import clsx from "clsx";
import HeartIcon from "mdi-react/HeartIcon.js";
import HeartBorderIcon from "mdi-react/HeartOutlineIcon.js";

import IconButton from "./IconButton";

type Props = {
  value: boolean;
  onClick?: (event: React.MouseEvent<SVGSVGElement, MouseEvent>) => void;
  alwaysLight?: boolean;
  size?: number;
  className?: string;
};

export default function FavoriteIconButton({
  size,
  value,
  onClick,
  alwaysLight,
  className,
}: Props): JSX.Element {
  const Icon = value ? HeartIcon : HeartBorderIcon;

  return (
    <IconButton
      size={size}
      alwaysLight={alwaysLight}
      Icon={Icon}
      onClick={onClick}
      className={clsx(className, {
        "!text-red-500 hover:!text-red-700 hover:dark:!text-red-700": value,
      })}
    />
  );
}
