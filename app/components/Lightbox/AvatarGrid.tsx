import Avatar from "../Avatar";
import LocaleLink from "../Link";

type Props = {
  value: { id: string; name: string; imageId?: string }[];
};

export default function AvatarGrid({ value }: Props) {
  return (
    <div className="grid grid-cols-3 gap-2">
      {value.map(({ id, imageId, name }) => (
        <div key={id}>
          <LocaleLink
            to={`/actor/${id}`}
            className="hover:brightness-[80%] transition-all"
          >
            <Avatar avatarId={imageId} />
          </LocaleLink>
          <div className="text-center mt-1 text-sm font-semibold opacity-80">
            {name}
          </div>
        </div>
      ))}
    </div>
  );
}
