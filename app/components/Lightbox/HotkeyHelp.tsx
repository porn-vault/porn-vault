import InformationIcon from "mdi-react/InformationIcon.js";

import CardTitle from "../CardTitle";
import Paper from "../Paper";

export default function HotkeyHelp() {
  return (
    <Paper
      rounded
      bordered
      className="flex flex-col gap-2 w-[240px] px-2 py-3 rounded z-[300] fixed bottom-2 right-2"
    >
      <div className="items-center flex gap-1">
        <InformationIcon />
        <CardTitle className="!text-white">Controls</CardTitle>
      </div>
      <div className="flex gap-1">
        <div className="font-bold w-[60px] text-right">[ {"← →"} ]</div>
        <span className="opacity-80">Change image</span>
      </div>
      <div className="flex gap-1">
        <span className="font-bold w-[60px] text-right">[ f ]</span>
        <span className="opacity-80">Favorite</span>
      </div>
      <div className="flex gap-1">
        <span className="font-bold w-[60px] text-right">[ b ]</span>
        <span className="opacity-80">Bookmark</span>
      </div>
      <div className="flex gap-1">
        <span className="font-bold w-[60px] text-right">[ 0-9 ]</span>
        <span className="opacity-80">Set rating</span>
      </div>
      <div className="flex gap-1">
        <span className="font-bold w-[60px] text-right">[ Tab ]</span>
        <span className="opacity-80">Toggle sidebar</span>
      </div>
      <div className="flex gap-1">
        <span className="font-bold w-[60px] text-right">[ ESC ]</span>
        <span className="opacity-80">Close view</span>
      </div>
    </Paper>
  );
}
