import CloseIcon from "mdi-react/CloseIcon.js";
import { useHotkeys } from "react-hotkeys-hook";

import IconButton from "../IconButton";

type Props = {
  onClose: () => void;
};

export default function CloseButton({ onClose }: Props) {
  useHotkeys("esc", onClose, []);

  return (
    <div className="bg-black bg-opacity-80 p-1 rounded-[30%] z-[300] fixed top-2 right-2">
      <IconButton onClick={onClose} Icon={CloseIcon} alwaysLight size={28} />
    </div>
  );
}
