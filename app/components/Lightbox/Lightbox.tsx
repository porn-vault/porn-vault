import clsx from "clsx";
import LeftIcon from "mdi-react/ChevronLeftBoxIcon.js";
import RightIcon from "mdi-react/ChevronRightBoxIcon.js";
import ContentCopyIcon from "mdi-react/ContentCopyIcon.js";
import InfoIcon from "mdi-react/InformationIcon.js";
import prettyBytes from "pretty-bytes";
import { useState } from "react";
import { useHotkeys } from "react-hotkeys-hook";
import { useTranslations } from "use-intl";

import { labelImages } from "~/util/mutations/image";

import { useSafeMode } from "../../composables/use_safe_mode";
import { useScrollLock } from "../../composables/use_scroll_lock";
import { IImage } from "../../types/image";
import { imageUrl, thumbnailUrl } from "../../util/thumbnail";
import AutoLayout from "../AutoLayout";
import BookmarkIconButton from "../BookmarkIconButton";
import Button from "../Button";
import Card from "../Card";
import CardSection from "../CardSection";
import CardTitle from "../CardTitle";
import FavoriteIconButton from "../FavoriteIconButton";
import Flex from "../Flex";
import IconButton from "../IconButton";
import { SelectableLabel } from "../LabelDropdownChoice";
import LabelGroup from "../LabelGroup";
import LocaleLink from "../Link";
import Paper from "../Paper";
import Rating from "../Rating";
import ResponsiveImage from "../ResponsiveImage";
import Spacer from "../Spacer";
import Text from "../Text";
import AvatarGrid from "./AvatarGrid";
import CloseButton from "./CloseButton";
import HotkeyHelp from "./HotkeyHelp";

type Props = {
  image?: IImage;
  alt?: string;
  onClose: () => void;

  hasPrevious: boolean;
  hasNext: boolean;
  loading?: boolean;

  onLabel?: (imageId: string, labels: SelectableLabel[]) => void;
  onFavorite?: () => void;
  onBookmark?: () => void;
  onRate?: (rating: number) => void;
  onPrevious?: () => void;
  onNext?: () => void;
};

export default function Lightbox(props: Props) {
  const {
    hasPrevious,
    hasNext,
    loading,
    image,
    alt,
    onLabel,
    onFavorite,
    onBookmark,
    onRate,
    onNext,
    onPrevious,
  } = props;

  const t = useTranslations();
  const { blur: safeModeBlur } = useSafeMode();

  const [showSidebar, setSidebar] = useState(false);

  function toggleSidebar(): void {
    setSidebar((x) => !x);
  }

  function onClose(): void {
    setSidebar(false);
    props.onClose();
  }

  useScrollLock(!!image, [image]);

  useHotkeys(
    ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
    (ev) => {
      if (!image) {
        return;
      }

      const parsedKey = parseInt(ev.key);

      if (parsedKey >= 0 && parsedKey <= 9) {
        const newRating = parsedKey || 10;

        const rating = image.rating === newRating ? 0 : newRating;
        onRate?.(rating);
      }
    },
    [image]
  );

  useHotkeys(
    "tab",
    (ev) => {
      ev.preventDefault();
      toggleSidebar();
    },
    [image]
  );

  useHotkeys(
    "f",
    () => {
      onFavorite?.();
    },
    [image]
  );

  useHotkeys(
    "b",
    () => {
      onBookmark?.();
    },
    [image]
  );

  return (
    <>
      {image && (
        <div className={clsx("fixed left-0 top-0 w-full h-full grid z-[1000]")}>
          <div className="absolute left-0 top-0 w-full h-full bg-black bg-opacity-90"></div>
          <div
            className="z-[100] max-h-[100vh] flex flex-col justify-center items-center px-3 pt-3 pb-14"
            onClick={onClose}
          >
            {loading && (
              <div className="z-[2000] flex gap-2 items-center bg-gray-800 p-3 shadow-xl rounded-lg text-white text-xl font-bold fixed left-[50%] top-2 translate-x-[-50%]">
                <InfoIcon />
                <span>{t("loading")}...</span>
              </div>
            )}
            <div className="flex items-center justify-center overflow-hidden rounded-3xl">
              <img
                style={{
                  filter: safeModeBlur,
                }}
                onClick={(ev) => ev.stopPropagation()}
                className="z-[1000] bg-sky-500 max-w-[100%] max-h-[100%] rounded-3xl overflow-hidden"
                src={imageUrl(image._id)}
                alt={alt}
              />
            </div>
            <Paper
              rounded
              bordered
              className="flex items-center gap-2 bg-gray-800 px-2 py-2 rounded fixed bottom-1 left-[50%] translate-x-[-50%]"
            >
              <IconButton
                disabled={!hasPrevious}
                alwaysLight
                size={28}
                Icon={LeftIcon}
                onClick={(ev) => {
                  ev.stopPropagation();
                  onPrevious?.();
                }}
              />
              <Rating
                value={image.rating}
                onChange={(rating) => {
                  onRate?.(rating);
                }}
              />
              <FavoriteIconButton
                value={image.favorite}
                alwaysLight
                size={28}
                onClick={(ev) => {
                  ev.stopPropagation();
                  onFavorite?.();
                }}
              />
              <BookmarkIconButton
                value={image.bookmark}
                alwaysLight
                size={28}
                onClick={(ev) => {
                  ev.stopPropagation();
                  onBookmark?.();
                }}
              />
              <IconButton
                disabled={!hasNext}
                alwaysLight
                size={28}
                Icon={RightIcon}
                onClick={(ev) => {
                  ev.stopPropagation();
                  onNext?.();
                }}
              />
            </Paper>
          </div>
          <div className="bg-black bg-opacity-80 p-1 rounded-[30%] z-[300] fixed top-2 left-2">
            <IconButton
              onClick={(ev) => {
                ev.stopPropagation();
                toggleSidebar();
              }}
              Icon={InfoIcon}
              alwaysLight
              size={28}
            />
          </div>
          <CloseButton onClose={onClose} />
          {showSidebar && (
            <div
              onClick={() => setSidebar(false)}
              className="z-[299] absolute left-0 top-0 w-full h-full bg-black bg-opacity-90"
            />
          )}
          {showSidebar && <HotkeyHelp />}
          {showSidebar && (
            <Card
              innerClassName="h-full"
              className="z-[300] fixed top-12 bottom-2 left-2 right-2 sm:w-[360px] !shadow-xl"
            >
              <CardTitle>{image.name}</CardTitle>
              <AutoLayout className="pb-3 overflow-y-scroll">
                {image.actors.length > 0 && (
                  <CardSection title={t("heading.actors")}>
                    <AvatarGrid
                      value={image.actors.map(({ _id, name, avatar }) => ({
                        id: _id,
                        name,
                        imageId: avatar?._id,
                      }))}
                    />
                  </CardSection>
                )}
                <CardSection title={t("heading.labels")}>
                  <LabelGroup
                    onEdit={async () => {
                      // TODO:
                      // onLabel?.(image._id, labels);
                    }}
                    onChange={(labels) => {
                      // TODO:
                      //  onLabel?.(image._id, labels);
                    }}
                    onDelete={() => {
                      // TODO:
                    }}
                    value={image.labels}
                    limit={999}
                  />
                </CardSection>
                <CardSection
                  title={
                    <div className="flex gap-1">
                      {t("path")}
                      <IconButton
                        Icon={ContentCopyIcon}
                        onClick={() => {
                          navigator.clipboard.writeText(image.path!).catch(() => {});
                        }}
                        size={16}
                      />
                    </div>
                  }
                >
                  <div className="text-sm">
                    <Text>{image.path}</Text>
                  </div>
                </CardSection>
                <CardSection title={t("meta")}>
                  <Flex className="gap-1">
                    <div className="text-sm">
                      <span className="text-gray-500 dark:text-gray-500">Dimensions:</span>{" "}
                      <b>
                        {image.meta.dimensions?.width ?? 0}x{image.meta.dimensions?.height ?? 0} px
                      </b>
                    </div>
                    <div className="text-sm">
                      <span className="text-gray-500 dark:text-gray-500">Size:</span>{" "}
                      <b>{prettyBytes(image.meta.size ?? 0)}</b>
                    </div>
                  </Flex>
                </CardSection>
                {image.scene && (
                  <CardSection title={`From scene "${image.scene.name}"`}>
                    <LocaleLink to={`/scene/${image.scene._id}`}>
                      <div className="hover:brightness-[80%] transition-all rounded-2xl overflow-hidden">
                        <ResponsiveImage
                          imgStyle={{
                            filter: safeModeBlur,
                          }}
                          src={thumbnailUrl(image.scene.thumbnail?._id)}
                        />
                      </div>
                    </LocaleLink>
                  </CardSection>
                )}
              </AutoLayout>
              <Spacer />
              <Flex className="gap-2">
                <a target="_blank" href={imageUrl(image._id)}>
                  <Button className="w-full">Open in new tab</Button>
                </a>
                {/* TODO: delete, then close lightbox, also: hot key */}
                <Button className="bg-red-600 hover:bg-red-700 dark:bg-red-600 hover:dark:bg-red-700 w-full">
                  Delete
                </Button>
              </Flex>
            </Card>
          )}
        </div>
      )}
    </>
  );
}
