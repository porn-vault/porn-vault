import clsx from "clsx";

import Paper from "./Paper";

type Props = {
  color?: string;
};

export default function SkeletonCard({ color }: Props) {
  return (
    <Paper
      className={clsx(
        "rounded-lg h-[300px] w-full animate-pulse bg-gray-300 dark:bg-gray-900",
        color
      )}
    />
  );
}
