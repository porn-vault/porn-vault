import { defu } from "defu";
import ActorIcon from "mdi-react/AccountIcon.js";
import ActorOutlineIcon from "mdi-react/AccountOutlineIcon.js";
import CameraIcon from "mdi-react/CameraIcon.js";
import CameraOutlineIcon from "mdi-react/CameraOutlineIcon.js";
import UnwatchedOnIcon from "mdi-react/EyeOffIcon.js";
import UnwatchedOffIcon from "mdi-react/EyeOffOutlineIcon.js";
import LabelIcon from "mdi-react/LabelIcon.js";
import LabelOutlineIcon from "mdi-react/LabelOutlineIcon.js";
import StarOutline from "mdi-react/StarBorderIcon.js";
import StarHalf from "mdi-react/StarHalfFullIcon.js";
import Star from "mdi-react/StarIcon.js";
import { useTranslations } from "use-intl";

import { IStudio } from "~/types/studio";

import { useCollabs } from "../composables/use_collabs";
import useLabelList from "../composables/use_label_list";
import { PersistentQueryManager, usePaginatedQuery } from "../composables/use_paginated_query";
import { editSceneList, fetchScenes } from "../composables/use_scene_list";
import { IScene } from "../types/scene";
import ActorSelector from "./ActorSelector";
import AutoLayout from "./AutoLayout";
import BookmarkIconButton from "./BookmarkIconButton";
import FavoriteIconButton from "./FavoriteIconButton";
import IconButton from "./IconButton";
import IconButtonMenu from "./IconButtonMenu";
import LabelSelector from "./LabelSelector";
import ListWrapper from "./ListWrapper";
import PageWrapper from "./PageWrapper";
import PaginatedQuery from "./PaginatedQuery";
import Rating from "./Rating";
import SceneCard from "./SceneCard";
import SimpleSelect from "./SimpleSelect";
import SortDirectionButton, { SortDirection } from "./SortDirectionButton";
import StudioSelector from "./StudioSelector";
import TextInput from "./TextInput";
import GridSizeSelector, { getGridSize } from "./GridSizeSelector";
import { useAtom } from "jotai";
import CardTitle from "./CardTitle";

export const DEFAULT_QUERY = {
  query: "",
  favorite: false,
  bookmark: false,
  rating: 0,
  sortBy: "relevance",
  sortDir: "desc" as SortDirection,
  include: [] as string[],
  actors: [] as string[],
  studios: [] as string[],
  unwatchedOnly: false,
};

type Props = {
  persistentQuery?: PersistentQueryManager<typeof DEFAULT_QUERY>;
  mergeQuery: Partial<typeof DEFAULT_QUERY>;
  actorId?: string;
  studioId?: string;
  writeHead?: boolean;
  substudios?: IStudio[];
  padless?: boolean;
};

export default function PaginatedSceneList(props: Props): JSX.Element {
  const t = useTranslations();

  const gridSize = getGridSize();

  const { labels: labelList, loading: labelLoader } = useLabelList();
  const hasNoLabels = !labelLoader && !labelList.length;

  const { collabs, loading: collabsLoader } = useCollabs(props.actorId);
  const hasNoCollabs = !collabsLoader && !collabs.length;

  const {
    query,
    setQuery,
    getPage,
    gotoPage,
    gotoStart,
    items: scenes,
    error,
    loading,
    numItems,
    numPages,
    page,
    resetAll,
    setItems,
  } = usePaginatedQuery({
    defaultQuery: DEFAULT_QUERY,
    fetchFn: (page, query) => {
      const q = defu(query, props.mergeQuery);

      // Use substudio filter instead of parent studio
      if (query.studios.length) {
        q.studios = query.studios;
      }

      return fetchScenes(page, q);
    },
    persistentQuery: props.persistentQuery,
  });

  const highlight = query.query.trim().split(" ");

  function editScene(sceneId: string, fn: (scene: IScene) => IScene) {
    setItems((prev) => editSceneList(prev, sceneId, fn));
  }

  return (
    <PageWrapper
      padless={props.padless}
      title={props.writeHead ? t("foundScenes", { numItems }) : undefined}
    >
      <AutoLayout>
        <div className="flex gap-2 justify-between items-center">
          <CardTitle>{loading ? "Loading..." : t("foundScenes", { numItems })}</CardTitle>
          <div className="flex gap-2 items-center">
            <GridSizeSelector />
          </div>
        </div>
        <PaginatedQuery
          loading={loading}
          errorMessage={error}
          resetAll={resetAll}
          refreshPage={() => getPage(page, query)}
          filters={
            <>
              <TextInput
                className="max-w-[120px]"
                onEnter={gotoStart}
                value={query.query}
                onChange={(text) =>
                  setQuery((prev) => ({
                    ...prev,
                    query: text,
                  }))
                }
                placeholder={t("action.findContent")}
              />
              <FavoriteIconButton
                value={query.favorite}
                onClick={() => {
                  setQuery((prev) => ({
                    ...prev,
                    favorite: !prev.favorite,
                  }));
                }}
              />
              <BookmarkIconButton
                value={query.bookmark}
                onClick={() => {
                  setQuery((prev) => ({
                    ...prev,
                    bookmark: !prev.bookmark,
                  }));
                }}
              />
              <IconButton
                Icon={query.unwatchedOnly ? UnwatchedOnIcon : UnwatchedOffIcon}
                onClick={() => {
                  setQuery((prev) => ({
                    ...prev,
                    unwatchedOnly: !prev.unwatchedOnly,
                  }));
                }}
              />
              <IconButtonMenu
                value={!!query.rating}
                activeIcon={query.rating === 10 ? Star : StarHalf}
                inactiveIcon={StarOutline}
              >
                <Rating
                  value={query.rating}
                  onChange={(rating) => {
                    setQuery((prev) => ({
                      ...prev,
                      rating,
                    }));
                  }}
                />
              </IconButtonMenu>
              <IconButtonMenu
                counter={query.include.length}
                value={!!query.include.length}
                activeIcon={LabelIcon}
                inactiveIcon={LabelOutlineIcon}
                isLoading={labelLoader}
                disabled={hasNoLabels}
              >
                <LabelSelector
                  multiple
                  value={query.include}
                  searchFn={(query, label) =>
                    label.name.toLowerCase().includes(query.toLowerCase()) ||
                    label.aliases.some((alias) => alias.toLowerCase().includes(query.toLowerCase()))
                  }
                  itemChoices={labelList}
                  onChange={(labels) => {
                    setQuery((prev) => ({
                      ...prev,
                      include: labels,
                    }));
                  }}
                />
              </IconButtonMenu>
              <IconButtonMenu
                counter={query.actors.length}
                value={!!query.actors.length}
                activeIcon={ActorIcon}
                inactiveIcon={ActorOutlineIcon}
                isLoading={props.actorId ? collabsLoader : undefined}
                disabled={props.actorId ? hasNoCollabs : undefined}
              >
                <ActorSelector
                  value={query.actors}
                  multiple
                  itemChoices={props.actorId ? collabs : undefined}
                  searchFn={(query, actor) =>
                    actor.name.toLowerCase().includes(query.toLowerCase()) ||
                    actor.aliases.some((alias) => alias.toLowerCase().includes(query.toLowerCase()))
                  }
                  onChange={(actors) => {
                    setQuery((prev) => ({
                      ...prev,
                      actors,
                    }));
                  }}
                />
              </IconButtonMenu>
              {!props.studioId && (
                <IconButtonMenu
                  counter={query.studios.length}
                  value={!!query.studios.length}
                  activeIcon={CameraIcon}
                  inactiveIcon={CameraOutlineIcon}
                  isLoading={false}
                  disabled={false}
                >
                  <StudioSelector
                    multiple
                    value={query.studios}
                    onChange={(studios) => {
                      setQuery((prev) => ({
                        ...prev,
                        studios,
                      }));
                    }}
                  />
                </IconButtonMenu>
              )}
              {!!props.substudios?.length && (
                <IconButtonMenu
                  counter={query.studios.length}
                  value={!!query.studios.length}
                  activeIcon={CameraIcon}
                  inactiveIcon={CameraOutlineIcon}
                  isLoading={false}
                  disabled={false}
                >
                  <StudioSelector
                    itemChoices={props.substudios}
                    multiple
                    value={query.studios}
                    onChange={(studios) => {
                      setQuery((prev) => ({
                        ...prev,
                        studios,
                      }));
                    }}
                  />
                </IconButtonMenu>
              )}
              <>
                <SimpleSelect
                  items={[
                    {
                      text: t("relevance"),
                      value: "relevance",
                    },
                    {
                      text: t("addedToCollection"),
                      value: "addedOn",
                    },
                    {
                      text: t("lastViewed"),
                      value: "lastViewedOn",
                    },
                    {
                      text: t("rating"),
                      value: "rating",
                    },
                    {
                      text: t("numViews"),
                      value: "numViews",
                    },
                    {
                      text: t("numActors"),
                      value: "numActors",
                    },
                    {
                      text: t("duration"),
                      value: "duration",
                    },
                    {
                      text: t("resolution"),
                      value: "resolution",
                    },
                    {
                      text: t("bitrate"),
                      value: "bitrate",
                    },
                    {
                      text: t("fileSize"),
                      value: "size",
                    },
                    {
                      text: t("releaseDate"),
                      value: "releaseDate",
                    },
                    {
                      text: t("bookmark"),
                      value: "bookmark",
                    },
                  ]}
                  value={query.sortBy}
                  onChange={(sortBy) => {
                    setQuery((prev) => ({
                      ...prev,
                      sortBy: sortBy ?? "relevance",
                    }));
                  }}
                />
                <SortDirectionButton
                  value={query.sortDir}
                  onChange={(sortDir) => {
                    setQuery((prev) => ({
                      ...prev,
                      sortDir,
                    }));
                  }}
                />
              </>
            </>
          }
          currentPage={page}
          numPages={numPages}
          setPage={gotoPage}
        >
          <ListWrapper size={gridSize} loading={loading} noResults={!numItems}>
            {scenes.map((scene) => (
              <SceneCard
                highlight={highlight}
                onFav={(value) => {
                  editScene(scene._id, (scene) => {
                    scene.favorite = value;
                    return scene;
                  });
                }}
                onBookmark={(value) => {
                  editScene(scene._id, (scene) => {
                    scene.bookmark = !!value;
                    return scene;
                  });
                }}
                onRate={(rating) => {
                  editScene(scene._id, (scene) => {
                    scene.rating = rating;
                    return scene;
                  });
                }}
                key={scene._id}
                scene={scene}
              />
            ))}
          </ListWrapper>
        </PaginatedQuery>
      </AutoLayout>
    </PageWrapper>
  );
}
