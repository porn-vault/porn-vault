import { NavLink } from "@remix-run/react";
import clsx from "clsx";
import { ReactNode } from "react";
import { useLocale } from "use-intl";

type Props = {
  children: ReactNode;
  locale?: string;
  to: string;
  className?: string;
  pendingClass?: string;
  activeClass?: string;
};

export default function LocaleLink({
  children,
  locale,
  to,
  pendingClass,
  activeClass,
  className,
}: Props) {
  const defaultLocale = useLocale();

  return (
    <NavLink
      className={({ isActive, isPending }) =>
        clsx(className, {
          [pendingClass ?? ""]: isPending,
          [activeClass ?? ""]: isActive,
        })
      }
      to={`/${locale ?? defaultLocale}${to}`}
    >
      {children}
    </NavLink>
  );
}
