import { Fragment } from "react";
import Highlighter from "react-highlight-words";

import { buildSearchWords,HIGHLIGHT_CLASSES } from "~/util/highlight";

import Link from "./Link";

type Props = {
  actors: { _id: string; name: string }[];

  highlight?: string[];
};

export default function ActorList({ actors, highlight }: Props) {
  if (actors.length > 4) {
    return <div className="text-xs dark:text-gray-600">With {actors.length} actors</div>;
  }

  return (
    <div className="text-xs dark:text-gray-400">
      With{" "}
      {actors.map((actor, index) => (
        <Fragment key={actor._id}>
          <Link key={actor._id} to={`/actor/${actor._id}`}>
            <span className="cursor-pointer transition-colors dark:text-gray-400 hover:text-gray-600 hover:dark:text-gray-500">
              <b>
                <Highlighter
                  highlightClassName={HIGHLIGHT_CLASSES}
                  searchWords={buildSearchWords(highlight)}
                  autoEscape={true}
                  textToHighlight={actor.name}
                />
              </b>
            </span>
          </Link>
          <span key={`${actor._id}-comma`}>{index < actors.length - 1 && ", "}</span>
        </Fragment>
      ))}
    </div>
  );
}
