import clsx from "clsx";
import { useMemo, useState } from "react";

import defaultCountries from "../../server/data/countries";
import Flag from "./Flag";
import Paper from "./Paper";
import TextInput from "./TextInput";

type Props = {
  value: string;
  onChange: (s: string) => void;
  relevancy?: number;
  style?: React.CSSProperties;
};

export function CountrySelector({ value, onChange, relevancy: minRelevancy }: Props) {
  const [query, setQuery] = useState("");

  const countries = useMemo(() => {
    return defaultCountries.filter(({ relevancy }) => relevancy > (minRelevancy ?? 1));
  }, [minRelevancy]);

  function isSelected(countryCode: string): boolean {
    return value === countryCode;
  }

  const filteredCountries =
    query === ""
      ? countries
      : countries.filter((country) =>
          [country.name, country.alias, country.alpha3]
            .map((x) => x?.toLowerCase())
            .some((x) => x?.includes(query.toLowerCase()))
        );

  return (
    <div className="min-w-[180px] flex flex-col gap-1 max-h-[50vh] overflow-x-hidden overflow-y-auto">
      <TextInput placeholder="Search countries" value={query} onChange={setQuery} />
      {filteredCountries.map((country) => (
        <Paper
          flat
          onClick={() => {
            if (isSelected(country.alpha2)) {
              onChange?.("");
            } else {
              onChange?.(country.alpha2);
            }
          }}
          className={clsx(
            "flex cursor-pointer items-center gap-2 rounded-lg px-3 py-2 text-black dark:text-white transition-colors",
            {
              "bg-blue-300 dark:bg-blue-950": isSelected(country.alpha2),
            }
          )}
          key={country.alpha2}
        >
          <Flag name={country.alias || country.name} code={country.alpha2} size={24} />
          <div className="text-black dark:text-white">{country.alias || country.name}</div>
        </Paper>
      ))}
    </div>
  );
}
