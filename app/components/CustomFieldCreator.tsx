import { Combobox } from "@headlessui/react";
import clsx from "clsx";
import { Fragment, useState } from "react";
import { useTranslations } from "use-intl";

import { useWindow } from "~/composables/use_window";
import { createCustomField } from "~/util/mutations/custom_field";
import { toTitleCase } from "~/util/string";

import type { CustomFieldTarget, CustomFieldType } from "../../server/types/custom_field";
import Button from "./Button";
import Subheading from "./Subheading";
import Textarea from "./Textarea";
import TextInput from "./TextInput";
import Window from "./Window";

type Props = {
  onCreate: () => void;
};

export default function CustomFieldCreator({ onCreate }: Props): JSX.Element {
  const t = useTranslations();
  const { isOpen, close, open } = useWindow();

  const [name, setName] = useState("");
  const [type, setType] = useState<CustomFieldType>("STRING" as CustomFieldType);
  const [targets, setTargets] = useState<CustomFieldTarget[]>([]);
  const [values, setValues] = useState<string[]>([]);
  const [unit, setUnit] = useState("");

  const [error, setError] = useState<string | undefined>();
  const [loading, setLoader] = useState(false);

  function reset() {
    setName("");
    setValues([]);
    setType("STRING" as CustomFieldType);
    setTargets([]);
    setValues([]);
    setUnit("");
  }

  return (
    <>
      <Button onClick={open}>+ {t("action.addCustomField")}</Button>
      <Window
        onClose={close}
        isOpen={isOpen}
        title={t("action.addCustomField")}
        actions={
          <>
            <Button
              disabled={!name.length || !targets.length}
              loading={loading}
              onClick={async () => {
                try {
                  setLoader(true);

                  await createCustomField({
                    name,
                    target: targets,
                    type,
                    unit,
                    values: values.filter(Boolean),
                  });

                  onCreate();
                  reset();
                  close();
                } catch (error) {
                  console.error(error);
                  if (error instanceof Error) {
                    setError(error.message);
                  } else {
                    setError("An error occurred");
                  }
                }

                setLoader(false);
              }}
            >
              Create
            </Button>
            <Button secondary onClick={close}>
              Close
            </Button>
          </>
        }
      >
        <div>
          <Subheading>Field name *</Subheading>
          <TextInput
            className="w-full"
            value={name}
            onChange={setName}
            placeholder="Enter a field name"
          />
        </div>
        <div>
          <Subheading>Type</Subheading>
          <div className="relative">
            <Combobox value={type} onChange={setType}>
              <Combobox.Input
                placeholder="Select type"
                className={clsx(
                  "w-full rounded border-2 px-2 py-1 !outline-none transition-colors",
                  "border-gray-300 bg-gray-50 text-black hover:bg-gray-200 focus:border-blue-500 focus:bg-gray-100",
                  "dark:border-gray-700 dark:bg-gray-800 dark:text-white hover:dark:bg-gray-900 focus:dark:border-blue-800 focus:dark:bg-gray-900"
                )}
                displayValue={(type: CustomFieldType) => toTitleCase(type)}
              />
              <Combobox.Options className="absolute left-0 top-[40px] z-[900] flex max-h-[240px] w-full flex-col gap-1 overflow-y-scroll rounded bg-white px-2 py-1 shadow dark:bg-gray-800">
                {["NUMBER", "STRING", "BOOLEAN", "SINGLE_SELECT", "MULTI_SELECT"].map((type) => (
                  <Combobox.Option key={type} value={type} as={Fragment}>
                    {({ selected }) => (
                      <span
                        className={clsx(
                          "cursor-pointer rounded p-2 transition-colors hover:bg-gray-300 hover:dark:bg-gray-700",
                          { "bg-blue-200 dark:bg-blue-900": selected }
                        )}
                      >
                        {toTitleCase(type)}
                      </span>
                    )}
                  </Combobox.Option>
                ))}
              </Combobox.Options>
            </Combobox>
          </div>
        </div>
        <div>
          <Subheading>For</Subheading>
          <div className="relative">
            <Combobox value={targets} multiple onChange={(x) => setTargets(x)}>
              <Combobox.Input
                placeholder="Select target(s)"
                className={clsx(
                  "w-full rounded border-2 px-2 py-1 !outline-none transition-colors",
                  "border-gray-300 bg-gray-50 text-black hover:bg-gray-200 focus:border-blue-500 focus:bg-gray-100",
                  "dark:border-gray-700 dark:bg-gray-800 dark:text-white hover:dark:bg-gray-900 focus:dark:border-blue-800 focus:dark:bg-gray-900"
                )}
                displayValue={(target: CustomFieldTarget[]) => target.map(toTitleCase).join(", ")}
              />
              <Combobox.Options className="absolute left-0 top-[40px] z-[900] flex max-h-[240px] w-full flex-col gap-1 overflow-y-scroll rounded bg-white px-2 py-1 shadow dark:bg-gray-800">
                {["SCENES", "ACTORS"].map((target) => (
                  <Combobox.Option key={target} value={target} as={Fragment}>
                    {({ selected }) => (
                      <span
                        className={clsx(
                          "cursor-pointer rounded p-2 transition-colors hover:bg-gray-300 hover:dark:bg-gray-700",
                          { "bg-blue-200 dark:bg-blue-900": selected }
                        )}
                      >
                        {toTitleCase(target)}
                      </span>
                    )}
                  </Combobox.Option>
                ))}
              </Combobox.Options>
            </Combobox>
          </div>
        </div>
        {type !== "BOOLEAN" && (
          <div>
            <Subheading>Suffix (optional)</Subheading>
            <TextInput
              className="w-full"
              value={unit}
              onChange={setUnit}
              placeholder="Enter a suffix (e.g. cm)"
            />
            {type === "NUMBER" && (
              <div className="text-xs mt-2 opacity-80">
                Use <kbd>singular | plural</kbd> (pipe symbol) for pluralization of numeric values
              </div>
            )}
          </div>
        )}
        {type.includes("SELECT") && (
          <div>
            <Subheading>Predefined values</Subheading>
            <Textarea
              rows={4}
              className="w-full"
              value={values.join("\n")}
              onChange={(text) => setValues(text.split("\n").map((s) => s.trim()))}
              placeholder="One per line"
            />
          </div>
        )}
      </Window>
    </>
  );
}
