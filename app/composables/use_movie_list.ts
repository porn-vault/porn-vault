import { movieCardFragment } from "../fragments/movie";
import { IMovie } from "../types/movie";
import { IPaginationResult } from "../types/pagination";
import { graphqlQuery } from "../util/gql";

// TODO: move out of composables

export function editMovieList(
  items: IMovie[],
  movieId: string,
  fn: (movie: IMovie) => IMovie
): IMovie[] {
  const copy = [...items];
  const index = copy.findIndex((movie) => movie._id === movieId);
  if (index > -1) {
    const movie = copy[index];
    copy.splice(index, 1, fn(movie));
  }
  return copy;
}

export async function fetchMovies(page = 0, query: any) {
  const q = `
  query($query: MovieSearchQuery!) {
    getMovies(query: $query) {
      items {
        ...MovieCard
      }
      numItems
      numPages
    }
  }
  ${movieCardFragment}
`;

  const { getMovies } = await graphqlQuery<{
    getMovies: IPaginationResult<IMovie>;
  }>(q, {
    query: {
      query: "",
      page,
      sortBy: "addedOn",
      sortDir: "desc",
      ...query,
    },
  });

  return getMovies;
}
