import { useState } from "react";

import { useMount } from "./use_mounted";

export function useVersion() {
  const [version, setVersion] = useState("");

  useMount(() => {
    fetch("/api/version")
      .then((res) => res.text())
      .then(setVersion);
  });

  return { version };
}
