import { studioCardFragment } from "../fragments/studio";
import { IPaginationResult } from "../types/pagination";
import { IStudio } from "../types/studio";
import { graphqlQuery } from "../util/gql";

// TODO: move out of composables

export function editStudioList(
  items: IStudio[],
  studioId: string,
  fn: (studio: IStudio) => IStudio
): IStudio[] {
  const copy = [...items];
  const index = copy.findIndex((studio) => studio._id === studioId);
  if (index > -1) {
    const studio = copy[index];
    copy.splice(index, 1, fn(studio));
  }
  return copy;
}

export async function fetchStudios(page = 0, query: any) {
  const q = `
  query($query: StudioSearchQuery!) {
    getStudios(query: $query) {
      items {
        ...StudioCard
      }
      numItems
      numPages
    }
  }
  ${studioCardFragment}
`;

  const { getStudios } = await graphqlQuery<{
    getStudios: IPaginationResult<IStudio>;
  }>(q, {
    query: {
      query: "",
      page,
      sortBy: "addedOn",
      sortDir: "desc",
      ...query,
    },
  });

  return getStudios;
}
