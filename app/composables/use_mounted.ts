import { type EffectCallback, useEffect, useRef } from "react";

export function useMount(fn: EffectCallback) {
  const mounted = useRef(false);

  useEffect(() => {
    if (!mounted.current) {
      fn();
      mounted.current = true;
    }
  }, []);
}
