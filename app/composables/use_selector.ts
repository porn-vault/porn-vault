import { useState } from "react";

import { useDebounce } from "./use_debounce";
import { useMount } from "./use_mounted";

type SelectorOptions<T extends { _id: string; name: string }> = {
  value: string[];
  onChange?: (ids: string[]) => void;
  fetchFn: (page: number, query: Record<string, unknown>) => Promise<T[]>;
  getItemById: (id: string) => Promise<T | null>;
  multiple?: boolean;
  itemChoices?: T[];
  searchFn?: (query: string, item: T) => boolean;
};

export function useSelector<T extends { _id: string; name: string }, Q>(opts: SelectorOptions<T>) {
  const { value, onChange, fetchFn, getItemById, multiple, itemChoices, searchFn } = opts;
  const searchFunction =
    searchFn ??
    ((query: string, item: T) => item.name.toLocaleLowerCase().includes(query.toLocaleLowerCase()));

  const [query, setQuery] = useState("");
  const [items, setItems] = useState<T[]>(itemChoices ?? []);
  const [loading, setLoading] = useState(true);

  function isSelected(studioId: string): boolean {
    return value.includes(studioId);
  }

  async function doSearch() {
    try {
      if (itemChoices) {
        const searchResult = itemChoices.filter((x) => searchFunction(query, x));
        const selectedItems = items.filter((x) => isSelected(x._id));
        const selectedIds = selectedItems.map((x) => x._id);

        setItems([...selectedItems, ...searchResult.filter((x) => !selectedIds.includes(x._id))]);
      } else {
        setLoading(true);
        const searchResult = await fetchFn(0, { query, sortBy: "relevance" });
        const selectedItems = items.filter((x) => isSelected(x._id));
        const selectedIds = selectedItems.map((x) => x._id);

        setItems([
          ...selectedItems,
          ...searchResult.filter((x) => !selectedIds.includes(x._id)).slice(0, 10),
        ]);
      }
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  }

  const debouncedSearch = useDebounce(doSearch, 250);

  useMount(() => {
    (async () => {
      if (value.length && !itemChoices) {
        const items: T[] = [];

        await Promise.all(
          value.map(async (itemId) => {
            const item = await getItemById(itemId);
            if (item) {
              items.push(item);
            }
          })
        );

        setItems(items);
        onChange?.(items.map((x) => x._id));
        setLoading(false);
      } else {
        debouncedSearch();
      }
    })().catch(() => {});
  });

  function handleClick(itemId: string): void {
    if (multiple) {
      if (isSelected(itemId)) {
        onChange?.(value.filter((x) => x !== itemId));
      } else {
        onChange?.([...value, itemId]);
      }
    } else {
      if (isSelected(itemId)) {
        onChange?.([]);
      } else {
        onChange?.([itemId]);
      }
    }
  }

  return {
    items,
    loading,
    query,
    setQuery,
    handleClick,
    isSelected,
    debouncedSearch,
  };
}
