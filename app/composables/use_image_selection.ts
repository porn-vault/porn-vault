import { useState } from "react";

import { IImage } from "../types/image";

export function useImageSelection(images: IImage[]) {
  const [selected, setSelected] = useState<IImage[]>([]);

  function toggleImage(image: IImage): void {
    setSelected((prev) => {
      const copy = structuredClone(prev);

      if (isSelected(image._id)) {
        return copy.filter((x) => x._id !== image._id);
      }

      return [...copy, image];
    });
  }

  function isSelected(imageId: string): boolean {
    return !!selected.find((x) => x._id === imageId);
  }

  function clear(): void {
    setSelected([]);
  }

  function selectAll(): void {
    setSelected(structuredClone(images));
  }

  return {
    selected,
    isSelected,
    toggleImage,
    clear,
    selectAll,
    allSelected: selected.length === images.length && images.length > 0,
  };
}
