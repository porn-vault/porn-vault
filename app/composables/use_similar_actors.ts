import { useState } from "react";

import { actorCardFragment } from "../fragments/actor";
import { IActor } from "../types/actor";
import { graphqlQuery } from "../util/gql";
import { useMount } from "./use_mounted";

export function useSimilarActors(actorId: string) {
  const [actors, setActors] = useState<IActor[]>([]);
  const [loading, setLoading] = useState(false);

  async function fetchSimilarActors() {
    const q = `
    query ($id: String!) {
      getActorById(id: $id) {
        similar {
          ...ActorCard
        }
      }
    }
    ${actorCardFragment}
  `;

    const { getActorById } = await graphqlQuery<{
      getActorById: {
        similar: IActor[];
      };
    }>(q, {
      id: actorId,
    });

    setActors(getActorById.similar);
  }

  useMount(() => {
    setLoading(true);
    fetchSimilarActors()
      .catch((_) => {})
      .finally(() => {
        setLoading(false);
      });
  });

  return {
    actors,
    setActors,
    loading,
  };
}
