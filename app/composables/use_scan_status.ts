import useSWR from "swr";

type ScanFolder = {
  path: string;
  include: string[];
  exclude: string[];
  extensions: string[];
  enable: boolean;
};

type ScanStatus = {
  folders: {
    videos: ScanFolder[];
    images: ScanFolder[];
  };
  isScanning: boolean;
  nextScanDate: string | null;
  nextScanTimestamp: number | null;
};

export async function getScanStatus(): Promise<ScanStatus> {
  return fetch("/api/scan/status").then((res) => res.json());
}

export function useScanStatus() {
  const { data: scanStatus, mutate } = useSWR("scan-status", getScanStatus, {
    revalidateOnFocus: true,
    revalidateOnReconnect: true,
    revalidateOnMount: true,
    refreshInterval: 2_500,
  });

  async function startScan() {
    if (scanStatus?.isScanning) {
      return;
    }
    await fetch("/api/scan", {
      method: "POST",
    });
    mutate().catch(() => {});
  }

  return {
    scanStatus,
    startScan,
  };
}
