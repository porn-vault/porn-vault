import { useState } from "react";
import { useHotkeys } from "react-hotkeys-hook";

import { IImage } from "../types/image";

type Options = {
  requestNextPage?: () => void;
  requestPreviousPage?: () => void;
};

export function useLightbox(images: IImage[], opts: Options) {
  const [index, setIndex] = useState(-1);

  const canGoBack = index > 0;
  const canGoNext = index < images.length - 1;

  function goBack() {
    if (canGoBack) {
      setIndex((x) => x - 1);
    } else {
      opts.requestPreviousPage?.();
    }
  }

  function goNext() {
    if (canGoNext) {
      setIndex((x) => x + 1);
    } else {
      opts.requestNextPage?.();
    }
  }

  useHotkeys(
    "ArrowLeft",
    () => {
      goBack();
    },
    [index]
  );

  useHotkeys(
    "ArrowRight",
    () => {
      goNext();
    },
    [index]
  );

  return {
    setIndex,

    canGoBack,
    canGoNext,

    goBack,
    goNext,

    activeImage: images[index] as IImage | undefined,
  };
}
