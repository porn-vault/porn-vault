import { actorCardFragment } from "../fragments/actor";
import { IActor } from "../types/actor";
import { IPaginationResult } from "../types/pagination";
import { graphqlQuery } from "../util/gql";

// TODO: move out of composables

export function editActorList(
  items: IActor[],
  actorId: string,
  fn: (actor: IActor) => IActor
): IActor[] {
  const copy = [...items];
  const index = copy.findIndex((actor) => actor._id === actorId);
  if (index > -1) {
    const actor = copy[index];
    copy.splice(index, 1, fn(actor));
  }
  return copy;
}

export async function fetchActors(page = 0, query: any) {
  const q = `
  query($query: ActorSearchQuery!) {
    getActors(query: $query) {
      items {
        ...ActorCard
        avatar {
          _id
        }
      }
      numItems
      numPages
    }
  }
  ${actorCardFragment}
`;

  const { getActors } = await graphqlQuery<{
    getActors: IPaginationResult<IActor>;
  }>(q, {
    query: {
      query: "",
      page,
      sortBy: "addedOn",
      sortDir: "desc",
      ...query,
    },
  });

  return getActors;
}
