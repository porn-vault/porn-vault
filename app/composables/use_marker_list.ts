import { markerCardFragment } from "../fragments/marker";
import { IMarker } from "../types/marker";
import { IPaginationResult } from "../types/pagination";
import { graphqlQuery } from "../util/gql";

// TODO: move out of composables

export function editMarkerList(
  items: IMarker[],
  markerId: string,
  fn: (marker: IMarker) => IMarker
): IMarker[] {
  const copy = [...items];
  const index = copy.findIndex((marker) => marker._id === markerId);
  if (index > -1) {
    const marker = copy[index];
    copy.splice(index, 1, fn(marker));
  }
  return copy;
}

export async function fetchMarkers(page = 0, query: any) {
  const q = `
  query($query: MarkerSearchQuery!) {
    getMarkers(query: $query) {
      items {
        ...MarkerCard
      }
      numItems
      numPages
    }
  }
  ${markerCardFragment}
`;

  const { getMarkers } = await graphqlQuery<{
    getMarkers: IPaginationResult<IMarker>;
  }>(q, {
    query: {
      query: "",
      page,
      sortBy: "addedOn",
      sortDir: "desc",
      ...query,
    },
  });

  return getMarkers;
}
