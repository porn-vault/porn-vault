import Color from "color";

import { useTheme } from "./use_dark_theme";

export function useTitleColor(
  str?: string | undefined | null
): string | undefined {
  const { theme } = useTheme();

  if (!str) {
    return undefined;
  }

  let color = new Color(str);

  if (theme === "dark") {
    color = color.hsl(color.hue(), 100, 85);
  } else {
    color = color.hsl(color.hue(), 100, 15);
  }

  return color.hex();
}
