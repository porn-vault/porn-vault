import debounce from "lodash.debounce";
import { useEffect, useMemo, useRef } from "react";

export function useDebounce(callback: () => unknown, ms: number) {
  const ref = useRef<typeof callback>();

  useEffect(() => {
    ref.current = callback;
  }, [callback]);

  const debouncedCallback = useMemo(() => {
    const func = () => {
      ref.current?.();
    };

    return debounce(func, ms);
  }, []);

  return debouncedCallback;
}
