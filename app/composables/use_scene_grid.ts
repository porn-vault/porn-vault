import { useEffect, useState } from "react";

export function useSceneGrid(sceneId: string) {
  const [showGrid, setGrid] = useState(false);
  const [gridLoader, setGridLoader] = useState(false);

  const gridUrl = `/api/media/scene/${sceneId}/grid`;

  async function loadGrid() {
    try {
      setGridLoader(true);
      setGrid(false);
      await fetch(gridUrl);
      setGrid(true);
    } catch (error) {
      throw error;
    } finally {
      setGridLoader(false);
    }
  }

  useEffect(() => {
    console.log("Loading grid", sceneId);

    loadGrid().catch((error) => {
      console.error("Failed to load grid", error);
    });
  }, [sceneId]);

  return { gridUrl, showGrid, gridLoader };
}
