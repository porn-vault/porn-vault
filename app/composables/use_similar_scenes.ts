import { useState } from "react";

import { sceneCardFragment } from "../fragments/scene";
import { IScene } from "../types/scene";
import { graphqlQuery } from "../util/gql";
import { useMount } from "./use_mounted";

export function useSimilarScenes(sceneId: string) {
  const [scenes, setScenes] = useState<IScene[]>([]);
  const [loading, setLoading] = useState(false);

  async function fetchSimilarScenes() {
    const q = `
    query ($id: String!) {
      getSceneById(id: $id) {
        similar {
          ...SceneCard
        }
      }
    }
    ${sceneCardFragment}
  `;

    const { getSceneById } = await graphqlQuery<{
      getSceneById: {
        similar: IScene[];
      };
    }>(q, {
      id: sceneId,
    });

    setScenes(getSceneById.similar);
  }

  useMount(() => {
    setLoading(true);
    fetchSimilarScenes()
      .catch((_) => {})
      .finally(() => {
        setLoading(false);
      });
  });

  return {
    scenes,
    setScenes,
    loading,
  };
}
