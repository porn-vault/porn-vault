import { useState } from "react";

import { IPaginationResult } from "../types/pagination";
import { useMount } from "./use_mounted";

export type PersistentQueryManager<Q extends Record<string, any>> = {
  restore: () => { page: number; query: Q } | null;
  save: (data: { page: number; query: Q }) => void;
};

export function usePaginatedQuery<
  T extends Record<string, any>,
  Q extends Record<string, any>
>(opts: {
  persistentQuery?: PersistentQueryManager<Q>;
  fetchFn: (page: number, query: Q) => Promise<IPaginationResult<T>>;
  defaultQuery: Q;
  // overrideQuery?: Partial<Q>;
}) {
  const { fetchFn, defaultQuery, persistentQuery } = opts;

  const [query, internalSetQuery] = useState(defaultQuery);
  const [items, setItems] = useState<T[]>([]);
  const [page, internalSetPage] = useState(0);
  const [numItems, setNumItems] = useState(0);
  const [numPages, setNumPages] = useState(0);
  const [error, setError] = useState<string | null>(null);
  const [loading, setLoading] = useState(true);

  async function gotoPage(x: number): ReturnType<typeof getPage> {
    return getPage(x, query);
  }

  async function getPage(page: number, query: Q): Promise<IPaginationResult<T> | void> {
    try {
      setError(null);
      setLoading(true);

      persistentQuery?.save({ page, query });
      const result = await fetchFn(page, query);

      internalSetPage(page);
      setItems(result.items);
      setNumItems(result.numItems);
      setNumPages(result.numPages);

      // Page is depleted, go to previous if possible
      if (result.items.length === 0 && page > 0) {
        return getPage(page - 1, query);
      }

      return result;
    } catch (error: any) {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-argument, @typescript-eslint/no-unsafe-member-access
      setError(error.message || "Fetch error");
    } finally {
      setLoading(false);
    }
  }

  function setQuery(fn: (prev: Q) => Q): void {
    const newQuery = fn(query);
    persistentQuery?.save({ page, query });
    internalSetQuery(newQuery);
    internalSetPage(0);
  }

  function gotoStart(): ReturnType<typeof getPage> {
    return gotoPage(0);
  }

  function resetAll(): void {
    internalSetPage(0);
    setQuery(() => defaultQuery);
    getPage(0, defaultQuery).catch((_) => {});
  }

  useMount((): void => {
    if (!persistentQuery) {
      getPage(page, query).catch((_) => {});
      return;
    }

    const fromStorage = persistentQuery.restore();

    if (fromStorage) {
      const { query, page } = fromStorage;
      internalSetQuery(query);
      internalSetPage(page);
      getPage(page, query).catch((_) => {});
    } else {
      getPage(page, query).catch((_) => {});
    }
  });

  return {
    items,
    setItems,

    query,
    setQuery,

    page,
    error,
    loading,
    numItems,
    numPages,

    getPage,
    gotoPage,
    gotoStart,
    resetAll,
  };
}
