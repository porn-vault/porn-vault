import { useState } from "react";

import { graphqlQuery } from "../util/gql";
import { useMount } from "./use_mounted";

export type CustomField = {
  _id: string;
  name: string;
  type: "BOOLEAN";
  target: ("SCENES" | "ACTORS")[];
  unit: string | null;
  values: string[];
};

export async function fetchCustomFields() {
  const q = `
  {
    getCustomFields {
      _id
      name
      type
      target
      unit
      values
    }
  }`;

  const { getCustomFields } = await graphqlQuery<{
    getCustomFields: CustomField[];
  }>(q, {});

  return getCustomFields;
}

export default function useLabelList() {
  const [fields, setFields] = useState<CustomField[]>([]);
  const [loading, setLoader] = useState(true);

  async function load() {
    try {
      setLoader(true);
      setFields(await fetchCustomFields());
    } catch (error) {}
    setLoader(false);
  }

  useMount(() => {
    load().catch(() => {});
  });

  return {
    fields,
    loading,
  };
}
