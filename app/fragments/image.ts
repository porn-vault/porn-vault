export const imageCardFragment = `
fragment ImageCard on Image {
  _id
  name
  path
  meta {
    dimensions {
      width
      height
    }
    size
  }
  actors {
    _id
    name
    avatar {
      _id
      color
    }
  }
  scene {
    _id
    name
    thumbnail {
      _id
    }
  }
  labels {
    _id
    name
    color
  }
  favorite
  bookmark
  rating
}
`;
