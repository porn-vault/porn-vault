import { imageCardFragment } from "../../fragments/image";
import { IImage } from "../../types/image";
import { graphqlQuery } from "../gql";
import { gqlIp } from "../ip";

export async function labelImages(imageIds: string[], labelIds: string[]): Promise<void> {
  const mutation = `
mutation($ids: [String!]!, $opts: ImageUpdateOpts!) {
  updateImages(ids: $ids, opts: $opts) {
    labels
  }
}`;

  await graphqlQuery(mutation, {
    ids: imageIds,
    opts: {
      labels: labelIds,
    },
  });
}

export async function rateImage(imageId: string, rating: number): Promise<void> {
  const mutation = `
mutation($ids: [String!]!, $opts: ImageUpdateOpts!) {
  updateImages(ids: $ids, opts: $opts) {
    rating
  }
}`;

  await graphqlQuery(mutation, {
    ids: [imageId],
    opts: {
      rating,
    },
  });
}

export async function favoriteImage(imageId: string, value: boolean): Promise<void> {
  const mutation = `
mutation($ids: [String!]!, $opts: ImageUpdateOpts!) {
  updateImages(ids: $ids, opts: $opts) {
    favorite
  }
}`;

  await graphqlQuery(mutation, {
    ids: [imageId],
    opts: {
      favorite: value,
    },
  });
}

export async function bookmarkImage(imageId: string, value: Date | null): Promise<void> {
  const mutation = `
mutation($ids: [String!]!, $opts: ImageUpdateOpts!) {
  updateImages(ids: $ids, opts: $opts) {
    bookmark
  }
}`;

  await graphqlQuery(mutation, {
    ids: [imageId],
    opts: {
      bookmark: value && value.valueOf(),
    },
  });
}

export async function uploadImage(
  image: { blob: File | Blob; name: string },
  actors?: string[],
  sceneId?: string
): Promise<IImage> {
  const query = `
mutation (
  $file: Upload!
  $name: String
  $scene: String
  $actors: [String!]
  $crop: Crop
) {
  uploadImage(file: $file, name: $name, actors: $actors, scene: $scene, crop: $crop) {
    ...ImageCard
  }
}
${imageCardFragment}`;

  const variables = {
    name: image.name,
    actors,
    scene: sceneId,
  };
  const formData = new FormData();

  const operations = JSON.stringify({ query, variables });
  formData.append("operations", operations);

  const map = {
    "0": ["variables.file"],
  };
  formData.append("map", JSON.stringify(map));
  formData.append("0", image.blob);

  const response = await fetch(gqlIp(), {
    body: formData,
    method: "POST",
    headers: {
      "apollo-require-preflight": "true",
    },
  });

  const body = await response.json();

  return body.data.uploadImage;
}

export async function deleteImages(ids: string[]): Promise<void> {
  const query = `
mutation ($ids: [String!]!) {
  removeImages(ids: $ids)
}`;

  await graphqlQuery(query, { ids });
}
