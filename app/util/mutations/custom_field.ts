import type CustomField from "../../../server/types/custom_field";
import type { CustomFieldTarget, CustomFieldType } from "../../../server/types/custom_field";
import { graphqlQuery } from "../gql";

type CustomFieldCreateInput = {
  name: string;
  target: CustomFieldTarget[];
  type: CustomFieldType;
  values?: string[];
  unit?: string;
};

type CustomFieldUpdateInput = Pick<CustomFieldCreateInput, "name" | "unit" | "values">;

export async function createCustomField(input: CustomFieldCreateInput): Promise<CustomField> {
  const mutation = `
mutation($input: CustomFieldCreateInput!) {
  createCustomField(input: $input) {
    _id
    name
    target
    type
    values
    unit
  }
}`;

  const { createCustomField } = await graphqlQuery<{ createCustomField: CustomField }>(mutation, {
    input,
  });

  return createCustomField;
}

export async function updateCustomField(
  id: string,
  input: CustomFieldUpdateInput
): Promise<CustomField> {
  const mutation = `
mutation($id: String!, $input: CustomFieldUpdateInput!) {
  updateCustomField(id: $id, input: $input) {
    _id
    name
    target
    type
    values
    unit
  }
}`;

  const { updateCustomField } = await graphqlQuery<{ updateCustomField: CustomField }>(mutation, {
    id,
    input,
  });

  return updateCustomField;
}

export async function removeCustomField(fieldId: string): Promise<void> {
  const mutation = `
mutation($id: String!) {
  removeCustomField(id: $id)
}`;

  await graphqlQuery(mutation, {
    id: fieldId,
  });
}
