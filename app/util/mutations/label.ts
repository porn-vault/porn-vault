import { graphqlQuery } from "../gql";

export async function deleteLabels(labelIds: string[]): Promise<void> {
  const mutation = `
mutation($ids: [String!]!) {
  removeLabels(ids: $ids)
}`;

  await graphqlQuery(mutation, {
    ids: labelIds,
  });
}
