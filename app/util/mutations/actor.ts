import ILabel from "~/types/label";

import { graphqlQuery } from "../gql";

type UpdateActorResponse = {
  updateActors: {
    avatar: { _id: string; name: string };
    thumbnail: { _id: string; name: string };
    altThumbnail: { _id: string; name: string };
    hero: { _id: string; name: string };
  }[];
};

export async function setActorThumbnail(actorId: string, imageId: string | null) {
  const mutation = `
mutation ($ids: [String!]!, $opts: ActorUpdateOpts!) {
  updateActors(ids: $ids, opts: $opts) {
    thumbnail {
      _id
    }
  }
}`;

  const { updateActors } = await graphqlQuery<UpdateActorResponse>(mutation, {
    ids: [actorId],
    opts: {
      thumbnail: imageId,
    },
  });

  return updateActors[0].thumbnail;
}

export async function setActorAltThumbnail(actorId: string, imageId: string | null) {
  const mutation = `
mutation ($ids: [String!]!, $opts: ActorUpdateOpts!) {
  updateActors(ids: $ids, opts: $opts) {
    altThumbnail {
      _id
    }
  }
}`;

  const { updateActors } = await graphqlQuery<UpdateActorResponse>(mutation, {
    ids: [actorId],
    opts: {
      altThumbnail: imageId,
    },
  });

  return updateActors[0].altThumbnail;
}

export async function setActorAvatar(actorId: string, imageId: string | null) {
  const mutation = `
mutation ($ids: [String!]!, $opts: ActorUpdateOpts!) {
  updateActors(ids: $ids, opts: $opts) {
    avatar {
      _id
      name
    }
  }
}`;

  const { updateActors } = await graphqlQuery<UpdateActorResponse>(mutation, {
    ids: [actorId],
    opts: {
      avatar: imageId,
    },
  });

  return updateActors[0].avatar;
}

export async function setActorHero(actorId: string, imageId: string | null) {
  const mutation = `
mutation ($ids: [String!]!, $opts: ActorUpdateOpts!) {
  updateActors(ids: $ids, opts: $opts) {
    hero {
      _id
    }
  }
}`;

  const { updateActors } = await graphqlQuery<UpdateActorResponse>(mutation, {
    ids: [actorId],
    opts: {
      hero: imageId,
    },
  });

  return updateActors[0].hero;
}

export async function runActorPlugins(actorId: string): Promise<void> {
  const mutation = `
mutation($id: String!) {
  runActorPlugins(id: $id) {
    _id
  }
}`;
  await graphqlQuery(mutation, {
    id: actorId,
  });
}

export async function rateActor(actorId: string, rating: number): Promise<void> {
  const mutation = `
mutation($ids: [String!]!, $opts: ActorUpdateOpts!) {
  updateActors(ids: $ids, opts: $opts) {
    rating
  }
}`;

  await graphqlQuery(mutation, {
    ids: [actorId],
    opts: {
      rating,
    },
  });
}

export async function favoriteActor(actorId: string, value: boolean): Promise<void> {
  const mutation = `
mutation($ids: [String!]!, $opts: ActorUpdateOpts!) {
  updateActors(ids: $ids, opts: $opts) {
    favorite
  }
}`;

  await graphqlQuery(mutation, {
    ids: [actorId],
    opts: {
      favorite: value,
    },
  });
}

export async function bookmarkActor(actorId: string, value: Date | null): Promise<void> {
  const mutation = `
mutation($ids: [String!]!, $opts: ActorUpdateOpts!) {
  updateActors(ids: $ids, opts: $opts) {
    bookmark
  }
}`;

  await graphqlQuery(mutation, {
    ids: [actorId],
    opts: {
      bookmark: value && value.valueOf(),
    },
  });
}

export async function removeActor(id: string) {
  const query = `
mutation ($ids: [String!]!) {
  removeActors(ids: $ids)
}`;

  await graphqlQuery(query, {
    ids: [id],
  });
}

export async function removeLabel(actorId: string, label: string): Promise<void> {
  const query = `
mutation($items: [String!]!, $label: String!) {
  detachLabel(items: $items, label: $label)
}`;

  await graphqlQuery(query, {
    items: [actorId],
    label,
  });
}

export async function updateLabels(actorId: string, updatedLabels: string[]): Promise<ILabel[]> {
  const query = `
mutation($ids: [String!]!, $opts: ActorUpdateOpts!) {
  updateActors(ids: $ids, opts: $opts) {
    _id
    labels {
      _id
      name
      color
    }
  }
}`;

  const result = await graphqlQuery<{
    updateActors: { _id: string; labels: ILabel[] }[];
  }>(query, {
    ids: [actorId],
    opts: {
      labels: updatedLabels,
    },
  });

  return result.updateActors[0].labels;
}

export async function updateCustomFields(
  actorId: string,
  customFields: Record<string, any>
): Promise<void> {
  const query = `
mutation($ids: [String!]!, $opts: ActorUpdateOpts!) {
  updateActors(ids: $ids, opts: $opts) {
    _id
  }
}`;

  const result = await graphqlQuery<{
    updateActors: { _id: string; labels: ILabel[] }[];
  }>(query, {
    ids: [actorId],
    opts: {
      customFields,
    },
  });
}
