import { graphqlQuery } from "../gql";

export async function favoriteStudio(studioId: string, value: boolean): Promise<void> {
  const mutation = `
mutation($ids: [String!]!, $opts: StudioUpdateOpts!) {
  updateStudios(ids: $ids, opts: $opts) {
    favorite
  }
}`;

  await graphqlQuery(mutation, {
    ids: [studioId],
    opts: {
      favorite: value,
    },
  });
}

export async function bookmarkStudio(studioId: string, value: Date | null): Promise<void> {
  const mutation = `
mutation($ids: [String!]!, $opts: StudioUpdateOpts!) {
  updateStudios(ids: $ids, opts: $opts) {
    bookmark
  }
}`;

  await graphqlQuery(mutation, {
    ids: [studioId],
    opts: {
      bookmark: value && value.valueOf(),
    },
  });
}

export async function removeStudio(id: string) {
  const mutation = `
mutation ($ids: [String!]!) {
  removeStudios(ids: $ids)
}`;

  await graphqlQuery(mutation, {
    ids: [id],
  });
}

export async function setStudioThumbnail(studioId: string, imageId: string | null) {
  const mutation = `
mutation ($ids: [String!]!, $opts: StudioUpdateOpts!) {
  updateStudios(ids: $ids, opts: $opts) {
    thumbnail {
      _id
    }
  }
}`;

  await graphqlQuery(mutation, {
    ids: [studioId],
    opts: {
      thumbnail: imageId,
    },
  });
}
