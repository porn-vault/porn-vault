import ILabel from "../../types/label";
import { graphqlQuery } from "../gql";

export async function runScenePlugins(sceneId: string): Promise<void> {
  const mutation = `
mutation($id: String!) {
  runScenePlugins(id: $id) {
    _id
  }
}
`;

  await graphqlQuery(mutation, {
    id: sceneId,
  });
}

export async function watchScene(sceneId: string): Promise<number[]> {
  const mutation = `
mutation($id: String!) {
  watchScene(id: $id) {
    watches
  }
}`;

  const {
    watchScene: { watches },
  } = await graphqlQuery<{
    watchScene: {
      watches: number[];
    };
  }>(mutation, {
    id: sceneId,
  });

  return watches;
}

export async function unwatchScene(sceneId: string): Promise<number[]> {
  const mutation = `
mutation($id: String!) {
  unwatchScene(id: $id) {
    watches
  }
}`;

  const {
    unwatchScene: { watches },
  } = await graphqlQuery<{
    unwatchScene: {
      watches: number[];
    };
  }>(mutation, {
    id: sceneId,
  });

  return watches;
}

export async function rateScene(sceneId: string, rating: number): Promise<void> {
  const mutation = `
mutation($ids: [String!]!, $opts: SceneUpdateOpts!) {
  updateScenes(ids: $ids, opts: $opts) {
    rating
  }
}`;

  await graphqlQuery(mutation, {
    ids: [sceneId],
    opts: {
      rating,
    },
  });
}

export async function favoriteScene(sceneId: string, value: boolean): Promise<void> {
  const mutation = `
mutation($ids: [String!]!, $opts: SceneUpdateOpts!) {
  updateScenes(ids: $ids, opts: $opts) {
    favorite
  }
}`;

  await graphqlQuery(mutation, {
    ids: [sceneId],
    opts: {
      favorite: value,
    },
  });
}

export async function bookmarkScene(sceneId: string, value: Date | null): Promise<void> {
  const mutation = `
mutation($ids: [String!]!, $opts: SceneUpdateOpts!) {
  updateScenes(ids: $ids, opts: $opts) {
    bookmark
  }
}`;

  await graphqlQuery(mutation, {
    ids: [sceneId],
    opts: {
      bookmark: value && value.valueOf(),
    },
  });
}

export async function screenshotScene(sceneId: string, time: number): Promise<void> {
  const mutation = `
mutation($id: String!, $sec: Float!) {
  screenshotScene(id: $id, sec: $sec) {
    bookmark
  }
}`;

  await graphqlQuery(mutation, {
    id: sceneId,
    sec: time,
  });
}

export async function setSceneThumbnail(sceneId: string, imageId: string | null): Promise<void> {
  const mutation = `
mutation($ids: [String!]!, $opts: SceneUpdateOpts!) {
  updateScenes(ids: $ids, opts: $opts) {
    _id
  }
}`;

  await graphqlQuery(mutation, {
    ids: [sceneId],
    opts: {
      thumbnail: imageId,
    },
  });
}

export async function removeLabel(sceneId: string, label: string): Promise<void> {
  const query = `
mutation($items: [String!]!, $label: String!) {
  detachLabel(items: $items, label: $label)
}`;

  await graphqlQuery(query, {
    items: [sceneId],
    label,
  });
}

export async function updateLabels(sceneId: string, updatedLabels: string[]): Promise<ILabel[]> {
  const query = `
mutation($ids: [String!]!, $opts: SceneUpdateOpts!) {
  updateScenes(ids: $ids, opts: $opts) {
    _id
    labels {
      _id
      name
      color
    }
  }
}`;

  const result = await graphqlQuery<{
    updateScenes: { _id: string; labels: ILabel[] }[];
  }>(query, {
    ids: [sceneId],
    opts: {
      labels: updatedLabels,
    },
  });

  return result.updateScenes[0].labels;
}

export async function removeScene(id: string) {
  const mutation = `
mutation ($ids: [String!]!) {
  removeScenes(ids: $ids, deleteImages: true)
}`;

  await graphqlQuery(mutation, {
    ids: [id],
  });
}

export async function updateCustomFields(
  sceneId: string,
  customFields: Record<string, any>
): Promise<void> {
  const query = `
mutation($ids: [String!]!, $opts: SceneUpdateOpts!) {
  updateScenes(ids: $ids, opts: $opts) {
    _id
  }
}`;

  const result = await graphqlQuery<{
    updateScenes: { _id: string; labels: ILabel[] }[];
  }>(query, {
    ids: [sceneId],
    opts: {
      customFields,
    },
  });
}
