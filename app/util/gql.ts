import { gqlIp } from "./ip";

export async function graphqlQuery<T>(
  query: string,
  variables: Record<string, unknown>,
  headers?: Record<string, string>
): Promise<T> {
  const response = await fetch(gqlIp(), {
    method: "POST",
    body: JSON.stringify({
      query,
      variables,
    }),
    headers: {
      "content-type": "application/json",
      "x-pass": "xxx",
      ...headers,
    },
  });

  const body = await response.json();

  if (body.errors?.length) {
    console.error(body.errors);
    throw new Error(body.errors[0].message);
  }

  return body.data;
}
