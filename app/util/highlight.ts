export const HIGHLIGHT_CLASSES = "rounded px-[1px] italic text-black bg-blue-300 dark:bg-blue-300";

function generateAllWindows(arr: string[]): string[] {
  const windows: string[] = [];

  for (let i = 0; i < arr.length; i++) {
    for (let j = i + 1; j <= arr.length; j++) {
      windows.push(arr.slice(i, j).join(" "));
    }
  }

  return windows;
}

export function buildSearchWords(arr?: string[]): string[] {
  return generateAllWindows(arr ?? []);
}
