import { PersistentQueryManager } from "../composables/use_paginated_query";

export function createLocalStorageQueryManager<Q extends Record<string, any>>(
  key: string
): PersistentQueryManager<Q> {
  return {
    restore(): { page: number; query: Q } | null {
      const fromStorage = localStorage.getItem(key);
      if (!fromStorage) {
        return null;
      }
      return JSON.parse(fromStorage) as { page: number; query: Q };
    },
    save(data: { page: number; query: Q }): void {
      localStorage.setItem(key, JSON.stringify(data));
    },
  };
}

export function createSessionStorageQueryManager<Q extends Record<string, any>>(
  key: string
): PersistentQueryManager<Q> {
  return {
    restore() {
      const fromStorage = sessionStorage.getItem(key);
      if (!fromStorage) {
        return null;
      }
      return JSON.parse(fromStorage) as { page: number; query: Q };
    },
    save(data: { page: number; query: Q }): void {
      sessionStorage.setItem(key, JSON.stringify(data));
    },
  };
}
