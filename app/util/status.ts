export enum CollectionBuildStatus {
  None = "none",
  Loading = "loading",
  Ready = "ready",
}

export enum ServiceStatus {
  Unknown = "unknown",
  Disconnected = "disconnected",
  Stopped = "stopped",
  Connected = "connected",
}

export interface CollectionBuildInfo {
  name: string;
  status: CollectionBuildStatus;
}

export enum IndexBuildStatus {
  None = "none",
  Created = "created",
  Indexing = "indexing",
  Ready = "ready",
}

export interface IndexBuildInfo {
  name: string;
  indexedCount: number;
  totalToIndexCount: number;
  eta: number;
  status: IndexBuildStatus;
}

export interface StatusData {
  izzy: {
    status: ServiceStatus;
    version: string;
    collections: {
      name: string;
      count: number;
      size: number;
      path: string;
    }[];
    collectionBuildInfoMap: {
      [indexName: string]: CollectionBuildInfo;
    };
    allCollectionsBuilt: boolean;
  };

  // Other
  serverUptime: number;
  osUptime: number;
  serverReady: boolean;
}

export interface StatusSimple {
  serverReady: boolean;
  setupMessage: string;
}

export function getSimpleStatus(): Promise<StatusSimple> {
  return fetch("/api/system/status/simple").then((res) => res.json());
}

export function getFullStatus(): Promise<StatusData> {
  return fetch("/api/system/status/full").then((res) => res.json());
}
