export function formatDuration(secs: number, normalize = true): string {
  const iso = new Date(secs * 1000).toISOString();

  if (normalize) {
    if (secs < 3600) {
      return iso.slice(14, 19);
    }
  }

  return iso.slice(11, 19);
}

export function toTitleCase(str: string): string {
  return str
    .toLowerCase()
    .split(" ")
    .map(function (word) {
      return word.replace(word[0], word[0].toUpperCase());
    })
    .join(" ");
}
