export function gqlIp() {
  if (typeof window === "undefined") {
    return `http://localhost:${process.env.PORT || 3000}/api/graphql`;
  }
  return "/api/graphql";
}
