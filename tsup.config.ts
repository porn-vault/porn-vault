import type { Options } from "tsup";

export const tsup: Options = {
  outDir: "dist",
  target: "esnext",
  splitting: true,
  clean: true,
  format: ["esm"],
  bundle: true,
  skipNodeModulesBundle: false,
  entry: ["server/index.ts"],
  platform: "node",
};
